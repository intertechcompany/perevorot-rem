<?php

$routeGroup=[
    'prefix' => Localization::setLocale(),
    'middleware' => [
        //'localeSessionRedirect',
        'localizationRedirect',
        'localeViewPath',
        'redirectTrailingSlash',
        'caching'
    ]
];

Route::group($routeGroup, function () {
    Route::post('/api/save/zone', 'Api\MapController@saveZone')->name('api.saveZone');
    Route::post('/api/map', 'Api\MapController@index')->name('api.map');
    Route::post('/api/map/menu', 'Api\MapController@menu')->name('api.map.menu');
    Route::get('/form/list/{code}', 'FormController@list')->name('form.list');
    Route::get('/form/show/{code}/{id}/{tab?}', 'FormController@show')->name('form.show');
    Route::post('/api/list/{code}/{id?}', 'Api\ListController@getList')->name('api.getList');
    Route::post('/api/images/{code}/{id?}', 'Api\ListController@getImages')->name('api.getImages');
    Route::get('/list/{domain}', 'FormController@listByDomain')->name('list.domain')->middleware('super-domen');
    Route::get('/list/group/{type}', 'FormController@listByType')->name('list.group')->middleware('super-domen');
    Route::post('/api/common_list/{domain}', 'Api\ListController@getCommonList')->name('api.getCommonList')->middleware('super-domen');
    Route::post('/api/home_list', 'Api\ListController@getHomeList')->name('api.getHomeList')->middleware('super-domen');
    Route::post('/api/pl_home_list', 'Api\ListController@getHomeList')->name('api.getHomeList');

    Route::post('/calc/save/{form_data_id}', 'Api\CalcController@save')->name('calc.save');
    Route::post('/calc/get/{form_data_id}', 'Api\CalcController@get')->name('calc.get');
    Route::get('/download/{code}/{width?}/{height?}', 'UploadController@download')->name('file.download');

    Route::get('/form/add/{code}/{id?}/{parent_code?}', 'FormController@add')->name('form.add');
    Route::post('/api/form/save/{code}/{form_data_id?}/{parent_code?}', 'Api\FormController@save')->name('api.form.save');

    Route::group(['middleware' => ['clean-files']], function() {
        Route::post('/api/form/schema/{code}/{form_data_id?}/{parent_code?}', 'Api\FormController@schema')->name('api.form.schema');
    });

    Route::group(['middleware' => ['auth']], function() {

        Route::get('/form/edit/{code}/{id}', 'FormController@edit')->name('form.edit');
        Route::post('/api/call/record', 'Api\CallController@getRecord')->name('api.callRecord');
        Route::post('/api/calls/comment/{call}', 'Api\CallController@comment')->name('calls.comment');
        Route::get('/calls/record/{call_id}', 'Api\CallController@getRecord')->name('api.incomingRecord');
        Route::post('/api/call', 'Api\CallController@createCall')->name('api.call');
        //Route::post('/api/calls/incoming', 'Api\CallController@getIncomingCalls')->name('api.incomingCalls');
        Route::post('/api/calls/incoming', 'Api\ListController@incomingCalls')->name('api.getIncomingCalls');
        Route::get('/calls', 'PageController@calls')->name('calls');
        Route::post('/user/update', 'ProfileController@update')->name('user.update');
        Route::get('/user/edit/{id}', 'ProfileController@edit')->name('user.edit');
        Route::get('/moderation', 'FormController@moderation')->name('moderation');
        Route::post('/api/moderation', 'Api\ListController@getUsers')->name('api.getModeration');
        Route::get('/history', 'FormController@history')->name('history');
        Route::post('/api/history', 'Api\ListController@getHistory')->name('api.getHistory');
        Route::post('/api/relation/{code}/{id}', 'Api\ListController@getRelationsForm')->name('api.getRelationsForm');
        Route::post('/api/distance/renew/{form_data_id}', 'Api\FormController@forceRenewDistances')->name('api.forceRenewDistances');

        Route::get('/smartapi/{id}', function($id) {
            \App\Classes\SmartApi::factory()->getTenderData($id);
        });

        Route::get('/profile', 'ProfileController@profile')->name('profile');
        Route::post('/profile/save', 'ProfileController@save')->name('profile.save');
        Route::post('/get/users', 'ProfileController@getUsers')->name('get.users');

        Route::post('/forms/submit/{code}/{id}', 'FormController@submit')->name('forms.submit');
        Route::post('/forms/update/{code}/{form_data_id}', 'FormController@update')->name('forms.update');
        Route::post('/forms/resave', 'FormController@resave')->name('forms.resave');

        Route::post('/upload/save', 'UploadController@save')->name('upload.save');
        Route::post('/upload/delete', 'UploadController@delete')->name('upload.delete');
        Route::post('/upload/update/doctype', 'UploadController@updateDoctype')->name('upload.update.doctype');
        Route::post('/upload/update/filename', 'UploadController@updateFilename')->name('upload.update.filename');

        Route::post('/api/status/save/{code}/{form_data_id?}', 'Api\StatusController@save')->name('api.status.save');
        Route::post('/api/status/schema/{code}/{form_data_id?}', 'Api\StatusController@schema')->name('api.status.schema');
        
        Route::get('/logout', 'AuthController@logout')->name('logout');
        Route::any('/distance', 'DistanceController@index')->name('distance');
    });

    Route::group(['middleware' => ['guest']], function() {
        Route::match(['get', 'post'], '/login', 'AuthController@login')->name('login');
        Route::match(['get', 'post'], '/registration', 'AuthController@registration')->name('registration');

        Route::get('/login/{provider}', 'AuthController@social')->name('login.social');
        Route::get('/login/{provider}/callback', 'AuthController@handleProviderCallback');
    });
    
    Route::get('marker/{color}/{size}', function($color, $size){
        $img = Image::canvas(24, 24);
        
        switch($size) {
            case 'small': $size=10; break;
            case 'medium': $size=14; break;
            case 'large': $size=18; break;
        }

        $img->circle($size, 24/2, 24/2, function ($draw) use($color) {
            $draw->background('#'.$color);
        });
        
        return $img->response('png')->setMaxAge(604800)->setPublic();
    })->where('color', '[a-fA-F0-9]+')->where('size', 'large|small|medium');
    
    Route::get('marker/{color}/object', function($color) {
        return response('<?xml version="1.0" encoding="UTF-8"?><svg width="38" height="63" xmlns="http://www.w3.org/2000/svg"><defs><radialGradient cx="48.529%" cy="51.124%" fx="48.529%" fy="51.124%" r="53.504%" gradientTransform="matrix(0 1 -.59772 0 .79 .026)" id="a"><stop stop-opacity=".4" offset="0%"/><stop stop-opacity="0" offset="100%"/></radialGradient></defs><g transform="translate(-5)" fill="none" fill-rule="evenodd"><ellipse fill="url(#a)" cx="25" cy="59.926" rx="25" ry="2.574"/><g transform="translate(5.147)"><path d="M19.118.735C8.966.735.735 8.965.735 19.118c0 2.843.647 5.54 1.8 7.941.065.137 16.583 32.5 16.583 32.5L35.5 27.462c1.278-2.503 2-5.34 2-8.344C37.5 8.964 29.271.735 19.118.735zm0 34.56c-8.934 0-16.177-7.247-16.177-16.177 0-8.931 7.243-16.177 16.177-16.177 8.93 0 16.176 7.246 16.176 16.177 0 8.93-7.246 16.176-16.176 16.176z" fill="#'.$color.'" fill-rule="nonzero"/><rect fill="#FFF" x=".707" y=".707" width="36.824" height="36.824" rx="18.412"/><rect stroke="#'.$color.'" stroke-width="3" fill-opacity=".8" fill="#'.$color.'" x="2.207" y="2.207" width="33.824" height="33.824" rx="16.912"/></g></g></svg>', 200)->header('Content-Type', 'image/svg+xml');
    })->where('color', '[a-fA-F0-9]+');
    
    Route::get('{slug}', 'PageController@page')->name('page');
	Route::get('/', 'PageController@homepage')->name('homepage');
});