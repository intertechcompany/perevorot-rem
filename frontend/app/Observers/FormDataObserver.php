<?php

namespace App\Observers;

use App\Models\FormData;
use App\Models\FormDataHistory;
use Carbon\Carbon;

class FormDataObserver
{
    /**
     * Handle the form data "created" event.
     *
     * @param  \App\Models\FormData  $formData
     * @return void
     */
    public function created(FormData $formData)
    {
        $history = new FormDataHistory();
        $history->form_id = $formData->id;
        $history->user_id = $formData->user_id;
        $history->action = 'form_created';
        $history->created_at = Carbon::now();
        $history->save();
    }

    /**
     * Handle the form data "updated" event.
     *
     * @param  \App\Models\FormData  $formData
     * @return void
     */
    public function updated(FormData $formData)
    {
        $history = new FormDataHistory();
        $history->form_id = $formData->id;
        $history->user_id = $formData->editor_id;
        $history->action = 'form_edited';
        $history->created_at = Carbon::now();
        $history->save();
    }

    /**
     * Handle the form data "deleted" event.
     *
     * @param  \App\Models\FormData  $formData
     * @return void
     */
    public function deleted(FormData $formData)
    {
        $history = new FormDataHistory();
        $history->form_id = $formData->id;
        $history->user_id = $formData->user_id;
        $history->action = 'form_deleted';
        $history->created_at = Carbon::now();
        $history->save();
    }

    /**
     * Handle the form data "restored" event.
     *
     * @param  \App\Models\FormData  $formData
     * @return void
     */
    public function restored(FormData $formData)
    {
        //
    }

    /**
     * Handle the form data "force deleted" event.
     *
     * @param  \App\Models\FormData  $formData
     * @return void
     */
    public function forceDeleted(FormData $formData)
    {
        //
    }
}
