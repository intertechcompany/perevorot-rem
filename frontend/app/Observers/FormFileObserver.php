<?php

namespace App\Observers;

use App\Models\FormFile;
use App\Models\FormDataHistory;
use Carbon\Carbon;

class FormFileObserver
{
    /**
     * Handle the form file "created" event.
     *
     * @param  \App\Models\FormFile  $formFile
     * @return void
     */
    public function created(FormFile $formFile)
    {
        if(!empty($formFile->form_data_id)) {
            $history = new FormDataHistory();
            $history->form_id = $formFile->form_data_id;
            $history->user_id = $formFile->user_id;
            $history->action = 'file_upload';
            $history->created_at = Carbon::now();
            $history->save();
        }
    }

    /**
     * Handle the form file "updated" event.
     *
     * @param  \App\Models\FormFile  $formFile
     * @return void
     */
    public function updated(FormFile $formFile)
    {
    }

    /**
     * Handle the form file "deleted" event.
     *
     * @param  \App\FormFile  $formFile
     * @return void
     */
    public function deleted(FormFile $formFile)
    {
        //
    }

    /**
     * Handle the form file "restored" event.
     *
     * @param  \App\Models\FormFile  $formFile
     * @return void
     */
    public function restored(FormFile $formFile)
    {
        //
    }

    /**
     * Handle the form file "force deleted" event.
     *
     * @param  \App\Models\FormFile  $formFile
     * @return void
     */
    public function forceDeleted(FormFile $formFile)
    {
        //
    }
}
