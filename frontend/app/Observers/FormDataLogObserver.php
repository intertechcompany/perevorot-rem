<?php

namespace App\Observers;

use App\Models\FormDataHistory;
use App\Models\FormDataLog;
use Carbon\Carbon;

class FormDataLogObserver
{
    /**
     * Handle the form data log "created" event.
     *
     * @param  \App\Models\FormDataLog  $formDataLog
     * @return void
     */
    public function created(FormDataLog $formDataLog)
    {
        $history = new FormDataHistory();
        $history->form_id = $formDataLog->form_data_id;
        $history->user_id = $formDataLog->user_id;
        $history->action = 'status';
        $history->comment = $formDataLog->status;
        $history->created_at = Carbon::now();
        $history->save();
    }

    /**
     * Handle the form data log "updated" event.
     *
     * @param  \App\Models\FormDataLog  $formDataLog
     * @return void
     */
    public function updated(FormDataLog $formDataLog)
    {
        //
    }

    /**
     * Handle the form data log "deleted" event.
     *
     * @param  \App\Models\FormDataLog  $formDataLog
     * @return void
     */
    public function deleted(FormDataLog $formDataLog)
    {
        //
    }

    /**
     * Handle the form data log "restored" event.
     *
     * @param  \App\Models\FormDataLog  $formDataLog
     * @return void
     */
    public function restored(FormDataLog $formDataLog)
    {
        //
    }

    /**
     * Handle the form data log "force deleted" event.
     *
     * @param  \App\Models\FormDataLog  $formDataLog
     * @return void
     */
    public function forceDeleted(FormDataLog $formDataLog)
    {
        //
    }
}
