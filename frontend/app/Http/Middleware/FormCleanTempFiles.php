<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Form;
use App\Models\FormFile;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FormCleanTempFiles
{
    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::check()) {
            $user_id=Auth::user()->id;
            
            $model=FormFile::query();
            
            $model->whereNull('form_data_id');
            $model->where('user_id', $user_id);

            if(!empty($request->code)) {
                if($form = Form::byCode($request->code)->enabled()->first()) {
                    $model->where('form_id', $form->id);
                }
            }

            $files = $model->get();

            foreach($files as $file) {
                Storage::delete($file->name);

                $file->delete();
            }
        }

        return $next($request);
    }
}
