<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

class DistanceController extends BaseController
{
    public function index(Request $request)
    {
        $from=$request->input('from');
        $to=$request->input('to');
        
        $output=null;

        if($from && $to) {
            $client = new \GuzzleHttp\Client();
            
            $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/distancematrix/json', [
                'query' => [
                    'origins' => $from,
                    'destinations' => $to,
                    'key' => 'AIzaSyDT8KqEYQCbnzduhUvaKyHsMmeNVCp5Dsk',
                    'mode' => 'walking'
                ]
            ]);

            if($response->getStatusCode()==200) {
                $output=(object) [
                    'distance'=>0,
                    'duration'=>0
                ];

                $body = $response->getBody();
                $json = json_decode($body->getContents());

                if(@$json->rows[0]->elements[0]->distance->value && @$json->rows[0]->elements[0]->duration->value) {
                    $output->distance=$json->rows[0]->elements[0]->distance->value;
                    $output->duration=ceil($json->rows[0]->elements[0]->duration->value/60);
                }
            }
        }

        return $this->render('pages/distance',[
            'output'=>$output,
            'from'=>$from,
            'to'=>$to
        ]);
    }
}
