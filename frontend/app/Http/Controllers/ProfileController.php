<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormData;
use App\Models\FormDataText;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function profile()
    {
        return $this->render('pages/profile');
    }

    public function edit($id)
    {
        $user = Auth::getUser();

        if (!$user->accessToModeration()) {
            abort(404);
        }

        $user = User::find($id);

        if(!$user) {
            abort(404);
        }

        $groups = UserGroup::where('code', '<>', 'administrator')->whereNotIn('id', $user->groups->pluck('id'))->get();

        return $this->render('pages/user', ['userData'=>$user, 'groups'=>$groups]);
    }

    public function update(Request $request)
    {
        $user = Auth::getUser();

        if (!$user->accessToModeration()) {
            abort(404);
        }

        if(!empty($request->get('group_id'))) {
            $user = User::find($request->get('id'));
            $group = UserGroup::find($request->get('group_id'));
            $guest = UserGroup::where('code', 'guest')->first();

            $user->groups()->detach($guest->id);
            $user->groups()->save($group);
        }

        return redirect()->route('moderation');
    }

    public function save(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'password' => 'required|min:6|confirmed',
        ]);

        if($validate->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validate);
        }

        $user = Auth::user();
        $user->password = Hash::make($request->get('password'));
        $user->save();

        return redirect()->back();
    }

    public function getUsers(Request $request)
    {
        $user = Auth::user();

        if(!$user->isAdmin()) {
            abort(404);
        }

        $users = User::whereHas('groups', function($q) {
            //$q->where('code', 'guest')->orWhere('code', 'supplier');
        })->get();

        $data = [];

        foreach($users as $item) {
            array_push($data, [
                'value' => $item->id,
                'label' => $item->name,
            ]);
        }

        return response()->json(
            $data
        , 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }
}
