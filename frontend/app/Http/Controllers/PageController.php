<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormData;
use App\Models\FormDataText;
use App\Models\FormDataZone;
use App\Models\IncomingCall;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class PageController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function homepage()
    {
        $user = Auth::getUser();

        if($user && $user->isSuperAdmin()) {

            return $this->render('pages/form/list', [
                'showHomeList' => true
            ]);

            /*
            FormData::setDomain(true);

            $formsData = FormData::withDataRelations()
                ->byFormCode('order')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            return $this->render('pages/homepage', [
                'formsData'=>$formsData,
                'hideObjects' => false,
            ]);*/
        }

        $hideObjects = @$this->settings->homeAuth;
        $hideObjects = $hideObjects ? !Auth::check() : false;
        
        if($hideObjects) {
            return redirect()->route('login');
        }

        if($user) {
            $group = $user->groups()->where('redirect_url', '<>', '')->whereNotNull('redirect_url')->first();

            if($group && $group->redirect_url != '/') {
                return redirect()->to($group->redirect_url);
            }
        }

        if(@$this->settings->homeViewMode != 'mapMode' && !$hideObjects) {
            /*$formsData = FormData::withDataRelations()
                ->byActiveForm($this->settings->homeViewMode)
                ->context('read')
                ->contextPersonal('read')
                ->orderBy('created_at', 'DESC')
                ->get();*/

            $form=Form::with(['childrenForm' => function($q) {
                $q->context('read');
            }])->byCode($this->settings->homeViewMode)->enabled()->context('read')->first();

            return $this->render('pages/form/list', [
                'hideObjects' => $hideObjects,
                'form' => $form
            ]);

        } else {
            $formsData = new Collection([]);
        }

        return $this->render('pages/homepage', [
            'formsData'=>$formsData,
            'hideObjects' => $hideObjects
        ]);
    }

    public function calls()
    {
        $user = Auth::getUser();

        if(!$user->isAdmin() && !$user->accessToCall()) {
            abort(404);
        }

        $calls = IncomingCall::all();

        return $this->render('pages/calls', ['calls' => $calls]);
    }

    public function page(Request $request)
    {
        return $this->render('pages/page');
    }
}
