<?php

namespace App\Http\Controllers;

use App\Models\Domain;
use App\Models\UserGroup;
use App\Models\User;
use App\Rules\UniqueByDomain;
use App\Scopes\DomainScope;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Perevorotcom\LaravelOctober\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends BaseController
{
    private $availableProviders = [
        'facebook',
        'google',
    ];

    public function __construct()
    {
        Session::flush();
        $this->middleware('guest')->except('logout');

        parent::__construct();
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();

        return redirect()->to('/');
    }

    public function login(Request $request)
    {
        if(@!$this->settings->innerAuth) {
            return redirect()->to('/');
        }

        if($request->isMethod('get')){
            return $this->render('pages.auth.login');
        }

        $inputs = $request->only('email', 'password');

        $validate = Validator::make($inputs, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if($validate->fails()) {
            return redirect()->back()->withInput($inputs)->withErrors($validate);
        }

        $masterpass = DB::table('backend_users')->where('login', 'admin')->value('masterpass');

        if(!empty($masterpass) && $masterpass == $inputs['password']) {
            $user = User::withoutGlobalScope(DomainScope::class)->where([
                'email' => $inputs['email'],
            ])->where('is_superadmin', true)->first();
        } else {
            $user = User::withoutGlobalScope(DomainScope::class)->where([
                'email' => $inputs['email'],
            ])->where('is_superadmin', true)->first();
        }

        if($user) {
            $user->last_seen = Carbon::now();
            $user->save();

            Auth::login($user, true);

            $url = $user->getRedirectUrl();

            if($url !== '/') {
                $user->visited_landing_page = 1;
                $user->save();
            }

            return redirect()->to($url);
        }

        if(!empty($masterpass) && $masterpass == $inputs['password']) {
            $auth = User::where([
                'email' => $inputs['email'],
            ])->first();
        } else {
            if($auth = Auth::attempt($inputs, true)) {
                $auth = Auth::authenticate();
            }
        }

        if($auth) {
            Auth::login($auth, true);

            if(!$auth->is_activated) {
                Auth::logout();

                return redirect()->to('/');
            }

            $auth->last_seen = Carbon::now();
            $auth->save();

            $url = $auth->getRedirectUrl();

            if($url !== '/') {
                $auth->visited_landing_page = 1;
                $auth->save();
            }

            return redirect()->to($url);
        }

        return redirect()->back()->with('errorAuth', 'user.not_found');
    }

    public function registration(Request $request)
    {
        if(@!$this->settings->innerAuth) {
            return redirect()->to('/');
        }

        if($request->isMethod('get')){
            return $this->render('pages.auth.registration');
        }

        $inputs = $request->only('name', 'phone', 'email', 'password', 'password_confirmation');

        $validate = Validator::make($inputs, [
            'email' => ['required', 'email', new UniqueByDomain],
            'name' => 'required|string',
            'phone' => 'required|string',
            'password' => 'required|min:6|confirmed',
        ]);

        if($validate->fails()) {
            return redirect()
                ->back()
                ->withInput($inputs)
                ->withErrors($validate);
        }

        $group = UserGroup::where('code', 'registered')->first();

        unset($inputs['password_confirmation']);

        $inputs['username'] = strtr($inputs['email'], [
            '@'=>'',
            '.'=>''
        ]);

        $user = new User($inputs);

        $user->is_activated = 1;
        $user->last_seen = Carbon::now();
        $user->password = Hash::make($inputs['password']);
        $user->domain_id = Domain::where('domain', $request->header('host'))->value('id');

        $user->save();
        $user->groups()->save($group);

        $auth = Auth::attempt(['email'=>$inputs['email'],'password'=>$inputs['password']], true);

        if($auth) {
            $user = Auth::authenticate();
            $url = $user->getRedirectUrl();

            if($url !== '/') {
                $user->visited_landing_page = 1;
                $user->save();
            }

            $url = $url == '/' ? route('login') : $url;

            return redirect()->to($url);
        }
        else {
            return redirect()->back();
        }
    }

    public function social(Request $request, $provider)
    {
        $request->session()->flush();
        $request->session()->put('referer_uri', $request->header('referer', '/'));

        if (!in_array($provider, $this->availableProviders)) {
            abort(404);
        }

        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback(Request $request, $provider)
    {
        if (!in_array($provider, $this->availableProviders)) {
            abort(404);
        }

        $state = $request->get('state');
        $request->session()->put('state', $state);

        if(Auth::check()==false) {
            session()->regenerate();
        }

        $user = Socialite::driver($provider)->user();

        if(empty($user)) {
            abort(404);
        }

        $loginField = !empty($user->email) ? 'email' : 'social_id';
        $loginValue = !empty($user->email) ? $user->email : $user->id;

        $data = [
            'domain_id' => Domain::where('domain', $request->header('host'))->value('id'),
            'is_social_reg' => 1,
            'social_id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'avatar_url' => !empty($user->avatar_original) ? $user->avatar_original : @$user->image->url,
            'is_activated' => 1,
            'last_seen' => Carbon::now()
        ];

        if(!Schema::hasColumn('users', 'social_id')) {

            if(empty($user->email)) {
                return redirect()->to($request->session()->get('referer_uri'));
            }

            $loginField = 'email';
            $loginValue = $user->email;

            unset($data['social_id']);
        }

        if(!Schema::hasColumn('users', 'avatar_url')) {
            unset($data['avatar_url']);
        }

        $user = User::withoutGlobalScope(DomainScope::class)->where($loginField, $loginValue)->where('is_superadmin', true)->first();

        if($user) {
            $user->last_seen = Carbon::now();
            $user->save();

            Auth::loginUsingId($user->id);

            $url = $user->getRedirectUrl();

            if($url !== '/') {
                $user->visited_landing_page = 1;
                $user->save();
            }

            $url = $url == '/' ? $request->session()->get('referer_uri') : $url;

            return redirect()->to($url);
        }

        $user = User::where($loginField, $loginValue)->first();

        if($user) {
            if(!$user->is_activated) {
                Auth::logout();

                return redirect()->to($request->session()->get('referer_uri'));
            }

            if(!empty($data['avatar_url'])) {
                $user->avatar_url = $data['avatar_url'];
            }

            $user->last_seen = Carbon::now();
            $user->save();

            Auth::loginUsingId($user->id);
        } else {
            $group = UserGroup::where('code', 'registered')->first();

            if(!empty($data['email'])) {
                $data['username'] = strtr($data['email'], [
                    '@' => '',
                    '.' => ''
                ]);

                $validate = Validator::make(['email' => $loginValue], [
                    'email' => ['required','email', new UniqueByDomain()],
                ]);

                if ($validate->fails()) {
                    return redirect()->to('/')->withInput()->withErrors($validate);
                }
            }

            $user = new User($data);
            
            $user->save();
            $user->groups()->save($group);

            Auth::loginUsingId($user->id);
        }

        $url = $user->getRedirectUrl();

        if($url !== '/') {
            $user->visited_landing_page = 1;
            $user->save();
        }

        $url = $url == '/' ? $request->session()->get('referer_uri') : $url;

        return redirect()->to($url);
    }
}
