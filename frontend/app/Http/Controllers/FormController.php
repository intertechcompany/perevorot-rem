<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormData;
use App\Models\FormDataNumber;
use App\Models\FormDataText;
use App\Models\FormDataVarchar;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use SEO;

class FormController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add(Request $request, $code, $id = null, $parentCode = null)
    {
        if(!Auth::check() && empty($this->settings->createByAnonim)) {
            return redirect()->to(route('login'));
        }

        $form=Form::with(['parentForm'=>function($q1) {
            $q1->context('read');
        }])->byCode($code)->enabled()->context('create')->first();

        if($form) {
            $form->title = $form->name;

            SEO::setData([
                'item' => $form
            ]);

            $url = request()->fullUrl();
            $showCustomHeader = strpos($url, 'form/add/') && !empty($this->settings->createByAnonim);

            return $this->render('pages/form/add', [
                'form' => $form,
                'parentId' => $id,
                'parentCode' => $parentCode,
                'showCustomHeader' => $showCustomHeader
            ]);
        }

        abort(404);
    }

    public function edit(Request $request, $code, $id)
    {
        $user = Auth::getUser();
        //FormData::setDomain($user->isSuperAdmin());

        $form=Form::byCode($code)->enabled()->context('edit')->first();

        if($form) {
            $form->title = $form->name;

            if($user && $user->isSuperAdmin()) {
                $formData = FormData::withDataRelations()->contextPersonal('read')->context('edit')->find($id);
            } else {
                $formData = $form->formData()->withDataRelations()->contextPersonal('read')->context('edit')->find($id);
            }

            if(!$formData) {
                abort(404);
            }

            $form->title = !empty($formData->data->houseAddress->value->address) ? $formData->data->houseAddress->value->address : @$formData->data->address->value->address;

            SEO::setData([
                'item' => $form
            ]);

            return $this->render('pages/form/edit', [
                'form' => $form,
                'formData' => $formData,
            ]);
        }

        abort(404);
    }

    public function listByDomain(Request $request, $domain)
    {
        return $this->render('pages/form/list', [
            'domain' => $domain
        ]);
    }

    public function listByType(Request $request, $type)
    {
        return $this->render('pages/form/list', [
            'type' => $type
        ]);
    }

    public function list(Request $request, $code)
    {
        $hideObjects = @$this->settings->homeAuth;
        $hideObjects = $hideObjects ? !Auth::check() : false;

        if($hideObjects) {
            return redirect()->route('login');
        }

        $form=Form::with(['childrenForm' => function($q) {
            $q->context('read');
        }])->byCode($code)->enabled()->context('read')->first();

        if($form) {

            SEO::setData([
                'item' => $form
            ]);

            return $this->render('pages/form/list', [
                'form' => $form
            ]);
        }

        abort(404);
    }

    public function history(Request $request)
    {
        $user = Auth::getUser();

        if(!$user->accessToHistory()) {
            abort(404);
        }

        return $this->render('pages/form/history');
    }

    public function moderation(Request $request)
    {
        $user = Auth::getUser();

        if(!$user->accessToModeration()) {
            abort(404);
        }

        return $this->render('pages/form/moderation');
    }

    public function show(Request $request, $code, $id, $tab = 'common')
    {
        $user = Auth::getUser();
        //FormData::setDomain($user->isSuperAdmin());

        $hideObjects = @$this->settings->homeAuth;
        $hideObjects = $hideObjects ? !Auth::check() : false;

        if($hideObjects) {
            return redirect()->route('login');
        }

        $form = Form::with(['fields', 'userGroupsEdit', 'userGroupsFileEdit', 'childrenForms' => function ($q) {
            $q->context('read');
        }, 'childrenForm' => function ($q) {
            $q->context('read');
        },
        'parentForm'=>function($q1) {
            $q1->context('read');
        }
        ])->byCode($code)->enabled()->context('read')->first();

        if($form) {
            $form->editable();

            if($user && $user->isSuperAdmin()) {
                $formData = FormData::withDataRelations()->contextPersonal('read', $form->code)->where('id', $id)->first();
            } else {
                $formData = FormData::withDataRelations()->contextPersonal('read', $form->code)->where('id', $id)->where('form_id', $form->id)->first();
            }

            if(!$formData) {
                abort(404);
            }

            $formData->form->canEdit = $form->canEdit;
            $formData = $formData->getCurrentStatus()->filterFields('read')->accessByStatus();

            //dd($formData);

            if($formData) {
                $form->title = $formData->getTitle();

                SEO::setData([
                    'item' => $form
                ]);

                if($form->code == 'bids' && $user && $user->groups->where('code', 'suppliers')->count()) {
                    $fieldCode = $formData->type->autocompleteForm[0]->fieldCode;
                    $value = @$formData->data->{camel_case(str_replace('-', '_', $fieldCode))}->value->id;
                    $user_id = $formData->data->{camel_case(str_replace('-', '_', $fieldCode))}->value->user_id;

                    if($user_id != $user->id) {
                        abort(404);
                    }

                    if ($value) {
                        $rows = DB::table('user_organizations')
                            ->where('user_id', $user_id)
                            ->where('organization_id', $value)
                            ->count();

                        if (!$rows) {
                            abort(404);
                        }
                    }
                }

                if($request->get('images')) {
                    $images=[];
                    $formImages=$formData->data->objectDocuments->value['images'];

                    if(!empty($formImages)) {
                        foreach($formImages as $image) {
                            array_push($images, [
                                'thumb'=>route('file.download', ['code'=>$image->code, 'width'=>670, 'height'=>480]),
                                'src'=>route('file.download', ['code'=>$image->code, 'width'=>0, 'height'=>1000]),
                                'caption'=>'<div class="columns is-gapless">
                                <div class="column has-text-left">'.mb_substr($image->name, 0, 50).'</div>
                                <div class="column has-text-right">
                                    <a style="margin-right: 10px;" href="'.$image->original_url.'" target="_blank" class="button is-dark"><span class="icon is-small"><i class="mdi mdi-file-search-outline"></i></span><span>Открыть</span></a>
                                    <a href="'.$image->url.'" target="_blank" class="button is-dark"><span class="icon is-small"><i class="mdi mdi-download"></i></span><span>Загрузить</span></a>
                                </div></div>'
                            ]);
                        }
                    }

                    return response()->json([
                        'images' => $images,
                    ], 200, [
                        'Content-Type' => 'application/json; charset=UTF-8',
                        'charset' => 'UTF-8'
                    ], JSON_UNESCAPED_UNICODE);
                }

                if(!empty($formData->type->file[0]->value['all'])) {
                    $formComments = Form::byCode('comments')->first();
                    $formComments = FormData::withDataRelations()->where('form_id', $formComments->id)->get();
                    $formCommentsCount = [];
                    
                    if(!$formComments->isEmpty()) {
                        foreach ($formData->type->file[0]->value['all'] as $file) {
                            $formCommentsCount[$file->id] = 0;

                            foreach ($formComments as $comment) {
                                if (@$comment->data->parentFileId->__original_value == $file->id) {
                                    $formCommentsCount[$file->id]++;
                                }
                            }
                        }
                    }
                } else {
                    $formCommentsCount = [];
                    $formComments = new Collection([]);
                }

                $formDataStatus = $formData->logs->first();
                $lastStatus = false;

                if($formDataStatus && !$formData->form->statuses->isEmpty()) {
                    $statuses = @$formData->form->statuses->where('code', $formDataStatus->status_code)
                        ->first()
                        ->getDependents()
                        ->toArray();

                    if(empty($statuses)) {
                        $lastStatus = true;
                    }
                } elseif($formData->form->statuses->isEmpty()) {
                    $lastStatus = true;
                }

                $tpl = 'pages/form/show/'.($form->parent_form ? 'children' : $code);

                if(!view()->exists($tpl)) {
                    $tpl = 'pages/form/show/unknown';
                }

                $parentsFormData = new Collection([]);

                if($form->parent_form && $user && $user->isAdmin()) {
                    if(empty($formData->data->parentFormObjectId->value)) {
                        $parentsFormData = FormData::withDataRelations()->where('form_id', $form->parent_form)->get();
                    }
                }

                return $this->render($tpl, [
                    'parentsFormData' => $parentsFormData,
                    'tab' => $tab,
                    'lastStatus' => $lastStatus,
                    'formData' => $formData,
                    'form' => $form,
                    'formComments' => $formComments,
                    'formCommentsCount' => $formCommentsCount,
                ]);
            }
        }

        abort(404);
    }

    public function resave(Request $request)
    {
        $user = Auth::getUser();
        $formData = FormData::with('form')->find($request->get('fid'));

        if(!$formData || !@$formData->form->parent_form || !$user->isAdmin()) {
            abort(404);
        }

        if($request->get('uid')) {
            $formData->user_id = $request->get('uid');
            $formData->save();
        }

        return redirect()->back();
    }

    public function update(Request $request, $code, $id)
    {
        $form = Form::with('parentForm.fields')->byCode($code)->enabled()->first();

        if(!$form) {
            abort(404);
        }

        $formData = $form->formData()->withDataRelations()->find($id);

        if($formData) {

            $model = new FormDataNumber();

            $model->user_id = @Auth::user()->id;
            $model->form_id = $id;
            $model->field_id = $form->fields->where('code', 'parent_form_object_id')->first()->id;
            $model->updated_at = Carbon::now();
            $model->value = $request->parentFormData;

            $model->save();

            return response()->json(['message'=>'Итоговая стоимость работ сохранена.']);
        }

        abort(404);
    }

    public function submit(Request $request, $code, $id)
    {
        $form = Form::with('parentForm.fields')->byCode($code)->enabled()->context('edit')->first();

        if(!$form) {
            abort(404);
        }

        $formsChildren = FormData::withDataRelations()->where('form_id', $form->id)->get();
        
        if(!$formsChildren->isEmpty()) {
            $sum = 0;

            foreach($formsChildren as $children) {
                if(@$children->data->parentFormObjectId->value == $id && !empty($children->data->costs->value)) {
                    $sum += $children->data->costs->value;
                }
            }

            // нужно FormDataNumber
            $model = new FormDataVarchar();

            $model->user_id = @Auth::user()->id;
            $model->form_id = $id;
            $model->field_id = $form->parentForm->fields->where('code', 'costs-total')->first()->id;
            $model->updated_at = Carbon::now();
            $model->value = $sum;

            $model->save();

            return response()->json(['message'=>'Итоговая стоимость работ сохранена.']);
        }

        abort(404);
    }
}
