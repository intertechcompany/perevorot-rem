<?php

namespace App\Http\Controllers\Api;

use App\Classes\RegestrApi;
use App\Models\Calendar;
use App\Models\FormDataHistory;
use App\Models\FormDataNumber;
use App\Models\FormStatus;
use App\Models\IncomingCall;
use App\Models\Tender;
use App\Models\User;
use App\Models\UserGroup;
use App\Scopes\GlobalIdScope;
use Carbon\Carbon;

use App\Models\Field;
use App\Models\Form;
use App\Models\FormData;
use App\Models\FormDataBoolean;
use App\Models\FormDataText;
use App\Models\FormDataVarchar;
use App\Models\FormFile;
use App\Models\FormSelectTable;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class ListController extends BaseController
{
    private $translates;

    public function __construct()
    {
        parent::__construct();

        $this->translates = [
            'types' => translate('filter.types'),
            'statuses' => translate('filter.statuses'),
            'date_from' => translate('filter.date-from'),
            'date_to' => translate('filter.date-to'),
            'moderation' => translate('filter.moderation'),
            'responsibles' => translate('filter.responsibles'),
            'form_add' => translate('list.form-add'),
            'tenders' => translate('list.filter.tenders'),
            'query' => translate('list.filter.query'),
            'tendersField' => translate('list.filter.tendersField'),
            'allTypes' => translate('list.filter.allTypes'),
            'allStatuses' => translate('list.filter.allStatuses'),
            'allResponsibles' => translate('list.filter.allResponsibles'),
            'allChildrenStatuses' => translate('list.filter.allChildrenStatuses'),
        ];
    }

    private $historyColumns = [
        'history.title.date','history.title.form-title','history.title.form-main-field', 'history.title.parent-main-field',
        'history.title.action', 'history.title.user'
    ];

    public function getHomeList(Request $request)
    {
        $user = Auth::getUser();

        $cached = Cache::tags(['homeList'])->remember('homeList-'.(empty($request->input('global_id')) ? '' : ($request->input('global_id').'-')).(!empty($user->id) ? $user->id : ''), 60, function () use($request, $user) {
            if (!empty($this->settings->createByAnonim)) {
                if (empty($request->input('global_id'))) {
                    $forms = FormData::withoutGlobalScope(GlobalIdScope::class)->withDataRelations()->ByActiveForm()->orderBy('created_at', 'DESC')->get();
                } else {
                    $forms = FormData::withDataRelations()->ByActiveForm()->orderBy('created_at', 'DESC')->get();
                }
            } else {
                $forms = FormData::withDataRelations()->byHome()->orderBy('created_at', 'DESC')->get();
            }

            $data['translates'] = $this->translates;
            $data['list'] = [];
            $data['filter'] = [
                'types' => [],
                'tenders' => [],
                'statuses' => [],
                'childrenStatuses' => [],
                'responsibles' => []
            ];
            $formsType = [];
            $list = [];
            $columns = [];
            $statuses = [];

            if (!empty($this->settings->createByAnonim)) {
                $_columns = ['list.form.name', 'list.form.comment'];
            } else {
                $_columns = ['list.form.domain', 'list.form.name', 'list.form.dt', 'list.form.status'];
            }

            foreach ($_columns as $column) {
                $columns[$column] = [
                    'field' => $column,
                    'label' => translate($column),
                    'class' => '',
                    'color' => ''
                ];
            }

            if (empty($this->settings->createByAnonim)) {
                $columns[$_columns[3]] = [
                    'field' => 'status',
                    'label' => translate($_columns[3]),
                    'class' => 'tag is-primary',
                ];
            }

            foreach ($forms as $k => $item) {
                $item = $item->getCurrentStatus()->accessByStatus();

                if(!$item) {
                    unset($forms[$k]);
                    continue;
                }

                $value = [];

                if (!empty($this->settings->createByAnonim)) {
                    $value[$_columns[0]] = $item->form->name . '<br>' . $item->created_at->format('Y.d.m H:i');

                    if (!empty($item->data->score->value->name)) {
                        $value[$_columns[1]] = '<strong>' . translate('list.form.score') . ': ' . @$item->data->score->value->name . '</strong><br>' . @$item->data->comment->value;
                    } else {
                        $value[$_columns[1]] = @$item->data->comment->value;
                    }
                } else {
                    $value[$_columns[0]] = $item->form->getDomain();
                    $value[$_columns[1]] = @$item->data->name->value;
                    $value[$_columns[2]] = @$item->data->endDate->value;
                }

                $extra = [
                    'id' => $item->form_id,
                    'form_code' => $item->form->code,
                    'url' => !empty($item->form) ? route('form.show', ['code' => $item->form->code, 'id' => $item->id]) : '#',
                ];

                if (empty($this->settings->createByAnonim)) {
                    if ($item->form->fields->where('is_filter', 1)->count()) {
                        $f = $item->form->fields->where('is_filter', 1)->first();
                        $extra['date'] = @$item->data->{camel_case(str_replace('-', '_', $f->code))}->value;
                    }

                    if (!empty($item->currentStatus)) {
                        $extra['status'] = $item->currentStatus->code;
                        $value[$_columns[3]] = $item->currentStatus->name;
                        $extra['statusColor'] = $item->currentStatus->color;
                    }
                }

                if (empty($this->settings->createByAnonim) || empty($request->input('global_id'))) {
                    if (empty($formsType[$item->form_id])) {
                        $formsType[$item->form_id] = [
                            'code' => $item->form->code,
                            'name' => $item->form->name,
                        ];

                        array_push($data['filter']['types'], $formsType[$item->form_id]);
                    }
                }

                if (!empty($this->settings->createByAnonim) && empty($request->input('global_id'))) {
                    if ($item->global_id) {
                        $tender = Tender::findAll($item->global_id);
                        $extra['tender'] = $tender;
                        $extra['url'] = 'https://rejestr.io/zamowienia_publiczne/' . $item->global_id;
                    } else {
                        $extra['tender'] = null;
                        $extra['url'] = '#';
                    }
                }

                array_push($list, [
                    'data' => $value,
                    'extra' => $extra,
                ]);
            }

            if (empty($this->settings->createByAnonim)) {
                $statuses = [];

                $forms->first()->form->filterStatuses();

                if (!$forms->first()->form->statuses->isEmpty()) {
                    foreach ($forms->first()->form->statuses as $status) {
                        $countObjects = $forms->where('currentStatusCode', $status->code)->count();

                        array_push($statuses, [
                            'code' => $status->code,
                            'name' => $status->name,
                            'count_objects' => $countObjects
                        ]);
                    }
                }

                $filterByDate = $forms->first()->form->fields->where('is_filter', 1)->first();
                $data['showDateFilter'] = $filterByDate ? $filterByDate->name : false;
            }

            $data['filter']['statuses'] = @$statuses;
            $data['list'] = $list;
            $data['columns'] = $columns;

            if (!empty($this->settings->createByAnonim) && empty($request->input('global_id'))) {
                $data['filter']['tenders'] = RegestrApi::factory()->getTenderFields();
            }

            return $data;
        });

        return response()->json($cached, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function getCommonList(Request $request)
    {
        $user = Auth::getUser();

        $cached = Cache::tags(['commonList'])->remember('commonList-'.$request->domain.'-'.(!empty($user->id) ? $user->id : ''), 60, function () use($request, $user) {
            if (!is_numeric($request->domain)) {
                $domain = strtr($request->domain, ['--' => ':']);
                FormData::setDomain($domain);
            } else {
                $type = $request->domain;
            }

            $forms = FormData::withDataRelations()->byFormType(@$type)->orderBy('created_at', 'DESC')->get();

            $data['translates'] = $this->translates;
            $data['filter']['types'] = [];
            $data['filter']['tenders'] = [];
            $formsType = [];
            $list = [];
            $columns = [];
            $_columns = ['list.form.name', 'list.form.dt'];

            foreach ($_columns as $column) {
                $columns[$column] = [
                    'field' => $column,
                    'label' => translate($column),
                    'class' => '',
                    'color' => ''
                ];
            }

            foreach ($forms as $item) {
                $value = [];

                $value[$_columns[0]] = $item->getTitle();
                $value[$_columns[1]] = $item->created_at->format('Y.d.m H:i');

                $extra = [
                    'id' => $item->form_id,
                    'form_code' => $item->form->code,
                    'url' => !empty($item->form) ? route('form.show', ['code' => $item->form->code, 'id' => $item->id]) : '#',
                ];

                if ($item->form->fields->where('is_filter', 1)->count()) {
                    $f = $item->form->fields->where('is_filter', 1)->first();
                    $extra['date'] = @$item->data->{camel_case(str_replace('-', '_', $f->code))}->value;
                }

                if (empty($formsType[$item->form_id])) {
                    $formsType[$item->form_id] = [
                        'code' => $item->form->code,
                        'name' => $item->form->name,
                    ];

                    array_push($data['filter']['types'], $formsType[$item->form_id]);
                }

                array_push($list, [
                    'data' => $value,
                    'extra' => $extra,
                ]);
            }

            $filterByDate = $forms->first()->form->fields->where('is_filter', 1)->first();
            $data['showDateFilter'] = $filterByDate ? $filterByDate->name : false;
            $data['filter']['statuses'] = [];
            $data['filter']['childrenStatuses'] = [];
            $data['filter']['responsibles'] = [];
            $data['filter']['tenders'] = [];
            $data['list'] = $list;
            $data['columns'] = $columns;

            return $data;
        });

        return response()->json($cached, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function incomingCalls(Request $request)
    {
        $user = Auth::getUser();

        if(!$user->isAdmin()) {
            abort(404);
        }

        $CallController = app('App\Http\Controllers\Api\CallController');
        $CallController->getIncomingCalls();

        $calls = IncomingCall::orderBy('call_time', 'desc')->get();
        $list = [];
        $columns = [];
        $data['translates'] = $this->translates;
        $data['list'] = [];
        $data['filter'] = [
            'types' => [],
            'tenders' => [],
            'statuses' => [],
            'childrenStatuses' => [],
            'responsibles' => []
        ];

        foreach(['phone', 'call-time', 'billsec', 'record', 'add-supplier'] as $column) {
            $columns[$column] = [
                'field' => $column,
                'label' => translate('calls.'.$column),
                'class' => '',
                'color' => ''
            ];
        }

        foreach($calls as $item) {
            $value = [];
            $value['phone'] = $item->phone;
            $value['call-time'] = $item->call_time->format('Y-m-d H:i');

            if(!empty($item->billsec)) {
                $value['billsec'] = $item->billsec < 60 ? ('0:'.(strlen($item->billsec) == 1 ? "0{$item->billsec}" : $item->billsec)) : strtr(round($item->billsec/60), ['.'=>':']);
                $value['billsec'] = !starts_with($value['billsec'], '0:') ? ($value['billsec'].':00') : $value['billsec'];
            }
            else {
                $value['billsec'] = '0:00';
            }

            $value['record'] = !empty($item->billsec) ? '<a href="/calls/record/'.$item->id.'?incoming=true" target="_blank">'.translate('forms.calls.record').'</a>' : translate('forms.call.nocall');
            $value['add-supplier'] = '';

            if(!$item->is_verified) {
                $value['add-supplier'] = '<a class="navbar-item" href="/form/add/supplier?phone='.$item->phone.'">'.translate('forms.add-supplier').'</a>';
            } else {
                $value['add-supplier'] = '<span class="navbar-item">'.$item->name.'</span>';
            }

            $extra = [
                'id' => $item->id,
                'url' => '#',
            ];

            array_push($list, [
                'data' => $value,
                'extra' => $extra,
            ]);
        }

        $data['translates'] = $this->translates;
        $data['list'] = $list;
        $data['columns'] = $columns;

        return response()->json($data, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function getUsers(Request $request)
    {
        $user = Auth::getUser();

        if (!$user->accessToModeration()) {
            abort(404);
        }

        $users = User::whereHas('groups', function($q) {
            $q->where('code', 'guest');
        })->orderBy('id','desc')->get();

        $list = [];
        $columns = [];

        foreach(['email', 'name'] as $column) {
            $columns[$column] = [
                'field' => $column,
                'label' => translate('user.'.$column),
                'class' => '',
                'color' => ''
            ];
        }

        if(!$users->isEmpty()) {
            foreach ($users as $item) {

                $value = [];
                $value['email'] = $item->email;
                $value['name'] = $item->name;

                $extra = [
                    'id' => $item->id,
                    'url' => route('user.edit', ['id' => $item->id]),
                ];

                array_push($list, [
                    'data' => $value,
                    'extra' => $extra,
                ]);
            }
        }

        $data['translates'] = $this->translates;
        $data['list'] = $list;
        $data['columns'] = $columns;

        return response()->json($data, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function getHistory(Request $request)
    {
        $user = Auth::getUser();

        if (!$user->accessToHistory()) {
            abort(404);
        }

        $cached = Cache::tags(['history'])->remember('history-'.(!empty($user->id) ? $user->id : ''), 60, function () use($request, $user) {

            $data['translates'] = $this->translates;
            $data['list'] = [];
            $data['filter'] = [
                'types' => [],
                'tenders' => [],
                'statuses' => [],
                'childrenStatuses' => [],
                'responsibles' => []
            ];

            $history = FormDataHistory::with(['user', 'formData' => function ($q) {
                $q->withDataRelations();
            }])->orderBy('created_at', 'DESC')->get();

            $list = [];
            $columns = [];

            foreach ($this->historyColumns as $column) {
                $columns[$column] = [
                    'field' => $column,
                    'label' => translate($column),
                    'class' => '',
                    'color' => ''
                ];
            }

            foreach ($history as $item) {

                $value = [];

                $field = !empty($item->formData->form) ? $item->formData->form->fields->where('is_title', 1)->first() : null;
                $fieldValue = '';
                $parentFieldValue = '';

                if ($field) {
                    $fieldValue = @$item->formData->data->{camel_case(str_replace('-', '_', $field->code))}->value;

                    if ($fieldValue && $field->type == 'autocompleteForm') {
                        $fieldValue = $fieldValue->getTitle();
                    } elseif ($fieldValue && $field->type == 'map') {
                        $fieldValue = $fieldValue->address;
                    }
                }

                if (!empty($item->formData->form->parent_form)) {
                    $field = $item->formData->form->parentForm->fields->where('is_title', 1)->first();

                    if ($field && !empty($item->formData->data->parentFormObjectId->value)) {

                        $parentFormData = FormData::where('id', $item->formData->data->parentFormObjectId->value)->first();
                        $parentFieldValue = $parentFormData->data->{camel_case(str_replace('-', '_', $field->code))}->value;

                        if ($field->type == 'autocompleteForm') {
                            $parentFieldValue = $parentFieldValue->getTitle();
                        } elseif ($field->type == 'map') {
                            $parentFieldValue = $parentFieldValue->address;
                        }
                    }
                }

                $value[$this->historyColumns[0]] = $item->created_at->format('Y.d.m H:i');
                $value[$this->historyColumns[1]] = @$item->formData->form->name;
                $value[$this->historyColumns[2]] = $fieldValue;
                $value[$this->historyColumns[3]] = $parentFieldValue;
                $value[$this->historyColumns[4]] = translate('history.' . strtr($item->action, ['_' => '-']));
                $value[$this->historyColumns[5]] = @$item->user->email;

                $extra = [
                    'id' => $item->form_id,
                    'url' => !empty($item->formData->form) ? route('form.show', ['code' => $item->formData->form->code, 'id' => $item->form_id]) : '#',
                ];

                array_push($list, [
                    'data' => $value,
                    'extra' => $extra,
                ]);
            }

            $data['translates'] = $this->translates;
            $data['list'] = $list;
            $data['columns'] = $columns;

            return $data;
        });

        return response()->json($cached, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function getRelationsForm(Request $request)
    {
        $user = Auth::getUser();

        $cached = Cache::tags(['relationList'])->remember('relationList-'.$request->id.'-'.$request->code.'-'.(!empty($user->id) ? $user->id : ''), 60, function () use($request, $user) {
            $formId = $request->id;
            $requestСode = $request->code;

            $customList = false;
            //$form=Form::with(['statuses', 'childrenForm.statuses'])->byCode($requestСode)->enabled()->context('read')->first();

            $formData = FormData::contextPersonal('read')->find($formId);

            if (empty($formData->form)) {
                abort(404);
            }

            $calculated = [];
            $extra = [];
            $list = [];
            $statuses = [];
            $columns = [];

            //$relationForm = $formData->form->relationForm;
            $formDataRelations = FormData::withDataRelations()->where('form_id', $formData->form->relation_form)->get();

            if ($formDataRelations->isEmpty()) {
                return [];
            }

            foreach ($formDataRelations as $fk => $fd) {
                if (empty($fd->type->autocompleteForm[0]->__original_value) || $fd->type->autocompleteForm[0]->__original_value !== $formData->id) {
                    unset($formDataRelations[$fk]);
                }
            }

            if ($formDataRelations->isEmpty()) {
                return [];
            }

            foreach ($formDataRelations as $ik => $item) {

                $item = $item->getCurrentStatus()->filterFields('read')->accessByStatus();

                if(!$item) {
                    unset($formDataRelations[$ik]);
                    continue;
                }

                $fieldsInList = $item->form->fields->filter(function ($item) {
                    return $item->is_list === 1;
                });

                $values[$ik] = [];

                foreach ($fieldsInList as $field) {
                    if ($field->type == 'hidden') {
                        continue;
                    }

                    if (!empty($item->data->parentFormObjectId->value)) {
                        if ($item->form->parent_form) {
                            $columns['parent_form'] = [
                                'field' => 'parent_form',
                                'label' => 'Родительская форма',
                                'class' => '',
                                'color' => ''
                            ];

                            $values[$ik]['parent_form'] = '';

                            if ($item->form->parentForm->parent_form) {
                                $columns['parent_form2'] = [
                                    'field' => 'parent_form2',
                                    'label' => 'Родительская форма2',
                                    'class' => '',
                                    'color' => ''
                                ];

                                $values[$ik]['parent_form2'] = '';

                                $columns['user_data'] = [
                                    'field' => 'user_data',
                                    'label' => translate('list.author'),
                                    'class' => '',
                                    'color' => ''
                                ];

                                $values[$ik]['user_data'] = '';
                            }
                        }
                    }
                }
            }

            foreach ($formDataRelations as $ik => $item) {

                $fieldsInList = $item->form->fields->filter(function ($item) {
                    return $item->is_list === 1;
                });

                $values[$ik] = [];

                foreach ($fieldsInList as $field) {
                    if ($field->type == 'hidden') {
                        continue;
                    }

                    $code = camel_case(str_replace('-', '_', $field->code));

                    $columns[$code] = [
                        'field' => $field->code,
                        'label' => $field->name,
                        'class' => $field->class
                    ];

                    if (isset($columns['parent_form']) && !isset($values[$ik]['parent_form'])) {
                        $values[$ik]['parent_form'] = '';
                    }

                    $values[$ik][$code] = '';
                }
            }

            foreach ($formDataRelations as $ik => $item) {
                if (!empty($item->data->parentFormObjectId->value)) {
                    $formParent = FormData::withDataRelations()->where('id', $item->data->parentFormObjectId->value)->first();

                    if ($formParent) {
                        $formParentName = $formParent->getTitle();
                        $values[$ik]['parent_form'] = $formParentName;
                        $columns['parent_form']['label'] = $formParent->form->name;

                        if (@$formParent->data->parentFormObjectId->value) {
                            $formParent2 = FormData::withDataRelations()->where('id', $formParent->data->parentFormObjectId->value)->first();
                            $formParentName2 = $formParent2->getTitle();
                            $values[$ik]['parent_form2'] = $formParentName2;
                            $columns['parent_form2']['label'] = $formParent2->form->name;
                            $values[$ik]['user_data'] = $item->user->name . "<br>" . $item->created_at->format('d.m.Y H:i');
                        }
                    }
                }

                foreach ($item->data as $code => $field) {
                    if (empty($field->inList)) {
                        continue;
                    }
                    if ($field->type == 'hidden') {
                        continue;
                    }

                    switch ($field->type) {
                        case 'map':
                            if (!empty($item->data->addressManual->value)) {
                                $value = $item->data->addressManual->value;
                            } else {
                                $value = @$field->value->address;
                            }
                            break;
                        case 'file':
                            $value = !empty($field->value['images'][0]->url) ? '<img src="' . $field->value['images'][0]->url . '">' : '';

                            if (!$value) {
                                $value = !empty($field->value['files'][0]->url) ? ('<a href="' . $field->value['files'][0]->url . '"><i class="mdi mdi-24px mdi-' . @$field->value['files'][0]->type . '"></i>' . $field->value['files'][0]->name . '</a>') : '';
                            }
                            break;
                        case 'textarea':
                            $value = nl2br(trim($field->value));
                            break;
                        case 'number':
                            $value = number_format($field->value, 0, '', '&nbsp;');

                            if ($field->isCalculate) {
                                $calculated[$field->fieldCode]['total'] += $field->value;
                            }
                            break;
                        case 'time':
                            $value = $field->value ? Carbon::createFromFormat('Y-m-d H:i:s', $field->value)->format('H:i') : $field->value;
                            break;
                        default:
                            $value = !empty($field->value->name) ? $field->value->name : $field->value;
                            break;
                    }

                    $values[$ik][$code] = $value;
                }

                $extra = [
                    'id' => $item->id,
                    'url' => route('form.show', ['code' => !$customList ? $requestСode : $item->form->code, 'id' => $item->id]),
                    'created_at' => $item->created_at->format('H:i d.m.Y'),
                    'childrenStatuses' => [],
                    'noParent' => true,
                    'moderation' => $item->user && !$item->user->groups->isEmpty() && !empty($item->user->groups->where('code', 'guest')->count()),
                ];

                if (!empty($item->data->parentFormObjectId->value)) {
                    $extra['noParent'] = false;
                }

                if (!empty($item->currentStatusCode)) {
                    $extra['status'] = $item->currentStatusCode;
                    $values[$ik]['status'] = $item->currentStatusName;
                    $extra['statusColor'] = $item->currentStatus->color;

                    $columns['status'] = [
                        'field' => 'status',
                        'label' => translate('list.status'),
                        'class' => 'tag is-primary',
                    ];
                }

                array_push($list, [
                    'data' => $values[$ik],
                    'extra' => $extra,
                ]);
            }

            $data['translates'] = $this->translates;
            $data['calculated'] = $calculated;
            $data['formName'] = $formData->form->name;
            $data['list'] = $list;
            $data['columns'] = $columns;

            $data['filter'] = [
                'statuses' => $statuses,
                'types' => [],
                'tenders' => [],
                'childrenStatuses' => [],
                'responsibles' => []
            ];

            return $data;
        });

        return response()->json($cached, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function getList(Request $request)
    {
        $user = Auth::user();

        $cached = Cache::tags(['list'])->remember('list-'.$request->code.'-'.(!empty($user->id) ? $user->id : ''), 60, function () use($request, $user) {
            if (strpos($request->code, 'autocomplete')) {
                $t = explode('-', $request->code);
                $requestСode = $t[0];
                $customList = true;
                $parentFormId = 0;
                $form = Form::with(['statuses.groupsRead', 'childrenForm' => function ($q) {
                    $q->context('read');
                }, 'childrenForm.statuses.groupsRead'])
                    ->byCode($requestСode)->enabled()->context('read')->first();
            } else {
                $parentFormId = @$request->id;
                $requestСode = $request->code;
                $customList = false;

                $form = Form::with(['statuses.groupsRead', 'childrenForm' => function ($q) {
                    $q->context('read');
                }, 'childrenForm.statuses.groupsRead'])->byCode($requestСode)->enabled()->context('read')->first();
            }

            $data['translates'] = $this->translates;
            $data['list'] = [];
            $data['filter'] = [
                'types' => [],
                'statuses' => [],
                'childrenStatuses' => [],
                'responsibles' => [],
                'tenders' => []
            ];

            $dates = Calendar::all();

            if ($form) {
                if ($customList) {
                    $fd = FormData::withDataRelations()->context('read')->where('id', $request->id)->first();

                    if ($fd) {
                        $autocompleteForms = $fd->getAutocompleteForms();

                        if (!$autocompleteForms->isEmpty()) {
                            $forms = FormData::withDataRelations()->context('read')->whereIn('id', array_column($autocompleteForms->toArray(), 'autocompleteId'))->orderBy('created_at', 'DESC')->get();
                        } else {
                            $data['msg'] = 'autocompleteForms is empty';
                            return $data;
                        }
                    } else {
                        $data['msg'] = "forms {$requestСode} is empty";
                        return $data;
                    }
                } else {
                    $forms = FormData::withDataRelations()->contextPersonal('read', $requestСode)->where('form_id', $form->id)->orderBy('created_at', 'DESC')->get();
                }

                if ($form->code == 'object') {
                    $formDataRelations = FormData::withDataRelations()->where('form_id', $form->id)->get();

                    foreach ($formDataRelations as $fk => $fd) {
                        if (empty($fd->type->autocompleteForm[0]->__original_value) || empty($fd->type->autocompleteForm[0]->value) || @$fd->type->autocompleteForm[0]->value->user_id != $user->id) {
                            unset($formDataRelations[$fk]);
                        }
                    }

                    if (!$formDataRelations->isEmpty()) {
                        $forms = $forms->merge($formDataRelations)->sortByDesc('created_at');
                    }
                }

                if (!$customList && $requestСode == 'bids' && $user && $user->groups->where('code', 'suppliers')->count()) {
                    foreach ($forms as $k => $_form) {

                        $fieldCode = @$_form->type->autocompleteForm[0]->fieldCode;

                        if ($fieldCode) {
                            $value = @$_form->data->{camel_case(str_replace('-', '_', $fieldCode))}->value->id;
                            $user_id = $_form->data->{camel_case(str_replace('-', '_', $fieldCode))}->value->user_id;

                            if ($user_id != $user->id) {
                                unset($forms[$k]);
                                continue;
                            }

                            if ($value) {
                                $rows = DB::table('user_organizations')
                                    ->where('user_id', $user_id)
                                    ->where('organization_id', $value)
                                    ->count();

                                if (!$rows) {
                                    unset($forms[$k]);
                                }
                            }
                        }
                    }
                }

                if ($forms->isEmpty()) {
                    $data['msg'] = "forms is empty";
                    return $data;
                }

                $calculated = [];
                $extra = [];
                $list = [];
                $statuses = [];
                $childrenStatuses = [];
                $columns = [];
                $formsChildren = new Collection([]);

                if ($form->childrenForm && !$form->childrenForm->statuses->isEmpty()) {

                    $formsChildren = FormData::withDataRelations()->where('form_id',
                        $form->childrenForm->id)->orderBy('created_at', 'DESC')->get();

                    foreach ($formsChildren as $ck => $child) {
                        $child = $child->getCurrentStatus()->accessByStatus();

                        if(!$child) {
                            unset($formsChildren[$ck]);
                        }
                    }
                }

                $fieldsInList = $form->fields->filter(function ($item) {
                    return $item->is_list === 1;
                });

                $colspan = 0;

                if (!$fieldsInList->isEmpty()) {
                    foreach ($fieldsInList as $item) {
                        if (!$item->is_calculate) {
                            $colspan++;
                        } else {
                            $calculated[$item->code] = ['id' => $item->id, 'colspan' => $colspan + 1, 'total' => 0];
                            $colspan = 0;
                        }
                    }

                    if (!empty($calculated) && !$fieldsInList->last()->is_calculate) {
                        $lastCalcId = array_last(array_column($calculated, 'id'));
                        $calculated[uniqid('z')] = ['colspan' => $fieldsInList->where('id', '>', $lastCalcId)->count(), 'total' => ''];
                    }
                }

                foreach ($forms as $ik => $item) {
                    if (
                        ($parentFormId && empty($item->data->parentFormObjectId->value))
                        ||
                        ($parentFormId && (!empty(@$item->data->parentFormObjectId->value) || $item->form->parentForm->parent_form) && @$item->data->parentFormObjectId->value != $parentFormId)
                        ||
                        (@$item->data->parentFileId->value)
                    ) {
                        unset($forms[$ik]);
                        continue;
                    }

                    $item = $item->getCurrentStatus()->filterFields('read')->accessByStatus();

                    if(!$item) {
                        unset($forms[$ik]);
                        continue;
                    }

                    $fieldsInList = $item->form->fields->filter(function ($item) {
                        return $item->is_list === 1;
                    });

                    $values[$ik] = [];

                    foreach ($fieldsInList as $field) {
                        if ($field->type == 'hidden') {
                            continue;
                        }

                        if (!empty($item->data->parentFormObjectId->value) && !$parentFormId) {
                            if ($form->parent_form && !$parentFormId) {
                                $columns['parent_form'] = [
                                    'field' => 'parent_form',
                                    'label' => 'Родительская форма',
                                    'class' => '',
                                    'color' => ''
                                ];

                                $values[$ik]['parent_form'] = '';

                                if ($form->parentForm->parent_form) {
                                    $columns['parent_form2'] = [
                                        'field' => 'parent_form2',
                                        'label' => 'Родительская форма2',
                                        'class' => '',
                                        'color' => ''
                                    ];

                                    $values[$ik]['parent_form2'] = '';

                                    $columns['user_data'] = [
                                        'field' => 'user_data',
                                        'label' => translate('list.author'),
                                        'class' => '',
                                        'color' => ''
                                    ];

                                    $values[$ik]['user_data'] = '';
                                }
                            }
                        }
                    }
                }

                foreach ($forms as $ik => $item) {
                    $fieldsInList = $item->form->fields->filter(function ($item) {
                        return $item->is_list === 1;
                    });

                    foreach ($fieldsInList as $field) {
                        if ($field->type == 'hidden') {
                            continue;
                        }

                        $code = camel_case(str_replace('-', '_', $field->code));

                        $columns[$code] = [
                            'field' => $field->code,
                            'label' => $field->name,
                            'class' => $field->class
                        ];

                        if (isset($columns['parent_form']) && !isset($values[$ik]['parent_form'])) {
                            $values[$ik]['parent_form'] = '';
                        }

                        $values[$ik][$code] = '';
                    }
                }

                if ($form->code == 'works') {
                    $columns['avg_percent'] = [
                        'field' => 'avg_percent',
                        'label' => translate('list.avg-percent'),
                        'class' => '',
                    ];
                    $columns['avg_price'] = [
                        'field' => 'avg_price',
                        'label' => translate('list.avg-price'),
                        'class' => '',
                    ];
                    /*$columns['avg_count'] = [
                        'field' => 'avg_count',
                        'label' => translate('list.avg-count'),
                        'class' => '',
                    ];*/

                    $works = FormData::withDataRelations()->byFormCode('works')->get();
                }

                foreach ($forms as $ik => $item) {
                    if (!empty($item->data->parentFormObjectId->value) && !$parentFormId) {
                        $formParent = FormData::withDataRelations()->where('id', $item->data->parentFormObjectId->value)->first();

                        if ($formParent) {
                            $formParentName = $formParent->getTitle();
                            $values[$ik]['parent_form'] = $formParentName;
                            $columns['parent_form']['label'] = $formParent->form->name;

                            if (@$formParent->data->parentFormObjectId->value) {
                                $formParent2 = FormData::withDataRelations()->where('id', $formParent->data->parentFormObjectId->value)->first();
                                $formParentName2 = $formParent2->getTitle();
                                $values[$ik]['parent_form2'] = $formParentName2;
                                $columns['parent_form2']['label'] = $formParent2->form->name;
                                $values[$ik]['user_data'] = $item->user->name . "<br>" . $item->created_at->format('d.m.Y H:i');
                            }
                        }
                    }

                    foreach ($item->data as $code => $field) {
                        if (empty($field->inList)) {
                            continue;
                        }
                        if ($field->type == 'hidden') {
                            continue;
                        }

                        switch ($field->type) {
                            case 'map':

                                if (!empty($item->data->addressManual->value)) {
                                    $value = $item->data->addressManual->value;
                                } else {
                                    $value = @$field->value->address;
                                }

                                break;
                            case 'file':
                                $value = !empty($field->value['images'][0]->url) ? '<img src="' . $field->value['images'][0]->url . '">' : '';

                                if (!$value) {
                                    $value = !empty($field->value['files'][0]->url) ? ('<a href="' . $field->value['files'][0]->url . '"><i class="mdi mdi-24px mdi-' . @$field->value['files'][0]->type . '"></i>' . $field->value['files'][0]->name . '</a>') : '';
                                }
                                break;
                            case 'textarea':
                                $value = nl2br(trim($field->value));
                                break;
                            case 'number':
                                $value = number_format($field->value, 0, '', '&nbsp;');

                                if ($field->isCalculate) {
                                    $calculated[$field->fieldCode]['total'] += $field->value;
                                }
                                break;
                            case 'time':
                                $value = $field->value ? Carbon::createFromFormat('Y-m-d H:i:s', $field->value)->format('H:i') : $field->value;
                                break;
                            default:
                                $value = !empty($field->value->name) ? $field->value->name : $field->value;
                                break;
                        }

                        $values[$ik][$code] = $value;
                    }

                    $extra = [
                        'id' => $item->id,
                        'url' => route('form.show', ['code' => !$customList ? $requestСode : $item->form->code, 'id' => $item->id]),
                        'created_at' => $item->created_at->format('H:i d.m.Y'),
                        'childrenStatuses' => [],
                        'noParent' => true,
                        'moderation' => $item->user && !$item->user->groups->isEmpty() && !empty($item->user->groups->where('code', 'guest')->count()),
                    ];

                    if ($form->fields->where('is_filter', 1)->count()) {
                        $f = $form->fields->where('is_filter', 1)->first();
                        $extra['date'] = @$item->data->{camel_case(str_replace('-', '_', $f->code))}->value;
                    }

                    if (!empty($item->data->parentFormObjectId->value) && !$parentFormId) {
                        $extra['noParent'] = false;
                    }

                    if (!empty($item->currentStatusCode)) {
                        $extra['status'] = $item->currentStatusCode;
                        $values[$ik]['status'] = $item->currentStatusName;
                        $extra['statusColor'] = @$item->currentStatus->color;

                        $columns['status'] = [
                            'field' => 'status',
                            'label' => translate('list.status'),
                            'class' => 'tag is-primary',
                        ];
                    }

                    if (!$formsChildren->isEmpty()) {
                        foreach ($formsChildren as $fv) {
                            if (@$fv->data->parentFormObjectId->value == $item->id
                                && !in_array($fv->currentStatusCode, $extra['childrenStatuses'])) {
                                $extra['childrenStatuses'][] = $fv->currentStatusCode;
                            }
                        }
                    }

                    if (@$form->parentForm->parent_form && $form->code == 'comments' && !empty($item->data->responsible->value)) {
                        $extra['responsible'] = $item->data->responsible->value->code;
                    }

                    if ($form->code == 'works') {

                        $sum = 0;
                        $objectCount = [];

                        foreach ($forms as $fw => $work) {
                            if (!empty($work->data->type->value) && !empty($item->data->type->value) && $work->data->type->value->code == $item->data->type->value->code) {
                                $object = FormData::find($work->data->parentFormObjectId->value)->getCurrentStatus()->accessByStatus();

                                if(!$object) {
                                    continue;
                                }

                                if ($object->currentStatus && $object->currentStatus->code != 'stopped' && in_array($object->currentStatus->code, ['discussion', 'bord-supervisory', 'sign', 'contract', 'active'])) {
                                    if(!empty($work->data->costs->value)) {
                                        $amount = (int)preg_replace('/[^0-9]/', '', $work->data->costs->value);
                                    } else {
                                        $amount = 0;
                                    }

                                    $sum += $amount;

                                    if (!in_array($object->id, $objectCount)) {
                                        $objectCount[] = $object->id;
                                    }
                                }
                            }
                        }

                        foreach ($works as $work) {
                            if (!empty($work->data->type->value) && !empty($item->data->type->value) && $work->data->type->value->code == $item->data->type->value->code) {
                                $object = FormData::find($work->data->parentFormObjectId->value)->getCurrentStatus()->accessByStatus();

                                if(!$object) {
                                    continue;
                                }

                                if (current($objectCount) != $object->id && $object->currentStatus && $object->currentStatus->code != 'stopped' && in_array($object->currentStatus->code, ['discussion', 'bord-supervisory', 'sign', 'contract', 'active'])) {
                                    if(!empty($work->data->costs->value)) {
                                        $amount = (int)preg_replace('/[^0-9]/', '', $work->data->costs->value);
                                    } else {
                                        $amount = 0;
                                    }

                                    $sum += $amount;

                                    if (!in_array($object->id, $objectCount)) {
                                        $objectCount[] = $object->id;
                                    }
                                }
                            }
                        }

                        $values[$ik]['avg_price'] = count($objectCount) > 1 ? $sum / count($objectCount) : $sum;
                        $values[$ik]['avg_percent'] = 0;
                        //$values[$ik]['avg_count'] = $sum.'-'.count($objectCount);

                        if ($values[$ik]['avg_price']) {
                            if(!empty($item->data->costs->value)) {
                                $amount = (int)preg_replace('/[^0-9]/', '', $item->data->costs->value);
                                $values[$ik]['avg_percent'] = round((($amount - $values[$ik]['avg_price']) / $amount) * 100, 0) . ' %';
                            }

                            $values[$ik]['avg_price'] = number_format($values[$ik]['avg_price'], 0, '.', ' ');
                        }
                    }

                    if (!empty($item->currentStatus->planned_time) && !empty($item->currentStatusDate)) {
                        $columns['deadline'] = [
                            'field' => 'deadline',
                            'label' => translate('list.deadline.time'),
                            'class' => '',
                        ];
                        $columns['deadline2'] = [
                            'field' => 'deadline2',
                            'label' => translate('list.deadline.date'),
                            'class' => '',
                        ];

                        $currentStatusDate = clone $item->currentStatusDate;
                        $currentStatusDate = $currentStatusDate->format('Y-m-d') . " 00:00:00";
                        $currentStatusDate = Carbon::createFromFormat('Y-m-d H:i:s', $currentStatusDate);

                        $dateStart = clone $currentStatusDate;
                        $dateStart = $dateStart->addDay(1)->format('Y-m-d') . " 00:00:00";
                        $dateStart = Carbon::createFromFormat('Y-m-d H:i:s', $dateStart);

                        $deadline = clone $dateStart;
                        $deadline = $deadline->addDays($item->currentStatus->planned_time)->format('Y-m-d') . " 00:00:00";
                        $deadline = Carbon::createFromFormat('Y-m-d H:i:s', $deadline);

                        $today = Carbon::now()->format('Y-m-d') . " 00:00:00";
                        $today = Carbon::createFromFormat('Y-m-d H:i:s', $today);

                        foreach ($dates as $date) {
                            $_date = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . " 00:00:00");

                            if ($_date >= $dateStart && $_date <= $deadline) {
                                $deadline->addDay(1);
                            }
                        }

                        foreach ($dates as $date) {
                            $_date = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . " 00:00:00");

                            if ($_date >= $deadline && $_date <= $today) {
                                $deadline->addDay(-1);
                            }
                        }

                        $diff = $deadline->diffInDays($today);

                        if ($deadline < $today) {
                            $diff *= -1;
                        }

                        $values[$ik]['deadline'] = $diff;
                        $values[$ik]['deadline2'] = $deadline->format('Y-m-d H:i:s');
                    }

                    if (!empty($this->settings->createByAnonim) && empty($request->input('global_id'))) {
                        if ($item->global_id) {
                            $tender = Tender::findAll($item->global_id);
                            $extra['tender'] = $tender;
                            $extra['url'] = 'https://rejestr.io/zamowienia_publiczne/' . $item->global_id;
                        } else {
                            $extra['tender'] = null;
                            $extra['url'] = '#';
                        }
                    }

                    array_push($list, [
                        'data' => $values[$ik],
                        'extra' => $extra,
                    ]);
                }

                if (!$form->statuses->isEmpty()) {

                    $form->filterStatuses();

                    foreach ($form->statuses as $status) {
                        $countObjects = $forms->where('currentStatusCode', $status->code)->count();

                        array_push($statuses, [
                            'code' => $status->code,
                            'name' => $status->name,
                            'count_objects' => $countObjects
                        ]);
                    }
                }

                if ($form->childrenForm && !$form->childrenForm->statuses->isEmpty()) {

                    $form->childrenForm->filterStatuses();

                    foreach ($form->childrenForm->statuses as $status) {
                        $countObjects = $formsChildren->where('currentStatusCode', $status->code)->count();

                        array_push($childrenStatuses, [
                            'code' => $status->code,
                            'name' => $status->name,
                            'count_objects' => $countObjects
                        ]);
                    }
                }

                $responsibles = [];

                if (@$form->parentForm->parent_form && $form->code == 'comments') {
                    $options = @$form->fields->where('code', 'responsible')->first()->options;

                    if (!empty($options)) {
                        $responsibles = json_decode($options);

                        foreach ($responsibles as &$item) {
                            $item->count_objects = 0;

                            foreach ($forms as $v) {
                                if (!empty($v->data->responsible->value) && $v->data->responsible->value->code == $item->code) {
                                    $item->count_objects++;
                                }
                            }
                        }
                    }
                }

                $filterByDate = $form->fields->where('is_filter', 1)->first();
                $data['showDateFilter'] = $filterByDate ? $filterByDate->name : false;
                $data['showAddButton'] = $form->code == 'object';
                $data['calculated'] = $calculated;
                $data['formName'] = $form->name;
                $data['childrenFormName'] = @$form->childrenForm->name;
                $data['list'] = $list;
                //$data['extra'] = $extra;
                $data['columns'] = $columns;

                $data['onModeration'] = $form->code == 'supplier' && Auth::check() && Auth::user()->accessToFilter();
                $data['noParentFilter'] = !empty($form->parent_form) && $user && !$user->groups->where('code', 'suppliers')->count() ? translate('bids-noparent') : false;
                $data['noParentFilterTitle'] = !empty($form->parent_form) ? translate('bids-noparent.title') : false;

                $data['filter'] = [
                    'types' => [],
                    'tenders' => [],
                    'statuses' => $statuses,
                    'childrenStatuses' => $user && $user->groups->where('code', 'suppliers')->count() ? [] : @$childrenStatuses,
                    'responsibles' => $user && $user->groups->where('code', 'suppliers')->count() ? [] : @$responsibles,
                ];

                if (!empty($this->settings->createByAnonim) && empty($request->input('global_id'))) {
                    $data['filter']['tenders'] = RegestrApi::factory()->getTenderFields();
                }
            }

          return $data;
       });

        return response()->json($cached, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }
}
