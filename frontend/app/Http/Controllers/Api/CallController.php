<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Classes\BinotelApi;
use App\Models\Call;
use App\Models\FormData;
use App\Models\IncomingCall;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CallController extends BaseController
{
    private $api;

    public function __construct()
    {
        $this->api = new BinotelApi(config('services.binotel.key'), config('services.binotel.secret'));
        $this->api->debug = false;
        $this->api->disableSSLChecks();
    }

    public function comment(Request $request, Call $call) {
        if(!Auth::user()->accessToCall()) {
            abort(404);
        }

        $call->comment = $request->get('comment');
        $call->save();

        return redirect()->back();
    }

    public function getIncomingCalls()
    {
        $calls = IncomingCall::all();

        if($calls->isEmpty()) {
            $lastRequestTimestamp = Carbon::now()->addMonth(-6)->timestamp;
        } else {
            $lastRequestTimestamp = Carbon::now()->addMonth(-1)->timestamp;
        }

        $result = $this->api->sendRequest('stats/all-incoming-calls-since', array(
            'timestamp' => $lastRequestTimestamp
        ));

        if ($result['status'] === 'success') {
            if(!$calls->isEmpty()) {
                $result['callDetails'] = array_filter($result['callDetails'], function($v, $k) use($calls) {
                    IncomingCall::where('call_id', $v['callID'])->update([
                        'billsec' => $v['billsec']
                    ]);

                    return !$calls->where('call_id', $v['callID'])->first();
                }, ARRAY_FILTER_USE_BOTH);
            }

            if(!empty($result['callDetails'])) {
                $inserts = [];

                foreach ($result['callDetails'] as $callDetail) {
                    $inserts[] = [
                        'call_id' => $callDetail['callID'],
                        'billsec' => $callDetail['billsec'],
                        'phone' => $callDetail['externalNumber'],
                        'call_time' => \Carbon\Carbon::createFromTimestamp($callDetail['startTime'])->format('Y-m-d H:i:s'),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }

                DB::table('perevorot_forms_incoming_calls')->insert($inserts);
            }

            //$calls = IncomingCall::where('is_verified', 0)->get();
            $forms = FormData::withDataRelations()->ByFormCode('supplier')->orderBy('created_at', 'DESC')->get();

            foreach ($forms as $form) {
                if (!empty($form->data->phone->value)) {
                    IncomingCall::where(['is_verified' => 0, 'phone' => $form->data->phone->value])->update([
                        'is_verified' => 1,
                        'updated_at' => Carbon::now()
                    ]);
                    IncomingCall::where(['is_verified' => 1, 'phone' => $form->data->phone->value, 'name' => null])->update([
                        'name' => @$form->data->organizationName->value
                    ]);
                }
            }
        } else {
            //printf('REST API ошибка %s: %s %s', $result['code'], $result['message'], PHP_EOL);
            Log::error('Binotel error: '.$result['message']);
        }
    }

    public function getRecord(Request $request, $id)
    {
        if(!$request->get('incoming')) {
            $call = Call::find($request->get('call_id'));
        } else {

            $user = Auth::getUser();

            if(!$user->isAdmin()) {
                abort(404);
            }

            $call = IncomingCall::find($id);
        }

        if(!$call) {
            return response()->json([
                'message' => translate('calls.error.record'),
            ], 500, [
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'UTF-8'
            ], JSON_UNESCAPED_UNICODE);
        }

        $result = $this->api->sendRequest('stats/call-record', array(
            'callID' =>$call->call_id
        ));

        if ($result['status'] === 'success') {
            $call->call_url = $result['url'];
            $call->save();

            if($request->get('incoming')) {
                return redirect()->to($call->call_url);
            }

            return response()->json([
                'message' => translate('calls.msg.get-record'),
                'url' => $call->call_url,
            ], 200, [
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'UTF-8'
            ], JSON_UNESCAPED_UNICODE);
        } else {
            //printf('REST API ошибка %s: %s %s', $result['code'], $result['message'], PHP_EOL);
            Log::error('Binotel error: '.$result['message']);

            if($request->get('incoming')) {
                return redirect()->back();
            }

            return response()->json([
                'message' => translate('calls.error.get-record'),
            ], 500, [
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'UTF-8'
            ], JSON_UNESCAPED_UNICODE);
        }
    }

    public function createCall(Request $request)
    {
        $user = Auth::getUser();
        $formData = FormData::withDataRelations()->find($request->get('form_id'));

        if(!$formData || empty($user->binotel_number)) {
            return response()->json([
                'message' => translate('calls.error.call'),
            ], 404, [
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'UTF-8'
            ], JSON_UNESCAPED_UNICODE);
        }

        $callResult = $this->api->sendRequest('calls/ext-to-phone', array(
            'ext_number' => $user->binotel_number,
            'phone_number' => $request->get('phone'),
            'playbackWaiting' => false
        ));

        if ($callResult['status'] === 'success') {

            /*$result = $this->api->sendRequest('stats/call-record', array(
                'callID' =>@$callResult['generalCallID']
            ));

            print_r($result);exit;*/

            $result = $this->api->sendRequest('stats/call-details', array(
                'generalCallID' => $callResult['generalCallID']
            ));

            if ($result['status'] == 'success') {

                $call = new Call();
                $call->name = !empty($request->get('company')) ? $request->get('company') : @$formData->data->organizationName->value;
                $call->username = $user->name;
                $call->user_id = $user->id;
                $call->form_id = $request->get('form_id');
                $call->call_id = $callResult['generalCallID'];
                $call->created_at = Carbon::now();
                $call->save();

                return response()->json([
                    'message' => translate('calls.msg.success'),
                ], 200, [
                    'Content-Type' => 'application/json; charset=UTF-8',
                    'charset' => 'UTF-8'
                ], JSON_UNESCAPED_UNICODE);
            }
        }

        //printf('REST API ошибка %s: %s %s', $result['code'], $result['message'], PHP_EOL);

        Log::error('Binotel error: '.$callResult['message']);

        return response()->json([
            'message' => translate('calls.error.call'),
        ], 500, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }
}
