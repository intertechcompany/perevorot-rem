<?php

namespace App\Http\Controllers\Api;

use App\Classes\RegestrApi;
use App\Models\DocumentType;
use App\Models\Domain;
use App\Models\FormDataDate;
use App\Models\FormDataLog;
use App\Models\FormDataNumber;
use App\Models\FormDataTime;
use App\Models\Tender;
use Carbon\Carbon;

use App\Models\Field;
use App\Models\Form;
use App\Models\FormData;
use App\Models\FormDataBoolean;
use App\Models\FormDataText;
use App\Models\FormDataVarchar;
use App\Models\FormFile;
use App\Models\FormSelectTable;
use App\Models\FormDistance;

use App\Http\Controllers\BaseController;

use Cache;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class FormController extends BaseController
{
    private $fields=[];
    private $model=[];
    private $formData=null;
    private $form=[
        '$errors'=>[],
    ];
    private $request;

    public function __construct()
    {
        $this->model=(object) $this->model;
        $this->form=(object) $this->form;
        $this->form->{'$errors'}=(object) $this->form->{'$errors'};

        parent::__construct();
    }

    public function save(Request $request)
    {
        if(!Auth::check() && empty($this->settings->createByAnonim)) {
            throw new \Exception(translate('error.access-denied'));
        }

        $user = Auth::user();
        //FormData::setDomain($user->isSuperAdmin());

        $inputs = $request->form;
        $requestFormDataId = (int)$request->form_data_id;
        $context = !empty($request->form_data_id) ? 'edit' : 'create';
        $domain = null;
        $canEditForm = true;

        if(!empty($inputs['domain'])) {
            $domain = Domain::with('companies')->find($inputs['domain']);
        }

        $form = Form::with('statuses')->byDomain(@$domain->domain)->byCode($request->code)->enabled()->context($context)->first();

        if(!$form) {

            $canEditForm = false;
            $form = Form::with('statuses')->byDomain(@$domain->domain)->byCode($request->code)->enabled()->first();

            if($form && !$form->editable('file')) {
                throw new \Exception(translate('error.forms-not-found'));
            }
        }

        if($form) {
            $user_id = @$user->id;

            if(empty($inputs)) {
                throw new \Exception(translate('error.empty-inputs'));
            }

            $form->fields = $form->fields()->byCode(array_keys($inputs))->get();
            $formData = null;

            if(!empty($requestFormDataId)) {

                if($user && $user->isSuperAdmin()) {
                    $formData = FormData::withDataRelations()->find($requestFormDataId);
                } else {
                    $formData = $form->formData()->withDataRelations()->find($requestFormDataId);
                }

                if($formData) {
                    $formData = $formData->getCurrentStatus()->filterFields('edit')->accessByStatus();

                    if(!$formData) {
                        throw new \Exception(translate('error.access-denied'));
                    }

                    $formData->editor_id = $user_id;
                    event('eloquent.updated: App\Models\FormData', $formData);

                    $form->fields = $formData->form->fields;
                } else {
                    $form->filterFields('edit');
                }
            } else {
                $form->filterFields('edit');
            }

            if(empty($formData)) {
                $formData = FormData::create([
                    'user_id' => $user_id,
                    'form_id' => $form->id,
                    'global_id' => $request->global_id,
                    'created_at' => Carbon::now(),
                ]);

                $formStatus = $form->statuses->first();

                if($formStatus && $user) {
                    $formDataLog = new FormDataLog();
                    $formDataLog->form_data_id = $formData->id;
                    $formDataLog->user_name = Auth::user()->name;
                    $formDataLog->user_id = Auth::user()->id;
                    $formDataLog->created_at = Carbon::now();
                    $formDataLog->status = $formStatus->name;
                    $formDataLog->status_code = $formStatus->code;
                    $formDataLog->save();
                }

                $request->session()->put('lastSavedFormDataId', $formData->id);

                if(!empty($request->global_id) && !$tender = Tender::findByTenderId($request->global_id)) {
                    $tenderFromApi = RegestrApi::factory()->getTenderData($request->global_id);

                    if(!empty($tenderFromApi)) {
                        foreach($tenderFromApi as $f => $v) {
                            $tender = new Tender();
                            $tender->tender_id = $request->global_id;
                            $tender->field_name = $f;
                            $tender->field_value = $v;
                            $tender->save();
                        }
                    }
                }
            }

            if($form->code == 'supplier') {
                $issetOrg = DB::table('user_organizations')->where([
                    'user_id' => $user_id,
                    'organization_id' => $formData->id
                ])->count();

                if(!$issetOrg) {
                    DB::table('user_organizations')->insert([
                        'user_id' => $user_id,
                        'organization_id' => $formData->id
                    ]);
                }
            }

            $files = FormFile::whereNull('form_data_id')
                ->where([
                    'user_id'=>$user_id,
                    'form_id'=>$formData->form_id
                ])->get();

            if(!$files->isEmpty()) {
                foreach($files as $fileModel) {
                    $fileModel->form_data_id = $formData->id;
                    $fileModel->save();
                    $fileModel->generateThumb();
                    event('eloquent.created: App\Models\FormFile', $fileModel);
                }
            }

            if(!$form->fields->isEmpty()) {
                foreach($inputs as $key => $value) {
                    if(@$request->parent_code && $key == 'parent_form_object_id') {
                        $formDataParent = FormData::where('id', $value)->first();
                        $form = Form::with('fields')->byDomain(@$domain->domain)->byCode($request->parent_code)->enabled()->first();

                        if($form) {
                            $field = $form->fields->where('type', 'autocompleteForm')->first();
                            $model = new FormDataNumber();

                            $model->user_id = $user_id;
                            $model->form_id = $formDataParent->id;
                            $model->field_id = $field->id;
                            $model->updated_at = Carbon::now();
                            $model->value = $formData->id;

                            $model->save();
                        }

                        continue;
                    }

                    if($form->is_company_field == 1 && !empty($inputs['domain'])) {

                        if ($domain->companies->isEmpty()) {
                            continue;
                        }

                        if ($key == 'procuring-entity' && empty($inputs['procuring-entity'])) {
                            $field = $form->fields->where('code', 'procuring-entity')->first();

                            if($field) {
                                $model = new FormDataVarchar();

                                $model->user_id = $user_id;
                                $model->form_id = $formData->id;
                                $model->field_id = $field->id;
                                $model->updated_at = Carbon::now();
                                $model->value = $domain->companies->count() > 1 ? $value : $domain->companies->first()->code;
                                $model->save();
                            }

                            continue;
                        }
                    }

                    if($value === null) {
                        $value = '';
                    }

                    if($field = $form->fields->where('code', $key)->first()) {
                        if(!$canEditForm && $field->type != 'file') {
                            continue;
                        }

                        switch ($field->type) {
                            case 'number':
                            case 'autocompleteForm':
                            case 'hidden':
                                $relation = 'dataNumberRelation';
                                $model = new FormDataNumber();
                                break;
                            case 'text':
                            case 'select':
                            case 'selectTable':
                            case 'checkbox':
                            case 'radio':
                            case 'phone':
                            case 'map':
                            case 'accordion':
                                $relation = 'dataVarcharRelation';
                                $model = new FormDataVarchar();
                                break;

                            case 'textarea':
                                $relation = 'dataTextRelation';
                                $model = new FormDataText();
                                break;

                            case 'date':
                                $relation = 'dataDateRelation';
                                $model = new FormDataDate();
                                $value = !empty($value) ? Carbon::createFromTimestamp(strtotime($value))->format('Y-m-d') : '';
                                break;

                            case 'time':
                                $relation = 'dataTimeRelation';
                                $model = new FormDataTime();
                                $value = !empty($value) ? Carbon::createFromTimestamp(strtotime($value))->format('Y-m-d H:i:s') : null;
                                break;

                            case 'switch':
                            case 'file':
                                $relation = 'dataBooleanRelation';
                                $model = new FormDataBoolean();
                                break;
                        }

                        $_field = $formData->$relation->where('field_id', $field->id)->first();

                        if(is_array($value)) {
                            $value = json_encode($value, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
                        }

                        if($field->type == 'file') {
                            $value = (int)(!empty($value) && $value != '[]');
                        }

                        if(!empty($model) && ((!$_field && $value !== '') || ($_field && $_field->value !== $value))) {
                            $model->user_id = @$user_id;
                            $model->form_id = $formData->id;
                            $model->field_id = $field->id;
                            $model->updated_at = Carbon::now();
                            $model->value = $value;

                            $model->save();

                            if($field->type == 'map') {
                                $formDataRenew = FormData::where('id', $formData->id)->withDataRelations()->first();
                                
                                $this->renewDistances($formDataRenew);
                            }

                            if(!empty($user) &&  $user->isAdmin() && $field->type == 'autocompleteForm' && $form->code == 'object') {
                                $fd = FormData::find($value);

                                if($fd->form->code == 'manager') {
                                    $object = FormData::find($formData->id);
                                    $object->user_id = $fd->user_id;
                                    $object->save();
                                }
                            }
                        }
                    }
                }
            }
        }

        Cache::tags(['markers','relationList','commonList','homeList','list','history'])->flush();
        
        return response()->json([
            'message' => (!empty($formData->id) ? translate('forms.msg.saved') : translate('forms.error.saved')),
            'id' => @$formData->id
        ], (!empty($formData->id) ? 200 : 500), [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function schema(Request $request)
    {
        $context = !empty($request->form_data_id) ? 'edit' : 'create';

        if((!Auth::check() && empty($this->settings->createByAnonim)) || ($context == 'edit' && !empty($this->settings->createByAnonim))) {
            throw new \Exception(translate('error.access-denied'));
        }

        $user = Auth::user();
        //FormData::setDomain($user->isSuperAdmin());

        $this->request=$request;
        $requestFormDataId = (int)$request->form_data_id;

        $form = Form::byCode($request->code)->enabled()->context($context)->first();
        $canEditForm = true;

        if(!$form) {

            $canEditForm = false;
            $form = Form::byCode($request->code)->enabled()->first();

            if(!$form || !$form->editable('file')) {
                return response()->json([
                    'fields' => $this->fields,
                    'model' => $this->model,
                    'form' => $this->form
                ], 200, [
                    'Content-Type' => 'application/json; charset=UTF-8',
                    'charset' => 'UTF-8'
                ], JSON_UNESCAPED_UNICODE);
            }
        }

        /*if($user->isSuperAdmin()) {
            $form->load(['fields.form' => function ($q) {
                $q->withoutGlobalScope('domain');
            }]);
        } else {*/
            $form->load('fields.form');
        //}

        if(!empty($requestFormDataId)) {

            if($user && $user->isSuperAdmin()) {
                $this->formData = FormData::WithDataRelations()->find($requestFormDataId);
            } else {
                $this->formData = $form->formData()->WithDataRelations()->find($requestFormDataId);
            }

            if($this->formData) {
                $this->formData = $this->formData->getCurrentStatus()->filterFields('edit')->accessByStatus();

                if(!$this->formData) {
                    throw new \Exception(translate('error.access-denied'));
                }

                $form->fields = $this->formData->getCurrentStatus()->form->filterFields('edit');
            }
        } else {
            $form->filterFields('edit');
        }

        $this->currentForm=$form;
        
        if(!empty($request->fields)) {
            $fields=!is_array($request->fields) ? [$request->fields] : $request->fields;

            $form->fields=$form->fields->filter(function($field) use($fields) {
                return in_array($field->code, $fields);
            });
        }

        $form->fields->each(function($field) use($canEditForm, $user, $request) {
            if($this->currentForm->is_company_field == 1 && $user && $field->code == 'procuring-entity') {
                if($user->isSuperAdmin()) {
                    $this->parseOrganizations($field);
                }
                /*$method = camel_case('parse_' . $field->type . '_field');
                $schema = $this->{$method}($field);
                $schema->wrapper='<div class="hide"></div>';

                array_push($this->fields, $schema);*/
            } else {
                if ($canEditForm || (!$canEditForm && $field->type == 'file')) {
                    $method = camel_case('parse_' . $field->type . '_field');

                    if (method_exists($this, $method)) {
                        array_push($this->fields, $this->{$method}($field));
                    }
                }
            }
        });

        /*if($this->currentForm->is_company_field && $user->isSuperAdmin() && empty($this->currentForm->parent_form)) {
            $this->parseOrganizations();
        }*/

        return response()->json([
            'fields' => $this->fields,
            'model' => $this->model,
            'form' => $this->form
        ], 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    private function parseOrganizations($field)
    {
        $_field = new \stdClass();
        $_field->code = 'domain';
        $_field->name = 'domain';
        $_field->label = translate('form.domain');
        $_field->is_required = true;
        $_field->placeholder = translate('form.domain');

        $schema = $this->parseTextField($_field);
        $schema->wrapper='<div class="hide"></div>';
        $this->model->{$_field->code} = $this->currentForm->domain->id;

        array_push($this->fields, $schema);

        $companies = [];

        if($this->currentForm->domain->companies->count() > 1) {
            foreach ($this->currentForm->domain->companies as $index => $company) {
                $companies[] = [
                    'name' => $company->name,
                    'code' => $company->code,
                ];
            }
        }

        if(!empty($companies)) {
            $_field = new \stdClass();
            $_field->code = $field->code;
            $_field->name = $field->name;
            $_field->label = $field->name;
            $_field->placeholder = $field->placeholder;
            $_field->is_required = true;
            $_field->options = $companies;

            $schema = $this->parseSelectField($_field);
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->__original_value;

            array_push($this->fields, $schema);
        }
    }

    private function parseAllDomains()
    {
        $domains = Domain::with('companies')->get();
        $options = [];
        $companies = [];

        foreach($domains as $domain) {
            $options[] = [
                'name' => $domain->name,
                'code' => $domain->id,
            ];

            if($domain->companies->count() > 1) {
                foreach ($domain->companies as $index => $company) {
                    $companies[$domain->id][] = [
                        'name' => $company->name,
                        'code' => $company->code,
                    ];
                }
            }
        }

        $field = new \stdClass();
        $field->code = 'domain';
        $field->name = 'domain';
        $field->label = translate('form.domain');
        $field->placeholder = translate('form.domain');
        $field->is_required = true;
        $field->options = $options;

        $schema = $this->parseSelectField($field);
        $this->model->{$field->code} = current($options)['code'];

        array_push($this->fields, $schema);

        foreach($domains as $domain) {
            if(!empty($companies[$domain->id])) {
                $field = new \stdClass();
                $field->code = 'procuringentity';
                $field->name = 'procuringentity';
                $field->label = translate('form.procuring-entity');
                $field->placeholder = translate('form.procuring-entity');
                $field->is_required = true;
                $field->parent = $domain->id;
                $field->options = $companies[$domain->id];

                $schema = $this->parseSelectField($field);
                $this->model->{$field->code} = "";

                array_push($this->fields, $schema);
            }
        }
    }

    private function addStatusData($request)
    {
        $formData = FormData::with(['form.statuses.groups', 'logs'])->where('id', $this->formData->id)
            ->first()
            ->getCurrentStatus()
            ->filteredStatuses();

        $formDataStatus = $formData->logs->first();

        if(!$formDataStatus && !$formData->form->statuses->isEmpty()) {
            $statuses = $formData->form->statuses->first()->getDependents()->toArray();
        } elseif($formDataStatus && !$formData->form->statuses->isEmpty()) {
            $status = $formData->form->statuses->where('code', $formDataStatus->status_code)->first();

            if($status) {
                $statuses = $status->getDependents()->toArray();
            }
        } else {
            $statuses = [];
        }

        if(empty($statuses)) {
            return;
        }

        array_unshift($statuses, [
            'code' => $formData->currentStatusCode,
            'name' => $formData->currentStatusName,
            'is_comment' => @$formData->is_comment,
        ]);

        $field = new \stdClass();
        $field->code = 'formstatus';
        $field->name = 'status';
        $field->label = 'Статус';
        $field->placeholder = 'status';
        $field->is_required = true;
        $field->options = $statuses;

        $schema = $this->parseSelectField($field);
        $this->model->{$field->code} = current($statuses)['code'];
        $schema->validators=[
            'required'=>[
                'message'=>translate('forms.validate.wrong-status'),
                'expression'=>'model[field.key]!="'.current($statuses)['code'].'"'
            ]
        ];

        array_push($this->fields, $schema);

        $field = new \stdClass();
        $field->code = 'formcomment';
        $field->name = 'comment';
        $field->label = 'Комментарий';
        $field->placeholder = translate('forms.validate.enter-comment');
        $field->is_required = false;
        $field->statuses = $statuses;

        $schema = $this->parseTextareaField($field);

        array_push($this->fields, $schema);
    }

    private function parseCommonData(&$schema, $field)
    {
        $schema->key=$field->code;

        if($field->is_required) {
            $schema->required=true;
        }

        $schema->templateOptions=(object) [
            'wrapper'=>[
                'properties'=>[
                  'addons'=>false,
                  'label'=>$field->name
                ]
            ]
        ];
        
        if(!empty($field->is_collapsed)) {
            $schema->templateOptions->collapsable=true;
        }

        $this->form->{$field->code}=(object) [
            '$active'=>false,
            '$dirty'=>false
        ];

        if(!empty($field->placeholder)) {
            $schema->templateOptions->properties=[
                'placeholder'=>$field->placeholder
            ];
        }

        if($field->name == 'procuringentity' && !empty($field->parent)) {
            $schema->display='model.domain == "'.trim($field->parent).'"';
        }
        elseif(!empty($field->parent_field_id)) {
            if($field->parent_field_condition=='not_empty') {
                $schema->display='model.'.$field->relatedField->code.' !== ""';
            }
            
            if($field->parent_field_condition=='has') {
                if($field->relatedField->type=='checkbox') {
                    $values=explode(',', $field->parent_field_value);
                    $condition=[];

                    foreach($values as $value) {
                        array_push($condition, 'model.'.$field->relatedField->code.'.indexOf("'.trim($value).'")!==-1');
                    }

                    $schema->display=implode(' || ', $condition);
                } else {
                    $schema->display='model.'.$field->relatedField->code.'=="'.trim($field->parent_field_value, '"').'"';
                }
            }
        }
    }

    private function parseFileField($field)
    {
        $schema=new \StdClass();
        
        $this->parseCommonData($schema, $field);

        $schema->type='dropzone-with-field';
        $this->model->{$field->code}=[];

        if(!empty($this->formData->files)) {
            $files = $this->formData->files;
            $_files = [];

            foreach($files as $file) {
                array_push($_files, [
                   'id' => $file->id,
                   'name' => $file->name,
                   'real_name' => $file->real_name,
                   'custom_name' => $file->custom_name,
                   'size' => $file->size,
                   'mime' => $file->mime,
                   'type_id' => $file->document_type_id,
                ]);
            }

            $this->model->{$field->code} = $_files;
        }

        $size = 0.1;

        if(!empty($this->settings->file_types) && !empty($this->settings->max_file_size)) {
            $types = $this->settings->file_types;
            $types = array_map(function(&$v) {
                switch ($v) {
                    case '.xlsx':
                        $v .= ',application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        break;
                    case '.xls':
                        $v .= ',application/vnd.ms-excel';
                        break;
                    case '.pdf':
                        $v .= ',application/pdf';
                        break;
                    case '.docx':
                        $v .= ',application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                        break;
                    case '.doc':
                        $v .= ',application/msword';
                        break;
                    case '.jpg':
                    case '.jfif':
                        $v .= ',image/jpeg';
                        break;
                    case '.png':
                        $v .= ',image/png';
                        break;
                    case '.mp4':
                        $v .= ',video/mp4';
                        break;
                }
                return $v;
            }, $types);

            $types = implode(',', $types);
            $size = $this->settings->max_file_size;
        }

        $schema->templateOptions->properties = (object) [
            'url'=>route('upload.save'),
            'maxFilesize'=>$size,
            'accessToRemove' => $field->form->editable('File'),
            //'dictRemoveFileConfirmation' => 'Точно удалить файл?',
            'customName'=>translate('dropzone.custom-name'),
            'dictRemoveFile'=>translate('dropzone.delete-file'),
            'dictDefaultMessage'=>translate('dropzone.upload-files'),
            'dictFileTooBig'=>translate('dropzone.wrong-file').' '.implode(',', @$this->settings->file_types).' ('.translate('dropzone.not-more').' '.$size.''.translate('dropzone.mb').')',
            'headers'=>[
                'x-form-code'=>$this->currentForm->code,
                'X-CSRF-TOKEN'=>csrf_token()
            ],
            'acceptedFiles'=>!empty($types) ? $types : '*',
            'docTypes' => DocumentType::all()
        ];

        return $schema;
    }

    private function parseSwitchField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='switch';
        $this->model->{$field->code}=false;
        
        $schema->wrapper='<div class="field"></div>';
        $schema->templateOptions->label=$field->name;

        if($this->formData) {
            $this->model->{$field->code} = (boolean)@$this->formData->data->{camel_case($field->code)}->__original_value;
        }

        return $schema;
    }

    private function parseHiddenField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='input-with-field';
        $schema->wrapper='<div class="hide"></div>';
        $schema->templateOptions->properties['type']='hidden';
        $schema->templateOptions->wrapper['properties']['label'] = null;

        return $schema;
    }

    private function parseDateField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='datepicker-with-field';

        $this->model->{$field->code}='';

        if($this->formData) {
           $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }

        return $schema;
    }

    private function parseTimeField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='timepicker-with-field';

        $this->model->{$field->code}='';

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }

        return $schema;
    }

    private function parseNumberField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='input-with-field';
        $schema->templateOptions->properties['type']='number';

        /*$schema->validators=[
            'format'=>[
                'message'=>'Пожалуйста, введите коректные данные',
                'expression'=>'model[field.key]'
            ]
        ];*/

        $this->model->{$field->code}='';

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }

        return $schema;
    }

    private function parsePhoneField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='input-with-field';
        $this->model->{$field->code}='';

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }
        elseif(strpos($this->request->header('referer'), '?phone=')) {
            $this->model->{$field->code} = explode('?phone=', $this->request->header('referer'))[1];
        }

        return $schema;
    }
    
    private function parseTextField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='input-with-field';
        $this->model->{$field->code}='';

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }

        return $schema;
    }

    private function parseMapField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);

        $schema->type='map-with-field';

        $prevCord = null;
        $referer = @$this->request->header('referer');

        if(strpos($referer, 'form/add/')) {
            $prevId = (int)array_last(explode('/', $referer));

            if($prevId) {
                $form = FormData::withDataRelations()->where('id', $prevId)->first();
                $address = $form->type->map[0]->value;
                $prevCord = (array)$address;
            }
        }

        if($prevCord) {
            $this->model->{$field->code} = (object)$prevCord;
        } else {
            $this->model->{$field->code} = (object)[
                'lat' => 0,
                'lng' => 0,
                'address' => ''
            ];
        }

        if(!empty($schema->required)) {
            $schema->validators=[
                'required'=>[
                    'message'=>translate('forms.validate.empty-field'),
                    'expression'=>'model[field.key].lat && model[field.key].lng && model[field.key].address'
                ]
            ];
        }

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }

        return $schema;
    }

    private function parseTextareaField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);
        
        $schema->type='input-with-field';
        $schema->templateOptions->properties['type']='textarea';

        $this->model->{$field->code}='';

        if(@$field->form->code == 'comments') {
            $schema->templateOptions->wrapper['properties']['label'] = null;
        }

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }

        return $schema;
    }

    private function parseCheckboxField($field)
    {
        $schema=$this->parseOptions($field, 'checkbox-with-field');
        $schema->templateOptions->childNodesWrapper='<div class="field"></div>';

        if($this->formData && !empty($this->formData->data->{camel_case($field->code)}->value)) {
            $codes = array_column($this->formData->data->{camel_case($field->code)}->value, 'code');
            $this->model->{$field->code} = $codes;
        } else {
            $this->model->{$field->code} = '';
        }

        if(!empty($schema->required)) {
            $schema->validators=[
                'required'=>[
                    'message'=>translate('forms.validate.empty-field'),
                    'expression'=>'Array.isArray(model[field.key]) && model[field.key].length > 0'
                ]
            ];
        }

        return $schema;
    }

    private function parseAccordionField($field)
    {
        $schema=$this->parseOptions($field, 'accordion-with-field');

        $schema->templateOptions->properties['size'] = 'is-fullwidth';
        $schema->wrapper='<div class="wrapper-accordion"></div>';
        
        $options=[];

        foreach($schema->templateOptions->options as $option) {
            $options[$option->properties->{'native-value'}]='';
        }

        $this->model->{$field->code}=(object) $options;

        if($this->formData && !empty($this->formData->data->{camel_case($field->code)}->value)) {
            $value = $this->formData->data->{camel_case($field->code)}->value;
            $data = !is_array($value) ? (array)json_decode($value) : $value;
            $values = [];

            foreach($data as $item) {
                $values[$item->code] = @$item->value;
            }

            $this->model->{$field->code} = $values;
        }

        if(!empty($schema->required)) {
            $schema->validators=[
                'required'=>[
                    'message'=>translate('forms.validate.empty-field'),
                    'expression'=>'Object.values(model[field.key]).filter(Boolean).length == Object.values(model[field.key]).length'
                ]
            ];
        }

        return $schema;
    }

    private function parseRadioField($field)
    {
        $schema=$this->parseOptions($field, 'radio-with-field');
        $schema->templateOptions->childNodesWrapper='<div class="field"></div>';
        
        $this->model->{$field->code}='';

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value->code;
        }
        
        if(!empty($schema->required)) {
            $schema->validators=[
                'required'=>[
                    'message'=>translate('forms.validate.empty-field'),
                    'expression'=>'model[field.key].length > 0'
                ]
            ];
        }

        return $schema;
    }

    private function parseSelectField($field)
    {
        $schema = $this->parseOptions($field, 'select-with-field', true);

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value->code;
        }

        return $schema;
    }

    private function parseSelectTableField($field)
    {
        $schema = $this->parseOptions($field, 'select-with-field', true, true);

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value->code;
        }

        return $schema;
    }

    private function parseAutocompleteFormField($field)
    {
        if(is_string($field->options)) {
            $options = !empty($field->options) ? json_decode($field->options) : null;
        } else {
            $options = $field->options;
        }

        $data=[];
        
        if($formCode=@$options[0]) {
            $form=Form::where('code', $formCode)->enabled()->with(['fields','statuses',
                'forms' => function($q) use($field) {
                    if($field->is_only_users) {
                        $q->OnlyUsers();
                    }
                },
                'forms.logs'])->first();

            if($form) {
                $fields = $form->fields->where('is_autocomplete', 1);
                $statusList = new Collection([]);

                if (!$form->statuses->isEmpty()) {
                    $statusList = $form->statuses->where('is_autocomplete', 1);
                }

                foreach ($form->forms as $dataForm) {
                    $dataForm->getCurrentStatus();

                    if (!$statusList->isEmpty() && @$dataForm->currentStatus && !@$statusList->where('id', $dataForm->currentStatus->id)->count()) {
                        continue;
                    }
                    if((@$form->autocomplete->is_equivalent && @$dataForm->currentStatus->code != 'active') || @$this->formData->id == $dataForm->id) {
                        continue;
                    }

                    $name = [];

                    foreach ($fields as $f) {

                        if($f->type == 'map') {
                            $value = @$dataForm->data->{camel_case($f->code)}->value->address;
                        }
                        elseif($f->type == 'autocompleteForm') {
                            if(!empty($dataForm->data->{camel_case($f->code)})) {
                                $_field = $dataForm->data->{camel_case($f->code)}->value->form->fields->where('is_title', 1)->first();
                                $value = @$dataForm->data->{camel_case($f->code)}->value->data->{camel_case($_field->code)}->value;
                            }
                        }
                        else {
                            $value = @$dataForm->data->{camel_case($f->code)}->value;
                        }

                        if (!empty($value)) {
                            array_push($name, $value);
                        }
                    }

                    array_push($data, [
                        'id' => $dataForm->id,
                        'name' => @implode(', ', $name)
                    ]);
                }

                $schema = new \StdClass();

                $this->parseCommonData($schema, $field);

                $this->model->{$field->code} = '';
                $parentFormUrl='/form/add/' . $formCode . ($this->request->parent_code ? "/{$this->request->form_data_id}/{$this->request->code}" : '');

                if(sizeof($data)<=10) {
                    $schema->type = 'select-with-field';
                    $schema->templateOptions->properties['size'] = 'is-fullwidth';
                    $schema->templateOptions->options=[];

                    foreach($data as $d) {
                        $optionObject = (object)[
                            'text' => $d['name'],
                            'value' => $d['id']
                        ];

                        array_push($schema->templateOptions->options, $optionObject);
                    }

                    $schema->templateOptions->wrapper['controls']=[
                        (object) [
                            'type'=>'Span',
                            'position'=>'after',
                            'options'=>(object) [
                                'label'=>'<small><a href="'.$parentFormUrl.'">'.translate('forms.autocomplete.add-new').'</a></small>',
                                'properties'=>(object) [
                                    'style'=>'margin-top: 0.2em'
                                ],
                            ]
                        ]
                    ];
                } else {
                    $schema->templateOptions->properties['data'] = $data;
                    $schema->type = 'autocomplete-with-field';
                    $schema->templateOptions->properties['parentFormUrl'] = $parentFormUrl;
                }

                $schema->templateOptions->properties['field'] = 'name';

                if(!empty($schema->required)) {
                    $schema->validators = [
                        'exists' => [
                            'message' => translate('forms.validate.select-or-new'),
                            'expression' => 'typeof model[field.key] == "number" || model[field.key] == ""'
                        ]
                    ];
                }
            } else {
                $schema = new \StdClass();

                $this->parseCommonData($schema, $field);
                $this->model->{$field->code} = '';

                $schema->type = 'autocomplete-with-field';
                $schema->templateOptions->properties['parentFormUrl'] = '/form/add/' . $formCode . ($this->request->parent_code ? "/{$this->request->form_data_id}/{$this->request->code}" : '');
                $schema->templateOptions->properties['field'] = 'name';

                if(!empty($schema->required)) {
                    $schema->validators = [
                        'exists' => [
                            'message' => translate('forms.validate.select-or-new'),
                            'expression' => 'typeof model[field.key] == "number" || model[field.key] == ""'
                        ]
                    ];
                }
            }
        }
        
        if($lastSavedFormDataId=$this->request->session()->get('lastSavedFormDataId', false)) {
            foreach($data as $item) {
                if($item['id']==$lastSavedFormDataId){
                    $lastSavedFormDataField=@array_dot($item)['name'];
                    $this->request->session()->forget('lastSavedFormDataId');
                }
            }
            
            if(!empty($lastSavedFormDataField)) {
                $schema->templateOptions->properties['value']=$lastSavedFormDataField;
            }

            //$this->model->{$field->code} = $lastSavedFormDataId;
        }

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value->id;
        }

        return $schema;
    }

    private function parseOptions($field, $type, $is_select=false, $fromTable=false)
    {
        //$options=!empty($field->options) ? json_decode($field->options) : null;

        if(is_string($field->options)) {
            $options = !empty($field->options) ? json_decode($field->options) : null;
        } else {
            $options = $field->options;
        }

        if($fromTable) {
            $table = current($options);
            $options = null;

            if(Schema::hasTable($table)) {
                $selectTable = new FormSelectTable($table);

                if($table == 'form_select_table_competitors') {
                    $options = $selectTable->orderBy('sort_order')->get();
                } else {
                    $options = $selectTable->get();
                }
            }
        }

        if($options) {
            $schema=new \StdClass();

            $this->parseCommonData($schema, $field);

            $this->model->{$field->code}=$is_select ? '' : [];

            $schema->type=$type;
            $schema->templateOptions->options=[];

            if($field->is_required) {
                $schema->templateOptions->properties['required']=true;
            }

            if($is_select) {
                $schema->templateOptions->properties['size'] = 'is-fullwidth';
            }

            foreach($options as $option) {

                if(!is_object($option)) {
                    $option = (object)$option;
                }

                if($is_select) {
                    $optionObject = (object)[
                        'text' => $option->name,
                        'value' => $option->code
                    ];
                } else {
                    $optionObject=(object) [
                        'label'=>$option->name,
                        'properties'=>(object)[
                            'native-value'=>$option->code
                        ]
                    ];
                }

                array_push($schema->templateOptions->options, $optionObject);
            }
        }

        return $schema;
    }

    public function forceRenewDistances(Request $request) {
        $formDataRenew = FormData::where('id', (int) $request->form_data_id)->withDataRelations()->first();
        
        $this->renewDistances($formDataRenew);

        return response()->json([
            'message' => translate('forms.msg.distance.renewed')
        ], 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    private function renewDistances($formData) {
        FormDistance::where('form_data_id', $formData->id)->delete();

        $client = new \GuzzleHttp\Client();
        
        foreach($formData->closest() as $item) {
            $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/distancematrix/json', [
                'query' => [
                    'origins' => $item->type->map[0]->value->lat.','.$item->type->map[0]->value->lng,
                    'destinations' => $formData->type->map[0]->value->lat.','.$formData->type->map[0]->value->lng,
                    'key' => 'AIzaSyDT8KqEYQCbnzduhUvaKyHsMmeNVCp5Dsk',
                    'mode' => 'walking'
                ]
            ]);

            if($response->getStatusCode()==200) {
                $body = $response->getBody();
                $json = json_decode($body->getContents());

                if($distance=@$json->rows[0]->elements[0]->distance->value && $duration=@$json->rows[0]->elements[0]->duration->value) {
                    $distance=new FormDistance();
                    
                    $distance->form_data_id=$formData->id;
                    $distance->form_data_parent_id=$item->id;
                    $distance->distance=$json->rows[0]->elements[0]->distance->value;
                    $distance->duration=ceil($json->rows[0]->elements[0]->duration->value/60);

                    $distance->save();
                }
            }
        }
    }
}
