<?php

namespace App\Http\Controllers\Api;

use App\Models\FormDataZone;
use Carbon\Carbon;
use Cache;

use App\Models\Form;
use App\Models\FormData;
use App\Models\FormDataText;
use App\Models\FormDataVarchar;
use App\Models\FormDataBoolean;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class MapController extends BaseController
{
    private $zonesData;
    private $zones;
    private $translates;

    public function __construct()
    {
        $this->zonesData = [
            ['code' => '01','title'=>translate('map.zone-01')],
            ['code' => '02','title'=>translate('map.zone-02')],
            ['code' => '03','title'=>translate('map.zone-03')],
            ['code' => '04','title'=>translate('map.zone-04')],
        ];

        $this->translates = [
            'searchAddress' => translate('map.search-address'),
            'statuses' => translate('map.statuses'),
            'types' => translate('map.types'),
            'cities' => translate('map.cities'),
            'notFound' => translate('map.not-found'),
            'showAll' => translate('map.show-all'),
            'allStatuses' => translate('map.allStatuses'),
            'allTypes' => translate('map.allTypes'),
            'allCities' => translate('map.allCities'),
        ];
    }

    public function saveZone(Request $request)
    {
        $referer = $request->header('referer');
        $this->zones = new Collection([]);

        if($referer && str_contains($referer, 'show/object/')) {
            $params = explode('show/object/', $referer)[1];
            $id = explode('/', $params)[0];

            FormDataZone::updateOrCreate(
                ['object_id' => $id, 'house_id' => $request->get('house')],
                ['value' => $request->get('value')]
            );

            Cache::tags(['markers'])->flush();
        }

        return response()->json([
            'msg'=>translate('forms.msg.save-zone'),
        ], 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function menu(Request $request)
    {
        $isContextMenu = true;
        $forms = [];

        if($user = Auth::getUser()) {
            $isContextMenu = $user->groups()->where('is_context_menu', 1)->count();
        }

        if($isContextMenu) {
            $formsData = Form::with('fields')->enabled()->menu()->context('create')->get();

            if (!$formsData->isEmpty()) {
                foreach ($formsData as $item) {
                    $form = new \stdClass();
                    $form->title = $item->name;
                    $form->code = $item->code;

                    $field = $item->fields->where('type', 'map')->first();

                    if ($field) {
                        $form->addressField = $field->code;
                    }

                    $forms[] = $form;
                }
            }
        }

        return response()->json([
            'forms'=>$forms,
        ], 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    public function index(Request $request)
    {
        // return response()->json(json_decode(file_get_contents(__DIR__.'/map.json')), 200, [
        //     'Content-Type' => 'application/json; charset=UTF-8',
        //     'charset' => 'UTF-8'
        // ], JSON_UNESCAPED_UNICODE);

        $markers=[];
        $referer = $request->header('referer');
        $this->zones = new Collection([]);
        $id = 0;

        if($referer && str_contains($referer, 'show/object/')) {
            $params = explode('show/object/', $referer)[1];
            $id = explode('/', $params)[0];
            $this->zones = FormDataZone::where('object_id', $id)->get()->keyBy('house_id');
        }

        $since = $request->input('since');
        
        $user = Auth::getUser();
        
        $cached = Cache::tags(['markers'])->remember('markers-'.$id.'-'.(!empty($user->id) ? $user->id : ''), 60, function () use($since) {
            $model=FormData::query();
            
            $model->contextPersonal('read')->orderBy('created_at', 'desc');
            //$model->with(['form.fields','form.statuses', 'logs']);
            $model->withDataRelations();
            
            if($since) {
                try {
                    $since = Carbon::parse($since);
                    $model->where('created_at', '>=', $since);  
                } catch(\Exception $e) {

                }
            }
            
            $items=$model->get();
            $markers=[];
            $types=[];
            $statuses=[];
            $cities=[];

            foreach($items as $ik => $item) {
                if(!empty($item->form)) {

                    $item = $item->getCurrentStatus()->accessByStatus();

                    if(!$item) {
                        unset($items[$ik]);
                        continue;
                    }

                    $method=camel_case('parse_'.$item->form->code.'_marker_data');

                    if($item->form->code=='object') {
                        $city = @$item->data->address->value->city;

                        if(empty($city)) {
                            $address = $item->data->address->value->address;
                            $city=trim(mb_substr($address, mb_strrpos($address, ',')+1));
                        }

                        $city=!empty($city) ? $city : 'Київ';
                        
                        if(empty($cities[$city])) {
                            $cities[$city]=0;
                        }
                        
                        $cities[$city]++;
                    }
                
                    if(method_exists($this, $method)) {
                        $marker=$this->{$method}($item);
                        
                        if(!empty($marker)) {
                            $marker->can_edit=Auth::check();

                            $fields = $item->form->fields->where('is_infobox', 1);
                            $values = [];

                            if(!$fields->isEmpty()) {
                                foreach ($fields as $f) {
                                    $data = @$item->data->{camel_case(str_replace('-', '_', $f->code))};

                                    if (!empty($data->value)) {
                                        $value = !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : (!empty($data->value) ? $data->value : $data->__original_value));
                                        $values[] = $f->name . ': ' . $value;
                                    }
                                }
                            }

                            $marker->values = $values;
                            
                            array_push($markers, $marker);
                        }
                                            
                        $marker->type_name=$item->form->name;
                        
                        if(!empty($item->currentStatus) && $marker->type=='object') {
                            $marker->status=[
                                'name'=>$item->currentStatus->name,
                                'code'=>$item->currentStatus->code,
                                'color'=>!empty($item->currentStatus->color) ? substr($item->currentStatus->color, 1) : null,
                            ];

                            $statuses[$item->currentStatus->code]=[
                                'name'=>$item->currentStatus->name,
                                'sort'=>$item->currentStatus->sort_order
                            ];
                        }

                        $types[$marker->type]=$item->form->name;
                    }
                }
            }

            uasort($statuses, function($a, $b){
                return $a['sort'] - $b['sort'];
            });

            foreach($statuses as $k=>$status) {
                $statuses[$k]=$status['name'];
            }

            arsort($cities, SORT_NUMERIC);

            $cities=array_keys($cities);

            return [
                'translates' => $this->translates,
                'zones' => $this->zones,
                'zonesData' => $this->zonesData,
                'markers'=>$markers,
                'filter'=>[
                    'statuses'=>$statuses,
                    'types'=>$types,
                    'cities'=>$cities
                ]
            ];
        });

        return response()->json($cached, 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    function parseHouseMarkerData($item) {        
        if(!empty($item->data->houseAddress)) {
            $marker=new \StdClass;

            $buildingType='';

            if(!empty($item->data->buildingType)) {
                if($item->data->buildingType->value->code == '03') {
                    $buildingType='construction';
                }elseif($item->data->buildingType->value->code == '02') {
                    $buildingType='office';
                }elseif($item->data->buildingType->value->code == '04') {
                    $buildingType='star';
                }
            }

            $marker->id=$item->id;
            $marker->type=$item->form->code;
            $marker->position=[$item->data->houseAddress->value->lat, $item->data->houseAddress->value->lng];
            $marker->address= !empty($item->data->addressManual->value) ? $item->data->addressManual->value : $item->data->houseAddress->value->address;
            $marker->zone='04';
            //$marker->zone=$item->data->zone->value->code;
            //
            if(!empty($this->zones[$item->id])) {
                $marker->zone = $this->zones[$item->id]->value;
            }

            $marker->buildingType=$buildingType;
            $marker->apartments=(int)@$item->data->apartments->value;
            $marker->floors=(int)@$item->data->floors->value;

            if(!empty($item->data->documents->value['images'][0]->url)) {
                $marker->image = route('file.download', ['code' => $item->data->documents->value['images'][0]->code, 'width' => 170, 'height' => 100]);
            } else {
                $marker->image = '';
            }

            return $marker;
        }

        return null;
    }

    function parseObjectMarkerData($item) {
        if(!empty($item->data->address)) {
            $marker=new \StdClass;

            $city = @$item->data->address->value->city;
            $address = !empty($item->data->addressManual->value) ? $item->data->addressManual->value : $item->data->address->value->address;

            if(empty($city)) {
                $city=trim(mb_substr($address, mb_strrpos($address, ',')+1));
            }

            $marker->id=$item->id;
            $marker->type=$item->form->code;
            $marker->position=[$item->data->address->value->lat, $item->data->address->value->lng];
            $marker->address=$address;
            $marker->city=$city;
            $marker->index=(int) @$item->data->index->value;

            if(!empty($item->data->objectDocuments->value['images'][0]->url)) {
                $marker->image = route('file.download', [
                    'code' => $item->data->objectDocuments->value['images'][0]->code,
                    'width' => 170,
                    'height' => 100
                ]);
            } else {
                $marker->image = '';
            }

            return $marker;
        }

        return null;
    }

    function parseCompetitorMarkerData($item) {
        if(!empty($item->data->place)) {
            $marker=new \StdClass;

            $marker->id=$item->id;
            $marker->type=$item->form->code;
            $marker->position=[$item->data->place->value->lat, $item->data->place->value->lng];
            $marker->address=!empty($item->data->addressManual->value) ? $item->data->addressManual->value : $item->data->place->value->address;
            $marker->icon=!empty($item->data->competitor->value->image) ? $item->data->competitor->value->image : 'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png';
            $marker->subtype=!empty($item->data->type->value->code) ? $item->data->type->value->code : '';
            $marker->is_other=(!empty($item->data->competitor->value->code) && $item->data->competitor->value->code=='other');

            if(!empty($item->data->documentsCompetitor->value['images'][0]->url)) {
                $marker->image = route('file.download', ['code' => $item->data->documentsCompetitor->value['images'][0]->code, 'width' => 170, 'height' => 100]);
            } else {
                $marker->image = '';
            }

            return $marker;
        }

        return null;
    }
}
