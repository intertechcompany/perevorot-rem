<?php

namespace App\Http\Controllers\Api;

use App\Models\Calc;
use App\Models\FormData;
use Carbon\Carbon;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalcController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function save(Request $request)
    {
        $inputs = $request->inputs;

        unset($inputs['apartmentsByZone']);
        unset($inputs['isSaving']);
        unset($inputs['trafic']);

        if(!empty($inputs)) {
            $calc = new Calc();
            $calc->json = $inputs;
            $calc->created_at = Carbon::now();
            $calc->user_id = @Auth::user()->id;
            $calc->form_data_id = $request->form_data_id;
            $calc->save();
        }

        return response()->json(['message' => translate('calc.msg.saved')], 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }
    
    public function get(Request $request)
    {
        $formData = FormData::find($request->form_data_id);
        $calc = $formData->calc()->value('json');
        $calc = !empty($calc) ? (object)$calc : '';
        $preset = [];

        if(!empty($formData->data->trafic->value)) {
            $preset['trafic'] = array_filter(array_column($formData->data->trafic->value, 'value'));
        }

        foreach(range(1, 7) as $index) {
            $preset['flowPerDay'][$index] = 100;
        }

        return response()->json(['message' => translate('calc.msg.ok'), 'calc' => $calc, 'preset'=>$preset], 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }
}
