<?php

namespace App\Http\Controllers\Api;

use App\Models\FormDataLog;
use App\Models\FormDataNumber;
use App\Models\FormStatus;
use Carbon\Carbon;

use Cache;

use App\Models\Field;
use App\Models\Form;
use App\Models\FormData;
use App\Models\FormDataBoolean;
use App\Models\FormDataText;
use App\Models\FormDataVarchar;
use App\Models\FormFile;
use App\Models\FormSelectTable;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class StatusController extends BaseController
{
    private $fields=[];
    private $model=[];
    private $formData=null;
    private $form=[
        '$errors'=>[],
    ];

    public function __construct()
    {
        $this->model=(object) $this->model;
        $this->form=(object) $this->form;
        $this->form->{'$errors'}=(object) $this->form->{'$errors'};

        parent::__construct();
    }

    public function save(Request $request)
    {
        $user = Auth::getUser();

        if($user && $user->isSuperAdmin()) {
            FormData::setDomain(true);
        }

        $inputs = $request->form;
        
        if(!empty($inputs['formstatus'])) {
            $formData = FormData::withDataRelations()->where('id', $request->form_data_id)->first()->filteredStatuses();
            $formStatus = $formData->form->statuses->where('code', $inputs['formstatus'])->first();

            if($formStatus) {
                $formDataLog = new FormDataLog();
                $formDataLog->form_data_id = $formData->id;
                $formDataLog->user_name = Auth::user()->name;
                $formDataLog->user_id = Auth::user()->id;
                $formDataLog->created_at = Carbon::now();
                $formDataLog->comment = @$inputs['formcomment'];
                $formDataLog->status = $formStatus->name;
                $formDataLog->substatus = @$inputs['childrenstatus'];
                $formDataLog->status_code = $inputs['formstatus'];
                $formDataLog->save();
            }

            Cache::tags(['markers','relationList','commonList','homeList','list','history'])->flush();

            return response()->json([
                'message' => isset($formDataLog) ? translate('forms.msg.status-saved') : translate('forms.error.status-saved'),
                'data' => $formData,
            ], isset($formDataLog) ? 200 : 500, [
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'UTF-8'
            ], JSON_UNESCAPED_UNICODE);
        }
    }

    public function schema(Request $request)
    {
        $user = Auth::getUser();

        if($user && $user->isSuperAdmin()) {
            FormData::setDomain(true);
        }

        $formData = FormData::withDataRelations()->where('id', $request->form_data_id)
            ->first()
            ->getCurrentStatus()
            ->filteredStatuses();

        $formDataStatus = $formData->logs->first();

        if(!$formDataStatus && !$formData->form->statuses->isEmpty()) {
            $statuses = @$formData->form->statuses->first()->getDependents()->toArray();
        } elseif($formDataStatus && !$formData->form->statuses->isEmpty()) {
            $status = $formData->form->statuses->where('code', $formDataStatus->status_code)->first();

            if($status) {
                $statuses = $status->getDependents()->toArray();
            }
        } else {
            $statuses = [];
        }

        if(empty($statuses)) {
            return response()->json([
                'fields' => $this->fields,
                'model' => $this->model,
                'form' => $this->form
            ], 200, [
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'UTF-8'
            ], JSON_UNESCAPED_UNICODE);
        }

        array_unshift($statuses, [
            'code' => $formData->currentStatusCode,
            'name' => $formData->currentStatusName,
            'is_comment' => @$formData->is_comment,
        ]);

        $field = new \stdClass();
        $field->code = 'formstatus';
        $field->name = '';
        $field->label = '';
        $field->placeholder = 'status';
        $field->is_required = true;
        $field->options = $statuses;

        $schema = $this->parseSelectField($field);
        $this->model->{$field->code} = current($statuses)['code'];
        $schema->validators=[
            'required'=>[
                'message'=>translate('forms.validate.wrong-status'),
                'expression'=>'model[field.key]!="'.current($statuses)['code'].'"'
            ]
        ];

        array_push($this->fields, $schema);

        $field = new \stdClass();
        $field->code = 'formcomment';
        $field->name = '';
        $field->label = '';
        $field->placeholder = translate('forms.field.enter-comment');
        $field->is_required = false;
        $field->statuses = $statuses;

        $schema = $this->parseTextareaField($field);

       /* if(!empty($field->statuses)) {
            $condition=[];

            foreach($field->statuses as $value) {
                if($value['is_comment']) {
                    array_push($condition, 'model.formstatus == "'.$value['code'].'"');
                }
            }
        }*/

        array_push($this->fields, $schema);

        $c = $formData->currentStatus->getChildren();

        foreach($c as $parent => $items) {
            $field = new \stdClass();
            $field->code = 'childrenstatus';
            $field->name = '';
            $field->label = translate('form.substatus');
            $field->placeholder = translate('form.substatus');
            $field->is_required = true;
            $field->parent = $parent;
            $field->options = $items;

            $schema = $this->parseSelectField($field);
            $this->model->{$field->code} = "";
            /*$schema->validators=[
                'required'=>[
                    'message'=>'Пожалуйста, выберите статус',
                    'expression'=>'model[field.key]!="" && model.formstatus=="'.$parent.'"'
                ]
            ];*/

            array_push($this->fields, $schema);
        }

        return response()->json([
            'fields' => $this->fields,
            'model' => $this->model,
            'form' => $this->form
        ], 200, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'UTF-8'
        ], JSON_UNESCAPED_UNICODE);
    }

    private function parseCommonData(&$schema, $field)
    {
        $schema->key=$field->code;

        if($field->is_required) {
            $schema->required=true;
        }

        $schema->templateOptions=(object) [
            'wrapper'=>[
                'properties'=>[
                  'addons'=>false,
                  'label'=>$field->name
                ]
            ]
        ];
        
        if(!empty($field->is_collapsed)) {
            $schema->templateOptions->collapsable=true;
        }

        $this->form->{$field->code}=(object) [
            '$active'=>false,
            '$dirty'=>false
        ];

        if(!empty($field->placeholder)) {
            $schema->templateOptions->properties=[
                'placeholder'=>$field->placeholder
            ];
        }

        if(!empty($field->parent)) {
            $schema->display='model.formstatus == "'.trim($field->parent).'"';
        }
    }

    private function parseTextareaField($field)
    {
        $schema=new \StdClass();

        $this->parseCommonData($schema, $field);
        
        $schema->type='input-with-field';
        $this->model->{$field->code}='';

        $schema->templateOptions->properties['type']='textarea';

        if($this->formData) {
            $this->model->{$field->code} = @$this->formData->data->{camel_case($field->code)}->value;
        }

        return $schema;
    }

    private function parseSelectField($field)
    {
        return $this->parseOptions($field, 'select-with-field', true);
    }

    private function parseOptions($field, $type, $is_select=false, $fromTable=false)
    {
        if(is_string($field->options)) {
            $options = !empty($field->options) ? json_decode($field->options) : null;
        } else {
            $options = $field->options;
        }

        if($fromTable) {
            $table = current($options);
            $options = null;

            if(Schema::hasTable($table)) {
                $selectTable = new FormSelectTable($table);
                $options = $selectTable->get();
            }
        }

        if(!empty($options)) {
            $schema=new \StdClass();
        
            $this->parseCommonData($schema, $field);
            
            $this->model->{$field->code}=$is_select ? '' : [];

            $schema->type=$type;
            $schema->templateOptions->options=[];
            
            if($field->is_required) {
                $schema->templateOptions->properties['required']=true;
            }

            if($is_select) {
                $schema->templateOptions->properties['size'] = 'is-fullwidth';
            }

            foreach($options as $option) {
                $option = (object)$option;
                if($is_select) {
                    $optionObject = (object)[
                        'text' => $option->name,
                        'value' => $option->code
                    ];
                } else {
                    $optionObject=(object) [
                        'label'=>$option->name,
                        'properties'=>(object)[
                            'native-value'=>$option->code
                        ]
                    ];                    
                }

                array_push($schema->templateOptions->options, $optionObject);
            }
        }

        return $schema;
    }
}
