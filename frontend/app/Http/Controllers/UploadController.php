<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UploadController extends BaseController
{
    public function download($code, $width = false, $height = false)
    {
        try{
            $file = FormFile::where('name', 'like', $code.'%')->first();

            if($height) {
                $fileData = Storage::get("{$width}x{$height}_".$file->name);
            } else {
                $fileData = Storage::get($file->name);
            }
        }
        catch(\Exception $e) {
            abort(404);
        }catch(FileNotFoundException $e){
            abort(404);
        }

        return Response::make($fileData, 200, [
            'Content-Type' => strpos($file->real_name, '.pdf') ? 'application/pdf' : 'application/octet-stream',
           //'Content-Length' => filesize(public_path('files').'/'.$file->real),
            'Content-Transfer-Encoding' => 'binary',
            'Accept-Ranges' => 'bytes',
            'Content-Disposition' => 'inline; filename="'.$file->real_name.'"'
        ]);
    }

    public function updateFilename(Request $request)
    {
        if (!$code = $request->header('x-form-code')) {
            throw new \Exception(translate('files.error.update-filename'));
        }

        if (!$form = Form::byCode($code)->enabled()->first()) {
            throw new \Exception(translate('files.error.update-filename'));
        }

        $inputs = $request->all();

        FormFile::where('id', $inputs['file_id'])->update(['custom_name' => $inputs['filename']]);

        return response()->json([
            'message' => translate('files.msg.update-filename')
        ], 200);
    }

    public function updateDoctype(Request $request)
    {
        if (!$code = $request->header('x-form-code')) {
            throw new \Exception(translate('files.error.update-doctype'));
        }

        if (!$form = Form::byCode($code)->enabled()->first()) {
            throw new \Exception(translate('files.error.update-doctype'));
        }

        $inputs = $request->all();

        if(empty($inputs['type_id']) || !is_numeric($inputs['type_id'])) {
            $inputs['type_id'] = 0;
        }

        FormFile::where('id', $inputs['file_id'])->update(['document_type_id' => $inputs['type_id']]);

        return response()->json([
            'message' => translate('files.msg.update-doctype')
        ], 200);
    }

    public function save(Request $request)
    {
        if(!$code = $request->header('x-form-code')) {
            throw new \Exception(translate('files.error.save'));
        }
        
        if(!$form = Form::byCode($code)->enabled()->first()) {
            throw new \Exception(translate('files.error.save'));
        }

        $files = $request->file('file');
 
        if (!is_array($files)) {
            $files = [$files];
        }

        if(!empty($this->settings->file_types) && !empty($this->settings->max_file_size)) {
            $types = strtr(implode(',', $this->settings->file_types), ['.'=>'']);
            $types .= str_contains($types, 'jpg') ? ',jpeg,jfif' : '';

            $rules = array(
                'file' => 'mime:' . $types . '|max:' . $this->settings->max_file_size*1024,
            );

            $validation = Validator::make($files, $rules);

            if ($validation->fails()) {
                $comment = translate('dropzone.wrong-file').' '.$types.' ('.translate('dropzone.not-more').' '.$this->settings->max_file_size.''.translate('dropzone.mb').')';

                throw new \Exception($comment);
            }
        }

        foreach($files as $file) {
            $fileName = str_random(32) . '.' . strtr(strtolower($file->getClientOriginalExtension()), ['jfif'=>'jpeg']);

            if (Storage::has($fileName)) {
                $fileName = str_random(33) . '.' . strtr(strtolower($file->getClientOriginalExtension()), ['jfif'=>'jpeg']);
            }

            try {
                $uploadSuccess = Storage::put($fileName, file_get_contents($file->getRealPath()));

                if($uploadSuccess) {
                    $formFile = new FormFile();

                    $formFile->user_id = Auth::user()->id;
                    $formFile->form_id = $form->id;
                    $formFile->name = $fileName;
                    $formFile->mime = $file->getClientMimeType();
                    $formFile->size = $file->getClientSize();
                    $formFile->real_name = $file->getClientOriginalName();
                    $formFile->created_at = Carbon::now();

                    $formFile->save();

                    return response()->json([
                        'message' => translate('files.msg.save'),
                        'image' => [
                            'id' => $formFile->id
                        ]
                    ], 200);
                } else {
                    throw new \Exception(translate('files.error.save'));
                }
            } catch (\Exception $e) {
                Log::error(json_decode($e->getMessage()));
                
                throw new \Exception($e->getMessage());
            }
        }
    }
    
    public function delete(Request $request)
    {
        if(!$code = $request->header('x-form-code')) {
            throw new \Exception(translate('files.error.delete'));
        }

        if(!$form = Form::byCode($code)->enabled()->first()) {
            throw new \Exception(translate('files.error.delete'));
        }
        
        if($file_id = $request->input('id')) {
            if($file = FormFile::where('id', $file_id)->where('form_id', $form->id)->first()) {
                Storage::delete($file->name);
    
                $file->delete();    
            }
        }

        return response()->json([
            'message' => translate('files.msg.delete')
        ], 200);
    }
}
