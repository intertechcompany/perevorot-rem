<?php

namespace App\Http\Controllers;

use App\Models\Domain;
use Auth;
use Localization;
use App\Models\Form;
use Illuminate\Http\Request;
use Perevorotcom\LaravelOctober\Models\Menu;
use Perevorotcom\LaravelOctober\Models\Page;
use Perevorotcom\LaravelOctober\Http\Controllers\Controller;

class BaseController extends Controller
{
    public $domain;

    public function __construct()
    {
        parent::__construct();

        $this->domain = Domain::with('companies')->where('domain', request()->getHttpHost())->first();

        $current=Localization::getCurrentLocale();
        $locales=Localization::getLocalesOrder();

        if(request()->header('host') == 'pl1.dozorro.org') {
            Localization::setLocale('en');
        }

        unset($locales[$current]);

        $this->setCommonData([
            'menu' => Menu::label('menu-main', [
                'depth' => 1
            ]),
            'locales' => (object) [
                'list' => $locales,
                'default' => Localization::getDefaultLocale(),
                'current' => $current,
            ]
        ]);
    }

    public function render($template, $data = [], $statusCode = 200)
    {
        $host = request()->getHttpHost();

        if($host == 'pl.rem.localhost') {
            \Debugbar::disable();

            return response()->view('pages/poland', [], 200);
        }

        $user = Auth::getUser();

        if($user && $user->isSuperAdmin()) {
            $domains = array_filter(array_pluck(Domain::all()->map(function($v) {
                $v->domain = strtr($v->domain, [':'=>'--']);
                return $v;
            })->toArray(), 'domain'));
            $menuFormsCreate = Form::withoutGlobalScope('domain')->enabled()->menu()->isCreate()->get()->keyBy('code');
            $groupForms = Form::withoutGlobalScope('domain')->enabled()->where('is_company_field', '>', 1)->get()->keyBy('is_company_field');
            $domainsFilter = [];
        } else {
            $menuFormsRead = Form::with('userGroupsRead')->enabled()->context('read')->menu()->get();

            if(!empty($this->settings->createByAnonim)) {
                $menuFormsCreate = Form::enabled()->isCreate()->context('create')->menu()->get();
            } else {
                $menuFormsCreate = Form::with(['userGroupsCreate', 'userGroupsEdit'])->enabled()->isCreate()->context('create')->menu()->get();
            }
        }

        $defaultData = [
            'domainsFilter' => @$domainsFilter,
            'domains' => @$domains,
            'groupForms' => @$groupForms,
            'logo' => $this->getSiteLogo(),
            'menuFormsRead' => @$menuFormsRead,
            'menuFormsCreate' => @$menuFormsCreate,
            'user' => $user,
            'page' => $this->page,
            'settings' => $this->settings
        ];

        return response()->view(
            $template,
            array_merge(
                $defaultData,
                $data
            ),
            $statusCode
        );
    }

    private function getSiteLogo()
    {
        if(!empty($this->settings->logoByDomain) && is_array($this->settings->logoByDomain)) {
            $request = Request::createFromGlobals();

            $logoByDomain = array_first($this->settings->logoByDomain, function($v, $k) use($request) {
                return $request->header('host') == $v->logo_domain;
            });

            $logo = $logoByDomain ? env('STORAGE_URL').'/media'.$logoByDomain->logo_image : @$this->settings->logo->path;
        } else {
            $logo = @$this->settings->logo->path;
        }

        return $logo;
    }
}
