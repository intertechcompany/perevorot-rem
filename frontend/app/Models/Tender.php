<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    public $timestamps = false;
    protected $table = 'tenders';

    public static function findByTenderId($value)
    {
        return self::where('tender_id', $value)->first();
    }

    public static function findAll($value)
    {
        return self::where('tender_id', $value)->get();
    }
}
