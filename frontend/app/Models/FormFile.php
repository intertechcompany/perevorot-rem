<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;

class FormFile extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $dates = ['created_at','deleted_at'];
    protected $table = 'form_files';
    public $fillable = ['user_id','form_id','form_data_id','created_at','name','size','real_name'];

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id', 'id');
    }

    public function formData()
    {
        return $this->belongsTo(FormData::class, 'form_data_id', 'id');
    }

    public function generateThumb()
    {
        if(!starts_with($this->mime, 'image')) {
            return;
        }

        foreach(config('image.image_sizes') as $size) {
            $img = Image::make(public_path('files') . '/' . $this->name);

            if ($size[0]) {
                $img->resize($size[0], $size[1]);
            } else {
                $img->resize(null, $size[1], function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            @$img->save(public_path('files') . "/{$size[0]}x{$size[1]}_" . $this->name, 90);
        }
    }
}
