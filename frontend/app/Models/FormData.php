<?php

namespace App\Models;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use App\Scopes\GlobalIdScope;

class FormData extends \LaravelOctoberModel
{
    use SoftDeletes;

    protected $dates = ['created_at', 'deleted_at', 'date'];
    protected $table = 'form_data';

    public $timestamps = false;
    public $backendModel='Perevorot\Forms\Models\FormData';
    public $fillable = [
        'form_id',
        'created_at',
        'user_id',
        'global_id'
    ];
    
    public $formSchema;
    public $formSchemaByType;
    public $formSchemaByTypes;
    static $domain;

    /*protected $dispatchesEvents = [
        'changedStatus' => UserSaved::class,
    ];*/

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new GlobalIdScope);
    }

    public static function setDomain($domain)
    {
        self::$domain = $domain;
    }

    public function zones()
    {
        return $this->hasMany(FormDataZone::class, 'house_id', 'id');
    }

    public function calls()
    {
        return $this->hasMany(Call::class, 'form_id', 'id')->orderBy('created_at','desc');
    }

    public function calc()
    {
        return $this->hasMany(Calc::class)->orderBy('created_at','desc');
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function dataTextRelation()
    {
        return $this->hasMany(FormDataText::class, 'form_id', 'id')->orderBy('updated_at', 'desc');
    }

    public function dataVarcharRelation()
    {
        return $this->hasMany(FormDataVarchar::class, 'form_id', 'id')->orderBy('updated_at', 'desc');
    }

    public function dataBooleanRelation()
    {
        return $this->hasMany(FormDataBoolean::class, 'form_id', 'id')->orderBy('updated_at', 'desc');
    }

    public function dataNumberRelation()
    {
        return $this->hasMany(FormDataNumber::class, 'form_id', 'id')->orderBy('updated_at', 'desc');
    }

    public function dataDateRelation()
    {
        return $this->hasMany(FormDataDate::class, 'form_id', 'id')->orderBy('updated_at', 'desc');
    }

    public function dataTimeRelation()
    {
        return $this->hasMany(FormDataTime::class, 'form_id', 'id')->orderBy('updated_at', 'desc');
    }

    public function files()
    {
        return $this->hasMany(FormFile::class, 'form_data_id', 'id')->orderBy('created_at', 'desc');
    }

    public function logs()
    {
        return $this->hasMany(FormDataLog::class, 'form_data_id', 'id')->orderBy('created_at', 'desc');
    }

    public function scopeByUserGroup($query)
    {
        if($user = Auth::getUser()) {
            $userGroups = $user->groups->pluck('id')->toArray();

            if($userGroups) {
                $query->where('user_id', $user->id);
            }
        }
    }

    public function scopeByUser($query, $byGroup)
    {
        if(is_array($byGroup)) {
            $byGroup = array_filter($byGroup);
        }

        if(!empty($byGroup) && $user = Auth::getUser()) {
            $userGroups = $user->groups->whereIn('id', $byGroup)->count();

            if($userGroups) {
                $query->where('user_id', $user->id);
            }
        }
    }

    public function scopeOnlyUsers($query)
    {
        $user = Auth::getUser();

        if($user) {
            $groups = $user->groups->pluck('code')->toArray();

            if(!in_array('pe', $groups) && !in_array('administrator', $groups)) {
                $query->where('user_id', $user->id);
            }
        }
    }

    public function scopeByFormType($query, $type = false)
    {
        if($type) {
            $query->whereHas('form', function ($q) use($type) {
                $q->where('is_company_field', $type);
            });
        }
    }

    public function scopeByActiveForm($query, $code = false, $id = false)
    {
        $query->whereHas('form', function($q) use($code) {
            $q->enabled();

            if($code) {
                $q->byCode($code);
            }
        });
    }

    public function scopeByHome($query, $code = false, $id = false)
    {
        $query->whereHas('form', function($q) use($code) {
            $q->byDomain(true)->enabled()->where('is_homepage', 1);
        });
    }

    public function scopeByFormCode($query, $code = false, $id = false)
    {
        $query->whereHas('form', function($q) use($code) {
            $q->byDomain(true);
            $q->enabled();

            if($code) {
                $q->byCode($code);
            }
        });
    }

    public function scopeByDomain($query, $domain)
    {
        if($domain) {
            $query->whereHas('form', function ($q) use ($domain) {
                $q->byDomain($domain);
            });
        }
    }
    
    public function scopeWithDataRelations($q, $date = null)
    {
        $q->byDomain(self::$domain);

        foreach(['text', 'varchar', 'boolean', 'number', 'date', 'time'] as $type) {

            $relation = camel_case('data_'.$type.'_relation');

            if($date) {
                $q->with([$relation => function ($q2) use ($date) {
                        $q2->whereRaw("DATE(updated_at) <= '$date'")->withTrashed();
                    }, $relation.'.field']);
            } else {
                $q->with([$relation.'.field',$relation.'.user']);
            }
        }

        $q->with([
            'form' => function($q1) {
                $q1->byDomain(self::$domain);
            },
            'form.parentForm' => function($q1) {
                $q1->byDomain(self::$domain);
            },
            'form.relationForm' => function($q1) {
                $q1->byDomain(self::$domain);
            },
            'user.groups',
            'form.statuses.groups',
            'form.statuses.groupsRead',
            'form.fields',
            'form.parentForm.fields',
            'form.fields.userGroupsRead',
            'form.fields.userGroupsEdit',
            'form.fields.formStatusesRead',
            'form.fields.formStatusesEdit',
            'form.userGroupsRead',
            'form.userGroupsEdit',
            'form.userGroupsReadPersonal',
            'form.formStatusesRead',
            'form.formStatusesEdit',
            'files.user',
            'files.formData.dataVarcharRelation',
            'logs.user',
            'dataNumberRelation.autocompleteForms',
            'dataVarcharRelation.user'
        ]);
        
        return $q;
    }

    public function scopeContext($q, $context, $formStatus = null)
    {
        $relation = 'form.userGroups'.studly_case($context);

        if($user = Auth::user()) {
            $user->load('groups');
        }

        if($user && !$user->isSuperAdmin()) {
            $q->whereHas($relation, function ($q3) use ($user) {
                if ($user && !$user->groups->isEmpty()) {
                    $q3->whereIn('user_group_id', $user->groups->pluck('id')->toArray());
                }
            })->orDoesntHave($relation);
        }
    }

    public function scopeContextPersonal($q, $context, $formCode = '')
    {
        $relation = 'form.userGroups'.studly_case($context).'Personal';

        if($user = Auth::user()) {
            $user->load('groups');
        }

        if($user && !$user->isSuperAdmin() && !$user->groups->isEmpty()) {
            if($formCode == 'bids' && $user->groups->where('code', 'suppliers')->count()) {
                $q->with($relation)->whereHas('dataNumberRelation.field', function ($q1) {
                    $q1->where('code', 'supplier')->where('type', 'autocompleteForm');
                });
            }
            elseif($formCode != 'contact') {
                $q->with($relation)->where(function($q5) use($user, $relation) {
                        $q5->whereHas($relation, function ($q3) use ($user) {
                            $q3->whereIn('user_group_id', $user->groups->pluck('id')->toArray());
                        })->where('user_id', $user->id);
                    })->orWhere(function($q6) use($user, $relation) {
                        $q6->WhereDoesntHave($relation, function ($q3) use ($user) {
                            $q3->whereIn('user_group_id', $user->groups->pluck('id')->toArray());
                        });
                    });
            }
            elseif($formCode == 'contact' && $user->region) {
                $q->with($relation)->whereHas('dataVarcharRelation', function ($q2) use($user) {
                    $q2->whereHas('field', function($q) {
                            $q->where('code', 'region');
                        })
                        //->where('user_id', $user->id)
                        ->join('users', 'users.id', '=', 'form_data_varchar.user_id')
                        ->select('users.region', 'form_data_varchar.*')
                        ->whereColumn('region', 'value');
                });
            }
        }
    }

    public function checkStatus($context = 'read')
    {
        $this->getCurrentStatus();

        if($this->currentStatus) {

            $relation = "formStatuses" . studly_case($context);

            if(in_array($this->currentStatus->id, array_column($this->form->$relation->toArray(), 'id'))) {
                return true;
            }
        }

        return false;
    }

    public function hasGetMutator($mutator)
    {
        if ($mutator=='data') {
            return true;
        }

        if ($mutator=='type') {
            return true;
        }

        if ($mutator=='types') {
            return true;
        }

        return method_exists($this, 'get'.studly_case($mutator).'Attribute');
    }

    public function mutateAttribute($mutator, $value)
    {
        if ($mutator=='data') {
            return $this->dataValue($mutator, $value);
        }

        if ($mutator=='type') {
            return $this->typeValue($mutator, $value);
        }

        if ($mutator=='types') {
            return $this->typesValue($mutator, $value);
        }

        return $this->{'get'.studly_case($mutator).'Attribute'}($value);
    }
    
    public function dataValue($value, $type)
    {
        if(!$this->formSchema) {
            $this->formSchema=[];
            $fields = $this->form->fields;

            foreach ($fields as $field) {

                // желательно оптимизоровать
                $relation = $this->getRelationByType($field->type)->first(function ($item) use ($field) {
                    return $item->field_id == $field->id;
                });

                if ($relation) {
                    $value = $relation->value;

                    $value2 = $this->parseValueByField($field, $value);
                    $format = $field->type == 'number' ? number_format($value2, 0, '', '&nbsp;') : null;
                    $format = $field->type == 'time' && $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('H:i') : null;

                    $this->formSchema[camel_case(str_replace('-', '_', $field->code))] = (object)[
                        'value' => $value2,
                        'formatValue' => $format,
                        'fieldName' => $field->name,
                        'fieldCode' => $field->code,
                        'fieldId' => $field->id,
                        'isCalculate' => $field->is_calculate,
                        'inList' => $field->is_list,
                        'class' => $field->class,
                        'type' => $field->type,
                        '__original_value' => $value,
                    ];
                }
            }
        }

        return (object) $this->formSchema;
    }

    public function typeValue($value)
    {
        if(!$this->formSchemaByType) {
            $this->formSchemaByType=[];

            foreach($this->form->fields as $field) {
                $relation = $this->getRelationByType($field->type)->first(function ($item) use ($field) {
                    return $item->field_id == $field->id;
                });

                if ($relation) {
                    $value = $relation->value;
                    $type = camel_case(str_replace('-', '_', $field->type));

                    if (empty($this->formSchemaByType[$type])) {
                        $this->formSchemaByType[$type] = [];
                    }

                    $value2 = $this->parseValueByField($field, $value);
                    $format = $field->type == 'number' ? number_format($value2, 0, '', '&nbsp;') : null;
                    $format = $field->type == 'time' && $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('H:i') : null;

                    $this->formSchemaByType[$type][] = (object)[
                        'value' => $value2,
                        'formatValue' => $format,
                        'fieldName' => $field->name,
                        'fieldCode' => $field->code,
                        'type' => $field->type,
                        'fieldId' => $field->id,
                        'inList' => $field->is_list,
                        'isCalculate' => $field->is_calculate,
                        'class' => $field->class,
                        '__original_value' => $value,
                    ];
                }
            }
        }

        return (object) $this->formSchemaByType;
    }

    public function typesValue($value)
    {
        if(!$this->formSchemaByTypes) {
            $this->formSchemaByTypes=[];

            foreach($this->form->fields as $field) {

                $relations = $this->getRelationByType($field->type)->where('field_id', $field->id);

                if (!$relations->isEmpty()) {
                    foreach($relations as $relation) {
                        $value = $relation->value;
                        $type = camel_case(str_replace('-', '_', $field->type));

                        if (empty($this->formSchemaByTypes[$type])) {
                            $this->formSchemaByTypes[$type] = [];
                        }

                        $value2 = $this->parseValueByField($field, $value);
                        $format = $field->type == 'number' ? number_format($value2, 0, '', '&nbsp;') : null;
                        $format = $field->type == 'time' && $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('H:i') : null;

                        $this->formSchemaByTypes[$type][] = (object)[
                            'value' => $value2,
                            'formatValue' => $format,
                            'fieldName' => $field->name,
                            'fieldCode' => $field->code,
                            'fieldId' => $field->id,
                            'inList' => $field->is_list,
                            'inHistory' => $field->is_history,
                            'isCalculate' => $field->is_calculate,
                            'class' => $field->class,
                            '__original_value' => $value,
                            'user' => $relation->user,
                            'updated_at' => $relation->updated_at,
                        ];
                    }
                }
            }
        }

        return (object) $this->formSchemaByTypes;
    }

    public function getRelationByType($type)
    {
        switch($type){
            case 'number':
            case 'autocompleteForm':
            case 'hidden':
                $relation = 'dataNumberRelation';
                break;
            case 'text':
            case 'select':
            case 'selectTable':
            case 'checkbox':
            case 'radio':
            case 'phone':
            case 'map':
            case 'accordion':
                $relation = 'dataVarcharRelation';
                break;
            case 'textarea':
               $relation = 'dataTextRelation';
                break;
            case 'date':
                $relation = 'dataDateRelation';
                break;
            case 'time':
                $relation = 'dataTimeRelation';
                break;
            case 'switch':
            case 'file':
                $relation = 'dataBooleanRelation';
                break;
        }

        return $this->{$relation};
    }

    private $autocompleteForms = [];

    public function parseValueByField($field, $value)
    {
        switch($field->type) {
            case 'accordion':
                $options = (array) json_decode($field->options);
                $values=json_decode($value, true);

                if(!empty($values) && is_array($values)) {
                    foreach ($values as $ik => $item) {
                        $options = array_map(function (&$option) use ($ik, $item) {
                            if($option->code == $ik) {
                                $option->value = $item;
                            }

                            return $option;
                        }, $options);
                    }
                }
                $value=$options;
                break;

            case 'file':
                $files = new Collection([]);
                $values = [];

                if(!empty($this->files)) {
                    foreach($this->files as $file) {
                        $ext = strtolower(explode('.', $file->name)[1]);
                        $code = explode('.', $file->name)[0];
                        $fileData = [
                            'id' => $file->id,
                            'code' => $code,
                            'dt' => $file->created_at->format('d.m.Y H:i'),
                            'created_at' => $file->created_at,
                            'user' => $file->user,
                            'name' => $file->real_name.($file->custom_name ? ", {$file->custom_name}" : ''),
                            'url' => route('file.download', ['code'=>$code]),
                            'original_url' => Storage::url($file->name),
                            'image' => in_array($ext, ['jpg', 'jpeg', 'png', 'gif', 'jfif']) ? 'images' : 'files',
                        ];

                        $comments = json_decode(@$file->formData->dataVarcharRelation->first(function ($item) {
                            return $item->field_id == null;
                        })->value);

                        $fileData['comments'] = @$comments->{(string)$file->id};

                        switch ($ext) {
                            case 'mp4':
                                $fileData['url'] = $file->video_url ? $file->video_url : $fileData['url'];
                                $fileData['type'] = 'file';
                                break;
                            case 'xls':
                            case 'xlsx':
                                $fileData['type'] = 'file-excel';
                                break;
                            case 'doc':
                            case 'docx':
                                $fileData['type'] = 'file-word';
                                break;
                            case 'pdf':
                                $fileData['type'] = 'file-pdf';
                                break;
                            case 'jfif':
                            case 'jpg':
                            case 'jpeg':
                            case 'png':
                                $fileData['type'] = 'file-image';
                                break;
                            default:
                                $fileData['type'] = 'file';
                                break;
                        }

                        $files->push((object) $fileData);
                    }

                    $values['all'] = $files->toArray();
                    $values = array_merge($values, $files->groupBy('image')->toArray());
                } else {
                    $values = [];
                }

                $value = $values;
                break;

            case 'text':
            case 'textarea':
            case 'phone':
            case 'number':
            case 'hidden':
                $value=$value;

                break;

            case 'autocompleteForm':
                /*if(!isset($this->autocompleteForms[($this->id.$value)])) {
                    $_value = $this->getAutocompleteForm($value);
                    $this->autocompleteForms[($this->id.$value)] = $_value;
                    $value = $_value;
                } else {
                    $value = $this->autocompleteForms[($this->id.$value)];
                }*/

                $_value = null;

               //foreach($forms as $form) {
                    if(!$this->dataNumberRelation->isEmpty()) {
                        foreach($this->dataNumberRelation as $item) {
                            if(!$item->autocompleteForms->isEmpty()) {
                                if($value == 42 || $value == 50) {
                                   // dd($item);
                                }

                                if($value == 42 || $value == 50) {
                                   // dd($value);
                                }

                                $_value = @$item->autocompleteForms->where('id', $value)->first();

                                if($_value) {
                                    $_value->getCurrentStatus()->filterFields('read');
                                    break;
                                }
                            }
                        }
                    }
                //}

                $value = $_value;

                if($value) {
                    $value->name = !empty($value->type->map[0]->value->address) ? $value->type->map[0]->value->address : (!empty($value->type->text[0]->value) ? $value->type->text[0]->value : 'Форма');
                }

                break;

            case 'selectTable':
                $options = (array) json_decode($field->options);
                $table = current($options);

                if(Schema::hasTable($table)) {
                    $selectTable = new FormSelectTable($table);
                    $item = $selectTable->where('code', $value)->first();

                    if($item) {
                        $item->getImage($table);

                        if ($image = @$item->image) {
                            $item->image = $item->image->path;
                        }

                        $value = (object) $item->toArray();
                    }
                }

                break;
            case 'select':
            case 'radio':
                if($field->type == 'select' && $field->code == 'procuring-entity') {
                    $v = array_first($field->form->domain->companies, function($item) use($value) {
                        return $item['code'] == $value;
                    });
                    $value = $v['name'];
                }
                else {
                    $options = json_decode($field->options);

                    $value = array_first($options, function ($option) use ($value) {
                        return $option->code == $value;
                    });
                }

                break;
            case 'checkbox':
                $options=json_decode($field->options);
                $values=json_decode($value);
                $out=[];

                if(!empty($values) && is_array($values)) {
                    foreach ($values as $item) {
                        array_push($out, array_first($options, function ($option) use ($item) {
                            return $option->code == $item;
                        }));
                    }
                }
                $value=$out;

                break;

            case 'map':
                $value=json_decode($value);

                break;

            case 'switch':
                $value=(boolean) $value;
                $value=$value?'Да':'Нет';

                break;
        }

        return $value;
    }

    private $closestMarkers=[];

    public function closest()
    {
        if($geo=@$this->type->map[0]->value) {
            $markers=DB::table('form_data_varchar')->select('form_data.id as id', 'form_data_varchar.value as geo')
                ->leftJoin('form_data', 'form_data.id', '=', 'form_data_varchar.form_id')
                ->leftJoin('perevorot_forms_fields', 'perevorot_forms_fields.id', '=', 'form_data_varchar.field_id')
                ->leftJoin('perevorot_forms_forms', 'perevorot_forms_forms.id', '=', 'form_data.form_id')
                ->whereIn('perevorot_forms_forms.code', ['competitor', 'object'])
                ->where('perevorot_forms_fields.type', '=', 'map')
                ->where('form_data.id', '!=', $this->id)
                ->get();

            $markers->each(function($marker) use($geo) {
                $marker_geo=json_decode($marker->geo);

                $marker->distance=$this->codexworldGetDistanceOpt($geo->lat, $geo->lng, $marker_geo->lat, $marker_geo->lng)*1000;
            });

            $markers=$markers->filter(function ($marker) {
                return $marker->distance <= 320;
            });
            
            $markerIds=$markers->pluck('id');

            $distances=FormDistance::where('form_data_id', $this->id)->get();

            if(!$markers->isEmpty()) {
                $this->closestMarkers=FormData::whereIn('id', $markerIds)->with('form')->get();

                $this->closestMarkers->each(function($marker) use($markers, $distances) {
                    $distance=$markers->firstWhere('id', $marker->id);
                    $marker->distance=!empty($distance->distance) ? (int) ceil($distance->distance) : 0;
                    
                    $distance_map=$distances->firstWhere('form_data_parent_id', $marker->id);

                    $marker->distance_walk=!empty($distance_map->distance) ? $distance_map->distance : 0;
                    $marker->duration=!empty($distance_map->duration) ? $distance_map->duration : 0;
                });

                $this->closestMarkers=$this->closestMarkers->sortBy('distance_walk');
            }
        }

        return $this->closestMarkers;
    }

    private $apartmentsByZone=[];

    public function apartmentsByZone()
    {
        if($this->apartmentsByZone){
            return $this->apartmentsByZone;
        }

        if($geo=@$this->type->map[0]->value) {
            $markers=DB::table('form_data_varchar')->select('form_data.id as id', 'form_data_varchar.value as geo')
                ->leftJoin('form_data', 'form_data.id', '=', 'form_data_varchar.form_id')
                ->leftJoin('perevorot_forms_fields', 'perevorot_forms_fields.id', '=', 'form_data_varchar.field_id')
                ->leftJoin('perevorot_forms_forms', 'perevorot_forms_forms.id', '=', 'form_data.form_id')
                ->whereIn('perevorot_forms_forms.code', ['house'])
                ->where('perevorot_forms_fields.type', '=', 'map')
                ->where('form_data.id', '!=', $this->id)
                ->get();

            $markers->each(function($marker) use($geo) {
                $marker_geo=json_decode($marker->geo);

                $marker->distance=$this->codexworldGetDistanceOpt($geo->lat, $geo->lng, $marker_geo->lat, $marker_geo->lng)*1000;
            });

            $markers=$markers->filter(function ($marker) {
                return $marker->distance <= 320;
            });
            
            $markerIds=$markers->pluck('id');
            
            $distances=FormDistance::whereIn('form_data_id', $markerIds)->get();

            if(!$markers->isEmpty()) {
                $id = $this->id;

                $houses=FormData::whereIn('id', $markerIds)->with(['form','zones'=>function($q) use($id) {
                    $q->where('object_id', $id);
                }])->get();

                $apartmentsByZone=[
                    1=>0,
                    2=>0,
                    3=>0,
                    4=>0
                ];

                foreach($houses as $house) {
                    foreach($house->zones as $zone) {
                        $zone = (int)$zone->value;
                        $apartmentsByZone[$zone] += (int)@$house->data->apartments->value;
                    }
                }
            }
            
            $this->apartmentsByZone=@$apartmentsByZone;
        }

        return $this->apartmentsByZone; 
    }

    private function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $rad = M_PI / 180;

        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin($latitudeFrom * $rad) 
            * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
            * cos($latitudeTo * $rad) * cos($theta * $rad);

        return acos($dist) / $rad * 60 *  1.853;
    }

    public function accessByStatus()
    {
        $user = Auth::user();
        $status = $this->form->statuses->where('code', $this->currentStatusCode)->first();

        if($status && !$status->groupsRead->isEmpty()) {
            $userGroups = array_column($user->groups->toArray(), 'id');
            $statusGroups = array_column($status->groupsRead->toArray(), 'id');
            $diff = array_diff($userGroups, $statusGroups);
            $result = !(count($diff) != count($userGroups));

            return $result == true ? $this : false;
        }

        return $this;
    }

    public function filteredStatuses()
    {
        $groups = Auth::user()->groups->pluck('id','id');

        foreach($this->form->statuses as $sk => $status) {
            if(!$status->groups->isEmpty() && !$status->groups->whereIn('id', $groups)->count()) {
                unset($this->form->statuses[$sk]);
            }
        }

        return $this;
    }

    public function getCurrentStatus()
    {
        if(!$this->logs->isEmpty()) {
            $this->currentStatus = $this->form->statuses->where('code', @$this->logs->first()->status_code)->first();
            $this->currentStatusDate = $this->logs->first()->created_at;
            $this->currentStatusName = $this->logs->first()->status;
            $this->currentStatusCode = $this->logs->first()->status_code;
            $this->currentStatusColor = $this->logs->first()->color;
        }
        elseif(!$this->form->statuses->isEmpty()) {
            $this->currentStatus = $this->form->statuses->first();
            $this->currentStatusName = $this->form->statuses->first()->name;
            $this->currentStatusCode = $this->form->statuses->first()->code;
            $this->currentStatusColor = $this->form->statuses->first()->color;
        }

        return $this;
    }

    public function filterFields($context) {
        $this->form->fields = $this->form->filterFields($context, $this->getAttribute('currentStatus'));
        return $this;
    }

    public function canEdit($fieldName, $onlyField = false)
    {
        if(!empty(Settings::instance()->createByAnonim)) {
            return false;
        }

        $fields = $this->getCurrentStatus()->filterFields('edit')->form->fields;
        $issetField = $fields->where('code', kebab_case($fieldName))->first();

        if(isset($this->form->canEdit)) {
            if($onlyField) {
                return !empty($issetField);
            } else {
                return $this->form->canEdit && !empty($issetField);
            }
        } else {
            if($onlyField) {
                return !empty($issetField);
            } else {
                return $this->form->editable() && !empty($issetField);
            }
        }
    }

    public function getAutocompleteForm($value)
    {
        $form = self::withDataRelations()
            ->where('id', $value)
            ->first();

        //dd($value, $form);

        return !$form ? null : $form->getCurrentStatus()->filterFields('read');
    }

    public function getAutocompleteForms()
    {
        $rows = DB::table('form_data_number')->select('form_data.id as id', 'form_data_number.value as autocompleteId')
            ->leftJoin('form_data', 'form_data.id', '=', 'form_data_number.form_id')
            ->leftJoin('perevorot_forms_fields', 'perevorot_forms_fields.id', '=', 'form_data_number.field_id')
            ->leftJoin('perevorot_forms_forms', 'perevorot_forms_forms.id', '=', 'form_data.form_id')
            ->where('perevorot_forms_forms.code', $this->form->code)
            ->where('perevorot_forms_fields.type', '=', 'autocompleteForm')
            ->where('form_data.id', $this->id)
            ->get();

        return $rows;
    }

    public function getAutocompleteFormsData()
    {
        $autocompleteForms = $this->getAutocompleteForms();

        if(!$autocompleteForms->isEmpty()) {
            return FormData::withDataRelations()->byUser($this->form->by_user_group)->whereIn('id', array_column($autocompleteForms->toArray(), 'autocompleteId'))->orderBy('created_at', 'DESC')->get();
        }

        return new Collection([]);
    }

    public function getTitle()
    {
        if(!empty($this->form->parent_form)) {

            $field = $this->form->parentForm->fields->where('is_title', true)->first();
            $childrenField = $this->form->fields->where('is_title', true)->first();

            if($field && !empty($this->data->parentFormObjectId->value)) {
                $FormData = FormData::withDataRelations()->find($this->data->parentFormObjectId->value);

                if($field->type == 'autocompleteForm' && !empty($FormData->data->{camel_case(str_replace('-', '_', $field->code))}->value)) {
                    $_field = $FormData->data->{camel_case(str_replace('-', '_', $field->code))}->value->form->fields->where('is_title', 1)->first();
                    $value = @$FormData->data->{camel_case(str_replace('-', '_', $field->code))}->value->data->{camel_case(str_replace('-', '_', $_field->code))}->value;
                } else {
                    $value = @$FormData->data->{camel_case(str_replace('-', '_', $field->code))}->value;
                }

                if(!empty($value)) {
                    $main = is_object($value) ? @$value->name : $value;
                    $main = mb_strlen($main) > 30 ? (mb_substr($main, 0, 30).'...') : $main;

                    if($childrenField) {
                        if($childrenField->type == 'autocompleteForm' && !empty($this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value)) {
                            $_field = @$this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value->form->fields->where('is_title', 1)->first();
                            $children = @$this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value->data->{camel_case(str_replace('-', '_', $_field->code))}->value;
                            //dd($_field, $children);
                        } else {
                            $children = @$this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value;
                        }

                        $children = is_object($children) ? @$children->name : $children;
                        $children = mb_strlen($children) > 30 ? (mb_substr($children, 0, 30) . '...') : $children;
                    } else {
                        $children = '';
                    }

                    return $main.' / '.$children;
                }
            } else {
                if($childrenField) {
                    if($childrenField->type == 'autocompleteForm' && !empty($this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value)) {
                        $_field = @$this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value->form->fields->where('is_title', 1)->first();
                        $children = @$this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value->data->{camel_case(str_replace('-', '_', $_field->code))}->value;
                        //dd($_field, $children);
                    } else {
                        $children = @$this->data->{camel_case(str_replace('-', '_', $childrenField->code))}->value;
                    }

                    $children = is_object($children) ? @$children->name : $children;
                    $children = mb_strlen($children) > 30 ? (mb_substr($children, 0, 30) . '...') : $children;

                    return $children;
                }
            }
        }

        if(!empty($this->data->addressManual->value)) {
             return $this->data->addressManual->value;
        }

        $name = !empty($this->type->map[0]->value->address) ? $this->type->map[0]->value->address : (!empty($this->type->text[0]->value) ? $this->type->text[0]->value : $this->form->name);

        $fieldsForTitle = $this->form->fields->where('is_for_title', 1);

        foreach($fieldsForTitle as $item) {
            if(!empty($this->data->{camel_case(str_replace('-', '_', $item->code))})) {
                $name .= ' ' . $this->data->{camel_case(str_replace('-', '_', $item->code))}->value;
            }
        }

        return $name;
    }

    public function getHistory()
    {
        $history = new Collection([]);

        foreach($this->types as $type => $fields) {
            foreach($fields as $field) {
                if($field->inHistory) {
                    $history->push($field);
                }
            }
        }

        if(!$this->logs->isEmpty()) {
            foreach($this->logs as $log) {
                $value = new \stdClass();
                $value->value = $log->status;
                $value->updated_at = $log->created_at->format('Y-m-d H:i:s');
                $value->fieldName = 'Статус';
                $value->user_name = $log->user_name;
                $history->push($value);
            }
        }

        $history = $history->sortByDesc('updated_at');

        return $history;
    }

    public function logsHasSubstatus()
    {
        return $this->logs->where('substatus', '<>', null)->count();
    }
}
