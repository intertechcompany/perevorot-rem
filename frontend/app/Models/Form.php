<?php

namespace App\Models;

use App\Scopes\DomainScope;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class Form extends \LaravelOctoberModel
{
    protected $table = 'perevorot_forms_forms';
    public $backendModel='Perevorot\Forms\Models\Form';
    public $similar;
    public $casts = ['by_user_group' => 'array'];

    protected static function boot()
    {
        parent::boot();

        $r = app(Request::class);
        $user = Auth::user();

        if(!$user || ($user && !$user->isSuperAdmin())) {
            static::addGlobalScope('domain', function (Builder $builder) use ($r) {
                $builder->where(function ($q) use ($r) {
                    $q->DoesntHave('domain')->orWhereHas('domain', function ($q) use ($r) {
                        $q->where('domain', $r->header('host'));
                    });
                });
            });
        }
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    public function formStatusesEdit()
    {
        return $this->belongsToMany(FormStatus::class, 'perevorot_forms_form_to_form_statuses', 'form_id', 'form_status_id')->isEdit();
    }

    public function formStatusesRead()
    {
        return $this->belongsToMany(FormStatus::class, 'perevorot_forms_form_to_form_statuses', 'form_id', 'form_status_id')->isRead();
    }

    public function userGroupsReadPersonal()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_form_to_user_groups', 'form_id', 'user_group_id')->isReadPersonal();
    }

    public function userGroupsRead()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_form_to_user_groups', 'form_id', 'user_group_id')->isRead();
    }

    public function userGroupsEdit()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_form_to_user_groups', 'form_id', 'user_group_id')->isEdit();
    }

    public function userGroupsCreate()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_form_to_user_groups', 'form_id', 'user_group_id')->isCreate();
    }

    public function userGroupsFileRead()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_form_to_user_groups', 'form_id', 'user_group_id')->isFileRead();
    }

    public function userGroupsFileEdit()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_form_to_user_groups', 'form_id', 'user_group_id')->isFileEdit();
    }

    public function formData()
    {
        return $this->hasOne(FormData::class);
    }

    public function forms()
    {
        return $this->hasMany(FormData::class, 'form_id');
    }

    public function childrenForm()
    {
        return $this->hasOne(Form::class, 'parent_form', 'id');
    }

    public function childrenForms()
    {
        return $this->hasMany(Form::class, 'parent_form', 'id');
    }

    public function parentForm()
    {
        return $this->hasOne(Form::class, 'id', 'parent_form');
    }

    public function relationForm()
    {
        return $this->hasOne(Form::class, 'id', 'relation_form');
    }

    public function statuses()
    {
        return $this->hasMany('App\Models\FormStatus');
    }

    public function fields()
    {
        return $this->hasMany('App\Models\Field')->enabled();
    }

    public function scopeByDomain($q, $value)
    {
        if($value && is_string($value)) {
            $q->withoutGlobalScope('domain')->whereHas('domain', function($q) use($value) {
                $q->where('domain', $value);
            });
        }
        elseif(!empty($value)) {
            $q->withoutGlobalScope('domain');
        }
    }

    public function scopeByCode($q, $code)
    {
        $q->where('code', $code);
    }

    public function scopeEnabled($q, $v = true)
    {
        $q->where('is_enabled', $v);
    }

    public function scopeIsCreate($q, $v = true)
    {
        $q->where('is_create', $v);
    }

    public function scopeMenu($q)
    {
        $q->where('is_hide', 0);
    }

    public function getDomain()
    {
        if(!empty($this->domain)) {
            $t = explode('.', $this->domain->domain);
            unset($t[0]);

            return implode('.', $t);
        }
        else {
            return '';
        }
    }

    public function editable($file = '')
    {
        $user = Auth::user();

        if($user && $user->isSuperAdmin()) {
            $this->canEdit = true;
        } else {
            $this->canEdit = false;
            $relation = "userGroups{$file}Edit";

            if ($user && !$this->$relation->isEmpty()) {
                $user->load('groups');
                $res = $this->$relation()->whereIn('user_group_id', array_column($user->groups->toArray(), 'id'))->count();

                if ($res) {
                    $this->canEdit = true;
                }
            } elseif ($user && $this->$relation->isEmpty()) {
                $this->canEdit = true;
            }
        }

        return $this->canEdit;
    }

    public function can($permission = '')
    {
        $user = Auth::user();

        if($user && $user->isSuperAdmin()) {
            return true;
        }

        $relation = "userGroups".studly_case($permission);

        if($user && !$this->$relation->isEmpty()) {
            $user->load('groups');
            $res = $this->$relation()->whereIn('user_group_id', array_column($user->groups->toArray(), 'id'))->count();
            return $res;
        }
        elseif($user && $this->$relation->isEmpty()) {
            return true;
        }

        return false;
    }

    public function scopeContext($q, $contexts)
    {
        if(!is_array($contexts)) {
            $contexts = [$contexts];
        }

        $user = Auth::getUser();

        if ($user && !$user->isSuperAdmin()) {
            $user->load('groups');

            foreach ($contexts as $ck => $context) {
                $relation = 'userGroups' . studly_case($context);

                $q->whereHas($relation, function ($q2) use ($user) {
                    if (!$user->groups->isEmpty()) {
                        $q2->whereIn('user_group_id', array_column($user->groups->toArray(), 'id'));
                    }
                })->orDoesntHave($relation);
            }
        }
    }

    public function filterStatuses()
    {
        $user = Auth::user();
        //$status = $this->form->statuses->where('code', $this->currentStatusCode)->first();

        foreach($this->statuses as $k => $status) {
            if ($status && !$status->groupsRead->isEmpty()) {
                $userGroups = array_column($user->groups->toArray(), 'id');
                $statusGroups = array_column($status->groupsRead->toArray(), 'id');
                $diff = array_diff($userGroups, $statusGroups);
                $result = !(count($diff) != count($userGroups));

                if(!$result) {
                    unset($this->statuses[$k]);
                }

                //return $result == true ? $this : false;
            }
        }

        return $this;
    }
    
    public function filterFields($context, $currentStatus = null)
    {
        $user = Auth::user();

        if($user) {

            if($user->isSuperAdmin()) {
                return $this->fields;
            }

            // желательно оптимизоровать
            $user->load('groups');

            $this->fields = $this->fields->filter(function ($field, $key) use ($user, $context) {
                $relation = "userGroups".studly_case($context);

                if(!$field->$relation->isEmpty()) {
                    $userGroups = array_column($user->groups->toArray(), 'id');
                    $fieldGroups = array_column($field->$relation->toArray(), 'id');
                    $diff = array_diff($userGroups, $fieldGroups);
                    return count($diff) != count($userGroups);
                }

                return true;
            });
        }

        if($currentStatus) {
            $this->fields = $this->fields->filter(function ($field, $key) use ($currentStatus, $context) {
                $relation = "formStatuses" . studly_case($context);

                if (!$field->$relation->isEmpty()) {
                    $fieldStatuses = array_column($field->$relation->toArray(), 'id');
                    return in_array($currentStatus->id, $fieldStatuses);
                }

                return true;
            });
        }

        return $this->fields;
    }

    public function getFieldsForTable()
    {
        return array_column($this->fields->filter(function($v, $k) {
            return !in_array($v->type, ['hidden','file','map','accordion','autocompleteForm']);
        })->toArray(), 'code');
    }

    public function generateBreads($formData = null)
    {
        $r = Route::current();
        $p = $r->parameters();
        $action = $r->getAction('as');
        $breads = [];

        if(!$this->parent_form) {
            array_push($breads, [
                'url' => route('form.list', ['code'=>$p['code']]),
                'text' => 'Список'
            ]);

            if($action == 'form.add') {
                array_push($breads, [
                    'text' => 'Добавление'
                ]);
            }
            elseif($action == 'form.edit') {
                array_push($breads, [
                    'url' => route('form.show', ['code' => $this->code, 'id' => $p['id']]),
                    'text' => $this->name
                ]);
                array_push($breads, [
                    'text' => 'Редактирование'
                ]);
            }
            else {
                array_push($breads, [
                    'text' => $this->name
                ]);
            }
        } else {
            if($this->parentForm->can('read')) {
                array_push($breads, [
                    'url' => route('form.list', ['code' => $this->parentForm->code]),
                    'text' => 'Список'
                ]);
            }

            if(!empty($formData->data->parentFormObjectId->value) && $this->parentForm->can('read')) {
                array_push($breads, [
                    'url' => route('form.show', ['code' => $this->parentForm->code, 'id' => $formData->data->parentFormObjectId->value]),
                    'text' => $this->parentForm->name
                ]);
            }

            if($action == 'form.add' && @$p['id']) {
                if($this->parentForm->can('read')) {
                    array_push($breads, [
                        'url' => route('form.show', ['code' => $this->parentForm->code, 'id' => $p['id']]),
                        'text' => $this->parentForm->name
                    ]);
                }
                array_push($breads, [
                    'text' => 'Добавление'
                ]);
            }
            elseif($action == 'form.edit' && @$p['id']) {
                array_push($breads, [
                    'url' => route('form.show', ['code' => $this->code, 'id' => $p['id']]),
                    'text' => $this->name
                ]);
                array_push($breads, [
                    'text' => 'Редактирование'
                ]);
            }
            else {
                if(!empty($formData->data->parentFormObjectId->value) && $this->parentForm->can('read')) {
                    array_push($breads, [
                        'url' => route('form.show', ['code' => $this->parentForm->code, 'id' => $formData->data->parentFormObjectId->value, 'tab' => $this->code]),
                        'text' => $this->name
                    ]);
                } else {
                    array_push($breads, [
                        'text' => $this->name
                    ]);
                }
            }
        }

        return $breads;
    }

    public function getAutocompleteAttribute()
    {
        return $this->fields->where('type', 'autocompleteForm')->first();
    }

    public function nameByType()
    {
        switch ($this->is_company_field) {
            case 1:
                return 'Закупка';
                break;
            case 2:
                return 'Предложения';
                break;
            case 3:
                return 'Oбъявления';
                break;
        }
    }
}
