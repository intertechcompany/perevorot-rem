<?php

namespace App\Models;

class FormDataZone extends \LaravelOctoberModel
{
    protected $table = 'form_objects_house_zones';
    public $timestamps = false;
    public $backendModel='Perevorot\Forms\Models\FormDataZone';
    protected $fillable = ['object_id','house_id','value'];
}
