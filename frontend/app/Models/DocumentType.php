<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends \LaravelOctoberModel
{
    public $timestamps = false;
    protected $table = 'perevorot_forms_document_types';
    public $backendModel='Perevorot\Forms\Models\DocumentType';
}
