<?php

namespace App\Models;

class Company extends \LaravelOctoberModel
{
    protected $table = 'perevorot_forms_companies';
    public $timestamps = false;
    public $backendModel='Perevorot\Forms\Models\Company';
}
