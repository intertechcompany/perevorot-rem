<?php

namespace App\Models;

class Calc extends \LaravelOctoberModel
{
    protected $table = 'perevorot_forms_calc_inputs';
    public $timestamps = false;
    public $backendModel='Perevorot\Forms\Models\Calc';
    public $dates = ['created_at'];
    public $casts = ['json' => 'array'];

    public function formData()
    {
        return $this->belongsTo(FormData::class);
    }
}
