<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormDataHistory extends Model
{
    public $timestamps = false;
    protected $dates = ['created_at'];
    protected $table = 'form_data_history_summary';

    public function formData()
    {
        return $this->belongsTo(FormData::class, 'form_id', 'id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
