<?php

namespace App\Models;

class UserGroup extends \LaravelOctoberModel
{
    use \ModelTrait;

    public $table = 'user_groups';
    public $backendModel='Perevorot\Users\Models\UserGroup';

    protected $casts = [
        'permissions' => 'array'
    ];

    public function scopeIsFileEdit($q)
    {
        return $q->where('permission', 'file_edit');
    }

    public function scopeIsFileRead($q)
    {
        return $q->where('permission', 'file_read');
    }

    public function scopeIsEdit($q)
    {
        return $q->where('permission', 'edit');
    }

    public function scopeIsRead($q)
    {
        return $q->where('permission', 'read');
    }

    public function scopeIsCreate($q)
    {
        return $q->where('permission', 'create');
    }

    public function scopeIsReadPersonal($q)
    {
        return $q->where('permission', 'read_p');
    }
}
