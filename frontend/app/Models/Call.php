<?php

namespace App\Models;

class Call extends \LaravelOctoberModel
{
    protected $table = 'perevorot_forms_calls';
    public $timestamps = false;
    public $backendModel='Perevorot\Forms\Models\Call';
    public $dates = ['created_at'];
}
