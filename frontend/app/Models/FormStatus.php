<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class FormStatus extends \LaravelOctoberModel
{
    public $timestamps = false;
    protected $dates = ['created_at'];
    protected $table = 'perevorot_forms_statuses';
    public $backendModel='Perevorot\Forms\Models\FormStatus';
    public $similar;
    protected $casts = ['dependents' => 'array'];

    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id', 'id');
    }

    public function groupsRead()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_form_status_to_user_group_read', 'form_status_id', 'user_group_id');
    }

    public function groups()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_form_status_to_user_group', 'form_status_id', 'user_group_id');
    }

    public function scopeIsEdit($q)
    {
        return $q->where('permission', 'edit');
    }

    public function scopeIsRead($q)
    {
        return $q->where('permission', 'read');
    }

    public function getChildren()
    {
        $statuses = $this->getDependents();
        $data = [];

        foreach($statuses as $status) {
            if(!empty($status->children)) {
                $children = array_filter(explode("\n", trim($status->children)."\n"));

                foreach($children as $item) {
                    $item = explode("=", $item);
                    $data[$status->code][] = [
                        'code' => trim($item[0]),
                        'name' => trim($item[1])
                    ];
                }
            }
        }

        return $data;
    }

    public function getDependents()
    {
        if(Auth::check()) {
            $groups = Auth::user()->groups->pluck('id', 'id');

            if(empty($this->dependents)) {
                return new Collection([]);
            }

            return self::where('form_id', $this->form_id)->whereIn('id', $this->dependents)->where(function($q2) use($groups) {
                $q2->whereHas('groups', function($q) use($groups) {
                    $q->whereIn('id', $groups);
                })->orDoesntHave('groups');
            })->get();
        }else{
            return self::get();
        }
    }
}
