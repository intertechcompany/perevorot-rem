<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Models\SystemFile;

class FormSelectTable extends \LaravelOctoberModel
{
    protected $table = '';

    public $backendModel='';
    public $similar;

    protected $attachments = ['image'];

    public function __construct($table)
    {
        if(!empty($table)) {
            $this->setSettings($table);
        }
    }

    public function getImage($table)
    {
        if(!empty($table)) {
            $this->setSettings($table);
            $this->image = SystemFile::where('field', 'image')->where('attachment_id', $this->id)->where('attachment_type', $this->backendModel)->where('is_public', 1)->orderBy('sort_order', 'ASC')->first();
        }
    }

    public function setSettings($table)
    {
        $this->table = $table;

        $name = explode('_', $table);
        $name = str_singular(ucfirst(end($name)));

        $this->backendModel = 'Perevorot\Forms\Models\FieldSelect' . $name;
    }
}
