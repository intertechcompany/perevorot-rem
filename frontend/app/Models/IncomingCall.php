<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncomingCall extends Model
{
    protected $table = 'perevorot_forms_incoming_calls';
    public $timestamps = true;
    public $dates = ['created_at','updated_at','call_time'];
}
