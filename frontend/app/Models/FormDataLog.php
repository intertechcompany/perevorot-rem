<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormDataLog extends \LaravelOctoberModel
{
    public $timestamps = false;
    protected $dates = ['created_at'];
    protected $table = 'form_data_logs';
    public $fillable = ['form_data_id','created_at','status','comment','user_name'];

    public function formData()
    {
        return $this->belongsTo(FormData::class, 'form_data_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function showSubStatus()
    {
        $status = $this->formData->form->statuses->where('code', $this->status_code)->first();
        $children = array_filter(explode("\n", trim($status->children)."\n"));

        if(!empty($children)) {
            $children = array_first($children, function ($v) {
                return starts_with($v, $this->substatus.'=');
            });

            $children = $children ? trim(strtr($children, [($this->substatus.'=')=>''])) : '';
        } else {
            $children = '';
        }

        return $children;
    }
}
