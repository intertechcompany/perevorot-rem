<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class FormDataBoolean extends \LaravelOctoberModel
{
    use SoftDeletes;

    public $timestamps = false;
    protected $dates = ['update_at', 'deleted_at'];
    protected $table = 'form_data_boolean';
    public $backendModel='Perevorot\Forms\Models\FormDataBoolean';

    public function formData()
    {
        return $this->belongsTo(FormData::class, 'form_id', 'id');
    }

    public function field()
    {
        return $this->belongsTo(Field::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
