<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends \LaravelOctoberModel
{
    use SoftDeletes;

    protected $table = 'perevorot_calendar_non_business';
    public $timestamps = false;
    public $backendModel='Perevorot\Forms\Models\Calendar';
    public $dates = ['created_at','deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
