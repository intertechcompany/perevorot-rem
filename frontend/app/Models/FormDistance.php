<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormDistance extends Model
{
    public $timestamps = false;
    protected $table = 'form_distances';
    public $fillable = ['form_data_id','form_data_parent_id','distance'];

    public function formData()
    {
        return $this->belongsTo(FormData::class, 'form_data_id', 'id');
    }
}
