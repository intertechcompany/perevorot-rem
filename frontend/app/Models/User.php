<?php

namespace App\Models;

use App\Models\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Scopes\DomainScope;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;
    
    protected $casts = [
        'permissions' => 'array'
    ];

    protected $permissions;

    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'social_id',
        'avatar',
        'is_activated',
        'last_seen',
        'is_social_reg',
        'domain'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new DomainScope);
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    public function groups()
    {
        return $this->belongsToMany(UserGroup::class, 'users_groups');
    }

    public function isSuperAdmin()
    {
        $r = app(Request::class);

        if($this->is_superadmin && env('SUPER_DOMEN') == $r->header('host')) {
            return true;
        }

        return false;
    }

    public function accessToCall()
    {
        $group = $this->groups()->where('is_call', 1);

        if($group && !empty($this->binotel_number)) {
            return true;
        } else {
            return false;
        }
    }

    public function accessToModeration()
    {
        if($this->groups()->where('is_moderation', 1)->count()) {
            return true;
        } else {
            return false;
        }
    }

    public function accessToFilter()
    {
        if($this->groups()->where('is_filter', 1)->count()) {
            return true;
        } else {
            return false;
        }
    }

    public function accessToHistory()
    {
        if($this->groups()->where('is_history', 1)->count()) {
            return true;
        } else {
            return false;
        }
    }

    public function isAdmin()
    {
        return $this->groups->where('code', 'administrator')->count();
    }

    public function getRedirectUrl()
    {
        $landing = $this->groups()->whereNotNull('landing_page')->where('landing_page', '<>', '')->first();
        $showLand = $landing && $landing->landing_page && stripos($landing->landing_page, $this->domain->domain) !== false && !$this->visited_landing_page;

        if($showLand) {
            $landing->landing_page = stripos($landing->landing_page, 'http') === false ? env('APP_URL') . $landing->landing_page : $landing->landing_page;
        }

        return $showLand ? $landing->landing_page : '/';
    }
    
    public function hasPermission($permission)
    {
        $this->setPermissions();

        return !empty($this->permissions[$permission]);
    }

    public function setPermissions()
    {
        if(is_null($this->permissions)) {
            $permissions = $this->groups->pluck('permissions')->toArray();
            $permissions = array_filter($permissions);

            if(!is_array($this->permissions)) {
                $this->permissions = [];
            }

            if(!empty($permissions)) {
                foreach($permissions as $permissionAr) {
                    if(!empty($this->permissions)) {
                        $diff = array_diff_assoc($permissionAr, $this->permissions);
                    } else {
                        $diff = $permissionAr;
                    }

                    if(!empty($diff)) {
                        $this->permissions = array_merge($this->permissions, $diff);
                    }
                }
            }

            $this->permissions = array_map(function($v) {
                return $v == 1 ? true : false;
            }, $this->permissions);
        }
    }
}
