<?php

namespace App\Models;

class Domain extends \LaravelOctoberModel
{
    protected $table = 'perevorot_forms_domains';
    public $timestamps = false;
    public $backendModel='Perevorot\Forms\Models\Domain';

    public function companies()
    {
        return $this->hasMany(Company::class);
    }
}
