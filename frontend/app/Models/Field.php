<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

class Field extends \LaravelOctoberModel
{
    protected $table = 'perevorot_forms_fields';
    public $backendModel='Perevorot\Forms\Models\Field';

    public function formStatusesEdit()
    {
        return $this->belongsToMany(FormStatus::class, 'perevorot_form_fields_to_form_statuses', 'form_field_id', 'form_status_id')->isEdit();
    }

    public function formStatusesRead()
    {
        return $this->belongsToMany(FormStatus::class, 'perevorot_form_fields_to_form_statuses', 'form_field_id', 'form_status_id')->isRead();
    }

    public function userGroupsRead()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_field_to_user_group', 'form_field_id', 'user_group_id')->isRead();
    }

    public function userGroupsEdit()
    {
        return $this->belongsToMany(UserGroup::class, 'perevorot_forms_field_to_user_group', 'form_field_id', 'user_group_id')->isEdit();
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }
    
    public function relatedField()
    {
        return $this->hasOne('App\Models\Field', 'id', 'parent_field_id');
    }

    public function scopeContext($q, $context, $formStatus = null)
    {
        $relation = 'userGroups'.studly_case($context);

        if($user = Auth::user()) {
            $user->load('groups');
        }

        if(!$user->isSuperAdmin()) {
            $q->whereHas($relation, function($q3) use($user) {
                if($user && !$user->groups->isEmpty()) {
                    $q3->whereIn('user_group_id', array_column($user->groups->toArray(), 'id'));
                }
            })->orDoesntHave($relation);
        }
    }

    public function scopeByCode($q, $code)
    {
        if(!is_array($code)) {
            $code = [$code];
        }

        return $q->whereIn('code', $code);
    }

    public function scopeEnabled($q)
    {
        return $q->where('is_enabled', true)->with('relatedField')->orderBy('sort_order', 'asc');
    }
    
    public function getOptionsValueAttribute()
    {
        return @json_decode($this->options, true)[0];
    }
}
