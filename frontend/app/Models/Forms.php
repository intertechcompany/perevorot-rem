<?php

namespace App\Models;

use Perevorotcom\LaravelOctober\Models\SystemSetting;

class Forms extends SystemSetting
{
    public $instance='forms';

    public $backendModel='Perevorot\Settings\Models\Form';
}
