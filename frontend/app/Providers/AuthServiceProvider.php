<?php

namespace App\Providers;

use App\Models\UserGroup;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    // protected $policies = [
    //     'App\Model' => 'App\Policies\ModelPolicy',
    // ];

    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        foreach ($this->getPermissions() as $permission) {
            $gate->define($permission, function($user) use ($permission) {
                return $user->hasPermission($permission);
            });
        }
    }

    protected function getPermissions()
    {
        $groups=UserGroup::get();
        $permissions=[];

        foreach($groups as $group) {
            if(!empty($group->permissions)) {
                foreach($group->permissions as $permission=>$value) {
                    $permissions[$permission]=true;
                }
            }
        };

        return array_keys($permissions);
    }
}
