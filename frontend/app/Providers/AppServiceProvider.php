<?php

namespace App\Providers;

use App\Models\FormData;
use App\Models\FormDataLog;
use App\Models\FormFile;
use App\Observers\FormDataLogObserver;
use App\Observers\FormDataObserver;
use App\Observers\FormFileObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        FormData::observe(FormDataObserver::class);
        FormDataLog::observe(FormDataLogObserver::class);
        FormFile::observe(FormFileObserver::class);
    }

    public function register()
    {
        
    }
}
