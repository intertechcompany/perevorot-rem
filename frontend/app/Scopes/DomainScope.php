<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DomainScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $r = app(Request::class);

        $builder->where(function($q) use($r) {
            $q->where('is_superadmin', true);
        })
        ->orWhere(function($q) use($r) {
            $q->WhereHas('domain', function($q) use($r) {
                $q->where('domain', $r->header('host'));
            })->where('is_superadmin', false);
        });
    }
}