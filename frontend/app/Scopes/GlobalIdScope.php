<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Settings;
use Illuminate\Http\Request;
use Cache;

class GlobalIdScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $isGlobalId=Cache::rememberForever('isGlobalId', function() {
            $settings=Settings::instance();
            
            return !empty($settings->isGlobalId);
        });
                
        if($isGlobalId) {
            // $exists=array_first($builder->getQuery()->wheres, function($w) {
            //     return !empty($w['column']) && strpos($w['column'], 'global_id') !== false;
            // });
            
            $request = app(Request::class);

            if(!empty($request->input('global_id'))) {
                $builder->where('global_id', $request->input('global_id'));
            } else {
                $builder->whereRaw('1!=1');
            }

            

            // if(!empty($request->input('global_id'))) {
            //     $request->session()->put('global_id', $request->input('global_id'));
            // }
            
            // $global_id = $request->session()->get('global_id');

            // if(!empty($global_id)) {
            //     $builder->where('global_id', $global_id);
            // } else {
            //     $builder->whereRaw('1!=1');
            // }
        }
    }
}