<?php

namespace App\Classes;

use App\Models\Company;
use GuzzleHttp\Exception\ServerException;

class SmartApi
{
    private $apiHost = 'https://smarttender.biz/ws/webservice.asmx';
    private $bidFields = [
        'organization-name'=>'bidderInfo.organizationName',
        'orginization-id'=>'bidderInfo.okpo',
        'phone'=>' bidderInfo.phoneNumber',
        'email'=>'bidderInfo.email',
        'contact_name'=>'bidderInfo.fullName',
    ];
    private $client;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
    }

    public static function factory(): self
    {
        return new self;
    }

    public function getTenderData($id)
    {
        $company = Company::find(3);

        //if(!$company->ticket) {
            $this->getTicket($company);
       // }

dd($this->apiHost . '/ExecuteEx?pureJson=1',json_encode([
    'ticket' => $company->ticket,
    'calcId' => 'SMARTAPI.COM.GETTENDER',
    'args' => '{tenderId:'.(int)$id.'}'
]));
        try {
            $response = $this->client->request('post', $this->apiHost . '/ExecuteEx?pureJson=1', [
                'form_params' => [
                    'ticket' => $company->ticket,
                    'calcId' => 'SMARTAPI.COM.GETTENDER',
                    'args' => '{tenderId:'.(int)$id.'}'
                ],
               /* 'headers' => [
                    'Content-Type' => 'application/json',
                ]*/
            ]);
            $body = (string)$response->getBody();
        }
        catch(ServerException $e) {
            dd($e->getResponse()->getHeaders(),(string)$e->getResponse()->getBody());
        }

dd($body,$response->getHeaders());
        $tender = json_decode(simplexml_load_string($body));
        dd($tender);
        $data = [];

        if(!empty($tender->data->id)) {
            foreach($this->tenderFields as $name => $field) {
                if(!empty($tender->data->{$field})) {
                    $data[$name] = $tender->data->{$field};
                }
            }
        }

        return $data;
    }

    private function getTicket($company)
    {
        $response = $this->client->request('get', $this->apiHost.'/LoginEx', [
            'query' => [
                'login' => $company->email,
                'password' => $company->password
            ]
        ]);

        if($response->getStatusCode() == 200) {
            $body = (string)$response->getBody();
            $body = json_decode(simplexml_load_string($body));

            if(!empty($body->Ticket)) {
                $company->ticket = $body->Ticket;
                $company->save();
            }
        }
    }
}
