<?php

namespace App\Classes;

class RegestrApi
{
    private $apiHost = 'https://rejestr.io/api/zamowienia_publiczne/';
    private $tenderFields = ['name'=>'name','cpv_code'=>'cpv','pe_id'=>' purchaser_id','pe_name'=>'purchaser_name','currency'=>'value_currency','ammount'=>'value_ammount','city'=>'purchaser_city','postal_code'=>'purchaser_postal_code'];

    public static function factory(): self
    {
        return new self;
    }

    public function getTenderFields()
    {
        $fields = [];

        foreach($this->tenderFields as $field => $v) {
            array_push($fields, [
                'code' => $field,
                'name' => translate('filter.tenders.'.strtr($field, ['_'=>'-']))
            ]);
        }

        return $fields;
    }

    public function getTenderData($id)
    {
        $tender = file_get_contents($this->apiHost.$id.'.json');
        $tender = json_decode($tender);
        $data = [];

        if(!empty($tender->data->id)) {
            foreach($this->tenderFields as $name => $field) {
                if(!empty($tender->data->{$field})) {
                    $data[$name] = $tender->data->{$field};
                }
            }
        }

        return $data;
    }
}
