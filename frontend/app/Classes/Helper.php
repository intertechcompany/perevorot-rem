<?php

namespace App\Classes;

class Helper
{
    public static function getMonths()
    {
        return json_encode([translate('month.january'), translate('month.february'), translate('month.march'), translate('month.april'), translate('month.june'), translate('month.july'), translate('month.august'), translate('month.september'), translate('month.october'), translate('month.november'), translate('month.december')]);
    }

    public static function getDays()
    {
        return json_encode([translate('day.short.monday'), translate('day.short.tuesday'), translate('day.short.wednesday'), translate('day.short.thursday'), translate('day.short.friday'), translate('day.short.saturday'), translate('day.short.sunday')]);
    }
}