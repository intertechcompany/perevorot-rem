<?php

namespace App\Console\Commands;

use App\Models\FormFile;
use Illuminate\Console\Command;

class ThumbsGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thumbs:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Thumbs generate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = FormFile::where('mime', 'like', "%image/%")->get();

        echo "Found ".$files->count()."\n";

        foreach($files as $file) {
            echo '.';
            $file->generateThumb();
        }

        echo "\n";
    }
}
