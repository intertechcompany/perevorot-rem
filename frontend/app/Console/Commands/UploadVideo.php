<?php

namespace App\Console\Commands;

use App\Models\FormFile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Vimeo;

class UploadVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:video';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload video';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = FormFile::where('mime', 'like', "%video/%")->get();

        echo "Found ".$files->count()."\n";

        foreach($files as $file) {
            echo '.';

            if($uri = Vimeo::upload(public_path('files').'/'.$file->name)) {
                $file->video_url = $uri;
                $file->save();

                Storage::delete($file->name);
            }
        }

        echo "\n";
    }
}
