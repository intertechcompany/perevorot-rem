<?php

namespace App\Console\Commands;

use App\Models\FormFile;
use Illuminate\Console\Command;

class UpdateIncomingCalls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:incoming_calls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update incoming calls';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $CallController = app('App\Http\Controllers\Api\CallController');
        $CallController->getIncomingCalls();
    }
}
