import './../scss/embed.scss'

import Vue from 'vue'
import RemEmbed from './RemEmbed.vue'

Vue.component('rem-embed', RemEmbed)

new Vue({
    el: 'rem-embed',
    methods: {
    },
    mounted () {

    }
})
