import './vue-formly-buefy/node_modules/vue2-dropzone/dist/vue2Dropzone.css'

import Vue from 'vue'
import Buefy from 'buefy'
import VueFormly from 'vue-formly'
import VueFormlyBuefy from './vue-formly-buefy'
import RemForm from './RemForm.vue'
import RemMap from './RemMap.vue'
import RemList from './RemList.vue'
import axios from 'axios'
import createHistory from 'history/createBrowserHistory'
import VueLazyLoad from 'vue-lazyload'
import RemImages from './RemImages.vue'
import autocomplete from 'autocompleter'
import sessionstorage from 'sessionstorage'

(function() {
    const url = new URL(window.location.href)
    const params = new URLSearchParams(url.search)

    if(params.has('global_id')) {
        sessionstorage.setItem('global_id', params.get('global_id'))
    }
})()

Vue.use(VueLazyLoad)
Vue.use(Buefy, {
    defaultMonthNames: JSON.parse(document.querySelector('body').getAttribute('data-months')),
    defaultDayNames: JSON.parse(document.querySelector('body').getAttribute('data-days')),
    timeLang: {now: 'сейчас'}
})
Vue.use(VueFormly)
Vue.use(VueFormlyBuefy)
Vue.use(RemForm)
Vue.use(RemMap)
Vue.use(RemList)

Vue.component('rem-map', RemMap)
Vue.component('rem-form', RemForm)
Vue.component('rem-list', RemList)
Vue.component('rem-images', RemImages)

new Vue({
    el: '#app',
    data: {
        showUsersAutocomplete: false,
        parentFormData: null,
        currentTab: null,
        history: null,
        location: null,
        showCommentsState: {},
        tabs: {
            object: 'common'
        },
        toggledFields: [],
        formObjectId: 0,
        trafficCalculator: {
            cheque: 58,
            weeks: 4.3452,
            apartmentsByZone: {
                1: null,
                2: null,
                3: null
            },
            percent: {
                1: 60,
                2: 50,
                3: 40
            },
            visits: {
                1: 4.5,
                2: 4.5,
                3: 4.5
            },
            flowPerDay: [],
            transitCheck: null,
            trafic: [],
            traficValue: [],
            flow: 12,
            transitWeekDays: 5,
            conversion: 1,
            averageTransitCheque: 12,
            isSaving: false
        },
        filterSelected: {
            domains: []
        },
        isLoadingRenew: false,
        hints: {
            qualificationProcess: false,
            conditions: false,
            contractExecution: false,
            order: false,
            bids: false,
            supplier: false
        }
    },
    methods: {
        filteredData(code) {
            if(this.filterSelected.domains.length <= 0) {
                return true;
            }
            else if(this.filterSelected.domains.length > 0 && this.filterSelected.domains.indexOf(code) > -1) {
                return true;
            }

            return false;
        },
        callRecord(id, incoming = '') {
            axios.post('/api/call/record', {call_id: id, incoming: incoming}).then(response => {
                //console.log(response);

                this.$snackbar.open({
                    message: response.data.message,
                    type: 'is-success'
                })

                window.open(response.data.url, '_blank');
            }).catch((e)=>{
                this.$snackbar.open({
                    message: e.response.data.message,
                    type: 'is-danger',
                    indefinite: true
                })
            })
        },
        call(id, phone, company) {
            axios.post('/api/call', {form_id: id, phone: phone, company: company}).then(response => {
                console.log(response);
                this.$snackbar.open({
                    message: response.data.message,
                    type: 'is-success'
                })
            }).catch((e)=>{
                this.$snackbar.open({
                    message: e.response.data.message,
                    type: 'is-danger',
                    indefinite: true
                })
            })
        },
        showImage(index) {
            this.$refs.lightbox.openGallery(index)
        },
        clickTab(tab) {
            this.tabs.object=tab;
            this.history.push(this.location + '/' + tab);
        },
        addChildrenToParent(code, id) {
            axios.post('/forms/update/'+code+'/'+id, {
                parentFormData: this.parentFormData
            }).then(response => {
                this.$snackbar.open({
                    message: response.data.message,
                    type: 'is-success'
                })

                window.location.reload(true)
            });
        },
        submitChildrenForms(code, id) {
            axios.post('/forms/submit/'+code+'/'+id).then(response => {
                this.$snackbar.open({
                    message: response.data.message,
                    type: 'is-success'
                })

                window.location.reload(true)
            });
        },
        getFormEditHref(href) {
            let globalIdParam = ''

            if(sessionstorage.getItem('global_id')) {
                globalIdParam = '?global_id='+sessionstorage.getItem('global_id')
            }

            window.location.href=href+globalIdParam;
        },
        onTrafficCalculatorSave(id) {
            this.trafficCalculator.isSaving=true

            axios.post('/calc/save/'+id, {
                 inputs: this.trafficCalculator
            });

            axios.post('/api/form/save/object/'+id, {
                form: {
                    'turnover-estimation': this.trafficCalculatorFormat(this.calculateTransitCheckInMonth()+this.trafficCalculatorCalculateRvenueTotal())
                }
            }).then(response => {
                this.trafficCalculator.isSaving=false

                this.$snackbar.open({
                    message: response.data.message,
                    type: 'is-success'
                })
                
                //window.location.reload(true)

            }).catch((e)=>{
                this.trafficCalculator.isSaving=false

                this.$snackbar.open({
                    message: e.response.data.message,
                    type: 'is-danger',
                    indefinite: true
                })
            })

            axios.post('/api/form/save/object/'+id, {
                form: {
                    'turnover-per-day': this.trafficCalculatorFormat(this.calculateTotalTransitInDay())
                }
            }).then(response => {
                this.trafficCalculator.isSaving=false

                this.$snackbar.open({
                    message: response.data.message,
                    type: 'is-success'
                })

                window.location.reload(true)

            }).catch((e)=>{
                this.trafficCalculator.isSaving=false

                this.$snackbar.open({
                    message: e.response.data.message,
                    type: 'is-danger',
                    indefinite: true
                })
            })
        },
        trafficCalculatorFormat(revenue, suffix) {
            let value=(revenue/1).toFixed(0).replace('.', '.')

            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')+(suffix ? suffix : '')
        },
        calculateTotalTransitInDay() {
            return (this.calculateTransitCheckInMonth()+this.trafficCalculatorCalculateRvenueTotal())*12/365;
        },
        calculateTransitCheckInMonth() {
            var res = Math.round(this.calculateTransitCheckInWeek()*this.trafficCalculator.weeks)
            return !isNaN(res) ? res : ''
        },
        calculateFlowInWeek() {
            var sum = 0;
            //console.log(this.trafficCalculator.flowPerDay);
            for(var index in  this.trafficCalculator.flowPerDay) {
                sum += this.calculateFlowPerDay(index)
            }

            return Math.round(sum)
        },
        calculateTransitCheckInWeek() {
            if(this.trafficCalculator.conversion && this.trafficCalculator.transitCheck) {
                return Math.round(this.calculateFlowInWeek() * this.trafficCalculator.conversion * this.trafficCalculator.transitCheck)
            }
        },
        calculateFlowPerDay(index){
            //console.log(this.trafficCalculator.flowPerDay[index]);
            if(this.trafficCalculator.flowPerDay[index] !== undefined) {
                if(this.trafficCalculator.flowPerDay[index] !== null) {
                    return Math.round(this.trafficCalculatorPerDay() * this.trafficCalculator.flowPerDay[index] / 100)
                } else {
                    return 0;
                }
            }
        },
        trafficCalculatorCalculatePerHour(index){
            //console.log(this.trafficCalculator.traficValue[index]);
            if(this.trafficCalculator.traficValue[index] !== undefined) {
                if(this.trafficCalculator.traficValue[index] !== null) {
                    return Math.round(this.trafficCalculator.trafic[index] * parseInt(this.trafficCalculator.traficValue[index]) * 4)
                } else {
                    return 0;
                }
            }
        },
        trafficCalculatorPerHour() {
            var sum = 0;
            var perDay = this.trafficCalculatorPerDay();

            for (var index in this.trafficCalculator.traficValue) {
                if(this.trafficCalculator.traficValue[index] !== null) {
                    sum += parseInt(this.trafficCalculator.traficValue[index])
                }
            }

            return sum && perDay ? Math.round(perDay/sum) : 0
        },
        trafficCalculatorPerDay() {
            var sum = 0;
            //nsole.log(this.trafficCalculator.trafic);
            //for (var i = 0; i < this.trafficCalculator.trafic.length; i++) {
            for(var index in this.trafficCalculator.trafic) {
                sum += this.trafficCalculatorCalculatePerHour(index)
            }

            return sum ? Math.round(sum) : 0
        },
        trafficCalculatorTrafficPerHour(trafficPerDay){
            return Math.round(trafficPerDay/this.trafficCalculator.flow)
        },
        trafficCalculatorCalculateRvenue(zone){
            return this.trafficCalculator.cheque*this.trafficCalculator.weeks*this.trafficCalculator.apartmentsByZone[zone]*(this.trafficCalculator.percent[zone]/100)*this.trafficCalculator.visits[zone]
        },
        trafficCalculatorCalculateRvenueTotal(){
            return this.trafficCalculatorCalculateRvenue(1)+this.trafficCalculatorCalculateRvenue(2)+this.trafficCalculatorCalculateRvenue(3)
        },
        trafficCalculatorFlowPerMonth(trafficPerDay){
            return trafficPerDay*this.trafficCalculator.transitWeekDays*this.trafficCalculator.weeks
        },
        trafficCalculatorTransitChequesPerMonth(trafficPerDay){
            return this.trafficCalculatorFlowPerMonth(trafficPerDay)*(this.trafficCalculator.conversion/100)*7*this.trafficCalculator.weeks
        },
        trafficCalculatorTotal(trafficPerDay){
            return this.trafficCalculatorTransitChequesPerMonth(trafficPerDay)*this.trafficCalculator.averageTransitCheque
        },
        showComments: function(id) {
            if(this.showCommentsState[id] === undefined || this.showCommentsState[id] === null) {
                Vue.set(this.showCommentsState, id, false);
            }

            this.showCommentsState[id] = !this.showCommentsState[id];
        },
        renewDistances: function(formObjectId) {
            this.isLoadingRenew=true

            axios.post('/api/distance/renew/'+formObjectId)
            .then(response => {
                this.$snackbar.open({
                    message: 'Данные обновлены',
                    type: 'is-success'
                })

                window.location.reload(true)
            });
        },
        removeFacebookAppendedHash: function() {
            if (window.location.hash && window.location.hash == '#_=_') {
                (window.history && history.replaceState) ? window.history.replaceState('', document.title, window.location.pathname) : window.location.hash = ''
            }
        },
        toggle: function(field) {
            if(this.toggledFields.indexOf(field)!==-1) {
                this.removeFromArray(this.toggledFields, field)
            } else {
                this.toggledFields.push(field)
            }
        },
        isToggled: function(field){
            return this.toggledFields.indexOf(field) !== -1
        },
        removeFromArray: function(arr) {
            var what, a = arguments, L = a.length, ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax= arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            
            return arr;
        }
    },
    mounted () {
        this.history = createHistory();
        this.location = window.location.pathname.replace('/'+this.currentTab, '');

        if(this.currentTab && this.currentTab !== 'common') {
            if(document.getElementById('form-files') === null && this.currentTab == 'files') {
                this.currentTab = 'common';
            }

            this.clickTab(this.currentTab);
        }

        this.removeFacebookAppendedHash()

        let burger = document.querySelector('.burger')
        
        if(burger) {
            let nav = document.querySelector('#'+burger.dataset.target);

            burger.addEventListener('click', function(){
                burger.classList.toggle('is-active');
                nav.classList.toggle('is-active');
            });
        }

        if(this.formObjectId) {
            axios.post('/calc/get/'+this.formObjectId)
            .then(response => {
                if(response.data.preset) {
                    for (var field in response.data.preset) {
                        this.trafficCalculator[field] = response.data.preset[field];
                    }
                }
                if(response.data.calc) {
                    for (var field in response.data.calc) {
                        this.trafficCalculator[field] = response.data.calc[field];
                    }
                }
            });
        }

        if(this.showUsersAutocomplete) {
            axios.post('/get/users')
                .then(response => {
                    var users = response.data;

                    autocomplete({
                        input: document.getElementById("users"),
                        fetch: function (text, update) {
                            text = text.toLowerCase();
                            var suggestions = users.filter(n => n.label.toLowerCase().indexOf(text)>-1)
                            update(suggestions);
                        },
                        onSelect: function (item) {
                            document.getElementById("user_id").value = item.value;
                            document.getElementById("users").value = item.label;
                        }
                    });
            });
        }
    },
    created () {
    },
    destroyed () {
    }
})