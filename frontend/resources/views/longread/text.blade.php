<div class="columns is-multiline">
	<div class="column is-12">
	    @if(!empty($block->text_title))
	        <div class="title" style="text-align: center;margin-top: 10px;">{{ $block->text_title }}</div>
	    @endif
		<div style="text-align: center;">
	    {!! $block->text_text !!}
		</div>
	</div>
</div>