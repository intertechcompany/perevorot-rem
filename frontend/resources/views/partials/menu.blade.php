@if($user && $user->isSuperadmin())
   {{--@foreach($domains as $domain)
        <a class="navbar-item{{ $_SERVER['REQUEST_URI']=='/list/'.$domain ? ' is-active' : '' }}" href="/list/{{ $domain }}">
            {{ $domain }}
        </a>
    @endforeach--}}
    @if(!$groupForms->isEmpty())
        @foreach($groupForms as $item)
            <a class="navbar-item{{ $_SERVER['REQUEST_URI']=='/list/group/'.$item->is_company_field ? ' is-active' : '' }}" href="/list/group/{{ $item->is_company_field }}">
                {{ $item->nameByType() }}
            </a>
        @endforeach
    @endif
   @if($user->accessToCall())
       <a class="navbar-item{{ $_SERVER['REQUEST_URI']=='/calls'? ' is-active' : '' }}" href="/calls">
           {{ translate('menu.calls') }}
       </a>
   @endif
@else
    @if(!$menuFormsRead->isEmpty() && !starts_with(\Route::currentRouteName(), 'registration'))
        @foreach($menuFormsRead as $item)
            <a class="navbar-item{{ $_SERVER['REQUEST_URI']=='/form/list/'.$item->code ? ' is-active' : '' }}" href="/form/list/{{ $item->code }}">
                {{ $item->name }}
            </a>
        @endforeach
    @endif
    @if($user && $user->accessToHistory())
        <a class="navbar-item{{ $_SERVER['REQUEST_URI']=='/history'? ' is-active' : '' }}" href="/history">
            {{ translate('forms.history') }}
        </a>
    @endif
    @if($user && $user->accessToModeration())
        <a class="navbar-item{{ $_SERVER['REQUEST_URI']=='/moderation'? ' is-active' : '' }}" href="/moderation">
            {{ translate('forms.moderation') }}
        </a>
    @endif
    @if($user && $user->isAdmin() && $user->accessToCall())
        <a class="navbar-item{{ $_SERVER['REQUEST_URI']=='/calls'? ' is-active' : '' }}" href="/calls">
            {{ translate('menu.calls') }}
        </a>
    @endif
@endif