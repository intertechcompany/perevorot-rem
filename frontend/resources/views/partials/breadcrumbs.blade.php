<nav class="breadcrumb" aria-label="breadcrumbs">
    <ul>
        @foreach($form->generateBreads(@$formData) as $bread)
            @if(!empty($bread['url']))
                <li><a v-on:click.prevent="getFormEditHref('{{ $bread['url'] }}')">{{ $bread['text'] }}</a></li>
            @else
                <li class="is-active"><a href="" aria-current="page">{{ $bread['text'] }}</a></li>
            @endif
        @endforeach
    </ul>
</nav>