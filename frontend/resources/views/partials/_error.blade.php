@if($errors->has($errorKey))
    <span class="icon is-right has-text-danger"><i class="mdi mdi-alert-circle mdi-24px"></i></span>
    @foreach($errors->get($errorKey) as $error)
        <p class="help is-danger">{{ translate($error) }}</p>
    @endforeach
@endif