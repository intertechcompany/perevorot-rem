<div class="calculator">
    <div class="title is-6">{{ translate('calc.warehouses') }}</div>
    <input type="hidden" v-model="formObjectId" :set="formObjectId={{ $formData->id }}">
    <table class="table calculator-table">
        <tr>
            <td>{{ translate('calc.warehouses-money') }}</td>
            <td colspan="3"><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.cheque"></td>
        </tr>
        <tr>
            <td>{{ translate('calc.days-in-month') }}</td>
            <td colspan="3"><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.weeks"></td>
        </tr>
        <tr>
            <td></td>
            <td><span class="tag" style="color:#FFFFFF; background-color: #69B017">{{ translate('calc.zone1') }}</span></td>
            <td><span class="tag" style="color:#FFFFFF; background-color: #FB810B">{{ translate('calc.zone2') }}</span></td>
            <td><span class="tag" style="color:#FFFFFF; background-color: #9013FE">{{ translate('calc.zone3') }}</span></td>
        </tr>
        <tr>
            <td>{{ translate('calc.warehouses-count') }}</td>
            <td><input type="text" class="input" disabled v-model="trafficCalculator.apartmentsByZone[1]" :set="trafficCalculator.apartmentsByZone[1]={{ !empty($formData->apartmentsByZone()[1]) ? $formData->apartmentsByZone()[1] : 0 }}"></td>
            <td><input type="text" class="input" disabled v-model="trafficCalculator.apartmentsByZone[2]" :set="trafficCalculator.apartmentsByZone[2]={{ !empty($formData->apartmentsByZone()[2]) ? $formData->apartmentsByZone()[2] : 0 }}"></td>
            <td><input type="text" class="input" disabled v-model="trafficCalculator.apartmentsByZone[3]" :set="trafficCalculator.apartmentsByZone[3]={{ !empty($formData->apartmentsByZone()[3]) ? $formData->apartmentsByZone()[3] : 0 }}"></td>
        </tr>
        <tr>
            <td>{{ translate('calc.traffic-percent') }}</td>
            <td><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.percent[1]"></td>
            <td><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.percent[2]"></td>
            <td><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.percent[3]"></td>
        </tr>
        <tr>
            <td>{{ translate('calc.weeks-visit') }}</td>
            <td><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.visits[1]"></td>
            <td><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.visits[2]"></td>
            <td><input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.visits[3]"></td>
        </tr>
        <tr>
            <td>{{ translate('calc.warehouses-money2') }}</td>
            <td><div v-text="trafficCalculatorFormat(trafficCalculatorCalculateRvenue(1), ' {{ translate('calc.currency') }}')"></div></td>
            <td><div v-text="trafficCalculatorFormat(trafficCalculatorCalculateRvenue(2), ' {{ translate('calc.currency') }}')"></div></td>
            <td><div v-text="trafficCalculatorFormat(trafficCalculatorCalculateRvenue(3), ' {{ translate('calc.currency') }}')"></div></td>
        </tr>
        <tr style="background: whitesmoke">
            <td>
                <br>
                <div class="title is-6">{{ translate('calc.total') }}</div>
            </td>
            <td colspan="2"></td>
            <td>
                <br>
                <div class="title is-6" v-text="trafficCalculatorFormat(trafficCalculatorCalculateRvenueTotal(), ' {{ translate('calc.currency') }}')"></div>
            </td>
        </tr>
    </table>

    @if(!empty($formData->data->trafic) && !empty($formData->data->trafic->value))
        <br>
        <br>
        <br>
        <div class="title is-6">{{ translate('calc.traffic') }}</div>
        <table class="table calculator-table">
            <tr>
                <td></td>
                <td>{{ translate('calc.traffic.text') }}</td>
                <td>{{ translate('calc.traffic.hours') }}</td>
                <td>{{ translate('calc.traffic.total') }}</td>
            </tr>
            @foreach($formData->data->trafic->value as $k=>$item)
                @if(!empty($item->value))
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td style="width: 20%;">
                            <input type="text" class="input" disabled v-model="trafficCalculator.trafic[{{ $loop->index }}]" :set="trafficCalculator.trafic[{{ $loop->index }}]={{ (int) $item->value }}">
                        </td>
                        <td style="width: 20%;">
                            <input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.traficValue[{{ $loop->index }}]">
                        </td>
                        <td v-text="trafficCalculatorCalculatePerHour({{ $loop->index }})"></td>
                    </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="3">{{ translate('calc.traffic.per-day') }}</td>
                <td><div v-text="trafficCalculatorPerDay()"></div></td>
            </tr>
            <tr>
                <td colspan="3">{{ translate('calc.traffic.per-hour') }}</td>
                <td><div v-text="trafficCalculatorPerHour()"></div></td>
            </tr>
        </table>
        <div class="title is-6">{{ translate('calc.flow-per-day') }}</div>
        <table class="table calculator-table">
            @foreach([translate('day.monday'), translate('day.tuesday'), translate('day.wednesday'), translate('day.thursday'), translate('day.friday'), translate('day.saturday'), translate('day.sunday')] as $day)
            <tr>
                <td>{{ $day }}</td>
                <td style="width: 20%;">
                    <input {{ !$form->canEdit ? 'disabled' : '' }} placeholder="%" type="text" class="input" v-model="trafficCalculator.flowPerDay[{{ $loop->iteration }}]">
                </td>
                <td></td>
                <td></td>
                <td><div v-text="calculateFlowPerDay({{ $loop->iteration }})"></div></td>
            </tr>
            @endforeach
            <tr>
                <td colspan="4">{{ translate('calc.flow-in-week') }}</td>
                <td>
                    <div v-text="calculateFlowInWeek()"></div>
                </td>
            </tr>
            <tr>
                <td colspan="4">{{ translate('calc.conversion') }}</td>
                <td style="width: 20%;">
                    <input {{ !$form->canEdit ? 'disabled' : '' }} placeholder="%" type="text" class="input" v-model="trafficCalculator.conversion">
                </td>
            </tr>
            <tr>
                <td colspan="4">{{ translate('calc.transit-check') }}</td>
                <td style="width: 20%;">
                    <input {{ !$form->canEdit ? 'disabled' : '' }} type="text" class="input" v-model="trafficCalculator.transitCheck">
                </td>
            </tr>
            <tr></tr>
            <tr>
                <td colspan="4">{{ translate('calc.turnover-in-week') }}</td>
                <td>
                    <div v-text="calculateTransitCheckInWeek()"></div>
                </td>
            </tr>
            <tr>
                <td colspan="4">{{ translate('calc.turnover-in-month') }}</td>
                <td>
                    <div v-text="calculateTransitCheckInMonth()"></div>
                </td>
            </tr>
        </table>
    <!--
        {{--<div class="title is-6">Трафик</div>
        <table class="table calculator-table">
            <tr>
                <td></td>
                <td>Трафик</td>
                <td>Трафик за чаc</td>
            </tr>
            <?php
                $trafficPerDay=0;
            ?>
            @foreach($formData->data->trafic->value as $k=>$item)
                @if(!empty($item->value))
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ (int) $item->value }}</td>
                        <td>{{ (int) $item->value*4 }}</td>
                    </tr>
                    <?php
                        $trafficPerDay+=(int) $item->value*4*(($k==1 || $k==3) ? 3 : 1);
                    ?>
                @endif
            @endforeach
            <tr>
                <td>Длительность потока (ч)</td>
                <td colspan="2"><input type="text" class="input" v-model="trafficCalculator.flow"></td>
            </tr>
            <tr>
                <td>Трафик в час</td>
                <td colspan="2"><div v-text="trafficCalculatorTrafficPerHour({{ $trafficPerDay }})"></div></td>
            </tr>
            <tr>
                <td>Трафик в день</td>
                <td colspan="2">{{ $trafficPerDay }}</td>
            </tr>
            <tr>
                <td>Тразитных дней в неделю</td>
                <td colspan="2"><input type="text" class="input" v-model="trafficCalculator.transitWeekDays"></td>
            </tr>
            <tr>
                <td>Поток в месяц</td>
                <td colspan="2"><div v-text="trafficCalculatorFormat(trafficCalculatorFlowPerMonth({{ $trafficPerDay }}))"></div></td>
            </tr>
            <tr>
                <td>Конверсия, %</td>
                <td colspan="2"><input type="text" class="input" v-model="trafficCalculator.conversion"></td>
            </tr>
            <tr>
                <td>Транзитных чеков в месяц</td>
                <td colspan="2"><div v-text="trafficCalculatorFormat(trafficCalculatorTransitChequesPerMonth({{ $trafficPerDay }}))"></div></td>
            </tr>
            <tr>
                <td>Средний транзитный чек</td>
                <td colspan="2"><input type="text" class="input" v-model="trafficCalculator.averageTransitCheque"></td>
            </tr>
            <tr style="background: whitesmoke">
                <td>
                    <br>
                    <div class="title is-6">Итого</div>
                </td>
                <td colspan="2" class="has-text-right">
                    <br>
                    <div class="title is-6" v-text="trafficCalculatorFormat(trafficCalculatorTotal({{ $trafficPerDay }}), ' грн')"></div>
                </td>
                <td></td>
            </tr>
        </table>--}}
        -->
    @endif

    <div class="title is-6">
        {{ translate('calc.turnover-total') }} <span class="title is-6" v-text="trafficCalculatorFormat(calculateTransitCheckInMonth()+trafficCalculatorCalculateRvenueTotal(), ' {{ translate('calc.currency') }}')"></span>
    </div>

    <div class="title is-6">
        {{ translate('calc.turnover-per-day') }} <span class="title is-6" v-text="trafficCalculatorFormat(this.calculateTotalTransitInDay(), ' {{ translate('calc.currency') }}')"></span>
    </div>

    <button {{ !$form->canEdit ? 'disabled' : '' }} class="button is-info" v-bind:class="{ 'is-loading': trafficCalculator.isSaving }" v-on:click="onTrafficCalculatorSave({{ $formData->id }})">{{ translate('calc.submit') }}</button>
</div>