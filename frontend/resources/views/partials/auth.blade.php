@if($user)
    <a class="navbar-link" href="#">
        @if(!empty($user->avatar_url))
            <figure class="image is-32x32" style="margin-right:.5em;">
                <img src="{{ $user->avatar_url }}" style="border-radius: 100%">
            </figure>
        @endif
        {{ $user->name }}
    </a>

    <div class="navbar-dropdown is-right">
        <a class="navbar-item" href="{{ route('profile') }}">{{ translate('menu.profile') }}</a>
        <a class="navbar-item" href="{{ route('logout') }}">
            <!--<span class="icon is-small">
                <i class="fa fa-power-off"></i>
            </span>-->
            {{ translate('menu.logout') }}
        </a>
    </div>
@else
    @if(env('FACEBOOK_CLIENT_ID') || env('GOOGLE_CLIENT_ID') || @$settings->innerAuth)
        <a class="navbar-link">
            {{ translate('menu.login') }}
        </a>

        <div class="navbar-dropdown is-right">
            @if(env('FACEBOOK_CLIENT_ID'))
                <a href="{{ route('login.social', ['provider' => 'facebook']) }}" class="navbar-item">
                    <span class="icon is-small">
                        <i class="mdi mdi-facebook"></i>
                    </span>
                </a>
            @endif
            @if(env('GOOGLE_CLIENT_ID'))
                <hr class="navbar-divider">
                <a href="{{ route('login.social', ['provider' => 'google']) }}" class="navbar-item">
                    <span class="icon is-small">
                        <i class="mdi mdi-google"></i>
                    </span>
                    
                </a>
            @endif
            @if(@$settings->innerAuth)
                <hr class="navbar-divider">
                <a href="{{ route('login') }}" class="navbar-item">
                    <span>
                        {{ translate('auth.inner') }}
                    </span>
                </a>
            @endif
        </div>
    @endif
@endif