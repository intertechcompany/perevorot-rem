@if(!$formData->logs->isEmpty())
<div class="columns is-gapless">
    <div class="column">
        <div class="title is-6">{{ translate('forms.status.history') }}</div>
        <table class="table specification">
                <tr>
                    <th>{{ translate('forms.status.date') }}</th>
                    <th>{{ translate('forms.status.name') }}</th>
                    <th>{{ translate('forms.status.comment') }}</th>
                    @if($formData->logsHasSubstatus())
                    <th>{{ translate('forms.status.substatus') }}</th>
                    @endif
                    <th>{{ translate('forms.status.email') }}</th>
                </tr>
            @foreach($formData->logs as $log)
                <tr>
                    <td>
                        {{ $log->created_at }}
                    </td>
                    <td>
                        {{ $log->status }}
                    </td>
                    <td>
                        {{ $log->comment }}
                    </td>
                    @if($formData->logsHasSubstatus())
                    <td>
                        {{ $log->substatus ? $log->showSubStatus() : '' }}
                    </td>
                    @endif
                    <td>
                        {{ @$log->user->email }}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endif