@if(!empty($formData->closest()))
    <h4 class="title is-6" style="margin-top: 2rem">{{ translate('forms.similar') }}</h4>
    <table class="table" style="width:50%">
        <tbody>
            @foreach($formData->closest() as $marker)
                <tr>
                    <td width="50">
                        @if(!empty($marker->data->competitor))
                            <a href="/form/show/{{ $marker->form->code }}/{{ $marker->id }}"><img src="{{ $marker->data->competitor->value->image }}"></a>
                        @endif
                    </td>
                    <td>
                        <a href="/form/show/{{ $marker->form->code }}/{{ $marker->id }}" class="title is-5" style="margin-bottom: 0">
                            {{ !empty($marker->data->competitor->value->name) ? ($marker->data->competitor->value->code == 'other' ? @$marker->data->competitorName->value: $marker->data->competitor->value->name) : $marker->form->name }}
                        </a>
                        <div class="comment">{{ $marker->type->map[0]->value->address }}</div>
                    </td>
                    <td>
                        <p>{{ $marker->distance_walk < 5 ? '<5' : round($marker->distance_walk) }} {{ translate('forms.similar.m') }}</p>
                        <p><small>{{ $marker->duration }} {{ translate('forms.similar.min') }}</small></p>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a class="button edit" v-on:click.prevent="renewDistances({{ $formData->id }})" v-bind:class="{ 'is-loading': isLoadingRenew}">
        <span class="icon">
            <i class="mdi mdi-autorenew"></i>
        </span>
        <span> {{ translate('forms.renew.distances') }}</span>
    </a>
@endif