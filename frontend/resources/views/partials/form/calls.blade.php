<div class="columns is-gapless">
    <div class="column">
        <div class="title is-6">{{ translate('forms.calls') }}</div>
        <table class="table">
            @foreach($formData->calls as $call)
                <tr>
                    <td style="width: 15%;">
                        {{ $call->name }}
                    </td>
                    <td style="width: 15%;">
                        {{ $call->created_at }}
                    </td>
                    <td style="width: 10%;">
                        <a href="#" v-on:click.prevent="callRecord({{ $call->id }})">{{ translate('forms.calls.record') }}</a>
                    </td>
                    <td>
                        {{ $call->username }}
                    </td>
                    <td>
                        <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('call-'+'{{ $call->id }}') }">
                            <div class="off">
                                <form method="post" action="{{ route('calls.comment', ['id'=>$call->id]) }}">
                                    {!! csrf_field() !!}
                                    <div class="columns">
                                        <div class="column">
                                            <textarea name="comment">{{ $call->comment }}</textarea>
                                        </div>
                                        <div class="column" style="display: flex;">
                                            <button type="submit" class="button is-success">{{ translate('calls.comment') }}</button>
                                            <button class="button" v-on:click.prevent="toggle('call-'+'{{ $call->id }}')">{{ translate('forms.field.cancel') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="on" v-on:click.prevent="toggle('call-'+'{{ $call->id }}')">
                                @if($call->comment)
                                <span>{{ $call->comment }}</span>
                                <a href="" class="icon">
                                    <i class="mdi mdi-lead-pencil"></i>
                                </a>
                                @else
                                    <button type="submit" class="button is-success">{{ translate('calls.comment') }}</button>
                                @endif
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>