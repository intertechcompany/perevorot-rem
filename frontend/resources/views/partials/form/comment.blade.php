<article class="media">
    <figure class="media-left">
        @if(!empty($comment->user->avatar))
        <p class="image is-32x32">
            <img src="{{ $comment->user->avatar }}" style="border-radius: 100%">
        </p>
        @endif
    </figure>
    <div class="media-content">
        <div class="content">
            <p>
                <strong>{{ @$comment->user->name }}</strong>
                <br>
                {!! nl2br($comment->data->comment->value) !!}
                <br>
                <small>{{ $comment->created_at }}</small>
            </p>
        </div>
    </div>
</article>