<div class="section">
    <div class="container">
        <rem-list :api-url="'/api/relation'" :code="'{{ $formData->form->relationForm->code }}'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="false"></rem-list>
    </div>
</div>
