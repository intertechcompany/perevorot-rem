@if(!empty($data->value['images']))
    <rem-images ref="lightbox"></rem-images>

    <?php $images=$data->value['images']; ?>
    <div class="columns is-gapless">    
        <div class="column is-6" style="line-height: 0;">
            <a href="" v-on:click.prevent="showImage(0)"><img src="{{ route('file.download', ['code'=>$images[0]->code, 'width'=>670, 'height'=>480]) }}" style="height: 30rem; width:100%; object-fit: cover"></a>
        </div>
        <div class="column is-6">
            <div class="columns is-multiline is-gapless is-mobile">    
                <div class="column is-6" style="line-height: 0;">
                    @if(!empty($images[1]->url))
                        <a href="" v-on:click.prevent="showImage(1)"><img src="{{ route('file.download', ['code'=>$images[1]->code, 'width'=>670, 'height'=>480]) }}" style="height: 15rem; width:100%; object-fit: cover"></a>
                    @endif
                </div>
                <div class="column is-6" style="line-height: 0;">
                    @if(!empty($images[2]->url))
                        <a href="" v-on:click.prevent="showImage(2)"><img src="{{ route('file.download', ['code'=>$images[2]->code, 'width'=>670, 'height'=>480]) }}" style="height: 15rem; width:100%; object-fit: cover"></a>
                    @endif
                </div>
                <div class="column is-6" style="line-height: 0;">
                    @if(!empty($images[3]->url))
                        <a href="" v-on:click.prevent="showImage(3)"><img src="{{ route('file.download', ['code'=>$images[3]->code, 'width'=>670, 'height'=>480]) }}" style="height: 15rem; width:100%; object-fit: cover"></a>
                    @endif
                </div>
                <div class="column is-6 is-12-mobile" style="line-height: 0;position:relative">
                    @if(@$images[4]->url)
                        @if(sizeof($images)>5)
                            <a href="" v-on:click.prevent="showImage(0)" style="background-color: rgba(0,0,0,.7);width:100%;height:100%;position: absolute;top:0;left:0;justify-content: center;display: flex;">
                                <div class="title has-text-white" style="align-self:center">
                                    <span class="icon is-medium" style="white-space: nowrap;">
                                        <i class="mdi mdi-file-image"></i>
                                        {{ sizeof($images) }}
                                    </span>
                                </div>
                            </a>
                        @endif
                        <a href="" v-on:click.prevent="showImage(4)"><img src="{{ route('file.download', ['code'=>$images[4]->code, 'width'=>670, 'height'=>480]) }}" style="height: 15rem; width:100%;  object-fit: cover"></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif