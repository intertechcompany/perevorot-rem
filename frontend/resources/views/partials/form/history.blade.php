<div class="columns is-gapless">
    <div class="column">
        <div class="title is-6">{{ translate('forms.history') }}</div>
        <table class="table">
            @foreach($formData->getHistory() as $field)
                <tr>
                    <td style="width: 15%;">
                        {{ $field->updated_at }}
                    </td>
                    <td style="width: 15%;">
                        {{ !empty($field->user_name) ? $field->user_name : @$field->user->name }}
                    </td>
                    <td style="width: 20%;">
                        {{ $field->fieldName }}
                    </td>
                    <td>
                        <div class="on">
                            @if(!is_array($field->value))
                                @if(!empty($field->value->address))
                                    <span>{{ $field->value->address }}</span>
                                @else
                                    <span>{{ !empty($field->value->name) ? $field->value->name : $field->value }}</span>
                                @endif
                            @else
                                <span>{{ $field->__original_value }}</span>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>