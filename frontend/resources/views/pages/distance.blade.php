@extends('layouts.app')

@section('content')
    <div class="columns">
        <div class="column is-6 is-offset-3" style="margin-top: 20px;">
            <form action="{{ route('distance') }}" method="post">
                <fieldset>
                    {!! csrf_field() !!}
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="from" placeholder="Координаты lat,lng" value="{{ $from }}">
                        </div>
                    </div>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="to" placeholder="Координаты lat,lng" value="{{ $to }}">
                        </div>
                    </div>
                    <button class="button is-success" type="submit">Рассчитать</button>
                </fieldset>
            </form>
            @if($output)
                <br>
                <div>{{ $output->distance }} meters / {{ $output->duration }} minutes</div>
            @endif
        </div>
    </div>
@endsection