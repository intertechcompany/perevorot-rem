@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    <div class="columns">
        <div class="column is-6 is-offset-3" style="margin-top: 20px;">
            <form action="{{ route('profile.save') }}" method="post">
                <fieldset>
                    {!! csrf_field() !!}
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="password" type="password" placeholder="{{ translate('profile.password') }}">
                            @include('partials._error', ['errorKey' => 'password'])
                        </div>
                    </div>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="password_confirmation" type="password" placeholder="{{ translate('profile.confirm.password') }}">
                            @include('partials._error', ['errorKey' => 'password_confirmation'])
                        </div>
                    </div>
                    <button class="button is-success is-blue" style="margin-top: 0;" type="submit">{{ translate('profile.submit') }}</button>
                </fieldset>
            </form>
        </div>
    </div>
@endsection