@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
<div class="section">
    <div class="container">
        <div class="columns is-multiline">
            <div class="column is-12">
                <div class="title" style="margin-top: 10px;">{{ translate('users.moderation') }}</div>
                <form action="{{ route('user.update') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $userData->id }}">
                    <table class="table specifications">
                        <tr>
                            <td>
                                {{ translate('users.email') }}
                            </td>
                            <td>
                                {{ $userData->email }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ translate('users.name') }}
                            </td>
                            <td>
                                {{ $userData->name }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ translate('users.phone') }}
                            </td>
                            <td>
                                {{ $userData->phone }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ translate('users.group') }}
                            </td>
                            <td>
                                <select name="group_id">
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                    <button class="button is-success" style="margin-top: 0;" type="submit">{{ translate('profile.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection