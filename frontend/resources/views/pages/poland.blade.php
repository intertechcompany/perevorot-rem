@extends('layouts.poland')

@section('content')
	<script>
		(function (d, id, callback) {			
			if (d.getElementById(id))
				return;
			
			var sdk = d.createElement('script');
			
			sdk.id = id;
			sdk.src = "//rem.localhost/js/embed.js";
			sdk.defer = true;
			
			callback && (sdk.onload = callback);

			d.getElementsByTagName('head')[0].appendChild(sdk);
		} (document, 'rem-sdk', function () {
			
		}));
	</script>

	<rem-embed :id="'256873'"></rem-embed>
@endsection
