@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    <div>
        <div class="container">
            <rem-list :api-url="'/api/moderation'" :show-title="false" :is-edit-click="true" :show-filter="false"></rem-list>
        </div>
    </div>
@endsection
