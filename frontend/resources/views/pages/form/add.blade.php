@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

{{--
@if($showCustomHeader)
    @section('customHeader')
        <nav class="navbar has-shadow is-fixed-top pl-header">
            <div class="container">
                <div class="navbar-brand">
                    <span class="navbar-item title" style="margin-bottom: 0">
                        {{ $form->name }}
                    </span>
                </div>
            </div>
        </nav>
    @endsection
@endif
--}}

@section('content')
    <div>
        <div class="container">
            <div class="columns">
                <div class="column is-12">
                    @include('partials.breadcrumbs')
                </div>
            </div>
            <div class="columns">
                <div class="column form-add">
                    <div class="title">{{ $form->name }}</div>
                    <rem-form
                        @if(!empty($parentId)) :preset-values="{ parent_form_object_id: {{ $parentId }} }" @endif
                        @if(empty($parentId) && $form->after_saving == 2) :redirect="'{{ \Url()->previous() }}'"
                        @elseif(!empty($parentId) && $form->after_saving != 1) :redirect="'{{ \Url()->previous() }}'" @endif
                        @if($form->button_add) :button-label="'{{ $form->button_add }}'" @endif
                        @if($form->button_cancel) :cancel-label="'{{ $form->button_cancel }}'" @endif
                        :code="'{{ $form->code }}'"
                        @if(!empty($parentCode)) :id="{{ $parentId }}" :parent-code="'{{ $parentCode }}'" @endif
                    ></rem-form>
                </div>
            </div>
        </div>
    </div>
@endsection
