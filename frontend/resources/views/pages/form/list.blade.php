@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    <div>
        @if(!empty(@$page->longread))
            <div class="container" style="margin-top: 35px;">
                {!! @$page->longread !!}
            </div>
        @endif
        <div class="container">
            @if(!empty($settings->createByAnonim) && isset($hideObjects))
                @if(!empty(app('request')->input('global_id')))
                <div class="columns" style="text-align: center;padding-bottom: 0;margin-top: 20px;margin-bottom: 0px;">
                    @if(!$menuFormsRead->isEmpty())
                        @foreach($menuFormsRead as $item)
                            <div class="column is-4 is-narrow" style="position: relative;padding-bottom: 0;margin-bottom: 0;" @if($loop->last) style="padding-bottom: 0;" @endif>
                                <a class="button is-secondary is-success" @if($item->hint) v-on:mouseleave="hints['{{ camel_case($item->code) }}']=false" v-on:mouseover="hints['{{ camel_case($item->code) }}']=true" @endif href="{{ route('form.add', ['code' => $item->code]) }}">
                                    {{ $item->name }}
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="columns" style="text-align: center;padding-bottom: 0;margin-top: 0px;">
                    @if(!$menuFormsRead->isEmpty())
                        @foreach($menuFormsRead as $item)
                            <div class="column is-4 is-narrow" style="position: relative;padding-bottom: 0;padding-top: 0;" @if($loop->last) style="padding-bottom: 0;" @endif>
                                @if($item->hint)
                                    <div class="info_text" v-if="hints['{{ camel_case($item->code) }}']">
                                        <div>
                                            {{ $item->hint }}
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>
                @endif
                <rem-list :api-url="'/api/pl_home_list'" :is-target-click="true" :show-filter="true"></rem-list>
            @elseif(@$showHomeList)
                <rem-list :api-url="'/api/home_list'" :is-target-click="true" :show-filter="true"></rem-list>
            @elseif(!@$form && @$domain)
                <rem-list :code="'{{ $domain }}'" :api-url="'/api/common_list'" :is-target-click="true" :show-filter="true"></rem-list>
            @elseif(!@$form && @$type)
                <rem-list :code="'{{ $type }}'" :api-url="'/api/common_list'" :is-target-click="true" :show-filter="true"></rem-list>
            @elseif(!@$form->parent_form)
                <rem-list :code="'{{ $form->code }}'" :is-target-click="true" :show-filter="true"></rem-list>
            @elseif(@$form->parent_form)
                <rem-list :code="'{{ $form->code }}'" :is-target-click="true" :show-title="false" :show-filter="true"></rem-list>
            @endif
        </div>
    </div>
@endsection
