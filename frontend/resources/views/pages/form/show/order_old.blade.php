@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if($formData)
        <div class="section tab-content">
            <div class="container">
                <div class="columns">
                    <div class="column is-8 is-offset-2">
                        @if(@$formData->currentStatusName)
                        <span class="tag is-primary">{{ $formData->currentStatusName }}</span>
                        @endif
                            <div class="columns">
                                <div class="column is-9">
                                    @if(!empty($formData->data->name->value))
                                        <div class="title has-text-primary">{{ $formData->data->name->value }}</div>
                                    @endif
                                </div>

                                <div class="column">
                                    <div class="column is-narrow">
                                        <a class="button edit" href="{{ route('form.edit', ['code' => $formData->form->code, 'id' => $formData->id]) }}">Редактировать</a>
                                    </div>
                                </div>
                            </div>

                            <table class="table" style="margin-top: 2rem">
                                @foreach(['pe','name', 'description', 'type', 'quantity', 'product-demands', 'qualification-criteria',
                                'delivery', 'amount', 'payment-conditions', 'contract', 'possible-suppliers', 'suppliers-name',
                                'active-contract', 'previous-experience', 'special-supplier', 'active-contract', 'previous-experience', 'special-supplier'
                                ] as $field)
                                    <?php
                                    $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                    ?>
                                    @if(!empty($data))
                                        <tr>
                                            <td>
                                                {{ $data->fieldName }}
                                            </td>
                                            <td>
                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                    <div class="off">
                                                        <rem-form :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'"></rem-form>
                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">Отменить</button>
                                                    </div>
                                                    <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                        <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                        @if($formData->canEdit($field))
                                                            <a href="" class="icon">
                                                                <i class="mdi mdi-lead-pencil"></i>
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>

                        @if(!empty($formData->type->file[0]->value['all']))
                            <div class="title is-6" style="margin-top: 2rem">{{ translate('block.files') }}
                                @if($formData->canEdit('documents'))
                                    <a href="" class="icon" v-on:click.prevent="toggle('documents')" v-if="!isToggled('documents')">
                                        <i class="mdi mdi-lead-pencil"></i>
                                    </a>
                                @endif
                            </div>
                            <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('documents') }">
                                <div class="off">
                                    <rem-form :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['documents']" :button-class="'is-small is-success'"></rem-form>
                                    <button class="button is-small is-cancel" v-on:click.prevent="toggle('documents')">Отменить</button>
                                </div>
                                <div class="on">
                                    <table class="table" style="width:100%">
                                        @foreach($formData->type->file[0]->value['all'] as $item)
                                        <tr>
                                            <td>
                                                <a target="_blank" href="{{ $item->url }}">
                                                    <i class="mdi mdi-24px mdi-{{ $item->type }}"></i>
                                                    {{ $item->name }}
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        @endif

                        @if(!$lastStatus && $user)
                            <div class="columns is-gapless">
                                <div class="column is-6">
                                    <rem-form :reload="true" :code="'status'" :id="{{ $formData->id }}" :api-schema-url="'/api/status/schema'" :api-save-url="'/api/status/save'" :button-label="'Изменить статус'"></rem-form>
                                </div>
                            </div>
                        @endif

                        @if($user && $form->childrenForm)
                            <div>
                                <rem-list :code="'{{ $form->childrenForm->code }}'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="true" :show-filter="false"></rem-list>
                                <a class="button is-success" href="{{ route('form.add', ['code' => $form->childrenForm->code, 'id' => $formData->id]) }}">Добавить {{ $form->childrenForm->name }}</a>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
