@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if($formData)
        <div class="tab-content" :set="currentTab='{{ $tab }}'">
            <div class="container">
                @include('partials/form/images', [
                    'data'=>@$formData->data->documentsCompetitor
                ])
                <div class="columns">
                    <div class="column is-12">
                        @if(@$formData->currentStatusName)
                            <span v-on:click="tabs.object='status'" class="tag is-primary" style="background-color: {{ @$formData->currentStatus->color }};">{{ $formData->currentStatusName }}</span>
                        @endif
                        <div class="columns">
                            <div class="column">
                                @if(!empty($formData->data->competitor->value))
                                    <div class="title">«{{ $formData->data->competitor->value->name }}», {{ $formData->getTitle() }}</div>
                                @endif
                            </div>
                            @if($form->canEdit)
                                <div class="column is-narrow">
                                    <a class="button edit" href="{{ route('form.edit', ['code' => $formData->form->code, 'id' => $formData->id]) }}">{{ translate('forms.edit') }}</a>
                                </div>
                            @endif
                        </div>
                        <div class="b-tabs">
                            <nav class="tabs">
                                <ul>
                                    <li v-bind:class="{ 'is-active': tabs.object=='common' }" v-on:click="clickTab('common')"><a><span>{{ translate('forms.tab.common') }}</span></a></li>
                                    @if(!empty($formData->data->address->value))
                                        <li v-bind:class="{ 'is-active': tabs.object=='map' }" v-on:click="clickTab('map')"><a><span>{{ translate('forms.tab.map') }}</span></a></li>
                                    @endif
                                    @if($user && $form->childrenForm)
                                        <li v-bind:class="{ 'is-active': tabs.object=='{{ $form->childrenForm->code }}' }" v-on:click="clickTab('{{ $form->childrenForm->code }}')"><a><span>{{ $form->childrenForm->name }}</span></a></li>
                                    @endif
                                    @if(!empty($formData->type->file[0]->value['all']))
                                        <li v-bind:class="{ 'is-active': tabs.object=='files' }" v-on:click="clickTab('files')"><a><span>{{ translate('forms.tab.files') }}</span></a></li>
                                    @endif
                                    @if($user && $form->canEdit)
                                        <li v-bind:class="{ 'is-active': tabs.object=='status' }" v-on:click="clickTab('status')"><a><span>{{ translate('forms.tab.status') }}</span></a></li>
                                    @endif
                                    @if(!empty($formData->type->autocompleteForm[0]->value))
                                        <li v-bind:class="{ 'is-active': tabs.object=='autocomplete' }" v-on:click="clickTab('autocomplete')"><a><span>{{ $formData->type->autocompleteForm[0]->value->form->name }}</span></a></li>
                                    @endif
                                    @if($formData->form->is_history && $user->accessToHistory())
                                        <li v-bind:class="{ 'is-active': tabs.object=='history' }" v-on:click="clickTab('history')"><a><span>{{ translate('forms.tab.history') }}</span></a></li>
                                    @endif
                                    @if($formData->form->relation_form)
                                        <li v-bind:class="{ 'is-active': tabs.object=='relation' }" v-on:click="clickTab('relation')"><a><span>{{ $formData->form->relationForm->name }}</span></a></li>
                                    @endif
                                </ul>
                            </nav>
                            <section class="tab-content">

                                <div class="tab-content" v-bind:class="{ 'hide': tabs.object!='common' }">
                                    <div class="columns is-gapless">
                                        <div class="column is-8">
                                            <h4 class="title is-6" style="margin-top: 2rem">{{ translate('forms.details') }}</h4>
                                            <table class="table" style="width:100%">
                                                @foreach($formData->form->getFieldsForTable() as $field)
                                                    <?php
                                                        $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                                    ?>
                                                    @if(!empty($data) && $data->value !== null)
                                                        <tr>
                                                            <td>
                                                                {{ $data->fieldName }}
                                                            </td>
                                                            <td>
                                                                @if($formData->canEdit($field))
                                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                                    <div class="off">
                                                                        <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.details') }}</button>
                                                                    </div>
                                                                    <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                        <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                        <a href="" class="icon">
                                                                            <i class="mdi mdi-lead-pencil"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                @else
                                                                    <div class="on">
                                                                        <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                            <?php
                                            $is_visible=false;

                                            if(!empty($formData->data->trafic)) {
                                                foreach($formData->data->trafic->value as $item) {
                                                    if(!empty($item->value)) {
                                                        $is_visible=true;
                                                    }
                                                }
                                            }
                                            ?>
                                            @if($is_visible)
                                                <div class="title is-6">
                                                    {{ $formData->data->trafic->fieldName }}
                                                    @if($formData->canEdit('trafic'))
                                                        <a href="" class="icon" v-on:click.prevent="toggle('trafic')" v-if="!isToggled('trafic')">
                                                            <i class="mdi mdi-lead-pencil"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('trafic') }">
                                                    <div class="off">
                                                        <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['trafic']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('trafic')">{{ translate('forms.field.cancel') }}</button>
                                                    </div>
                                                    <div class="on">
                                                        <table class="table specifications">
                                                            @foreach($formData->data->trafic->value as $traffic)
                                                                @if(!empty($traffic->value))
                                                                    <tr>
                                                                        <td>{{ $traffic->name }}</td>
                                                                        <td>{{ $traffic->value }}</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                @if(!empty($formData->data->documentsCompetitor->value['files']))
                                    <div class="tab-content" id="form-files" v-bind:class="{ 'hide':tabs.object!='files' }">
                                        <div class="title is-6" style="margin-top: 2rem">{{ translate('block.files') }}
                                            @if($formData->canEdit('documents_competitor'))
                                                <a href="" class="icon" v-on:click.prevent="toggle('documents_competitor')" v-if="!isToggled('documents_competitor')">
                                                    <i class="mdi mdi-lead-pencil"></i>
                                                </a>
                                            @endif
                                        </div>
                                        <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('documents_competitor') }">
                                            @if($formData->canEdit('documents_competitor'))
                                            <div class="off">
                                                <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['documents_competitor']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                <button class="button is-small is-cancel" v-on:click.prevent="toggle('documents_competitor')">{{ translate('forms.field.cancel') }}</button>
                                            </div>
                                            @endif
                                            <div class="on">
                                                <table class="table" style="width:100%">
                                                    @foreach($formData->data->documentsCompetitor->value['files'] as $item))
                                                        <tr>
                                                            <td>
                                                                <a target="_blank" href="{{ $item->url }}">{{ $item->name }}</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($user && $form->childrenForm)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='{{ $form->childrenForm->code }}' }">
                                        <rem-list :code="'{{ $form->childrenForm->code }}'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="true"></rem-list>
                                        <div style="text-align: center;">
                                            @if($form->childrenForm->can('create'))
                                                <a class="button is-success" href="{{ route('form.add', ['code' => $form->childrenForm->code, 'id' => $formData->id]) }}">{{ translate('forms.add') }}</a>
                                            @endif
                                            @if($form->canEdit)
                                                <a class="button is-info" style="margin-left:2rem" v-on:click.prevent="submitChildrenForms('{{ $form->childrenForm->code }}', {{ $formData->id }})">{{ translate('forms.submit') }}</a>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($formData->data->address->value))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='map' }">
                                        <h4 class="title is-6">
                                            {{ translate('forms.map-and-similar') }}
                                        </h4>
                                        <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('address') }">
                                            <div class="on">
                                                <rem-map :initial-filter-selected="['object', 'house', 'competitor']" :initial-center="[{{ $formData->data->address->value->lat }},{{ $formData->data->address->value->lng }}]" :initial-zoom="17" :is-autoload="false" :is-info-box="true" :is-autocomplete="false" :is-context-menu="true"></rem-map>

                                                <a class="button edit" style="margin: 10px 10px 0 0" href="{{ route('form.add', ['code' => 'house', 'id' => $formData->id]) }}">
                                                    <span class="icon">
                                                        <i class="mdi mdi-plus"></i>
                                                    </span>
                                                    <span>{{ translate('forms.house') }}</span>
                                                </a>
                                                <a class="button edit" style="margin-top: 10px" href="{{ route('form.add', ['code' => 'competitor', 'id' => $formData->id]) }}">
                                                    <span class="icon">
                                                        <i class="mdi mdi-plus"></i>
                                                    </span>
                                                    <span> {{ translate('forms.competitor') }}</span>
                                                </a>
                                            </div>
                                            @include('partials.form.similar')
                                        </div>
                                    </div>
                                @endif
                                @if($user && $form->canEdit)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='status' }">
                                        @if(!$lastStatus)
                                        <div class="columns is-gapless">
                                            <div class="column is-6">
                                                <rem-form :reload="true" :code="'status'" :id="{{ $formData->id }}" :api-schema-url="'/api/status/schema'" :api-save-url="'/api/status/save'" :button-label="'{{ translate('forms.status.change') }}'" :cancel-label="'{{ translate('forms.status.cancel') }}'"></rem-form>
                                            </div>
                                        </div>
                                        @endif
                                        @include('partials.form.statuses')
                                    </div>
                                @endif
                                @if(!empty($formData->type->autocompleteForm[0]->value))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='autocomplete' }">
                                        <table class="table specifications">
                                            @foreach($formData->type->autocompleteForm[0]->value->form->getFieldsForTable() as $field)
                                                <?php
                                                $data=@$formData->type->autocompleteForm[0]->value->data->{camel_case(str_replace('-', '_', $field))};
                                                ?>
                                                @if(!empty($data))
                                                    <tr>
                                                        <td>
                                                            {{ $data->fieldName }}
                                                        </td>
                                                        <td>
                                                            @if($formData->type->autocompleteForm[0]->value->canEdit($field))
                                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                                    <div class="off">
                                                                        <rem-form :reload="true" :code="'{{ $formData->type->autocompleteForm[0]->value->form->code }}'" :id="{{ $formData->type->autocompleteForm[0]->value->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                                    </div>
                                                                    <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                        <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                                        <a href="" class="icon">
                                                                            <i class="mdi mdi-lead-pencil"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            @else
                                                                <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                    <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                                    <a href="" class="icon">
                                                                        <i class="mdi mdi-lead-pencil"></i>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </table>
                                    </div>
                                @endif
                                @if($formData->form->is_history && $user->accessToHistory())
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='history' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.history')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($formData->form->relation_form)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='relation' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.relation')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
