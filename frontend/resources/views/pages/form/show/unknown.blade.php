@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if($formData)
        <div :set="currentTab='{{ $tab }}'">
            <div class="container">
                @include('partials/form/images', [
                    'data'=>@$formData->type->file[0]->value['images']
                ])
                <div class="columns is-multiline">
                    <div class="column is-12">
                        @include('partials.breadcrumbs')
                    </div>
                    <div class="column is-12">
                        @if(@$formData->currentStatusName)
                            <span v-on:click="tabs.object='status'" class="tag is-primary" style="background-color: {{ @$formData->currentStatus->color }};">{{ $formData->currentStatusName }}</span>
                        @endif
                            @if($user && $user->isAdmin() && !empty($formData->form->parent_form) && $formData->form->is_company_field == 2)
                                <div style="font-size: 14px; margin: 10px 0 10px 0;">Пользователи</div>
                                <form action="{{ route('forms.resave') }}" method="post" :set="showUsersAutocomplete=true">
                                    {!! csrf_field() !!}
                                    <input type="hidden" value="{{ $formData->id }}" name="fid">
                                    <input type="hidden" value="" name="uid" id="user_id">
                                    <div class="columns">
                                        <div class="column is-2">
                                            <div class="field-wrapper">
                                                <div class="control">
                                                    <input id="users" type="text" autocomplete="off"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column">
                                            <button type="submit" class="button edit">{{ translate('forms.resave.ok') }}</button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        <div class="columns">
                            <div class="column">
                                <div class="title">{{ $formData->getTitle() }}</div>
                            </div>
                            @if($form->canEdit)
                                <div class="column is-narrow">
                                    <button class="button edit" v-on:click.prevent="getFormEditHref('{{ route('form.edit', ['code' => $formData->form->code, 'id' => $formData->id]) }}')">{{ translate('forms.edit') }}</button>
                                </div>
                            @endif
                        </div>
                        
                        <div class="b-tabs">
                            <nav class="tabs">
                                <ul>
                                    <li v-bind:class="{ 'is-active': tabs.object=='common' }" v-on:click="clickTab('common')"><a><span>{{ translate('forms.tab.common') }}</span></a></li>
                                    @if(!empty($formData->type->map[0]->value->address))
                                        <li v-bind:class="{ 'is-active': tabs.object=='map' }" v-on:click="clickTab('map')"><a><span>{{ translate('forms.tab.map') }}</span></a></li>
                                    @endif
                                    @if(!empty($formData->type->file[0]->value['all']))
                                        <li v-bind:class="{ 'is-active': tabs.object=='files' }" v-on:click="clickTab('files')"><a><span>{{ translate('forms.tab.files') }}</span></a></li>
                                    @endif
                                    @if($user && !$form->childrenForms->isEmpty())
                                        @foreach($form->childrenForms as $childrenForm)
                                            <li v-bind:class="{ 'is-active': tabs.object=='{{ $childrenForm->code }}' }" v-on:click="clickTab('{{ $childrenForm->code }}')"><a><span>{{ $childrenForm->name }}</span></a></li>
                                        @endforeach
                                    @endif
                                    @if($user && $form->canEdit)
                                        <li v-bind:class="{ 'is-active': tabs.object=='status' }" v-on:click="clickTab('status')"><a><span>{{ translate('forms.tab.status') }}</span></a></li>
                                    @endif
                                    @if(!empty($formData->type->autocompleteForm[0]->value))
                                        <li v-bind:class="{ 'is-active': tabs.object=='autocomplete' }" v-on:click="clickTab('autocomplete')"><a><span>{{ $formData->type->autocompleteForm[0]->value->form->name }}</span></a></li>
                                    @endif
                                    @if($formData->form->is_history && $user && $user->accessToHistory())
                                        <li v-bind:class="{ 'is-active': tabs.object=='history' }" v-on:click="clickTab('history')"><a><span>{{ translate('forms.tab.history') }}</span></a></li>
                                    @endif
                                    @if($formData->form->relation_form)
                                        <li v-bind:class="{ 'is-active': tabs.object=='relation' }" v-on:click="clickTab('relation')"><a><span>{{ $formData->form->relationForm->name }}</span></a></li>
                                    @endif
                                    @if($user && $user->accessToCall() && $form->is_calls)
                                        <li v-bind:class="{ 'is-active': tabs.object=='calls' }" v-on:click="clickTab('calls')"><a><span>{{ translate('forms.tab.calls') }}</span></a></li>
                                    @endif
                                </ul>
                            </nav>
                            <section class="tab-content">
                                <div class="tab-content" v-bind:class="{ 'hide': tabs.object!='common' }">
                                    <div class="columns is-gapless">
                                        <div class="column is-8">
                                            <div class="title is-6">{{ translate('forms.details') }}</div>
                                            <table class="table specifications">
                                                @foreach($formData->form->getFieldsForTable() as $field)
                                                    <?php
                                                        $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                                    ?>
                                                    @if(!empty($data) && $data->value !== null)
                                                        <tr>
                                                            <td>
                                                                {{ $data->fieldName }}
                                                            </td>
                                                            <td>
                                                                @if($formData->canEdit($field))
                                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                                    <div class="off">
                                                                        <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                                    </div>
                                                                    <div class="on">
                                                                        @if($data->type == 'phone' && $user && $user->accessToCall())
                                                                            <a class="button is-success" v-on:click.prevent="call({{ $formData->id }}, '{{ $data->value }}', '')">
                                                                               <span class="icon is-small">
                                                                                 <i class="mdi mdi-phone" style="color:white;"></i>
                                                                               </span>
                                                                               <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                            </a>
                                                                        @else
                                                                        <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                        @endif
                                                                        <a href="" class="icon" v-on:click.prevent="toggle('{{ $field }}')">
                                                                            <i class="mdi mdi-lead-pencil"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                @else
                                                                    <div class="on">
                                                                        @if($data->type == 'phone' && $user && $user->accessToCall())
                                                                            <a class="button is-success" v-on:click.prevent="call({{ $formData->id }}, '{{ $data->value }}', '')">
                                                                               <span class="icon is-small">
                                                                                 <i class="mdi mdi-phone" style="color:white;"></i>
                                                                               </span>
                                                                               <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                            </a>
                                                                        @else
                                                                            <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                        @endif
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                            @if(!empty($formData->type->autocompleteForm[0]->value))
                                                <div class="title is-6">{{ $formData->type->autocompleteForm[0]->fieldName }}</div>
                                                @foreach([$formData->type->autocompleteForm[0]->fieldCode] as $field)
                                                    <?php
                                                    $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                                    ?>
                                                    @if(!empty($data) && $data->value !== null)
                                                        @if($formData->canEdit($field))
                                                        <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                            <div class="off">
                                                                <rem-form :reload="true" :code="'{{ $form->code }}'" :parent-code="'{{ $formData->type->autocompleteForm[0]->value->form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                            </div>
                                                            <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                                @if($formData->canEdit($field))
                                                                    <a href="" class="icon">
                                                                        <i class="mdi mdi-lead-pencil"></i>
                                                                    </a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @else
                                                            <div class="on">
                                                                <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                            </div>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @elseif(!empty($form->autocomplete) && $formData->canEdit($form->autocomplete->code))
                                                <div class="title is-6">{{ $form->autocomplete->name }}</div>
                                                @foreach([$form->autocomplete->code] as $field)
                                                    <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                        <div class="off">
                                                            <rem-form :reload="true" :code="'{{ $form->code }}'" :parent-code="'{{ $form->parentForm->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                            <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                        </div>
                                                        <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                            <span>{{ translate('forms.field.empty') }}</span>
                                                            @if($formData->canEdit($field))
                                                                <a href="" class="icon">
                                                                    <i class="mdi mdi-lead-pencil"></i>
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                            <?php
                                                $is_visible=false;

                                                if(!empty($formData->data->trafic)) {
                                                    foreach($formData->data->trafic->value as $item) {
                                                        if(!empty($item->value)) {
                                                            $is_visible=true;
                                                        }
                                                    }
                                                }
                                            ?>
                                            @if($is_visible)
                                                <div class="title is-6">
                                                    {{ $formData->data->trafic->fieldName }}
                                                    @if($formData->canEdit('trafic'))
                                                    <a href="" class="icon" v-on:click.prevent="toggle('trafic')" v-if="!isToggled('trafic')">
                                                        <i class="mdi mdi-lead-pencil"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('trafic') }">
                                                    <div class="off">
                                                        <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['trafic']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('trafic')">{{ translate('forms.field.cancel') }}</button>
                                                    </div>
                                                    <div class="on">
                                                        <table class="table specifications">
                                                            @foreach($formData->data->trafic->value as $traffic)
                                                                @if($traffic->value)
                                                                    <tr>
                                                                        <td>{{ $traffic->name }}</td>
                                                                        <td>{{ $traffic->value }}</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($formData->type->file[0]->value['all']))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='files' }">
                                        <div class="field-toggle{{ $formData->canEdit($formData->type->file[0]->fieldCode, true) ? ' toggled' : '' }}">
                                            @if($formData->canEdit($formData->type->file[0]->fieldCode, true))
                                                <h4 class="title is-6">
                                                    {{ translate('forms.docs.add') }}
                                                </h4>
                                                <div style="margin: 1rem 0" class="page-object-documents">
                                                    <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $formData->type->file[0]->fieldCode }}']" :button-class="'is-success is-blue'" :button-label="'{{ translate('forms.field.save') }}'" :cancel-label="'{{ translate('forms.field.cancel') }}'"></rem-form>
                                                </div>
                                            @endif
                                            <h4 class="title is-6">
                                                {{ translate('forms.docs') }}
                                            </h4>
                                            <div class="columns is-gapless">
                                                <div class="column is-6">
                                                    <table class="table comments">
                                                        @if(!empty($formData->type->file[0]->value['all']))
                                                            @foreach($formData->type->file[0]->value['all'] as $item)
                                                                <tr>
                                                                    <td>
                                                                        <div class="columns is-gapless">
                                                                            <div class="column">
                                                                                <div>
                                                                                    <a target="_blank" href="{{ $item->url }}">
                                                                                        <i class="mdi mdi-24px mdi-{{ $item->type }}"></i>
                                                                                        {{ $item->name }}
                                                                                    </a>
                                                                                </div>
                                                                                <div class="content">
                                                                                    <small>
                                                                                        {{ $item->dt }}, {{ @$item->user->name }}
                                                                                    </small>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($formData->type->map[0]->value))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='map' }">
                                        <h4 class="title is-6">
                                            {{ translate('forms.map-and-similar') }}
                                        </h4>
                                        <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('address') }">
                                            <div class="on">
                                                <rem-map :initial-center="[{{ $formData->type->map[0]->value->lat }},{{ $formData->type->map[0]->value->lng }}]" :initial-zoom="17" :is-autoload="false" :is-info-box="true" :is-autocomplete="false" :info-window="'{{ $formData->type->map[0]->value->address }}'" :is-context-menu="true"></rem-map>
                                            </div>
                                            @include('partials.form.similar')
                                        </div>
                                    </div>
                                @endif
                                @if($user && !$form->childrenForms->isEmpty())
                                    @foreach($form->childrenForms as $childrenForm)
                                        <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='{{ $childrenForm->code }}' }">
                                            <rem-list
                                                    @if($childrenForm->can('create')) :add-url-text="'{{ translate('forms.add') }}'" :add-url="'{{ route('form.add', ['code' => $childrenForm->code, 'id' => $formData->id]) }}'" @endif
                                                    :code="'{{ $childrenForm->code }}'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="true"></rem-list>
                                        </div>
                                    @endforeach
                                @endif
                                @if($user && $form->canEdit)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='status' }">
                                        @if(!$lastStatus)
                                        <div class="columns is-gapless">
                                            <div class="column is-6">
                                                <rem-form :reload="true" :code="'status'" :id="{{ $formData->id }}" :api-schema-url="'/api/status/schema'" :api-save-url="'/api/status/save'" :button-label="'{{ translate('forms.status.change') }}'" :cancel-label="'{{ translate('forms.status.cancel') }}'"></rem-form>
                                            </div>
                                        </div>
                                        @endif
                                        @include('partials.form.statuses')
                                    </div>
                                @endif
                                @if(!empty($formData->type->autocompleteForm[0]->value))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='autocomplete' }">
                                        <rem-list :code="'{{ $formData->form->code }}-autocomplete'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="true"></rem-list>
                                    </div>
                                @endif
                                @if($formData->form->is_history && $user && $user->accessToHistory())
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='history' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.history')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($formData->form->relation_form)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='relation' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.relation')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($user && $user->accessToCall() && $form->is_calls)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='calls' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.calls')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </section>
                        </div>                            
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
