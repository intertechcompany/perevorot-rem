@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if($formData)
        <div :set="currentTab='{{ $tab }}'">
            <div class="container">
                <div class="columns is-gapless">
                    @include('partials.breadcrumbs')
                </div>
                @include('partials/form/images', [
                    'data'=>@$formData->data->objectDocuments
                ])
                <div class="columns is-multiline">
                    <div class="column is-12">
                        @if(@$formData->currentStatusName)
                            <span v-on:click="tabs.object='status'" class="is-status-tag tag is-primary" style="background-color: {{ @$formData->currentStatus->color }};">{{ $formData->currentStatusName }}</span>
                        @endif
                        <div class="columns">
                            <div class="column">
                                @if(!empty($formData->data->number->value))
                                    <div class="title">{{ $formData->data->number->value.' - '.$formData->getTitle() }}</div>
                                @else
                                    <div class="title">{{ $formData->getTitle() }}</div>
                                @endif
                            </div>
                            @if($form->canEdit)
                            <div class="column is-narrow">
                                <a class="button edit" href="{{ route('form.edit', ['code' => $formData->form->code, 'id' => $formData->id]) }}">{{ translate('forms.edit') }}</a>
                            </div>
                            @endif
                        </div>
                        <div class="b-tabs">
                            <nav class="tabs">
                                <ul>
                                    <li v-bind:class="{ 'is-active': tabs.object=='common' }" v-on:click="clickTab('common')"><a><span>{{ translate('forms.tab.common') }}</span></a></li>
                                    @if(!empty($formData->data->address->value))
                                        <li v-bind:class="{ 'is-active': tabs.object=='map' }" v-on:click="clickTab('map')"><a><span>{{ translate('forms.tab.map') }}</span></a></li>
                                    @endif
                                    @if(!empty($settings->calc))
                                        <li v-bind:class="{ 'is-active': tabs.object=='calc' }" v-on:click="clickTab('calc')"><a><span>{{ translate('forms.tab.calc') }}</span></a></li>
                                    @endif
                                    @if($user && !$form->childrenForms->isEmpty())
                                        @foreach($form->childrenForms as $childrenForm)
                                            <li v-bind:class="{ 'is-active': tabs.object=='{{ $childrenForm->code }}' }" v-on:click="clickTab('{{ $childrenForm->code }}')"><a><span>{{ $childrenForm->name }}</span></a></li>
                                        @endforeach
                                    @endif
                                    @if(!empty($formData->type->file[0]->value['all']))
                                        <li v-bind:class="{ 'is-active': tabs.object=='files' }" v-on:click="clickTab('files')"><a><span>{{ translate('forms.tab.files') }}</span></a></li>
                                    @endif
                                    @if($user && $form->canEdit)
                                        <li v-bind:class="{ 'is-active': tabs.object=='status' }" v-on:click="clickTab('status')"><a><span>{{ translate('forms.tab.status') }}</span></a></li>
                                    @endif
                                    @if(@$form->autocomplete->show_in == 1 && !empty($formData->type->autocompleteForm[0]->value))
                                        <li v-bind:class="{ 'is-active': tabs.object=='autocomplete' }" v-on:click="clickTab('autocomplete')"><a><span>{{ $formData->type->autocompleteForm[0]->value->form->name }}</span></a></li>
                                    @endif
                                    @if($user && $formData->form->is_history && $user->accessToHistory())
                                    <li v-bind:class="{ 'is-active': tabs.object=='history' }" v-on:click="clickTab('history')"><a><span>{{ translate('forms.tab.history') }}</span></a></li>
                                    @endif
                                    @if($formData->form->relation_form)
                                        <li v-bind:class="{ 'is-active': tabs.object=='relation' }" v-on:click="clickTab('relation')"><a><span>{{ $formData->form->relationForm->name }}</span></a></li>
                                    @endif
                                </ul>
                            </nav>
                            <section class="tab-content">
                                <div class="tab-content" v-bind:class="{ 'hide': tabs.object!='common' }">
                                    <div class="columns is-gapless">
                                        <div class="column is-8">
                                            <div class="title is-6">{{ translate('forms.details') }}</div>
                                            <table class="table specifications">
                                                @foreach($formData->form->getFieldsForTable() as $field)
                                                    <?php
                                                        if($field == 'turnover-estimation') { continue; }

                                                        $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                                    ?>
                                                    @if(!empty($data) && $data->value !== null)
                                                        <tr>
                                                            <td>
                                                                {{ $data->fieldName }}
                                                            </td>
                                                            <td>
                                                                @if($formData->canEdit($field))
                                                                    <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                                        <div class="off">
                                                                            <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                            <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                                        </div>
                                                                        <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                            <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                            <a href="" class="icon">
                                                                                <i class="mdi mdi-lead-pencil"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="on">
                                                                        <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                <tr>
                                                    <td>
                                                        {{ translate('forms.calc') }}
                                                    </td>
                                                    <td>
                                                        <span>{{ !empty($formData->data->turnoverEstimation) ? $formData->data->turnoverEstimation->value : translate('forms.calc.empty') }}</span>
                                                        @if($formData->canEdit('turnoverEstimation'))
                                                        <a href="" class="icon" v-on:click.prevent="tabs.object='calc'">
                                                            <i class="mdi mdi-lead-pencil"></i>
                                                        </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                {{-- @if(!empty($formData->data->competitor->value))
                                                    <tr>
                                                        <td>{{ $formData->data->competitor->fieldName }}</td>
                                                        <td>{{ $formData->data->competitor->value->name }}
                                                            @if(!empty($formData->data->competitor->value->image))
                                                                <img src="{{ $formData->data->competitor->value->image }}">
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif --}}
                                            </table>

                                            @if(@$form->autocomplete->show_in == 2 && !empty($formData->type->autocompleteForm[0]->value))
                                                @if(@$form->autocomplete->is_equivalent)
                                                    <?php $_formData = $formData->type->autocompleteForm[0]->value; ?>
                                                    <div style="margin-top: 20px;">
                                                        <div class="title is-6">
                                                            <p>{{ translate('forms.equivalent') }}</p><br>
                                                            <a class="is-3" href="{{ route('form.show', ['code'=>$_formData->form->code,'id'=>$_formData->id]) }}">{{ $_formData->getTitle() }}</a>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($formData->getAutocompleteFormsData() as $_formData)
                                                        <div style="margin-top: 20px;">
                                                            <div class="title is-6">
                                                                <a href="{{ route('form.show', ['code'=>$_formData->form->code,'id'=>$_formData->id]) }}">{{ $_formData->getTitle() }}</a>
                                                            </div>
                                                            @if(!@$form->autocomplete->is_equivalent)
                                                            <table class="table specifications">
                                                                @foreach($_formData->form->getFieldsForTable() as $field)
                                                                    <?php
                                                                    $data=@$_formData->data->{camel_case(str_replace('-', '_', $field))};
                                                                    ?>
                                                                    @if(!empty($data) && $data->value !== null)
                                                                        <tr>
                                                                            <td>
                                                                                {{ $data->fieldName }}
                                                                            </td>
                                                                            <td>
                                                                                <div class="field-toggle">
                                                                                    <div class="on">
                                                                                        <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            </table>
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                @endif
                                            @endif

                                            <?php
                                                $is_visible=false;

                                                if(!empty($formData->data->trafic)) {
                                                    foreach($formData->data->trafic->value as $item) {
                                                        if(!empty($item->value)) {
                                                            $is_visible=true;
                                                        }
                                                    }
                                                }
                                            ?>
                                            @if($is_visible)
                                                <div class="title is-6">
                                                    {{ $formData->data->trafic->fieldName }}
                                                    @if($formData->canEdit('trafic'))
                                                    <a href="" class="icon" v-on:click.prevent="toggle('trafic')" v-if="!isToggled('trafic')">
                                                        <i class="mdi mdi-lead-pencil"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('trafic') }">
                                                    <div class="off">
                                                        <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['trafic']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('trafic')">{{ translate('forms.field.cancel') }}</button>
                                                    </div>
                                                    <div class="on">
                                                        <table class="table specifications">
                                                            @foreach($formData->data->trafic->value as $traffic)
                                                                @if(!empty($traffic->value))
                                                                    <tr>
                                                                        <td>{{ $traffic->name }}</td>
                                                                        <td>{{ $traffic->value }}</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($formData->type->file[0]->value['all']))
                                    <div class="tab-content" id="form-files" v-bind:class="{ 'hide':tabs.object!='files' }">
                                        <h4 class="title is-6">
                                            {{ translate('forms.docs') }}
                                        </h4>
                                        <div class="field-toggle{{ $formData->canEdit('object-documents', true) ? ' toggled' : '' }}">
                                            <div class="columns is-gapless">
                                                <div class="column is-6">
                                                    <table class="table comments">
                                                        @if(!empty($formData->type->file[0]->value['all']))
                                                            @foreach($formData->type->file[0]->value['all'] as $item)
                                                                <tr>
                                                                    <td v-bind:class="{ 'is-toggled': showCommentsState[{{ $item->id }}] }">
                                                                        <div class="columns is-gapless">
                                                                            <div class="column">
                                                                                <div>
                                                                                    <a target="_blank" href="{{ $item->url }}">
                                                                                        <i class="mdi mdi-24px mdi-{{ $item->type }}"></i>
                                                                                        {{ $item->name }}
                                                                                    </a>
                                                                                </div>
                                                                                <div class="content">
                                                                                    <small>
                                                                                        {{ $item->dt }}, {{ @$item->user->name }}
                                                                                    </small>
                                                                                </div>
                                                                            </div>
                                                                            <div class="column is-narrow">
                                                                                <a class="button" v-on:click.prevent="showComments({{ $item->id }})">
                                                                                    <span class="icon is-small">
                                                                                        <i class="mdi mdi-12px mdi-comment-outline"></i>
                                                                                    </span>
                                                                                    <span>
                                                                                        <span v-if="!showCommentsState[{{ $item->id }}]">{{ !empty($formCommentsCount[$item->id]) ? translate('form.comments.show').', '.$formCommentsCount[$item->id] : translate('form.comments.addcomment') }}</span>
                                                                                        <span v-if="showCommentsState[{{ $item->id }}]">{{ translate('form.comments.hide') }}</span>
                                                                                    </span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-comments">
                                                                            @if(!$formComments->isEmpty())
                                                                                @foreach($formComments as $comment)
                                                                                    @if(empty($comment->data->parentCommentId) && @$comment->data->parentFileId->value == $item->id)
                                                                                        <article class="media">
                                                                                            <figure class="media-left">
                                                                                                @if(!empty($comment->user->avatar))
                                                                                                <p class="image is-32x32">
                                                                                                    <img src="{{ $comment->user->avatar }}" style="border-radius: 100%">
                                                                                                </p>
                                                                                                @endif
                                                                                            </figure>
                                                                                            <div class="media-content">
                                                                                                <div class="content">
                                                                                                    <p>
                                                                                                        <strong>{{ @$comment->user->name }}</strong>
                                                                                                        <br>
                                                                                                        {!! nl2br($comment->data->comment->value) !!}
                                                                                                        <br>
                                                                                                        <small>{{ $comment->created_at }}</small>
                                                                                                    </p>
                                                                                                </div>

                                                                                                @foreach($formComments as $_comment)
                                                                                                    @if($comment->id == @$_comment->data->parentCommentId->value)
                                                                                                        @include('partials.form.comment', ['comment'=>$_comment])
                                                                                                    @endif
                                                                                                    @if($loop->last)
                                                                                                        <article class="media">
                                                                                                            <figure class="media-left">
                                                                                                                @if(!empty($user->avatar))
                                                                                                                    <p class="image is-32x32">
                                                                                                                        <img src="{{ $user->avatar }}" style="border-radius: 100%">
                                                                                                                    </p>
                                                                                                                @endif
                                                                                                            </figure>
                                                                                                            <div class="media-content">
                                                                                                                <rem-form :reload="true" :code="'comments'" :button-class="'no-margin'" :button-label="'{{ translate('forms.comment.answer') }}'" :preset-values="{ parent_file_id: {{ $item->id }}, parent_comment_id: {{ $comment->id }} }"></rem-form>
                                                                                                            </div>
                                                                                                        </article>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </article>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                            <article class="media">
                                                                                <figure class="media-left">
                                                                                    @if(!empty($user->avatar))
                                                                                        <p class="image is-32x32">
                                                                                            <img src="{{ $user->avatar }}" style="border-radius: 100%">
                                                                                        </p>
                                                                                    @endif
                                                                                </figure>
                                                                                <div class="media-content">
                                                                                    <rem-form :reload="true" :code="'comments'" :button-class="'no-margin'" :button-label="'{{ translate('forms.comment.submit') }}'" :preset-values="{ parent_file_id: {{ $item->id }} }"></rem-form>
                                                                                </div>
                                                                            </article>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                </div>
                                            </div>
                                            @if($formData->canEdit('object-documents', true))
                                                <h4 class="title is-6">
                                                    {{ translate('forms.docs-add') }}
                                                </h4>
                                                <div style="margin: 1rem 0" class="page-object-documents">
                                                    <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['object-documents']" :button-class="'is-success'" :button-label="'{{ translate('forms.field.save') }}'" :cancel-label="'{{ translate('forms.field.cancel') }}'"></rem-form>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                {{--@if($user && $form->childrenForm)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='{{ $form->childrenForm->code }}' }">
                                        <rem-list :code="'{{ $form->childrenForm->code }}'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="true"></rem-list>
                                        @if(!empty($formData->data->costsTotal->value))
                                            <div class="title is-6">
                                                {{ translate('forms.costs-total') }} <span class="title is-6"> {{ $formData->data->costsTotal->value }} {{ translate('forms.currency') }}</span>
                                            </div>
                                        @endif
                                        <div style="text-align: center;">
                                            @if($form->childrenForm->can('create'))
                                            <a class="button is-success" href="{{ route('form.add', ['code' => $form->childrenForm->code, 'id' => $formData->id]) }}">{{ translate('forms.add') }}</a>
                                            @endif
                                            @if($form->canEdit)
                                            <a class="button is-info" style="margin-left:2rem" v-on:click.prevent="submitChildrenForms('{{ $form->childrenForm->code }}', {{ $formData->id }})">{{ translate('forms.submit') }}</a>
                                            @endif
                                        </div>
                                    </div>
                                @endif--}}
                                @if($user && !$form->childrenForms->isEmpty())
                                    @foreach($form->childrenForms as $childrenForm)
                                        <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='{{ $childrenForm->code }}' }">
                                            <rem-list :code="'{{ $childrenForm->code }}'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="true"></rem-list>
                                            @if(!empty($formData->data->costsTotal->value) && $childrenForm->code == 'works')
                                                <div class="title is-6">
                                                    {{ translate('forms.costs-total') }} <span class="title is-6"> {{ $formData->data->costsTotal->value }} {{ translate('forms.currency') }}</span>
                                                </div>
                                            @endif
                                            <div style="text-align: center;">
                                                @if($childrenForm->can('create'))
                                                    <a class="button is-success" href="{{ route('form.add', ['code' => $childrenForm->code, 'id' => $formData->id]) }}">{{ translate('forms.add') }}</a>
                                                @endif
                                                @if($form->canEdit && $childrenForm->code == 'works')
                                                    <a class="button is-info" style="margin-left:2rem" v-on:click.prevent="submitChildrenForms('{{ $childrenForm->code }}', {{ $formData->id }})">{{ translate('forms.submit') }}</a>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @if(!empty($formData->data->address->value))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='map' }">
                                        <h4 class="title is-6">
                                            {{ translate('forms.map-and-similar') }}
                                        </h4>
                                        <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('address') }">
                                            <div class="on">
                                                <rem-map :initial-filter-selected="['object', 'house', 'competitor']" :form-data-id="{{ $formData->id }}" :initial-center="[{{ $formData->data->address->value->lat }},{{ $formData->data->address->value->lng }}]" :initial-zoom="17" :is-autoload="false" :is-info-box="true" :is-autocomplete="false" :is-context-menu="true"></rem-map>
                                                
                                                <a class="button edit" style="margin: 10px 10px 0 0" href="{{ route('form.add', ['code' => 'house', 'id' => $formData->id]) }}">
                                                    <span class="icon">
                                                        <i class="mdi mdi-plus"></i>
                                                    </span>
                                                    <span>{{ translate('forms.house') }}</span>
                                                </a>
                                                <a class="button edit" style="margin-top: 10px" href="{{ route('form.add', ['code' => 'competitor', 'id' => $formData->id]) }}">
                                                    <span class="icon">
                                                        <i class="mdi mdi-plus"></i>
                                                    </span>
                                                    <span> {{ translate('forms.competitor') }}</span>
                                                </a>
                                            </div>
                                            @include('partials.form.similar')
                                        </div>
                                    </div>
                                @endif
                                @if($user && $form->canEdit)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='status' }">
                                        @if(!$lastStatus)
                                        <div class="columns is-gapless">
                                            <div class="column is-6">
                                                <rem-form :reload="true" :code="'status'" :id="{{ $formData->id }}" :api-schema-url="'/api/status/schema'" :api-save-url="'/api/status/save'" :button-label="'{{ translate('forms.status.change') }}'" :cancel-label="'{{ translate('forms.status.cancel') }}'"></rem-form>
                                            </div>
                                        </div>
                                        @endif
                                        @include('partials.form.statuses')
                                    </div>
                                @endif
                                @if(!empty($settings->calc))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='calc' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.calc')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if(@$form->autocomplete->show_in == 1 && !empty($formData->type->autocompleteForm[0]->value))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='autocomplete' }">
                                        <table class="table specifications">
                                            @foreach($formData->type->autocompleteForm[0]->value->form->getFieldsForTable() as $field)
                                                <?php
                                                $data=@$formData->type->autocompleteForm[0]->value->data->{camel_case(str_replace('-', '_', $field))};
                                                ?>
                                                @if(!empty($data))
                                                    <tr>
                                                        <td>
                                                            {{ $data->fieldName }}
                                                        </td>
                                                        <td>
                                                            @if($formData->type->autocompleteForm[0]->value->canEdit($field))
                                                            <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                                <div class="off">
                                                                    <rem-form :reload="true" :code="'{{ $formData->type->autocompleteForm[0]->value->form->code }}'" :id="{{ $formData->type->autocompleteForm[0]->value->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                    <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                                </div>
                                                                <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                    <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                                    <a href="" class="icon">
                                                                        <i class="mdi mdi-lead-pencil"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            @else
                                                                <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                    <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                                    <a href="" class="icon">
                                                                        <i class="mdi mdi-lead-pencil"></i>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </table>
                                    </div>
                                @endif
                                @if($user && $formData->form->is_history && $user->accessToHistory())
                                <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='history' }">
                                    <div class="columns is-gapless">
                                        <div class="column is-12">
                                            @include('partials.form.history')
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($formData->form->relation_form)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='relation' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.relation')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </section>
                        </div>                            
                    </div>
                </div>
            </div>


            {{-- @if(!empty($formData->data->address->value))
                <div class="container">
                    <div class="columns">
                        <div class="column is-12">
                            <h4 class="title is-4" style="margin-top: 2rem">Расположение и соседние объекты</h4>
                            <rem-map :initial-center="[{{ $formData->data->address->value->lat }},{{ $formData->data->address->value->lng }}]" :initial-zoom="18" :is-autoload="false" :is-autocomplete="false" :info-window="'{{ $formData->data->address->value->address }}'"></rem-map>
                        </div>
                    </div>
                </div>
            @endif

            <div class="container">
                <div class="columns">
                    <div class="column is-6">
                        @include('partials.form.similar')
                        <a class="button is-success edit" style="margin-top:2rem" href="{{ route('form.edit', ['code' => $formData->form->code, 'id' => $formData->id]) }}">Редактировать</a>
                    </div>
                </div>
            </div> --}}
        </div>
    @endif
@endsection
