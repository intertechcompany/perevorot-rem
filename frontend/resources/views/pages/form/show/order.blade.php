@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if($formData)
        <div :set="currentTab='{{ $tab }}'">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-12">
                        @include('partials.breadcrumbs')
                    </div>
                    <div class="column is-12">
                        @if(@$formData->currentStatusName)
                        <span v-on:click="tabs.object='status'" class="tag is-primary" style="background-color: {{ @$formData->currentStatus->color }};">{{ $formData->currentStatusName }}</span>
                        @endif
                        <div class="columns" style="margin-top:1rem">
                            <div class="column">
                                @if(!empty($formData->data->name->value))
                                    <div class="title has-text-primary">{{ $formData->data->name->value }}</div>
                                @endif
                            </div>
                            @if($form->canEdit)
                            <div class="column is-narrow">
                                <a class="button edit" href="{{ route('form.edit', ['code' => $formData->form->code, 'id' => $formData->id]) }}"> {{ translate('forms.edit') }}</a>
                            </div>
                            @endif
                        </div>
                        
                        <div class="b-tabs">
                            <nav class="tabs">
                                <ul>
                                    <li v-bind:class="{ 'is-active': tabs.object=='common' }" v-on:click="clickTab('common')"><a><span>{{ translate('forms.tab.common') }}</span></a></li>
                                    @if(!empty($formData->type->file[0]->value['all']))
                                        <li v-bind:class="{ 'is-active': tabs.object=='files' }" v-on:click="clickTab('files')"><a><span>{{ translate('forms.tab.files') }}</span></a></li>
                                    @endif
                                    @if($user && !$form->childrenForms->isEmpty())
                                        @foreach($form->childrenForms as $childrenForm)
                                        <li v-bind:class="{ 'is-active': tabs.object=='{{ $childrenForm->code }}' }" v-on:click="clickTab('{{ $childrenForm->code }}')"><a><span>{{ $childrenForm->name }}</span></a></li>
                                        @endforeach
                                    @endif
                                    @if($user && $form->canEdit)
                                        <li v-bind:class="{ 'is-active': tabs.object=='status' }" v-on:click="clickTab('status')"><a><span>{{ translate('forms.tab.status') }}</span></a></li>
                                    @endif
                                    @if(!empty($formData->type->autocompleteForm[0]->value))
                                        <li v-bind:class="{ 'is-active': tabs.object=='autocomplete' }" v-on:click="clickTab('autocomplete')"><a><span>{{ $formData->type->autocompleteForm[0]->value->form->name }}</span></a></li>
                                    @endif
                                    @if($formData->form->is_history && $user && $user->accessToHistory())
                                        <li v-bind:class="{ 'is-active': tabs.object=='history' }" v-on:click="clickTab('history')"><a><span>{{ translate('forms.tab.history') }}</span></a></li>
                                    @endif
                                    @if($formData->form->relation_form)
                                        <li v-bind:class="{ 'is-active': tabs.object=='relation' }" v-on:click="clickTab('relation')"><a><span>{{ $formData->form->relationForm->name }}</span></a></li>
                                    @endif
                                </ul>
                            </nav>
                            <section class="tab-content">
                                <div class="tab-content" v-bind:class="{ 'hide': tabs.object!='common' }">
                                    <div class="columns is-gapless">
                                        <div class="column is-8">
                                            <div class="title is-6">{{ translate('forms.details') }}</div>
                                            <table class="table specifications">
                                                @foreach($formData->form->getFieldsForTable() as $field)
                                                    <?php
                                                        $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                                    ?>
                                                    @if(!empty($data) && $data->value !== null)
                                                        <tr>
                                                            <td>
                                                                {{ $data->fieldName }}
                                                            </td>
                                                            <td>
                                                                @if($formData->canEdit($field))
                                                                    <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                                        <div class="off">
                                                                            <rem-form :reload="true" :code="'{{ $form->code }}'" :parent-code="'{{ @$formData->type->autocompleteForm[0]->value->form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                            <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                                        </div>
                                                                        <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                            <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : (!empty($data->value) ? $data->value : $data->__original_value)) !!}</span>
                                                                            <a href="" class="icon">
                                                                                <i class="mdi mdi-lead-pencil"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="on">
                                                                        <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : (!empty($data->value) ? $data->value : $data->__original_value)) !!}</span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                            @if(!empty($formData->type->autocompleteForm[0]->value))
                                                <div class="title is-6">{{ $formData->type->autocompleteForm[0]->fieldName }}</div>
                                                @foreach([$formData->type->autocompleteForm[0]->fieldCode] as $field)
                                                    <?php
                                                    $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                                    ?>
                                                    @if(!empty($data) && $data->value !== null)
                                                        @if($formData->canEdit($field))
                                                        <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                            <div class="off">
                                                                <rem-form :reload="true" :code="'{{ $form->code }}'" :parent-code="'{{ $formData->type->autocompleteForm[0]->value->form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                                <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                            </div>
                                                            <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                                <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                                <a href="" class="icon">
                                                                    <i class="mdi mdi-lead-pencil"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        @else
                                                            <div class="on">
                                                                <span>{{ !empty($data->value->name) ? $data->value->name : $data->value }}</span>
                                                            </div>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                            <?php
                                                $is_visible=false;

                                                if(!empty($formData->data->trafic)) {
                                                    foreach($formData->data->trafic->value as $item) {
                                                        if(!empty($item->value)) {
                                                            $is_visible=true;
                                                        }
                                                    }
                                                }
                                            ?>
                                            @if($is_visible)
                                                <div class="title is-6">
                                                    {{ $formData->data->trafic->fieldName }}
                                                    @if($formData->canEdit('trafic'))
                                                    <a href="" class="icon" v-on:click.prevent="toggle('trafic')" v-if="!isToggled('trafic')">
                                                        <i class="mdi mdi-lead-pencil"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('trafic') }">
                                                    <div class="off">
                                                        <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['trafic']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('trafic')">{{ translate('forms.field.cancel') }}</button>
                                                    </div>
                                                    <div class="on">
                                                        <table class="table specifications">
                                                            @foreach($formData->data->trafic->value as $traffic)
                                                                @if($traffic->value)
                                                                    <tr>
                                                                        <td>{{ $traffic->name }}</td>
                                                                        <td>{{ $traffic->value }}</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($formData->type->file[0]->value['all']))
                                    <div class="tab-content" id="form-files" v-bind:class="{ 'hide':tabs.object!='files' }">
                                        @if(!empty($formData->type->file[0]->value['all']))
                                            <div class="title is-6" style="margin-top: 2rem">{{ translate('block.files') }}
                                                @if($formData->canEdit('documents', true))
                                                    <a href="" class="icon" v-on:click.prevent="toggle('documents')" v-if="!isToggled('documents')">
                                                        <i class="mdi mdi-lead-pencil"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('documents') }">
                                                @if($formData->canEdit('documents', true))
                                                <div class="off">
                                                    <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['documents']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                    <button class="button is-small is-cancel" v-on:click.prevent="toggle('documents')">{{ translate('forms.field.cancel') }}</button>
                                                </div>
                                                @endif
                                                <div class="on">
                                                    <table class="table" style="width:100%">
                                                        @foreach($formData->type->file[0]->value['all'] as $item)
                                                            <tr>
                                                                <td>
                                                                    <div class="columns is-gapless">
                                                                        <div class="column">
                                                                            <div>
                                                                                <a target="_blank" href="{{ $item->url }}">
                                                                                    <i class="mdi mdi-24px mdi-{{ $item->type }}"></i>
                                                                                    {{ $item->name }}
                                                                                </a>
                                                                            </div>
                                                                            <div class="content">
                                                                                <small>
                                                                                    {{ $item->dt }}, {{ @$item->user->name }}
                                                                                </small>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endif
                                @if($user && !$form->childrenForms->isEmpty())
                                    @foreach($form->childrenForms as $childrenForm)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='{{ $childrenForm->code }}' }">
                                        <rem-list @if($childrenForm->can('create')) :add-url-text="'{{ translate('forms.add') }}'" :add-url="'{{ route('form.add', ['code' => $childrenForm->code, 'id' => $formData->id]) }}'" @endif
                                            :code="'{{ $childrenForm->code }}'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="true"></rem-list>
                                    </div>
                                    @endforeach
                                @endif
                                @if($user && $form->canEdit)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='status' }">
                                        @if(!$lastStatus)
                                        <div class="columns is-gapless">
                                            <div class="column is-6">
                                                <rem-form :reload="true" :code="'status'" :id="{{ $formData->id }}" :api-schema-url="'/api/status/schema'" :api-save-url="'/api/status/save'" :button-label="'{{ translate('forms.status.change') }}'" :cancel-label="'{{ translate('forms.status.cancel') }}'"></rem-form>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                @endif
                                @if(!empty($formData->type->autocompleteForm[0]->value))
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='autocomplete' }">
                                        <rem-list :code="'{{ $formData->form->code }}-autocomplete'" :id="{{ $formData->id }}" :show-title="false" :is-edit-click="false" :show-filter="true"></rem-list>
                                    </div>
                                @endif
                                @if($formData->form->is_history && $user && $user->accessToHistory())
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='history' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.history')
                                            </div>
                                        </div>
                                        @include('partials.form.statuses')
                                    </div>
                                @endif
                                @if($formData->form->relation_form)
                                    <div class="tab-content" v-bind:class="{ 'hide':tabs.object!='relation' }">
                                        <div class="columns is-gapless">
                                            <div class="column is-12">
                                                @include('partials.form.relation')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </section>
                        </div>                            
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
