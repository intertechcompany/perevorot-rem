@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if($formData)
        <div class="tab-content">
            <div class="container">
                @include('partials/form/images', [
                    'data'=>@$formData->data->documents
                ])
                @if(@$formData->currentStatusName)
                    <span class="tag is-primary" style="background-color: {{ @$formData->currentStatus->color }};">{{ $formData->currentStatusName }}</span>
                @endif
                <div class="columns">
                    <div class="column is-6">
                        <div class="title">{{ $formData->getTitle() }}</div>
                        {{-- <a class="button edit" href="{{ route('form.add', ['code' => $form->code, 'id' => $formData->id]) }}">Добавить</a> --}}
                        @if($form->canEdit)
                            <button class="button edit" v-on:click.prevent="getFormEditHref('{{ route('form.edit', ['code' => $formData->form->code, 'id' => $formData->id]) }}')">{{ translate('forms.edit') }}</button>
                        @endif
                        
                        <h4 class="title is-4" style="margin-top: 2rem">{{ translate('forms.details') }}</h4>

                            <table class="table" style="width:100%">
                                @foreach(['zone', 'apartments', 'square'] as $field)
                                    <?php
                                    $data=@$formData->data->{camel_case(str_replace('-', '_', $field))};
                                    ?>
                                    @if(!empty($data))
                                        <tr>
                                            <td>
                                                {{ $data->fieldName }}
                                            </td>
                                            <td>
                                                @if($formData->canEdit($field))
                                                <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('{{ $field }}') }">
                                                    <div class="off">
                                                        <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['{{ $field }}']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                                        <button class="button is-small is-cancel" v-on:click.prevent="toggle('{{ $field }}')">{{ translate('forms.field.cancel') }}</button>
                                                    </div>
                                                    <div class="on" v-on:click.prevent="toggle('{{ $field }}')">
                                                        <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                        <a href="" class="icon">
                                                            <i class="mdi mdi-lead-pencil"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                @else
                                                    <div class="on">
                                                        <span>{!! !empty($data->value->name) ? $data->value->name : (!empty($data->formatValue) ? $data->formatValue : $data->value) !!}</span>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>

                        @if(!empty($formData->data->documents->value['files']))                        
                            <div class="title is-6" style="margin-top: 2rem">{{ translate('block.files') }}
                                @if($formData->canEdit('documents'))
                                <a href="" class="icon" v-on:click.prevent="toggle('documents')" v-if="!isToggled('documents')">
                                    <i class="mdi mdi-lead-pencil"></i>
                                </a>
                                @endif
                            </div>
                            <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('documents') }">
                                <div class="off">
                                    <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['documents']" :button-class="'is-small is-success'" :button-label="'{{ translate('forms.field.save') }}'"></rem-form>
                                    <button class="button is-small is-cancel" v-on:click.prevent="toggle('documents')">{{ translate('forms.field.cancel') }}</button>
                                </div>
                                <div class="on">
                                    <table class="table" style="width:100%">
                                        @foreach($formData->data->documents->value['files'] as $item))
                                            <tr>
                                                <td>
                                                    <a target="_blank" href="{{ $item->url }}">{{ $item->name }}</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            @if(!$lastStatus && $user)
                <div class="container">
                    <div class="columns is-gapless">
                        <div class="column is-6">
                            <rem-form :reload="true" :code="'status'" :id="{{ $formData->id }}" :api-schema-url="'/api/status/schema'" :api-save-url="'/api/status/save'" :button-label="'{{ translate('forms.status.change') }}'" :cancel-label="'{{ translate('forms.status.cancel') }}'"></rem-form>
                        </div>
                    </div>
                    @include('partials.form.statuses')
                </div>
            @endif

            <div class="container">
                <div class="columns">
                    <div class="column is-12">
                        @if(!empty($formData->type->map[0]->value->address))
                            <h4 class="title is-4" style="margin-top: 2rem">{{ translate('forms.map-and-similar') }}
                                @if($formData->canEdit('house-address'))
                                    <!--<a href="" class="icon" v-on:click.prevent="toggle('house-address')" v-if="!isToggled('house-address')">
                                    <i class="mdi mdi-lead-pencil"></i>
                                </a>-->
                                @endif
                            </h4>
                            <div class="field-toggle" v-bind:class="{ 'toggled': isToggled('house-address') }">
                                <!--<div class="off">
                                    <rem-form :reload="true" :code="'{{ $form->code }}'" :id="{{ $formData->id }}" :only-fields="['house-address']" :button-class="'is-small is-success'"></rem-form>
                                    <button class="button is-small is-cancel" v-on:click.prevent="toggle('house-address')">Отменить</button>
                                </div>-->
                                <div class="on">
                                    <rem-map :initial-center="[{{ $formData->type->map[0]->value->lat }},{{ $formData->type->map[0]->value->lng }}]" :initial-zoom="18" :is-autoload="false" :is-info-box="true" :info-window="'{{ $formData->type->map[0]->value->address }}'" :is-context-menu="true"></rem-map>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="columns">
                    <div class="column is-6">
                        @include('partials.form.similar')
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
