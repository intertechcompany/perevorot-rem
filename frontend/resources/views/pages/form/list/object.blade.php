@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if(!$forms->isEmpty())
        <div class="section">
            <div class="container">
                <div class="columns">
                    <div class="column is-6">
                        <div class="title">{{ !empty($form->name) ? $form->name : '' }}</div>
                        @foreach($forms as $formData)
                            <div class="card">
                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            @if(!empty($formData->data->address->value->address))
                                                <p class="title is-4"><a href="/form/show/object/{{ $formData->id }}">{{ $formData->data->address->value->address }}</a></p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="content">
                                        <time>{{ translate('forms.date_add') }} {{ $formData->created_at }}</time>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
