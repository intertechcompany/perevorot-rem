@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if(!$forms->isEmpty())
        <div class="columns">
            <div class="column is-6 is-offset-3">
                <div class="title has-text-primary">{{ !empty($form->name) ? $form->name : '' }}</div>
                @foreach($forms as $formData)
                    <div class="card">
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4"><a href="/form/show/object/{{ $formData->id }}">{{ json_decode($formData->data('map'))->address }}</a></p>
                                    <p class="subtitle is-6">{{ $formData->data('owner') }}</p>
                                </div>
                            </div>

                            <div class="content">
                                {{ $formData->data('comment') }}
                                <br>
                                <time>Дата добавления: {{ $formData->created_at }}</time>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endsection
