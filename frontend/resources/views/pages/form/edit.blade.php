@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    <div>
        <div class="container">
            <div class="columns">
                <div class="column is-12">
                    @include('partials.breadcrumbs')
                </div>
            </div>
            <div class="columns">
                <div class="column form-add">
                    @if(!empty($formData->data->address->value->address))
                        <div class="title">{{ $formData->data->address->value->address }}</div>
                    @endif
                    <rem-form
                        :id="{{ $formData->id }}"
                        @if(empty($parentId) && $form->after_saving == 2) :redirect="'{{ \Url()->previous() }}'"
                        @elseif(!empty($parentId) && $form->after_saving != 1) :redirect="'{{ \Url()->previous() }}'" @endif
                        @if($form->button_save) :button-label="'{{ $form->button_save }}'" @endif
                        @if($form->button_cancel) :cancel-label="'{{ $form->button_cancel }}'" @endif
                        :code="'{{ $form->code }}'"
                        :button-class="'is-success'"
                    ></rem-form>
                </div>
            </div>
        </div>
    </div>
@endsection
