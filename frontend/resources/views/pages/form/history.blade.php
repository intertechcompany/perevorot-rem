@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    <div>
        <div class="container">
            <rem-list :api-url="'/api/history'" :show-title="false" :is-edit-click="false" :show-filter="false"></rem-list>
        </div>
    </div>
@endsection
