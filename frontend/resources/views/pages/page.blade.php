@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
	@if(!empty( @$page->longread ))
		<div class="container">
			{!! @$page->longread !!}
		</div>
	@endif
@endsection
