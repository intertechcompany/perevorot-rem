@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    <div class="columns">
        <div class="column is-6 is-offset-3">
            <h1 class="title is-4">{{ translate('reg.register') }}</h1>
            <form action="{{ route('registration') }}" method="post">
                {!! csrf_field() !!}
                <fieldset>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="email" type="email" value="{{ old('email') }}" placeholder="{{ translate('reg.email') }}">
                            @include('partials._error', ['errorKey' => 'email'])
                        </div>
                    </div>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="phone" type="text" value="{{ old('phone') }}" placeholder="{{ translate('reg.phone') }}">
                            @include('partials._error', ['errorKey' => 'phone'])
                        </div>
                    </div>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="name" type="text" value="{{ old('name') }}" placeholder="{{ translate('reg.name') }}">
                            @include('partials._error', ['errorKey' => 'name'])
                        </div>
                    </div>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="password" type="password" placeholder="{{ translate('reg.password') }}">
                            @include('partials._error', ['errorKey' => 'password'])
                        </div>
                    </div>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="password_confirmation" type="password" placeholder="{{ translate('reg.confirm.password') }}">
                            @include('partials._error', ['errorKey' => 'password_confirmation'])
                        </div>
                    </div>
                    <button class="button is-success" style="margin-top: 0;" type="submit">{{ translate('registration.submit') }}</button>
                </fieldset>
            </form>
            <a href="{{ route('registration') }}"></a>
        </div>
    </div>    
@endsection
