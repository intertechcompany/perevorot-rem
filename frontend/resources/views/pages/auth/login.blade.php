@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    <div class="columns">
        <div class="column is-6 is-offset-3">
            <h1 class="title is-4">{{ translate('auth.login') }}</h1>

            @if(!empty(session('errorAuth')))
                <p class="help is-danger" style="margin-bottom: 20px;">{{ session('errorAuth') }}</p>
            @endif
            <form action="{{ route('login') }}" method="post">
                <fieldset>
                    {!! csrf_field() !!}
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="email" type="email" placeholder="{{ translate('auth.enter.login') }}">
                            @include('partials._error', ['errorKey' => 'email'])
                        </div>
                    </div>
                    <div class="field">
                        <div class="control has-icons-right">
                            <input required class="input" name="password" type="password" placeholder="{{ translate('auth.enter.password') }}">
                            @include('partials._error', ['errorKey' => 'password'])
                        </div>
                    </div>

                    <div class="columns is-mobile is-variable is-2">
                        <div class="column is-narrow">
                            <button class="button is-success" style="margin-top: 0;" type="submit">{{ translate('auth.login.submit') }}</button>
                        </div>
                        <div class="column is-narrow">
                            @if(env('FACEBOOK_CLIENT_ID'))
                                <a href="{{ route('login.social', ['provider' => 'facebook']) }}" class="button">
                                    <span class="icon is-small">
                                        <i class="mdi mdi-facebook"></i>
                                    </span>
                                </a>
                            @endif
                        </div>
                        <div class="column is-narrow has-text-right">
                            @if(env('GOOGLE_CLIENT_ID'))
                                <a href="{{ route('login.social', ['provider' => 'google']) }}" class="button">
                                    <span class="icon is-small">
                                        <i class="mdi mdi-google"></i>
                                    </span>
                                    
                                </a>
                            @endif
                        </div>
                        <div class="column"></div>
                        <div class="column is-narrow has-text-right">
                            <a class="button is-secondary" href="{{ route('registration') }}">{{ translate('auth.registration') }}</a>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>    
@endsection
