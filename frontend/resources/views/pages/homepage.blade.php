@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
    @if(!$hideObjects)
        @if($user && $user->isSuperAdmin())
            <rem-list :api-url="'/api/home_list'" :show-filter="true"></rem-list>
        @elseif(@$settings->homeViewMode == 'mapMode')
            <rem-map :is-autoload="false" :is-autocomplete="true" :is-context-menu="true" :is-fullscreen="true"></rem-map>
        @else
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    @if(!$formsData->isEmpty())
                        <h2 class="title">{{ translate('site.objects') }}</h2>
                        @foreach($formsData as $formData)

                            @if($user && $user->isSuperAdmin())
                                <div class="card" v-if="filteredData('{{ $formData->form->domain }}')">
                            @else
                                <div class="card">
                            @endif

                                <div class="card-content">
                                    <div class="media">
                                        <div class="media-content">
                                            <p class="title is-4">
                                                <a href="/form/show/{{ $formData->form->code }}/{{ $formData->id }}">
                                                    @if(!empty($formData->data->manualAddress->value))
                                                        {{ $formData->data->manualAddress->value }}
                                                    @elseif(!empty($formData->data->houseAddress->value))
                                                        {{ $formData->data->houseAddress->value->address }}
                                                    @elseif(!empty($formData->data->address->value))
                                                        {{ $formData->data->address->value->address }}
                                                    @elseif(!empty($formData->data->name->value))
                                                        {{ $formData->data->name->value }}
                                                    @elseif(!empty($formData->data->competitor->value))
                                                        {{ $formData->data->competitor->value->name }}
                                                    @else
                                                        {{ $formData->getTitle() }}
                                                    @endif
                                                </a>
                                            </p>
                                        </div>
                                    </div>

                                    @if($user && $user->isSuperAdmin())
                                        <div class="content" style="text-align: right;">
                                            <span style="float: left;">
                                            {{ $formData->form->getDomain() }}
                                            </span>
                                            <time>{{ translate('site.objects.date') }} {{ $formData->created_at }}</time>
                                        </div>
                                    @else
                                        <div class="content">
                                            <time>{{ translate('site.objects.date') }} {{ $formData->created_at }}</time>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach

                        @if($user && $user->isSuperAdmin())
                            {{ $formsData->render() }}
                        @endif

                    @endif
                </div>
                @if($user && $user->isSuperAdmin())
                <div class="column is-6">
                    <h2 class="title">{{ translate('site.domains') }}</h2>
                    <div>
                        @foreach($domainsFilter as $domain)
                            <input id="domain-{{ $loop->index }}" type="checkbox" v-model="filterSelected.domains" value="{{ $domain }}">
                            <label for="domain-{{ $loop->index }}">{{ $domain }}</label>
                            <br>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        @endif
    @endif
@endsection
