@extends('layouts.'.(!empty($settings->pageDefaultTemplate) ? $settings->pageDefaultTemplate : 'app'))

@section('content')
	<div>
		<div class="container">
			<div class="title is-6">{{ translate('forms.incoming-calls') }}</div>
			<rem-list :api-url="'/api/calls/incoming'" :show-title="false" :is-edit-click="false" :show-filter="false"></rem-list>
		</div>
	</div>
@endsection
