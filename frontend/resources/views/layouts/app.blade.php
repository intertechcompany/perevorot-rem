@spaceless
    <!doctype html>
    <html lang="{{ Localization::getCurrentLocale() }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
            <meta name="csrf-token" content="%%csrf_token%%">

            @if(env('APP_ADDITIONAL_CSS'))
                <link href="{{ mix('css/'.env('APP_ADDITIONAL_CSS')) }}" rel="stylesheet">
            @else
                <link href="{{ mix('css/app.css') }}" rel="stylesheet">
            @endif

            {!! SEO::head() !!}
            <style>
                html, body, #app {
                    height: 100%;
                    margin: 0;
                    padding: 0;
                }
            </style>
        </head>
        <body data-months="{{ \App\Classes\Helper::getMonths() }}" data-days="{{ \App\Classes\Helper::getDays() }}">
            {!! SEO::bodyTop() !!}

            @yield('customHeader')

            <nav class="navbar has-shadow is-fixed-top">
                <div class="container">
                    <div class="navbar-brand">
                        @if(empty(app('request')->input('global_id')))
                            <a href="/" class="navbar-item title" style="margin-bottom: 0">
                                @if($logo)
                                    <img style="height: 50px;" src="{{ $logo }}" alt="{{ translate('site.logo') }}">
                                @else
                                    {{ translate('site.logo') }}
                                @endif
                            </a>
                            <span class="navbar-burger burger" data-target="navMenu" style="align-self: center">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        @else
                            <a href="/" class="navbar-item title" style="margin-bottom: 0">
                                {{ translate('site.title') }}
                                <sup style="font-weight:700;background: #ed352e;color: #FFFFFF;border-radius: 3px;margin-left: .5rem;margin-bottom: 1rem;padding: 0 .2rem;">beta</sup>
                            </a>
                        @endif
                    </div>
                    <div id="navMenu" class="navbar-menu">
                        <div class="navbar-start">
                            @if($user)
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="#">{{ translate('menu.add') }}</a>
                                    @if(!$menuFormsCreate->isEmpty())
                                        <div class="navbar-dropdown">
                                            @foreach($menuFormsCreate as $item)
                                                <a class="navbar-item" href="/form/add/{{ $item->code }}">
                                                    {{ $item->name }}
                                                </a>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            @endif
                            @include('partials.menu')
                        </div>
                        <div class="navbar-end">
                            <div class="navbar-item has-dropdown is-hoverable">
                                @include('partials.auth')
                            </div>
                        </div>
                    </div>
                </div>
            </nav>

            <main class="section">
                <div id="app">
                    @yield('content')
                </div>        
            </main>

            {!! SEO::bodyBottom() !!}
            
            @yield('js')

            <script src="{{ mix('js/app.js') }}"></script>
        </body>
    </html>
@endspaceless
