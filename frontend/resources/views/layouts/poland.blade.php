@spaceless
<!doctype html>
<html lang="en" data-crsf_token="wDgT3DsnUuQRunU6L5EKLmS2eOxLuRnHo1wWoNCV">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EMBED</title>
    <link rel="stylesheet" href="https://rejestr.io/css/app.css">
    <link rel="stylesheet" href="https://rejestr.io/css/procurement.css">
    <style type="text/css">
        .fade-enter-active,
        .fade-leave-active {
            transition: opacity .15s linear
        }
        
        .fade-enter,
        .fade-leave-to {
            opacity: 0
        }
    </style>
    <style type="text/css">
        .input-group>.input-group-append:last-child>.b-dropdown:not(:last-child):not(.dropdown-toggle)>.btn,
        .input-group>.input-group-append:not(:last-child)>.b-dropdown>.btn,
        .input-group>.input-group-prepend>.b-dropdown>.btn {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0
        }
        
        .input-group>.input-group-append>.b-dropdown>.btn,
        .input-group>.input-group-prepend:first-child>.b-dropdown:not(:first-child)>.btn,
        .input-group>.input-group-prepend:not(:first-child)>.b-dropdown>.btn {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0
        }
    </style>
    <style type="text/css">
        input.form-control[type=color],
        input.form-control[type=range] {
            height: 2.25rem
        }
        
        input.form-control.form-control-sm[type=color],
        input.form-control.form-control-sm[type=range] {
            height: 1.9375rem
        }
        
        input.form-control.form-control-lg[type=color],
        input.form-control.form-control-lg[type=range] {
            height: 3rem
        }
        
        input.form-control[type=color] {
            padding: .25rem
        }
        
        input.form-control.form-control-sm[type=color] {
            padding: .125rem
        }
    </style>
    <style type="text/css">
        table.b-table.b-table-fixed {
            table-layout: fixed
        }
        
        table.b-table[aria-busy=false] {
            opacity: 1
        }
        
        table.b-table[aria-busy=true] {
            opacity: .6
        }
        
        table.b-table>tfoot>tr>th,
        table.b-table>thead>tr>th {
            position: relative
        }
        
        table.b-table>tfoot>tr>th.sorting,
        table.b-table>thead>tr>th.sorting {
            padding-right: 1.5em;
            cursor: pointer
        }
        
        table.b-table>tfoot>tr>th.sorting:after,
        table.b-table>tfoot>tr>th.sorting:before,
        table.b-table>thead>tr>th.sorting:after,
        table.b-table>thead>tr>th.sorting:before {
            position: absolute;
            bottom: 0;
            display: block;
            opacity: .4;
            padding-bottom: inherit;
            font-size: inherit;
            line-height: 180%
        }
        
        table.b-table>tfoot>tr>th.sorting:before,
        table.b-table>thead>tr>th.sorting:before {
            right: .75em;
            content: "\2191"
        }
        
        table.b-table>tfoot>tr>th.sorting:after,
        table.b-table>thead>tr>th.sorting:after {
            right: .25em;
            content: "\2193"
        }
        
        table.b-table>tfoot>tr>th.sorting_asc:after,
        table.b-table>tfoot>tr>th.sorting_desc:before,
        table.b-table>thead>tr>th.sorting_asc:after,
        table.b-table>thead>tr>th.sorting_desc:before {
            opacity: 1
        }
        
        table.b-table.b-table-stacked {
            width: 100%
        }
        
        table.b-table.b-table-stacked,
        table.b-table.b-table-stacked>caption,
        table.b-table.b-table-stacked>tbody,
        table.b-table.b-table-stacked>tbody>tr,
        table.b-table.b-table-stacked>tbody>tr>td,
        table.b-table.b-table-stacked>tbody>tr>th {
            display: block
        }
        
        table.b-table.b-table-stacked>tbody>tr.b-table-bottom-row,
        table.b-table.b-table-stacked>tbody>tr.b-table-top-row,
        table.b-table.b-table-stacked>tfoot,
        table.b-table.b-table-stacked>thead {
            display: none
        }
        
        table.b-table.b-table-stacked>tbody>tr>:first-child {
            border-top-width: .4rem
        }
        
        table.b-table.b-table-stacked>tbody>tr>[data-label] {
            display: grid;
            grid-template-columns: 40% auto;
            grid-gap: .25rem 1rem
        }
        
        table.b-table.b-table-stacked>tbody>tr>[data-label]:before {
            content: attr(data-label);
            display: inline;
            text-align: right;
            overflow-wrap: break-word;
            font-weight: 700;
            font-style: normal
        }
        
        @media (max-width:575.99px) {
            table.b-table.b-table-stacked-sm {
                width: 100%
            }
            table.b-table.b-table-stacked-sm,
            table.b-table.b-table-stacked-sm>caption,
            table.b-table.b-table-stacked-sm>tbody,
            table.b-table.b-table-stacked-sm>tbody>tr,
            table.b-table.b-table-stacked-sm>tbody>tr>td,
            table.b-table.b-table-stacked-sm>tbody>tr>th {
                display: block
            }
            table.b-table.b-table-stacked-sm>tbody>tr.b-table-bottom-row,
            table.b-table.b-table-stacked-sm>tbody>tr.b-table-top-row,
            table.b-table.b-table-stacked-sm>tfoot,
            table.b-table.b-table-stacked-sm>thead {
                display: none
            }
            table.b-table.b-table-stacked-sm>tbody>tr>:first-child {
                border-top-width: .4rem
            }
            table.b-table.b-table-stacked-sm>tbody>tr>[data-label] {
                display: grid;
                grid-template-columns: 40% auto;
                grid-gap: .25rem 1rem
            }
            table.b-table.b-table-stacked-sm>tbody>tr>[data-label]:before {
                content: attr(data-label);
                display: inline;
                text-align: right;
                overflow-wrap: break-word;
                font-weight: 700;
                font-style: normal
            }
        }
        
        @media (max-width:767.99px) {
            table.b-table.b-table-stacked-md {
                width: 100%
            }
            table.b-table.b-table-stacked-md,
            table.b-table.b-table-stacked-md>caption,
            table.b-table.b-table-stacked-md>tbody,
            table.b-table.b-table-stacked-md>tbody>tr,
            table.b-table.b-table-stacked-md>tbody>tr>td,
            table.b-table.b-table-stacked-md>tbody>tr>th {
                display: block
            }
            table.b-table.b-table-stacked-md>tbody>tr.b-table-bottom-row,
            table.b-table.b-table-stacked-md>tbody>tr.b-table-top-row,
            table.b-table.b-table-stacked-md>tfoot,
            table.b-table.b-table-stacked-md>thead {
                display: none
            }
            table.b-table.b-table-stacked-md>tbody>tr>:first-child {
                border-top-width: .4rem
            }
            table.b-table.b-table-stacked-md>tbody>tr>[data-label] {
                display: grid;
                grid-template-columns: 40% auto;
                grid-gap: .25rem 1rem
            }
            table.b-table.b-table-stacked-md>tbody>tr>[data-label]:before {
                content: attr(data-label);
                display: inline;
                text-align: right;
                overflow-wrap: break-word;
                font-weight: 700;
                font-style: normal
            }
        }
        
        @media (max-width:991.99px) {
            table.b-table.b-table-stacked-lg {
                width: 100%
            }
            table.b-table.b-table-stacked-lg,
            table.b-table.b-table-stacked-lg>caption,
            table.b-table.b-table-stacked-lg>tbody,
            table.b-table.b-table-stacked-lg>tbody>tr,
            table.b-table.b-table-stacked-lg>tbody>tr>td,
            table.b-table.b-table-stacked-lg>tbody>tr>th {
                display: block
            }
            table.b-table.b-table-stacked-lg>tbody>tr.b-table-bottom-row,
            table.b-table.b-table-stacked-lg>tbody>tr.b-table-top-row,
            table.b-table.b-table-stacked-lg>tfoot,
            table.b-table.b-table-stacked-lg>thead {
                display: none
            }
            table.b-table.b-table-stacked-lg>tbody>tr>:first-child {
                border-top-width: .4rem
            }
            table.b-table.b-table-stacked-lg>tbody>tr>[data-label] {
                display: grid;
                grid-template-columns: 40% auto;
                grid-gap: .25rem 1rem
            }
            table.b-table.b-table-stacked-lg>tbody>tr>[data-label]:before {
                content: attr(data-label);
                display: inline;
                text-align: right;
                overflow-wrap: break-word;
                font-weight: 700;
                font-style: normal
            }
        }
        
        @media (max-width:1199.99px) {
            table.b-table.b-table-stacked-xl {
                width: 100%
            }
            table.b-table.b-table-stacked-xl,
            table.b-table.b-table-stacked-xl>caption,
            table.b-table.b-table-stacked-xl>tbody,
            table.b-table.b-table-stacked-xl>tbody>tr,
            table.b-table.b-table-stacked-xl>tbody>tr>td,
            table.b-table.b-table-stacked-xl>tbody>tr>th {
                display: block
            }
            table.b-table.b-table-stacked-xl>tbody>tr.b-table-bottom-row,
            table.b-table.b-table-stacked-xl>tbody>tr.b-table-top-row,
            table.b-table.b-table-stacked-xl>tfoot,
            table.b-table.b-table-stacked-xl>thead {
                display: none
            }
            table.b-table.b-table-stacked-xl>tbody>tr>:first-child {
                border-top-width: .4rem
            }
            table.b-table.b-table-stacked-xl>tbody>tr>[data-label] {
                display: grid;
                grid-template-columns: 40% auto;
                grid-gap: .25rem 1rem
            }
            table.b-table.b-table-stacked-xl>tbody>tr>[data-label]:before {
                content: attr(data-label);
                display: inline;
                text-align: right;
                overflow-wrap: break-word;
                font-weight: 700;
                font-style: normal
            }
        }
        
        table.b-table>tbody>tr.b-table-details>td {
            border-top: none
        }
    </style>
    <style type="text/css">
        .Cookie {
            position: fixed;
            overflow: hidden;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            z-index: 9999;
            width: 100%;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-box-align: baseline;
            -ms-flex-align: baseline;
            align-items: baseline;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
        }
        
        .Cookie > * {
            margin: 0.9375rem 0;
            -ms-flex-item-align: center;
            align-self: center;
        }
        
        @media screen and (min-width: 48rem) {
            .Cookie {
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-flow: row;
                flex-flow: row;
            }
            .Cookie > * {
                margin: 0;
            }
        }
        
        .Cookie--top {
            top: 0;
            left: 0;
            right: 0;
        }
        
        .Cookie--bottom {
            bottom: 0;
            left: 0;
            right: 0;
        }
        
        .Cookie__buttons {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
        }
        
        .Cookie__buttons > * {
            margin: 0.3125rem 0;
        }
        
        @media screen and (min-width: 48rem) {
            .Cookie__buttons {
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
            }
            .Cookie__buttons > * {
                margin: 0 0.9375rem;
            }
        }
        
        .Cookie__button {
            cursor: pointer;
            -ms-flex-item-align: center;
            align-self: center;
        }
        
        .Cookie--base {
            background: #F1F1F1;
            color: #232323;
            padding: 1.250em;
        }
        
        .Cookie--base .Cookie__button {
            background: #97D058;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 0;
        }
        
        .Cookie--base .Cookie__button:hover {
            background: #7ebf36;
        }
        
        .Cookie--base--rounded {
            background: #F1F1F1;
            color: #232323;
            padding: 1.250em;
        }
        
        .Cookie--base--rounded .Cookie__button {
            background: #97D058;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 20px;
        }
        
        .Cookie--base--rounded .Cookie__button:hover {
            background: #7ebf36;
        }
        
        .Cookie--blood-orange {
            background: #424851;
            color: #fff;
            padding: 1.250em;
        }
        
        .Cookie--blood-orange .Cookie__button {
            background: #E76A68;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 0;
        }
        
        .Cookie--blood-orange .Cookie__button:hover {
            background: #e03f3c;
        }
        
        .Cookie--blood-orange--rounded {
            background: #424851;
            color: #fff;
            padding: 1.250em;
        }
        
        .Cookie--blood-orange--rounded .Cookie__button {
            background: #E76A68;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 20px;
        }
        
        .Cookie--blood-orange--rounded .Cookie__button:hover {
            background: #e03f3c;
        }
        
        .Cookie--dark-lime {
            background: #424851;
            color: #fff;
            padding: 1.250em;
        }
        
        .Cookie--dark-lime .Cookie__button {
            background: #97D058;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 0;
        }
        
        .Cookie--dark-lime .Cookie__button:hover {
            background: #7ebf36;
        }
        
        .Cookie--dark-lime--rounded {
            background: #424851;
            color: #fff;
            padding: 1.250em;
        }
        
        .Cookie--dark-lime--rounded .Cookie__button {
            background: #97D058;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 20px;
        }
        
        .Cookie--dark-lime--rounded .Cookie__button:hover {
            background: #7ebf36;
        }
        
        .Cookie--royal {
            background: #FBC227;
            color: #232323;
            padding: 1.250em;
        }
        
        .Cookie--royal .Cookie__button {
            background: #726CEA;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 0;
        }
        
        .Cookie--royal .Cookie__button:hover {
            background: #473fe4;
        }
        
        .Cookie--royal--rounded {
            background: #FBC227;
            color: #232323;
            padding: 1.250em;
        }
        
        .Cookie--royal--rounded .Cookie__button {
            background: #726CEA;
            padding: 0.625em 3.125em;
            color: #fff;
            border-radius: 20px;
        }
        
        .Cookie--royal--rounded .Cookie__button:hover {
            background: #473fe4;
        }
        
        .slideFromTop-enter,
        .slideFromTop-leave-to {
            -webkit-transform: translate(0px, -12.5em);
            transform: translate(0px, -12.5em);
        }
        
        .slideFromTop-enter-to,
        .slideFromTop-leave {
            -webkit-transform: translate(0px, 0px);
            transform: translate(0px, 0px);
        }
        
        .slideFromBottom-enter,
        .slideFromBottom-leave-to {
            -webkit-transform: translate(0px, 12.5em);
            transform: translate(0px, 12.5em);
        }
        
        .slideFromBottom-enter-to,
        .slideFromBottom-leave {
            -webkit-transform: translate(0px, 0px);
            transform: translate(0px, 0px);
        }
        
        .slideFromBottom-enter-active,
        .slideFromBottom-leave-active,
        .slideFromTop-enter-active,
        .slideFromTop-leave-active {
            -webkit-transition: -webkit-transform .4s ease-in;
            transition: -webkit-transform .4s ease-in;
            transition: transform .4s ease-in;
            transition: transform .4s ease-in, -webkit-transform .4s ease-in;
        }
        
        .fade-enter-active,
        .fade-leave-active {
            -webkit-transition: opacity .5s;
            transition: opacity .5s;
        }
        
        .fade-enter,
        .fade-leave-to {
            opacity: 0;
        }
    </style>
    <style type="text/css">
        .texthelper .text[data-v-6b7f56fc] {
            max-height: inherit;
            -webkit-transition: max-height .3s ease;
            transition: max-height .3s ease;
            overflow: hidden
        }
        
        .texthelper.shorten .text[data-v-6b7f56fc] {
            max-height: 150px
        }
        
        .texthelper .toolbar[data-v-6b7f56fc] {
            margin-top: -50px;
            position: relative;
            z-index: 1
        }
        
        .texthelper .toolbar p[data-v-6b7f56fc] {
            margin: 0;
            padding: 0
        }
        
        .texthelper .toolbar .cover[data-v-6b7f56fc] {
            background: -webkit-gradient(linear, left top, left bottom, from(hsla(0, 0%, 100%, 0)), to(#fff));
            background: linear-gradient(180deg, hsla(0, 0%, 100%, 0) 0, #fff);
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#00ffffff", endColorstr="#ffffff", GradientType=0);
            width: 100%;
            height: 50px;
            display: none
        }
        
        .texthelper.shorten .toolbar .cover[data-v-6b7f56fc] {
            display: block
        }
        
        .texthelper .toolbar .link[data-v-6b7f56fc] {
            text-align: center;
            height: 35px;
            padding-top: 15px;
            margin-top: 50px;
            margin-bottom: 15px
        }
        
        .texthelper.shorten .toolbar .link[data-v-6b7f56fc] {
            margin-top: 0
        }
    </style>
    <script type="text/javascript" src="chrome-extension://aadgmnobpdmgmigaicncghmmoeflnamj/ng-inspector.js"></script>
    <object class="chrome-extension://jffafkigfgmjafhpkoibhfefeaebmccg/" style="display: none; visibility: hidden;"></object>
</head>

<body id="body" class="object menu-min">

    <div id="menu-main-wrapper">
        <div class="container">
            <div id="menu-main" data-mode="min" data-app="zamowienia_publiczne" data-q="" class="menu-main min">
                <div class="menu-main-inner">
                    <a href="/"><img src="https://rejestr.io/img/logo.svg" class="logo"></a>
                    <div class="inner">
                        <form action="/zamowienia_publiczne" method="get" class="form">
                            <div class="first_row">
                                <div class="input-wrapper">
                                    <input name="q" type="text" placeholder="Szukaj w rejestrach..." autocomplete="off" id="main_search_input" class="form-control">
                                    <div class="btns">
                                        <button type="submit" class="btn search"><span class="icon-search"></span></button>
                                        <button type="reset" class="btn reset"><span class="icon-close"></span></button>
                                    </div>
                                </div>
                                <ul class="nav">
                                    <li class="nav-item"><a href="https://rejestr.io/login" class="nav-link"><span class="icon icon-login"></span> <span class="txt">Zaloguj się</span></a></li>
                                </ul>
                            </div>
                            <div class="second_row">
                                <ul class="submenu">
                                    <li class="krs"><a href="/krs" title="Krajowy Rejestr Sądowy">KRS</a></li>
                                    <li class="zamowienia_publiczne active"><a href="/zamowienia_publiczne" title="Zamówienia publiczne">Zamówienia publiczne</a></li>
                                    <li class="nik"><a href="/nik" title="Raporty Najwyższej Izby Kontroli">NIK</a></li>
                                    <li class="nauka"><a href="/nauka" title="Bazy naukowe">Bazy naukowe</a></li>
                                    <li class="ipn"><a href="/ipn" title="Katalogi Instytutu Pamięci Narodowej">IPN</a></li>
                                    <li class="patenty"><a href="/patenty" title="Patenty">Patenty</a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="banner">
                    <div class="row">
                        <div class="col-lg-6">
                            <p>Szukaj w rejestrach publicznych:</p>
                            <ul>
                                <li class="registry krs"><a href="/krs">Krajowy Rejestr Sądowy</a></li>
                                <li class="registry zamowienia_publiczne"><a href="/zamowienia_publiczne">Zamówienia publiczne</a></li>
                                <li class="registry nik"><a href="/nik">Raporty Najwyższej Izby Kontroli</a></li>
                                <li class="registry nauka"><a href="/nauka">Bazy naukowe</a></li>
                                <li class="registry ipn"><a href="/ipn">Katalogi Instytutu Pamięci Narodowej</a></li>
                                <li class="registry patenty"><a href="/patenty">Patenty</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <div class="check-also">
                                <p>Sprawdź też:</p>
                                <ul>
                                    <li class="tool"><a href="https://rejestr.io/analiza">Analiza</a>
                                        <p>Narzędzie przeznaczone do analizowania dłuższych tekstów i dokumentów.</p>
                                    </li>
                                    <li class="tool"><a href="https://rejestr.io/premium">Rejestrio Premium</a>
                                        <p>Poszerzony dostęp do danych z Krajowego Rejestru Sądowego.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content-wrapper" class="">
        <div id="procurement" itemscope="itemscope" itemtype="https://schema.org/Demand" class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Zamówienia publiczne</a></li>
                <li itemprop="identifier" class="breadcrumb-item active">DZP-OG.7720.16.2018</li>
                <li class="breadcrumb-item active text-normalize">510053443-N-201</li>
            </ol>
            <div data-global-id="78823363" class="media object main procurement mb-2">
                <div class="align-self-start icon">
                    <a href="#" class="icon-handshake"></a>
                </div>
                <div class="media-body">
                    <h1 itemprop="name" class="mt-0"><a href="#">"Remont lokali mieszkalnych przed zasiedleniem w zasobie AMW OReg. Gdynia".</a></h1></div>
            </div>
            <div class="object-page">
                <div class="row">
                    <div class="col-md-3">
                        <div class="info inline">
                            <p class="lab">Data publikacji</p>
                            <p class="val">
                                <time itemprop="validFrom" datetime="2019-03-20">
                                    20&nbsp;marca&nbsp;2019&nbsp;r.
                                </time>
                            </p>
                        </div>
                        <div class="info inline">
                            <p class="lab">Rodzaj</p>
                            <p class="val"><span>Roboty budowlane</span></p>
                        </div>
                        <div class="info inline">
                            <p class="lab">mode</p>
                            <p class="val"><span>0</span></p>
                        </div>
                        <div class="info lg">
                            <p class="lab">Zamawiający</p>
                            <p class="val"><span itemprop="seller">Agencja Mienia Wojskowego</span></p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card mb-4">
                            <div class="card-header">Przedmiot zamówienia</div>
                            <div class="card-body">
                                <div itemprop="description" class="card-text">
                                    <div data-v-6b7f56fc="" class="texthelper shorten">
                                        <div data-v-6b7f56fc="" class="text" style="max-height: 150px;">
                                            <p data-v-6b7f56fc="">1. Przedmiotem zamówienia jest wykonanie remontu lokali mieszkalnych przed zasiedleniem w zasobie OR AMW Gdynia, położonych w miejscowości Gdynia, Rumia, Ustka, Słupsk, Redzikowo, Gdańsk, Malbork, Kwidzyn, Siemirowice w zakres których wchodzą głównie:
                                            </p>
                                            <p data-v-6b7f56fc="">a) roboty remontowe ogólnobudowlane (malarskie, posadzkowe, stolarskie, licowanie ścian płytkami),
                                            </p>
                                            <p data-v-6b7f56fc="">b) wymiana i montaż urządzeń sanitarnych,
                                            </p>
                                            <p data-v-6b7f56fc="">c) remont instalacji elektrycznej, sanitarnej.
                                            </p>
                                            <p data-v-6b7f56fc="">2. Zamawiający dopuszcza możliwość składania ofert częściowych. Przedmiot zamówienia został podzielony na 6 części:
                                            </p>
                                            <p data-v-6b7f56fc=""></p>
                                            <p data-v-6b7f56fc=""></p>
                                            <p data-v-6b7f56fc=""> Część I
                                            </p>
                                            <p data-v-6b7f56fc="">1 Gdynia Korzenna 14 A / 3
                                            </p>
                                            <p data-v-6b7f56fc="">2 Gdynia Abrahama 44 / 14
                                            </p>
                                            <p data-v-6b7f56fc="">3 Gdynia Żeromskiego 37 / 1
                                            </p>
                                            <p data-v-6b7f56fc="">4 Rumia Krakowska 5D /2 9
                                            </p>
                                            <p data-v-6b7f56fc=""> Część II
                                            </p>
                                            <p data-v-6b7f56fc="">1 Ustka Legionów Pol. 10 / 23
                                            </p>
                                            <p data-v-6b7f56fc="">2 Ustka Bałtycka 5/38
                                            </p>
                                            <p data-v-6b7f56fc=""> Część III
                                            </p>
                                            <p data-v-6b7f56fc="">1 Słupsk Górna 9 /60
                                            </p>
                                            <p data-v-6b7f56fc="">2 Słupsk Boh. Westerplatte 53A/13
                                            </p>
                                            <p data-v-6b7f56fc="">3 Słupsk Górna 11/21
                                            </p>
                                            <p data-v-6b7f56fc="">4 Redzikowo Redzikowo 26/19
                                            </p>
                                            <p data-v-6b7f56fc=""> Część IV
                                            </p>
                                            <p data-v-6b7f56fc="">1 Gdańsk Partyzantów 65A/2
                                            </p>
                                            <p data-v-6b7f56fc=""> Część V
                                            </p>
                                            <p data-v-6b7f56fc="">1 Malbork Reymonta 11/5
                                            </p>
                                            <p data-v-6b7f56fc="">2 Kwidzyn Słowackiego 18/22
                                            </p>
                                            <p data-v-6b7f56fc=""> Część VI
                                            </p>
                                            <p data-v-6b7f56fc="">1 Siemirowice Osiedle Na Skarpie 15B/7
                                            </p>
                                            <p data-v-6b7f56fc="">Szczegółowy zakres robót stanowi załącznik w postaci przedmiaru robót, stanowiący załącznik nr 5 do SIWZ, Specyfikacja Techniczna Wykonania i Odbioru Robót –stanowiąca załącznik nr 6 do SIWZ.</p>
                                        </div>
                                        <div data-v-6b7f56fc="" class="toolbar">
                                            <p data-v-6b7f56fc="" class="cover"></p>
                                            <p data-v-6b7f56fc="" class="link"><a data-v-6b7f56fc="" href="#">Rozwiń</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div data-id="200306" class="card mb-4 card-announcement">
                            <div class="card-header">Udzielenie zamówienia
                                <span>20&nbsp;marca&nbsp;2019&nbsp;r.</span></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="info lg">
                                            <p class="lab">
                                                Wykonawcy:
                                            </p>
                                            <div class="val">
                                                <div>CHADACZ ARTUR-FLOOR</div>
                                                <div>CHADACZ ARTUR-FLOOR</div>
                                                <div>MIKAR Karol Miller</div>
                                                <div>MIKAR Karol Miller</div>
                                            </div>
                                        </div>
                                        <div class="info inline dimmed" style="display: none;">
                                            <p class="lab">Liczba otrzymanych ofert:</p>
                                            <p class="val">1</p>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="info lg">
                                            <p class="lab">Wartość umowy:</p>
                                            <p class="val currency">90471.86 PLN</p>
                                        </div>
                                        <div class="info inline dimmed" style="display: none;">
                                            <p class="lab">Najniższa cena:</p>
                                            <p class="val currency">90471.86 PLN</p>
                                        </div>
                                        <div class="info inline dimmed" style="display: none;">
                                            <p class="lab">Najwyższa cena:</p>
                                            <p class="val currency">90471.86 PLN</p>
                                        </div>
                                    </div>
                                </div>
                                <p class="source"><a href="https://bzp.uzp.gov.pl/ZP403/Preview/5a68b013-c433-462e-bd4a-0a22c56cbf7b" target="_blank" itemprop="url">Źródło</a></p>
                            </div>
                        </div>

                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer id="footer">
        <div class="container">
            <div class="footer-inner">
                <div class="footer-logo-and-menu">
                    <div class="footer-logo"><img src="https://rejestr.io/img/logo-gs.svg" class="logo"></div>
                    <div class="footer-menu">
                        <ul>
                            <li><a href="https://rejestr.io/o-portalu">O&nbsp;portalu</a></li>
                            <li><a href="https://rejestr.io/informacje-prawne">Informacje prawne</a></li>
                            <li class="before-copyright"><a href="https://rejestr.io/premium">Rejestrio Premium</a></li>
                            <li class="copyright"><a target="_blank" href="https://epf.org.pl">© 2019 Fundacja ePaństwo</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
@endspaceless