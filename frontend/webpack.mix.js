let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/scss/app.scss', 'public/css');

mix.js('resources/assets/js/embed.js', 'public/js/embed.js');

mix.sass('resources/assets/scss/srm.scss', 'public/css/srm.css');

mix.sass('resources/assets/scss/poland.scss', 'public/css/poland.css');

if (mix.inProduction()) {
    mix.version();
}

if (!mix.inProduction()) {
    mix.sourceMaps();
}