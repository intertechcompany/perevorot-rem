## Требования к ПО на сервере

npm 6.13+

nodejs 11.6+

composer (https://getcomposer.org)

PHP >= 7.1.3

OpenSSL PHP Extension

PDO PHP Extension

Mbstring PHP Extension

Tokenizer PHP Extension

XML PHP Extension

Ctype PHP Extension

JSON PHP Extension

Mysql или Mariadb latest

ssh доступ к серверу

ключи на google map

ключи на google auth

ключи на facebook auth

git

ssl сертификат на сервере