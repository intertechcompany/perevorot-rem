<?php namespace Perevorot\Page\Traits;

use ApplicationException;
use Flash;
use DB;
use Redirect;

trait PageModelEvents
{
    public function beforeCreate()
    {
        $pages=DB::table($this->table)->select('id')->get();
        $autoincrement=0;

        foreach($pages as $page) {
            $autoincrement=max($autoincrement, (int) substr($page->id, 2));
        }

        $this->id=(int) $this->menu_id.'00'.($autoincrement+1);
    }

    public function beforeValidate()
    {
        if($this->type==self::PAGE_TYPE_ALIAS)
            $this->url='';
    }
}
