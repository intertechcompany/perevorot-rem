<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1022 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('region')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('region');
        });
    }
}