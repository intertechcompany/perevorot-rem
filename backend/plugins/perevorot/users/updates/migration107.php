<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration107 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('domain')->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('domain');
        });
    }
}