<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration103 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->rememberToken();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropRememberToken();
        });
    }
}