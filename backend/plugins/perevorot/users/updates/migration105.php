<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration105 extends Migration
{
    public function up()
    {
        Schema::table('user_groups', function($table)
        {
            $table->text('forms_permissions')->nullable();
        });
    }

    public function down()
    {
        Schema::table('user_groups', function($table)
        {
            $table->dropColumn('forms_permissions');
        });
    }
}