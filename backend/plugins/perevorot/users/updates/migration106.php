<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration106 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('is_social_reg')->nullable()->default(0);
            $table->string('password')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('is_social_reg');
        });
    }
}