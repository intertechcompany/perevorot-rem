<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddBinotelToUsers extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('binotel_number')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('binotel_number');
        });
    }
}