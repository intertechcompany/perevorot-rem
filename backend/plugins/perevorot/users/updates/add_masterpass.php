<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddMasterpass extends Migration
{
    public function up()
    {
        Schema::table('backend_users', function($table)
        {
            $table->string('masterpass')->nullable();
        });
    }

    public function down()
    {
        Schema::table('backend_users', function($table)
        {
            $table->dropColumn('masterpass');
        });
    }
}