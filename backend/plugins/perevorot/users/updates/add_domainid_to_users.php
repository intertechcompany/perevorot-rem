<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddDomainidToUsers extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->unsignedInteger('domain_id')->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('domain_id');
        });
    }
}