<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddVisitedLandingPage extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('visited_landing_page')->nullable();
        });
        Schema::table('user_groups', function($table)
        {
            $table->string('landing_page')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('visited_landing_page');
        });
        Schema::table('user_groups', function($table)
        {
            $table->dropColumn('landing_page');
        });
    }
}