<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration102 extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->dropUnique(['email']);
            $table->string('email')->nullable()->index()->change();
            $table->string('avatar')->nullable();
            $table->string('social_id')->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropIndex(['email']);
            $table->string('email')->unique()->change();
            $table->dropColumn('avatar');
            $table->dropColumn('social_id');
        });
    }
}