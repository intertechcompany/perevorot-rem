<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class DeleteDomainField extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            if(Schema::hasColumn('users', 'domain')) {
                $table->dropColumn('domain');
            }
        });
    }

    public function down()
    {
    }
}