<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class RenameAvatarField extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->renameColumn('avatar', 'avatar_url');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->renameColumn('avatar_url', 'avatar');
        });
    }
}