<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddAccessToFilter extends Migration
{
    public function up()
    {
        Schema::table('user_groups', function($table)
        {
            $table->boolean('is_filter')->nullable();
        });
    }

    public function down()
    {
        Schema::table('user_groups', function($table)
        {
            $table->dropColumn('is_filter');
        });
    }
}