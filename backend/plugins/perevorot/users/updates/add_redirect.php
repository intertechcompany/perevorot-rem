<?php namespace Perevorot\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddRedirect extends Migration
{
    public function up()
    {
        Schema::table('user_groups', function($table)
        {
            $table->string('redirect_url')->nullable();
        });
    }

    public function down()
    {
        Schema::table('user_groups', function($table)
        {
            $table->dropColumn('redirect_url');
        });
    }
}