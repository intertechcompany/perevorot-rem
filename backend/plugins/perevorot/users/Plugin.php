<?php namespace Perevorot\Users;

use Backend;
use Illuminate\Support\Facades\Event;
use Validator;
use Perevorot\Users\Models\User;
use Perevorot\Users\Models\UserGroup;
use System\Classes\PluginBase;

/**
 * Users Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'RainLab.User'
    ];

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('backend.form.extendFields', function($widget) {

            if (!$widget->model instanceof Backend\Models\User || $widget->model->login != 'admin') {
                return;
            }

            $widget->model->rules['masterpass'] = 'between:6,255|confirmed';
            $widget->model->rules['masterpass_confirmation'] = 'required_with:masterpass|between:4,255';
            $widget->model->addPurgeable('masterpass_confirmation');

            $widget->addTabFields([
                'masterpass@myaccount' => [
                    'label'   => 'Мастер пароль',
                    'comment' => '',
                    'type'    => 'password',
                    'span' =>'full',
                    'tab' => 'Permission',
                ],
                'masterpass_confirmation@myaccount' => [
                    'label'   => 'Мастер пароль - подтверждение',
                    'comment' => '',
                    'type'    => 'password',
                    'span' =>'full',
                    'tab' => 'Permission',
                ],
            ]);

        });

        Event::listen('backend.menu.extendItems', function($manager) {
            $manager->removeMainMenuItem('Rainlab.User', 'user');

            if (env('APP_ENV') != 'local') {
                $manager->removeMainMenuItem('Rainlab.Builder', 'builder');
            }
        });

        Validator::extend('bydomain', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            if(!empty($data['domain']) && !empty($data['email'])) {
                if(@!$data['id']) {
                    return !User::where('email', $data['email'])->whereHas('domain', function($q) use($data) {
                        $q->where('domain', $data['domain']);
                    })->first();
                } else {
                    return !User::where('id', '<>', $data['id'])->where('email', $data['email'])->whereHas('domain', function($q) use($data) {
                        $q->where('domain', $data['domain']);
                    })->first();
                }
            } else {
                return false;
            }
        });
    }

    public function registerNavigation()
    {
        return [
            'user' => [
                'label'       => 'rainlab.user::lang.users.menu_label',
                'url'         => Backend::URL('perevorot/users/users'),
                'icon'        => 'icon-user',
                'iconSvg'     => 'plugins/rainlab/user/assets/images/user-icon.svg',
                'permissions' => ['rem.users.*'],
                'order'       => 500,
            ]
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Perevorot\FormWidgets\UsersPermissionEditor' => 'userspermissioneditor',
            'Perevorot\FormWidgets\FormsPermissionEditor' => 'formspermissioneditor',
        ];
    }
}
