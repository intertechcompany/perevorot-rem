<?php

namespace Perevorot\Users\Controllers;

use BackendMenu;
use Illuminate\Http\RedirectResponse;
use RainLab\User\Controllers\Users as BaseUsers;

/**
 * Users Back-end Controller
 */
class Users extends BaseUsers
{
    protected $jsonable = [
        'data',
    ];

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend\Behaviors\ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $externalSettings;
    public $settings;

    public $requiredPermissions = [
        'rem.users.*'
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Perevorot.Users', 'user');
    }
}
