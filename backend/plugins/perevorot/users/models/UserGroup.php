<?php

namespace Perevorot\Users\Models;

use RainLab\User\Models\UserGroup as BaseUserGroup;
use ApplicationException;

/**
 * User Group Model
 */
class UserGroup extends BaseUserGroup
{
    protected $jsonable = ['permissions', 'forms_permissions'];

    public function scopeIsEdit($q)
    {
        return $q->where('permission', 'edit');
    }

    public function scopeIsRead($q)
    {
        return $q->where('permission', 'read');
    }

    public function scopeIsCreate($q)
    {
        return $q->where('permission', 'create');
    }

    public function scopeIsFileEdit($q)
    {
        return $q->where('permission', 'file_edit');
    }

    public function scopeIsFileRead($q)
    {
        return $q->where('permission', 'file_read');
    }

    public function scopeIsReadPersonal($q)
    {
        return $q->where('permission', 'read_p');
    }
}
