<?php

namespace Perevorot\Users\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Model;
use Perevorot\Forms\Models\Domain;
use RainLab\User\Models\User as BaseUser;
use RainLab\User\Models\Settings as UserSettings;

/**
 * User Model
 */
class User extends BaseUser
{
    protected $fillable = ['*'];

    public $rules = [
        'domain' => 'required',
        'email'    => 'required|between:6,255|email|bydomain',
        'avatar'   => 'nullable|image|max:4000',
        'username' => 'required|between:2,255|unique:users',
        'password' => 'required:create|between:4,255|confirmed',
        'password_confirmation' => 'required_with:password|between:4,255',
    ];

    public $belongsTo = [
        'domain' => ['Perevorot\Forms\Models\Domain'],
    ];

    public function scopeByDomain($q, $v)
    {
        if(!empty($v)) {
            $q->whereIn('domain', $v);
        }
    }

    public function getDomainIdOptions()
    {
        return Domain::all()->pluck('domain', 'id');
    }

    public function beforeValidate()
    {
        /*
         * Guests are special
         */
        if ($this->is_guest && !$this->password) {
            $this->generatePassword();
        }

        unset($this->rules['avatar']);
    }

    public function getGroupsOptions()
    {
        $result = [];

        foreach (UserGroup::all() as $group) {
            $result[$group->id] = [$group->name, $group->description];
        }

        return $result;
    }

    public function beforeSave()
    {
        if($this->region && $this->region < 10) {
            $this->region = '0'.$this->region;
        }
    }
}
