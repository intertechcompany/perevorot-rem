<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsForms13 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->boolean('is_calls')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->dropColumn('is_calls');
        });
    }
}
