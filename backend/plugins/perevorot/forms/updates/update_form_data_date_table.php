<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateFormDataDateTable extends Migration
{
    public function up()
    {
        Schema::table('form_data_date', function($table)
        {
            if(!Schema::hasColumn('form_data_date','user_id')) {
                $table->unsignedInteger('user_id')->index();
            }
        });
    }

    public function down()
    {
        Schema::table('form_data_date', function($table)
        {
            if(Schema::hasColumn('form_data_date','user_id')) {
                $table->dropColumn('user_id');
            }
        });
    }
}