<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddRealNameToFilesTable extends Migration
{
    public function up()
    {
        Schema::table('form_files', function($table)
        {
            $table->string('real_name')->nullable();
        });
    }

    public function down()
    {
        Schema::table('form_files', function($table)
        {
            $table->dropColumn('real_name');
        });
    }
}