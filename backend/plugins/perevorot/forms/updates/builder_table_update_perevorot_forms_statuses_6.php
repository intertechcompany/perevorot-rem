<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsStatuses6 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            if(!Schema::hasColumn('perevorot_forms_statuses','planned_time')) {
                $table->integer('planned_time')->nullable()->default(0);
            }
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            if(Schema::hasColumn('perevorot_forms_statuses','planned_time')) {
                $table->dropColumn('planned_time');
            }
        });
    }
}
