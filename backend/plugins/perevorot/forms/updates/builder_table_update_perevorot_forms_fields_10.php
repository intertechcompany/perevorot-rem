<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsFields10 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_fields', function($table)
        {
            $table->boolean('is_filter')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_fields', function($table)
        {
            $table->dropColumn('is_filter');
        });
    }
}
