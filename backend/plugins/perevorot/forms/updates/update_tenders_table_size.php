<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateTendersTableSize extends Migration
{
    public function up()
    {
        Schema::table('tenders', function($table)
        {
            $table->longText('field_value')->change();
        });
    }

    public function down()
    {
        Schema::table('tenders', function($table)
        {
            $table->string('field_value')->change();
        });
    }
}