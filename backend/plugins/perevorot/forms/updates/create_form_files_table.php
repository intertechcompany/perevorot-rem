<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormFilesTable extends Migration
{
    public function up()
    {
        Schema::create('form_files', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('form_id')->index();
            $table->unsignedInteger('form_data_id')->index()->nullable();
            $table->string('name');
            $table->timestamp('created_at')->nullable();
        });

        Schema::table('form_files', function($table)
        {
            $table->foreign('form_data_id')->references('id')->on('form_data')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('form_files');
    }
}