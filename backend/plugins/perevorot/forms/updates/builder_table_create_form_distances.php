<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFormDistances extends Migration
{
    public function up()
    {
        Schema::create('form_distances', function($table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('form_data_id')->nullable()->unsigned();
            $table->integer('form_data_parent_id')->nullable()->unsigned();

            $table->integer('distance')->nullable()->unsigned();
            $table->integer('duration')->nullable()->unsigned();
        });

        Schema::table('form_distances', function($table)
        {
            $table->foreign('form_data_id')->references('id')->on('form_data')->onDelete('cascade');
            $table->foreign('form_data_parent_id')->references('id')->on('form_data')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('form_distances');
    }
}