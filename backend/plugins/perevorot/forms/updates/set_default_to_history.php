<?php namespace Perevorot\Forms\Updates;

use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class setDefaultToHistory extends Migration
{
    public function up()
    {
        DB::table('perevorot_forms_fields')->update(['is_history'=>1]);
    }

    public function down()
    {
        // Schema::drop('perevorot_forms_table');
    }
}