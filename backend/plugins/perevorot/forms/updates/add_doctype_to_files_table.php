<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddDoctypeToFilesTable extends Migration
{
    public function up()
    {
        Schema::table('form_files', function($table)
        {
            $table->unsignedInteger('document_type_id')->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('form_files', function($table)
        {
            $table->dropColumn('document_type_id');
        });
    }
}