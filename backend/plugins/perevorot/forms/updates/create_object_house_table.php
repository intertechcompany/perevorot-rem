<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateObjectHouseTable extends Migration
{
    public function up()
    {
        Schema::create('form_objects_house_zones', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('object_id')->index();
            $table->unsignedInteger('house_id')->index();
            $table->string('value');

            $table->foreign('object_id')->references('id')->on('form_data')->onDelete('cascade');
            $table->foreign('house_id')->references('id')->on('form_data')->onDelete('cascade');
        });

    }

    public function down()
    {
        Schema::drop('form_objects_house_zones');
    }
}