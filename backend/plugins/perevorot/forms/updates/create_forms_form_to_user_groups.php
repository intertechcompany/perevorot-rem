<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormsFormToUserGroups extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_form_to_user_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('form_id')->nullable()->unsigned();
            $table->integer('user_group_id')->nullable()->unsigned();
            $table->string('permission', 10)->nullable();

            $table->index(['form_id', 'user_group_id'], 'form_to_user_group');

            $table->foreign('form_id')->references('id')->on('perevorot_forms_forms')->onDelete('cascade');
            $table->foreign('user_group_id')->references('id')->on('user_groups')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_forms_form_to_user_groups');
    }
}