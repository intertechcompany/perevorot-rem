<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateFormsDataTimesTable1 extends Migration
{
    public function up()
    {
        Schema::table('form_data_time', function($table)
        {
            $table->datetime('value')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('form_data_time', function($table)
        {
            $table->time('value')->change();
        });
    }
}