<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ChangeByUserGroupToForms extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->unsignedInteger('by_user_group')->change();
        });
    }

    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->boolean('by_user_group')->change();
        });
    }
}