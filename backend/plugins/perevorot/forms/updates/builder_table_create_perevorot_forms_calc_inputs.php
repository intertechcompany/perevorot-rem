<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsCalcInputs extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_calc_inputs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('form_data_id')->nullable()->unsigned();
            $table->text('json');
            $table->timestamp('created_at');
            $table->string('domain')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_calc_inputs');
    }
}
