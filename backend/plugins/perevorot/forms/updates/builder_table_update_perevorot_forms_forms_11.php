<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsForms11 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->text('hint')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->dropColumn('hint');
        });
    }
}
