<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsCalls extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_calls', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('form_id')->nullable()->index();
            $table->string('name')->nullable();
            $table->string('username');
            $table->string('call_url')->nullable();
            $table->integer('call_id');
            $table->timestamp('created_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_calls');
    }
}