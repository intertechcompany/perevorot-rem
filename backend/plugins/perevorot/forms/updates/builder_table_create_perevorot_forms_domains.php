<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsDomains extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_domains', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('domain');
            $table->string('name');
            $table->text('companies')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_domains');
    }
}
