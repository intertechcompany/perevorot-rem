<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTendersTable extends Migration
{
    public function up()
    {
        Schema::create('tenders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('tender_id')->index();
            $table->string('field_name');
            $table->string('field_value')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('tenders');
    }
}