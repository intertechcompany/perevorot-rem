<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTableCompetitors extends Migration
{
    public function up()
    {
        Schema::create('form_select_table_competitors', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
        });
    }

    public function down()
    {
        Schema::drop('form_select_table_competitors');
    }
}