<?php namespace Perevorot\Forms\Updates;

use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1032 extends Migration
{
    public function up()
    {
        DB::table('form_select_tables')->insert(['name' => 'perevorot_forms_document_types']);
        DB::table('form_select_tables')->insert(['name' => 'perevorot_forms_customers']);
    }

    public function down()
    {
        // Schema::drop('perevorot_forms_table');
    }
}