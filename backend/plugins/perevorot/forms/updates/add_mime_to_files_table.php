<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddMimeToFilesTable extends Migration
{
    public function up()
    {
        Schema::table('form_files', function($table)
        {
            $table->string('mime')->nullable();
        });
    }

    public function down()
    {
        Schema::table('form_files', function($table)
        {
            $table->dropColumn('mime');
        });
    }
}