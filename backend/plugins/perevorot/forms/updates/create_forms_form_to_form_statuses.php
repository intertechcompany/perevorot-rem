<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormsFormToFormStatuses extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_form_to_form_statuses', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('form_id')->nullable()->unsigned();
            $table->integer('form_status_id')->nullable()->unsigned();
            $table->string('permission', 10)->nullable();

            $table->index(['form_id', 'form_status_id'], 'form_to_form_status');

            $table->foreign('form_id')->references('id')->on('perevorot_forms_forms')->onDelete('cascade');
            $table->foreign('form_status_id')->references('id')->on('perevorot_forms_statuses')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_forms_form_to_form_statuses');
    }
}