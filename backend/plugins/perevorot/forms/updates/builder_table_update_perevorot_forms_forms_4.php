<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsForms4 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->string('button_save')->nullable();
            $table->string('button_add')->nullable();
            $table->smallInteger('after_saving')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->dropColumn('button_save');
            $table->dropColumn('button_add');
            $table->dropColumn('after_saving');
        });
    }
}
