<?php namespace Perevorot\Forms\Updates;

use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class CascadeDeleteFormData extends Migration
{
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('form_data', function($table)
        {
            $table->foreign('form_id')->references('id')->on('perevorot_forms_forms')->onDelete('cascade');
        });

        Schema::table('form_data_boolean', function($table)
        {
            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
        });

        Schema::table('form_data_text', function($table)
        {
            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
        });

        Schema::table('form_data_varchar', function($table)
        {
            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
        });

        Schema::table('form_data_decimal', function($table)
        {
            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
        });

        Schema::table('form_data_number', function($table)
        {
            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    public function down()
    {
    }
}