<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormsStatusToUserGroups extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_form_status_to_user_group', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('form_status_id')->nullable()->unsigned();
            $table->integer('user_group_id')->nullable()->unsigned();

            $table->index(['form_status_id', 'user_group_id'], 'form_status_to_user_group');

            $table->foreign('form_status_id')->references('id')->on('perevorot_forms_statuses')->onDelete('cascade');
            $table->foreign('user_group_id')->references('id')->on('user_groups')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_forms_form_status_to_user_group');
    }
}