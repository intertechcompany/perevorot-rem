<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormLogsTable extends Migration
{
    public function up()
    {
        Schema::create('form_data_logs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('form_data_id')->index()->nullable();
            $table->string('status');
            $table->string('user_name');
            $table->text('comment')->nullable();
            $table->timestamp('created_at')->nullable();
        });

        Schema::table('form_data_logs', function($table)
        {
            $table->foreign('form_data_id')->references('id')->on('form_data')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('form_data_logs');
    }
}