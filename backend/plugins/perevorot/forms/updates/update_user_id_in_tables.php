<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUserIdInTables extends Migration
{
    public function up()
    {
        Schema::table('form_data', function($table)
        {
            $table->unsignedInteger('user_id')->nullable()->change();
        });

        Schema::table('form_data_text', function($table)
        {
            $table->unsignedInteger('user_id')->nullable()->change();
        });

        Schema::table('form_data_number', function($table)
        {
            $table->unsignedInteger('user_id')->nullable()->change();
        });

        Schema::table('form_data_decimal', function($table)
        {
            $table->unsignedInteger('user_id')->nullable()->change();
        });

        Schema::table('form_data_varchar', function($table)
        {
            $table->unsignedInteger('user_id')->nullable()->change();
        });

        Schema::table('form_data_boolean', function($table)
        {
            $table->unsignedInteger('user_id')->nullable()->change();
        });

        Schema::table('form_data_date', function($table)
        {
            $table->unsignedInteger('user_id')->nullable()->change();
        });
    }

    public function down()
    {
    }
}