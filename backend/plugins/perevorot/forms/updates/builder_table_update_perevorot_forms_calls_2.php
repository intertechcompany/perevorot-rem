<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsCalls2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_calls', function($table)
        {
            $table->text('comment')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_calls', function($table)
        {
            $table->dropColumn('comment');
        });
    }
}
