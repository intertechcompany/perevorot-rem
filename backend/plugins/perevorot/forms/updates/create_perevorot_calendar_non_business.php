<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePerevorotCalendarNonBusiness extends Migration
{
    public function up()
    {
        Schema::create('perevorot_calendar_non_business', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->date('date');
            $table->enum('type', ['saturday', 'sunday', 'holiday']);
            $table->timestamp('created_at')->nullable();
            $table->softDeletes()->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('perevorot_calendar_non_business');
    }
}