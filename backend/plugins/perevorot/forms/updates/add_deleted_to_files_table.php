<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddDeletedToFilesTable extends Migration
{
    public function up()
    {
        Schema::table('form_files', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('form_files', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}