<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddUserIdToTables extends Migration
{
    public function up()
    {
        Schema::table('form_data_text', function($table)
        {
            $table->unsignedInteger('user_id')->index();
        });

        Schema::table('form_data_number', function($table)
        {
            $table->unsignedInteger('user_id')->index();
        });

        Schema::table('form_data_decimal', function($table)
        {
            $table->unsignedInteger('user_id')->index();
        });

        Schema::table('form_data_varchar', function($table)
        {
            $table->unsignedInteger('user_id')->index();
        });

        Schema::table('form_data_boolean', function($table)
        {
            $table->unsignedInteger('user_id')->index();
        });
    }

    public function down()
    {
        Schema::table('form_data_text', function($table)
        {
            $table->dropColumn('user_id');
        });

        Schema::table('form_data_number', function($table)
        {
            $table->dropColumn('user_id');
        });

        Schema::table('form_data_decimal', function($table)
        {
            $table->dropColumn('user_id');
        });

        Schema::table('form_data_varchar', function($table)
        {
            $table->dropColumn('user_id');
        });

        Schema::table('form_data_boolean', function($table)
        {
            $table->dropColumn('user_id');
        });
    }
}