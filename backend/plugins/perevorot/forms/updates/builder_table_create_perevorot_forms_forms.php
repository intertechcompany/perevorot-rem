<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsForms extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_forms', function($table)
        {
            $table->engine = 'InnoDB';
            
            $table->increments('id');

            $table->string('name', 255);
            $table->string('code', 255);
            
            $table->boolean('is_enabled')->default(0);
            $table->timestamp('created_at')->nullable();
            
            $table->index(['code', 'is_enabled']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_forms');
    }
}
