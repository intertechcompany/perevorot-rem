<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddIsAutocompleteToFormStatuses extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->boolean('is_autocomplete')->nullable()->default(0)->index();
        });
    }

    public function down()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->dropColumn('is_autocomplete');
        });
    }
}