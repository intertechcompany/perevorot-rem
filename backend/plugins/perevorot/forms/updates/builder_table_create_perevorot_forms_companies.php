<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsCompanies extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_companies', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('name');
            $table->string('code');
            $table->string('url')->nullable();
            $table->string('ticket')->nullable();
            $table->unsignedInteger('domain_id')->index();
        });

        Schema::table('perevorot_forms_companies', function($table)
        {
            $table->foreign('domain_id')->references('id')->on('perevorot_forms_domains')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_companies');
    }
}