<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormsDataDatesTables extends Migration
{
    public function up()
    {
        Schema::create('form_data_date', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('form_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->datetime('value');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('form_data_date');
    }
}