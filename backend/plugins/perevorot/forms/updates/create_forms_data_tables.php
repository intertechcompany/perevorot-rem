<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormsDataTables extends Migration
{
    public function up()
    {
        // texarea
        Schema::create('form_data_text', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('form_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->mediumText('value');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        // number
        Schema::create('form_data_number', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('form_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->integer('value');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        // decimal, currency_uah, currency_usd
        Schema::create('form_data_decimal', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('form_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->float('value');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        // text, select, checkbox, phone, map
        Schema::create('form_data_varchar', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('form_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->string('value');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        // switch
        Schema::create('form_data_boolean', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('form_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->boolean('value');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('form_data_boolean');
        Schema::drop('form_data_varchar');
        Schema::drop('form_data_decimal');
        Schema::drop('form_data_text');
        Schema::drop('form_data_number');
    }
}