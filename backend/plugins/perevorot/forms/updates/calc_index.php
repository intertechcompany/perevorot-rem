<?php namespace Perevorot\Forms\Updates;

use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class CalcIndex extends Migration
{
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('perevorot_forms_calc_inputs', function($table)
        {
            $table->foreign('form_data_id')->references('id')->on('form_data')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    public function down()
    {
        // Schema::drop('perevorot_forms_table');
    }
}