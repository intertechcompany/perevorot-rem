<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsIncomingCalls extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_incoming_calls', function($table)
        {
            $table->unsignedInteger('billsec')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_incoming_calls', function($table)
        {
            $table->dropColumn('billsec');
        });
    }
}