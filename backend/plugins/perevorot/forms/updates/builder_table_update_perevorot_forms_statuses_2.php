<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsStatuses2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->boolean('is_comment')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->dropColumn('is_comment');
        });
    }
}
