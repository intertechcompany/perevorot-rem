<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsForms7 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->integer('domain_id')->nullable()->index();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->dropColumn('domain_id');
        });
    }
}
