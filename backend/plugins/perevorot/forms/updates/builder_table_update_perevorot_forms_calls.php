<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsCalls extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_calls', function($table)
        {
            $table->integer('billsec')->nullable()->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_calls', function($table)
        {
            $table->dropColumn('billsec');
        });
    }
}
