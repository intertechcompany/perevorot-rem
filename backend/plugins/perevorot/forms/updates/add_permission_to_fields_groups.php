<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddPermissionToFieldsGroups extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_field_to_user_group', function($table)
        {
            $table->string('permission', 10)->nullable();
        });
    }

    public function down()
    {
        Schema::table('perevorot_forms_field_to_user_group', function($table)
        {
            $table->dropColumn('permission');
        });
    }
}