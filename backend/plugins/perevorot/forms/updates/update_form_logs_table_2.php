<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateFormLogsTable2 extends Migration
{
    public function up()
    {
        Schema::table('form_data_logs', function($table)
        {
            $table->string('substatus')->nullable();
        });
    }

    public function down()
    {
        Schema::table('form_data_logs', function($table)
        {
            $table->dropColumn('substatus');
        });
    }
}