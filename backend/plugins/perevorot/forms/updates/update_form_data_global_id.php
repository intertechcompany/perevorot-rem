<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateFormDataGlobalId extends Migration
{
    public function up()
    {
        Schema::table('form_data', function($table)
        {
            $table->string('global_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('form_data', function($table)
        {
            $table->dropColumn('global_id');
        });
    }
}