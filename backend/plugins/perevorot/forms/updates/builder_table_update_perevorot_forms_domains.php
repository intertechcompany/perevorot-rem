<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsDomains extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_domains', function($table)
        {
            $table->dropColumn('companies');
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_domains', function($table)
        {
            $table->text('companies')->nullable();
        });
    }
}
