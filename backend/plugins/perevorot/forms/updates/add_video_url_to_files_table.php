<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddVideoUrlToFilesTable extends Migration
{
    public function up()
    {
        Schema::table('form_files', function($table)
        {
            $table->string('video_url')->nullable();
        });
    }

    public function down()
    {
        Schema::table('form_files', function($table)
        {
            $table->dropColumn('video_url');
        });
    }
}