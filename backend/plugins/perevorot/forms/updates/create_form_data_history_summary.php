<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormDataHistorySummary extends Migration
{
    public function up()
    {
        Schema::dropIfExists('form_data_history_summary');

        Schema::create('form_data_history_summary', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->index()->nullable();
            $table->unsignedInteger('form_id')->index()->nullable();
            $table->enum('action', ['form_created', 'form_edited', 'file_upload', 'status']);
            $table->string('comment')->nullable();
            $table->timestamp('created_at');

            $table->foreign('form_id')->references('id')->on('form_data')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::drop('form_data_history_summary');
    }
}