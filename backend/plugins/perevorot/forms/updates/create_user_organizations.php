<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateUserOrganizations extends Migration
{
    public function up()
    {
        Schema::create('user_organizations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('organization_id');
        });
    }

    public function down()
    {
        Schema::drop('form_data');
    }
}