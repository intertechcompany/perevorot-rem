<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsFields extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_fields', function($table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('form_id')->unsigned()->nullable();
            $table->string('name', 255);
            $table->string('placeholder', 255)->nullable();
            $table->string('code', 255);
            $table->string('type');
            $table->boolean('is_required')->default(0);
            $table->text('options')->nullable();
            $table->integer('parent_field_id')->nullable()->unsigned();

            $table->boolean('is_parent')->default(0);
            $table->string('parent_field_condition')->nullable();
            $table->text('parent_field_value')->nullable();

            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->boolean('is_enabled')->default(0);

            $table->index('form_id');
            $table->index('code');
            $table->index('is_enabled');
            $table->index('parent_field_id');
            $table->index('sort_order');
        });

        Schema::table('perevorot_forms_fields', function($table)
        {
            $table->foreign('form_id')->references('id')->on('perevorot_forms_forms')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_fields');
    }
}
