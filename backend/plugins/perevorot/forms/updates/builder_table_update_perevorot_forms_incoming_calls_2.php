<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsIncomingCalls2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_incoming_calls', function($table)
        {
            $table->string('name')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_incoming_calls', function($table)
        {
            $table->dropColumn('name');
        });
    }
}
