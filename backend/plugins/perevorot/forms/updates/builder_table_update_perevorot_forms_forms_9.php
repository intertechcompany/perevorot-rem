<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsForms9 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            if(!Schema::hasColumn('perevorot_forms_forms', 'is_company_field')) {
                $table->boolean('is_company_field')->nullable()->default(0);
            }
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            if(Schema::hasColumn('perevorot_forms_forms', 'is_company_field')) {
                $table->dropColumn('is_company_field');
            }
        });
    }
}
