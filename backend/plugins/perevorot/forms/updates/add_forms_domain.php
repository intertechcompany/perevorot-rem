<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddFormsDomain extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->string('domain')->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->dropColumn('domain');
        });
    }
}