<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class DropUniqueLogin extends Migration
{
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->dropUnique('users_login_unique');
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->unique('username', 'users_login_unique');
        });
    }
}