<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddStatusIdToFormData extends Migration
{
    public function up()
    {
        Schema::table('form_data_logs', function($table)
        {
            $table->string('status_code');
        });
    }

    public function down()
    {
        Schema::table('form_data_logs', function($table)
        {
            $table->dropColumn('status_code');
        });
    }
}