<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Change2ByUserGroupToForms extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->string('by_user_group')->change();
        });
    }

    public function down()
    {
        Schema::table('perevorot_forms_forms', function($table)
        {
            $table->unsignedInteger('by_user_group')->change();
        });
    }
}