<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsStatuses5 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->integer('planned_time')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->dropColumn('planned_time');
        });
    }
}
