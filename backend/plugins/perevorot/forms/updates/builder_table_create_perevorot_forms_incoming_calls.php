<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsIncomingCalls extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_incoming_calls', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('call_id')->unsigned();
            $table->string('call_url')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('is_verified')->nullable()->default(0);
            $table->dateTime('call_time')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_incoming_calls');
    }
}