<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsStatuses4 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->text('children')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->dropColumn('children');
        });
    }
}
