<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormsFieldsToUserGroups extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_field_to_user_group', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('form_field_id')->nullable()->unsigned();
            $table->integer('user_group_id')->nullable()->unsigned();

            $table->index(['form_field_id', 'user_group_id'], 'form_field_to_user_group');

            $table->foreign('form_field_id')->references('id')->on('perevorot_forms_fields')->onDelete('cascade');
            $table->foreign('user_group_id')->references('id')->on('user_groups')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_forms_field_to_user_group');
    }
}