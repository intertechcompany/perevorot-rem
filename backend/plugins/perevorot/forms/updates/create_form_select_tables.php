<?php namespace Perevorot\Forms\Updates;

use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormSelectTables extends Migration
{
    public function up()
    {
        Schema::create('form_select_tables', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });
        DB::table('form_select_tables')->insert(['name' => 'form_select_table_competitors']);
    }

    public function down()
    {
        Schema::drop('form_select_tables');
    }
}