<?php namespace Perevorot\Forms\Updates;

use Illuminate\Support\Facades\DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateTableCompetitors1 extends Migration
{
    public function up()
    {
        Schema::table('form_select_table_competitors', function ($table) {
            $table->integer('sort_order')->unsigned()->nullable(1)->index();
        });

        DB::statement('update form_select_table_competitors set sort_order = id');
    }

    public function down()
    {
        Schema::table('form_select_table_competitors', function ($table) {
            $table->dropColumn('sort_order');
        });
    }
}