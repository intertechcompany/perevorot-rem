<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsFields2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_fields', function($table)
        {
            $table->boolean('is_list')->nullable()->default(0);
            $table->string('class')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_fields', function($table)
        {
            $table->dropColumn('is_list');
            $table->dropColumn('class');
        });
    }
}
