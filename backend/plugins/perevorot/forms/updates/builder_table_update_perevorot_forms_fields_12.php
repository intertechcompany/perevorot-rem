<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotFormsFields12 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_forms_fields', function($table)
        {
            $table->boolean('is_equivalent')->nullable()->default(0);
            $table->boolean('is_infobox')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_forms_fields', function($table)
        {
            $table->dropColumn('is_equivalent');
            $table->boolean('is_infobox')->default(null)->change();
        });
    }
}
