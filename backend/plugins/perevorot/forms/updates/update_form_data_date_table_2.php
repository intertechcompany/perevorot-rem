<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateFormDataDateTable2 extends Migration
{
    public function up()
    {
        Schema::table('form_data_date', function($table)
        {
            $table->date('value')->change();
        });
    }

    public function down()
    {
        Schema::table('form_data_date', function($table)
        {
            $table->datetime('value')->change();
        });
    }
}