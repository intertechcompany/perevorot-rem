<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateFormLogsTable extends Migration
{
    public function up()
    {
        Schema::table('form_data_logs', function($table)
        {
            $table->unsignedInteger('user_id')->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    public function down()
    {


        Schema::table('form_data_logs', function($table)
        {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });
    }
}