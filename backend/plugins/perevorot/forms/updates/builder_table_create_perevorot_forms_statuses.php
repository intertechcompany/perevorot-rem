<?php namespace Perevorot\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotFormsStatuses extends Migration
{
    public function up()
    {
        Schema::create('perevorot_forms_statuses', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('form_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('code');
            $table->text('dependents')->nullable();

            $table->index('code');
        });

        Schema::table('perevorot_forms_statuses', function($table)
        {
            $table->foreign('form_id')->references('id')->on('perevorot_forms_forms')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_forms_statuses');
    }
}