<?php namespace Perevorot\Forms;

use Backend;
use Perevorot\Forms\Models\Field;
use Perevorot\Forms\Models\FormStatus;
use Validator;
use System\Classes\PluginBase;
use App;

/**
 * Forms Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Validator::extend('statuscode', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            if(!empty($data['form_id'])) {
                $q = FormStatus::where('form_id', $data['form_id'])->where('code', $value);

                if (!empty($data['id'])) {
                    $q->where('id', '<>', $data['id']);
                }

                $isset = $q->value('id');
                return empty($isset);
            } else {
                return true;
            }
        });

        Validator::extend('fieldcode', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            if(!empty($data['form_id'])) {
                $q = Field::where('form_id', $data['form_id'])->where('code', $value);

                if (!empty($data['id'])) {
                    $q->where('id', '<>', $data['id']);
                }

                $isset = $q->value('id');
                return empty($isset);
            } else {
                return true;
            }
        });
    }
}
