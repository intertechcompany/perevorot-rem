<?php namespace Perevorot\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Customers extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'forms.customers' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Forms', 'tables', 'customers');
    }
}
