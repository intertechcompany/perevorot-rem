<?php namespace Perevorot\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Tables extends Controller
{
    public $implement = ['Backend\Behaviors\ListController'];
    
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'forms.tables'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Forms', 'tables', '');
    }
}