<?php namespace Perevorot\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Perevorot\Forms\Models\Field;

class Objects extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend.Behaviors.RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = [
        'forms.objects'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Forms', 'objects', 'forms');
    }

    public function formExtendQuery($q)
    {
        //with(['fieldsText.field','fieldsVarchar.field']);
    }
}
