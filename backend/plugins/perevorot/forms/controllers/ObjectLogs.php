<?php namespace Perevorot\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Perevorot\Forms\Models\Field;

class ObjectLogs extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'forms.logs'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Forms', 'objects', 'logs');
    }
}
