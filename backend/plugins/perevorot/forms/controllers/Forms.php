<?php namespace Perevorot\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Perevorot\Forms\Models\Field;
use Flash;
use Perevorot\Forms\Models\Form;
use Backend;
use Perevorot\Forms\Models\FormStatus;

class Forms extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend.Behaviors.RelationController',
    ];

    public $relationConfig = 'config_relation.yaml';
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'forms.forms' 
    ];

    public $form;
    public $existForm;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Forms', 'forms', 'forms');
    }

    public function index()
    {
        Field::whereNull('form_id')->delete();
        FormStatus::whereNull('form_id')->delete();
        parent::index();
    }

    public function update($id)
    {
        $this->addCss('/plugins/perevorot/forms/assets/css/sortable.css');
        $this->addJs('/plugins/perevorot/forms/assets/js/html5sortable.js');
        $this->addJs('/plugins/perevorot/forms/assets/js/sortable.js');

        parent::update($id);
    }

    public function import()
    {
        BackendMenu::setContext('Perevorot.Forms', 'forms', 'forms');
    }

    public function onImportConfirm()
    {
        $this->form = json_decode(file_get_contents(storage_path('temp/form_import.json')));
        $form = Form::with(['fields','statuses'])->where('code', $this->form->code)->first();

        if(!$form) {
            $form = new Form();
            $form->code = $this->form->code;
        }

        $form->after_saving = @$this->form->after_saving;
        $form->button_add = @$this->form->button_add;
        $form->button_save = @$this->form->button_save;
        $form->parent_form = @$this->form->parent_form;
        $form->name = $this->form->name;
        $form->is_hide = $this->form->is_hide;

        $form->save();

        foreach($this->form->fields as $field) {

            $_field = $form->fields->where('code', $field->code)->first();

            if(!$_field) {
                $_field = new Field();
            }

            foreach($field as $key => $value) {
                $_field->$key = $field->$key;
            }

            if($_field->type == 'selectTable' && !empty($field->options[0])) {
                if(!\Schema::hasTable($field->options[0])) {
                    $_field->options = null;
                }
            }

            $form->fields()->save($_field);
        }

        foreach($this->form->statuses as $status) {
            $_status = $form->statuses->where('code', $status->code)->first();

            if(!$_status) {
                $_status = new FormStatus();
            }

            foreach($status as $key => $value) {
                $_status->$key = $status->$key;
            }

            $form->statuses()->save($_status);
        }

        @unlink(storage_path('temp/form_import.json'));
    }

    public function onImport()
    {
        @unlink(storage_path('temp/form_import.json'));
        \Request::file('file')->move(storage_path('temp'), 'form_import.json');

        $this->form = json_decode(file_get_contents(storage_path('temp/form_import.json')));
        $this->existForm = Form::with('fields')->where('code', $this->form->code)->first();
    }

    public function update_onExport()
    {
        $form = Form::with(['fields','statuses'])->find(post('model_id'));
        $pathToFile = storage_path('temp/form-'.$form->code.'.json');

        if($form->after_saving === null) {
            unset($form->after_saving);
        }
        if($form->button_save === null) {
            unset($form->button_save);
        }
        if($form->button_add === null) {
            unset($form->button_add);
        }
        if($form->parent_form === null) {
            unset($form->parent_form);
        }

        $form->fields->each(function(&$v, $k) {
            if($v->options === null) {
                unset($v->options);
            }
            if($v->parent_field_id === null) {
                unset($v->parent_field_id);
            }
            if($v->parent_field_condition === null) {
                unset($v->parent_field_condition);
            }
            if($v->parent_field_value === null) {
                unset($v->parent_field_value);
            }
            if($v->placeholder === null) {
                unset($v->placeholder);
            }
            if($v->class === null) {
                unset($v->class);
            }
        });
        $form->statuses->each(function(&$v, $k) {
            if(empty($v->dependents)) {
                unset($v->dependents);
            }
        });

        if(file_put_contents($pathToFile, $form->toJson(JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT))) {
            return Backend::redirect('perevorot/forms/forms/download-file?code='.$form->code);
        }
    }

    public function downloadFile()
    {
        return response()->download(storage_path('temp/form-'.get('code').'.json'))->deleteFileAfterSend(true);
    }

    public function update_onUpdateFieldsPosition()
    {
        $moved = [];
        $position = 0;
        if (($reorderIds = post('checked')) && is_array($reorderIds) && count($reorderIds)) {
            foreach ($reorderIds as $id) {
                if (in_array($id, $moved) || !$record = Field::find($id))
                    continue;
                $record->sort_order = $position;
                $record->save();
                $moved[] = $id;
                $position++;
            }
            Flash::success('Successfully re-ordered fields.');
        }

        $model = Form::find(post('model_id'));
        $this->initForm($model);
        $this->initRelation($model, 'fields');

        return $this->relationRefresh('fields');
    }

    public function update_onUpdateStatusesPosition()
    {
        $moved = [];
        $position = 0;
        if (($reorderIds = post('checked')) && is_array($reorderIds) && count($reorderIds)) {
            foreach ($reorderIds as $id) {
                if (in_array($id, $moved) || !$record = FormStatus::find($id))
                    continue;
                $record->sort_order = $position;
                $record->save();
                $moved[] = $id;
                $position++;
            }
            Flash::success('Successfully re-ordered statuses.');
        }

        $model = Form::find(post('model_id'));
        $this->initForm($model);
        $this->initRelation($model, 'statuses');

        return $this->relationRefresh('statuses');
    }
}
