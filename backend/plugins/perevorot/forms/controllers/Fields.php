<?php namespace Perevorot\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;
use Flash;
use Perevorot\Forms\Models\Field;
use Perevorot\Forms\Models\Form;

class Fields extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'forms.fields' 
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function listExtendQuery($query)
    {
        $query->orderBy('sort_order', 'asc');
    }
}
