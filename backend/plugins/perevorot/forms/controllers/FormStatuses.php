<?php namespace Perevorot\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;

class FormStatuses extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'forms.statuses' 
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function listExtendQuery($query)
    {
        $query->orderBy('sort_order', 'asc');
    }
}
