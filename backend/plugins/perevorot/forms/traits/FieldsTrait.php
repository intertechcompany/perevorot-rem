<?php

namespace Perevorot\Forms\Traits;

trait FieldsTrait
{
    public function getFiles()
    {
        $files = $this->form->files;
        $values = [];

        if(!$files->isEmpty()) {
            foreach($files as $file) {
                $values[] = env('PUBLIC_DOMAIN').'/'.$file->name;
            }
        }

        $this->value = $values;
    }
    
    public function parseOptions()
    {
        $field = $this->field;

        if (in_array($field->type, ['select', 'selectTable', 'checkbox', 'radio'])) {
            if (!empty($field->options)) {

                $options = !is_array($field->options) ? json_decode($field->options) : $field->options;
                $values = [];

                foreach ($options as $opt) {
                    if ($field->type != 'checkbox' && $opt['code'] == $this->value) {
                        $values = $opt['name'];
                    } elseif ($field->type == 'checkbox') {
                        $itemValue = (array)json_decode($this->value);

                        $t = array_first($itemValue, function ($v, $k) use ($opt) {
                            return $opt['code'] == $v;
                        });

                        if (!empty($t)) {
                            $values[] = $opt['name'];
                        }
                    }
                }

                $this->value = $values;
            }
        }
    }
}