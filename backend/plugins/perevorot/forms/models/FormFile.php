<?php namespace Perevorot\Forms\Models;

use Illuminate\Support\Carbon;
use Model;

/**
 * Model
 */
class FormFile extends Model
{
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'form_files';
}
