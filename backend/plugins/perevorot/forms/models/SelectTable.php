<?php namespace Perevorot\Forms\Models;

use Illuminate\Support\Carbon;
use Model;

/**
 * Model
 */
class SelectTable extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'=>'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'form_select_tables';
}
