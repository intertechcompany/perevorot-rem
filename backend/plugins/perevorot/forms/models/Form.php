<?php namespace Perevorot\Forms\Models;

use Illuminate\Support\Carbon;
use Model;
use Perevorot\Users\Models\User;
use Perevorot\Users\Models\UserGroup;
use DB;

/**
 * Model
 */
class Form extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $hidden = ['id','created_at','is_enabled'];

    public $jsonable = ['by_user_group'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'=>'required',
        'code'=>'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_forms_forms';

    public $belongsTo = [
        'domain' => ['Perevorot\Forms\Models\Domain'],
        'user' => ['Perevorot\Forms\Models\User'],
    ];

    public $hasMany = [
        'fields' => ['Perevorot\Forms\Models\Field'],
        'statuses' => ['Perevorot\Forms\Models\FormStatus'],
    ];

    public $belongsToMany = [
        'form_statuses_read' => [
            'Perevorot\Forms\Models\FormStatus',
            'table'    => 'perevorot_forms_form_to_form_statuses',
            'key' => 'form_id',
            'otherKey' => 'form_status_id',
            'scope' => "isRead"
        ],
        'form_statuses_edit' => [
            'Perevorot\Forms\Models\FormStatus',
            'table'    => 'perevorot_forms_form_to_form_statuses',
            'key' => 'form_id',
            'otherKey' => 'form_status_id',
            'scope' => "isEdit"
        ],
        'user_groups_edit' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_form_to_user_groups',
            'key' => 'form_id',
            'otherKey' => 'user_group_id',
            'scope' => "isEdit"
        ],
        'user_groups_read' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_form_to_user_groups',
            'key' => 'form_id',
            'otherKey' => 'user_group_id',
            'scope' => "isRead"
        ],
        'user_groups_create' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_form_to_user_groups',
            'key' => 'form_id',
            'otherKey' => 'user_group_id',
            'scope' => "isCreate"
        ],
        'user_groups_file_edit' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_form_to_user_groups',
            'key' => 'form_id',
            'otherKey' => 'user_group_id',
            'scope' => "isFileEdit"
        ],
        'user_groups_file_read' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_form_to_user_groups',
            'key' => 'form_id',
            'otherKey' => 'user_group_id',
            'scope' => "isFileRead"
        ],
        'user_groups_read_personal' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_form_to_user_groups',
            'key' => 'form_id',
            'otherKey' => 'user_group_id',
            'scope' => "isReadPersonal"
        ],
    ];

    public function getDomainIdOptions()
    {
        return Domain::all()->pluck('domain', 'id');
    }

    public function getFormStatusesEditOptions()
    {
        if(!$this->statuses->isEmpty()) {
            return $this->statuses->lists('name', 'id');
        }
        else {
            return [];
        }
    }

    public function getFormStatusesReadOptions()
    {
        if(!$this->statuses->isEmpty()) {
            return $this->statuses->lists('name', 'id');
        }
        else {
            return [];
        }
    }

    public function getByUserGroupOptions()
    {
        $data = UserGroup::all();
        $data->push(['id'=>0,'name'=>'все']);
        $data = $data->lists('name', 'id');

        return $data;
    }

    public function getUserGroupsReadOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getUserGroupsReadPersonalOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getUserGroupsEditOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getUserGroupsCreateOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getUserGroupsFileReadOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getUserGroupsFileEditOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function afterSave() {
        $inputs = post();
        $groups = UserGroup::all();
        $form = $this;

        if(empty($inputs)) {
            return;
        }

        $statuses = $form->statuses;

        DB::table('perevorot_forms_form_to_form_statuses')->whereNull('permission')->delete();
        DB::table('perevorot_forms_form_to_form_statuses')->where('form_id', $form->id)->delete();
        DB::table('perevorot_forms_form_to_user_groups')->whereNull('permission')->delete();
        DB::table('perevorot_forms_form_to_user_groups')->where('form_id', $form->id)->delete();

        if(!empty($inputs['Form']['form_statuses_edit'])) {
            foreach($inputs['Form']['form_statuses_edit'] as $postItem) {
                $item = $statuses->where('id', $postItem)->first();
                $form->form_statuses_edit()->save($item, [
                    'permission' => 'edit',
                ]);
            }
        }

        if(!empty($inputs['Form']['form_statuses_read'])) {
            foreach($inputs['Form']['form_statuses_read'] as $postItem) {
                $item = $statuses->where('id', $postItem)->first();
                $form->form_statuses_read()->save($item, [
                    'permission' => 'read',
                ]);
            }
        }

        if(!empty($inputs['Form']['user_groups_edit'])) {
            foreach($inputs['Form']['user_groups_edit'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $form->user_groups_edit()->save($item, [
                    'permission' => 'edit',
                ]);
            }
        }

        if(!empty($inputs['Form']['user_groups_read'])) {
            foreach($inputs['Form']['user_groups_read'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $form->user_groups_read()->save($item, [
                    'permission' => 'read',
                ]);
            }
        }

        if(!empty($inputs['Form']['user_groups_read_personal'])) {
            foreach($inputs['Form']['user_groups_read_personal'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $form->user_groups_read_personal()->save($item, [
                    'permission' => 'read_p',
                ]);
            }
        }

        if(!empty($inputs['Form']['user_groups_create'])) {
            foreach($inputs['Form']['user_groups_create'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $form->user_groups_create()->save($item, [
                    'permission' => 'create',
                ]);
            }
        }

        if(!empty($inputs['Form']['user_groups_file_edit'])) {
            foreach($inputs['Form']['user_groups_file_edit'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $form->user_groups_file_edit()->save($item, [
                    'permission' => 'file_edit',
                ]);
            }
        }

        if(!empty($inputs['Form']['user_groups_file_read'])) {
            foreach($inputs['Form']['user_groups_file_read'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $form->user_groups_file_read()->save($item, [
                    'permission' => 'file_read',
                ]);
            }
        }
    }

    public function getParentFormOptions()
    {
        $forms = Form::where('id', '<>', $this->id)->get();
        $forms->prepend(['id'=>0,'name'=>'не важно']);
        $forms = $forms->lists('name', 'id');

        return $forms;
    }

    public function getRelationFormOptions()
    {
        $forms = Form::where('id', '<>', $this->id)->get();
        $forms->prepend(['id'=>0,'name'=>'не важно']);
        $forms = $forms->lists('name', 'id');

        return $forms;
    }

    public function beforeCreate()
    {
        $this->created_at = Carbon::now();
    }

    public function scopeEnabled($q)
    {
        $q->where('is_enabled', 1);
    }

    public function scopeByDomain($q, $v)
    {
        if(!empty($v)) {
            $q->where('domain_id', $v);
        }
    }
}
