<?php namespace Perevorot\Forms\Models;

use Carbon\Carbon;
use Model;
use October\Rain\Database\Traits\SoftDelete;
use Perevorot\Users\Models\User;

/**
 * Calendar Model
 */
class Calendar extends Model
{
    use SoftDelete, \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_calendar_non_business';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $timestamps = false;
    public $dates = ['created_at','deleted_at'];

    public $rules = [
        'date'=>'required',
        'user_id'=>'required',
        'type'=>'required',
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => ['Perevorot\Users\Models\User'],
    ];

    public function getUserIdOptions()
    {
        return User::all()->lists('name', 'id');
    }

    public function beforeSave()
    {
        $this->created_at = Carbon::now();
    }
}
