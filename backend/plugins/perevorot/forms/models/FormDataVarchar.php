<?php namespace Perevorot\Forms\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Model;
use Perevorot\Forms\Traits\FieldsTrait;

class FormDataVarchar extends Model
{
    use SoftDeletes, FieldsTrait;

    public $timestamps = false;
    protected $dates = ['update_at', 'deleted_at'];
    public $table = 'form_data_varchar';

    public $belongsTo = [
        'field' => ['Perevorot\Forms\Models\Field'],
        'form' => ['Perevorot\Forms\Models\FormData'],
    ];

    public function getFieldValueAttribute()
    {
        if(in_array(@$this->field->type, ['checkbox', 'select', 'radio'])) {
            $this->parseOptions();
        }
        elseif(@$this->field->type == 'switch') {
            $this->value = $this->value ? 'Да' : 'Нет';
        }
        elseif(@$this->field->type == 'file') {
            $this->getFiles();
            return $this->value;
        }

        $value = $this->value;

        if(is_array($value)) {
            $value = implode(', ', $value);
        }

        return $value;
    }
}
