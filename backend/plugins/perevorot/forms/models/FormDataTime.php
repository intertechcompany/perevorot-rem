<?php namespace Perevorot\Forms\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Model;
use Perevorot\Forms\Traits\FieldsTrait;

class FormDataTime extends Model
{
    use SoftDeletes, FieldsTrait;

    public $timestamps = false;
    protected $dates = ['update_at', 'deleted_at'];
    public $table = 'form_data_time';

    public $belongsTo = [
        'field' => ['Perevorot\Forms\Models\Field'],
    ];
}
