<?php namespace Perevorot\Forms\Models;

use Illuminate\Support\Carbon;
use Model;
use October\Rain\Database\Traits\Sortable;

/**
 * Model
 */
class FieldSelectCompetitor extends Model
{
    use \October\Rain\Database\Traits\Validation, Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'=>'required',
        'code'=>'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'form_select_table_competitors';

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];
}
