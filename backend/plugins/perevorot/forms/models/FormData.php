<?php namespace Perevorot\Forms\Models;

use Illuminate\Support\Carbon;
use Model;
use Perevorot\Users\Models\User;

/**
 * Model
 */
class FormData extends Model
{
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'form_data';

    public $belongsTo = [
        'form' => ['Perevorot\Forms\Models\Form'],
        'user' => ['Perevorot\Users\Models\User'],
    ];

    public $hasMany = [
        'logs' => [
            'Perevorot\Forms\Models\FormDataLog',
            'key' => 'form_data_id',
            'otherKey' => 'id',
        ],
        'files' => [
            'Perevorot\Forms\Models\FormFile',
            'key' => 'form_data_id',
            'otherKey' => 'id',
        ],
        'fieldsBoolean' => [
            'Perevorot\Forms\Models\FormDataBoolean',
            'key' => 'form_id',
            'otherKey' => 'id',
        ],
        'fieldsDecimal' => [
            'Perevorot\Forms\Models\FormDataDecimal',
            'key' => 'form_id',
            'otherKey' => 'id',
        ],
        'fieldsNumber' => [
            'Perevorot\Forms\Models\FormDataNumber',
            'key' => 'form_id',
            'otherKey' => 'id',
        ],
        'fieldsVarchar' => [
            'Perevorot\Forms\Models\FormDataVarchar',
            'key' => 'form_id',
            'otherKey' => 'id',
        ],
        'fieldsText' => [
            'Perevorot\Forms\Models\FormDataText',
            'key' => 'form_id',
            'otherKey' => 'id',
        ],
        'fieldsDate' => [
            'Perevorot\Forms\Models\FormDataDate',
            'key' => 'form_id',
            'otherKey' => 'id',
        ],
        'fieldsTime' => [
            'Perevorot\Forms\Models\FormDataTime',
            'key' => 'form_id',
            'otherKey' => 'id',
        ],
    ];

    public function getUserOptions()
    {
        return User::all()->pluck('name', 'id')->toArray();
    }

    public function getDomainOptions()
    {
        return Domain::all()->pluck('domain', 'id')->toArray();
    }

    public function getDomainAttribute()
    {
        return @$this->form()->first()->domain->domain;
    }

    public function scopeByDomain($q, $v)
    {
        if(!empty($v)) {
            $q->whereHas('form', function($q2) use ($v) {
                $q2->byDomain($v);
            });
        }
    }

    public function withRelation()
    {
        return $this->fieldsVarchar()->with('field')
        ->union($this->fieldsText()->with('field')->toBase())
        ->union($this->fieldsBoolean()->with('field')->toBase())
        ->union($this->fieldsNumber()->with('field')->toBase())
        ->union($this->fieldsDate()->with('field')->toBase())
        ->union($this->fieldsTime()->with('field')->toBase())
        ->union($this->fieldsDecimal()->with('field')->toBase())->orderBy('updated_at', 'desc')->get()->groupBy('field_id');
    }

    public function getNameAttribute()
    {
        $fields = $this->fieldsVarchar;
        $field = array_first($fields, function($v, $k) {
            return strpos($v, 'address');
        });

        if($field) {
            $value = json_decode($field->value);
            return @$value->address;
        }

        $field = $fields->first();

        if(empty($field)) {
            $field = $this->fieldsText->first();
        }

        return @$field->value;
    }

    public function scopeByForm($q, $values)
    {
        if(!empty($values)) {
            $q->whereIn('form_id', $values);
        }
    }

    public function scopeByUser($q, $v)
    {
        if(!empty($v)) {
            $q->where('user_id', $v);
        }
    }
}
