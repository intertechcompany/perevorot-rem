<?php namespace Perevorot\Forms\Models;

use Illuminate\Support\Facades\Schema;
use Model;
use October\Rain\Exception\ApplicationException;
use Perevorot\Users\Models\UserGroup;
use Route;
use DB;
use October\Rain\Database\Traits\Sortable;

/**
 * Model
 */
class Field extends Model
{
    use \October\Rain\Database\Traits\Validation, Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $hidden = ['id','form_id','sort_order'];

    protected $jsonable = ['options','parent_field_value'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'=>'required',
        'type'=>'required',
        'code'=>'required|fieldcode',
    ];

    public $customMessages = [
        'code.fieldcode' => 'Код поля должен быть уникальным',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_forms_fields';

    public $belongsTo = [
        'form' => ['Perevorot\Forms\Models\Form'],
        'parent' => [
            'Perevorot\Forms\Models\Field',
            'key' => 'parent_field_id', 'otherKey' => 'id'
        ],
    ];

    public $belongsToMany = [
        'form_statuses_read' => [
            'Perevorot\Forms\Models\FormStatus',
            'table'    => 'perevorot_form_fields_to_form_statuses',
            'key' => 'form_field_id',
            'otherKey' => 'form_status_id',
            'scope' => "isRead"
        ],
        'form_statuses_edit' => [
            'Perevorot\Forms\Models\FormStatus',
            'table'    => 'perevorot_form_fields_to_form_statuses',
            'key' => 'form_field_id',
            'otherKey' => 'form_status_id',
            'scope' => "isEdit"
        ],
        'user_groups_edit' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_field_to_user_group',
            'key' => 'form_field_id',
            'otherKey' => 'user_group_id',
            'scope' => "isEdit"
        ],
        'user_groups_read' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_field_to_user_group',
            'key' => 'form_field_id',
            'otherKey' => 'user_group_id',
            'scope' => "isRead"
        ],
    ];

    public function getFormStatusesEditOptions()
    {
        if($this->form && !$this->form->statuses->isEmpty()) {
            return $this->form->statuses->lists('name', 'id');
        }
        else {
            return [];
        }
    }

    public function getFormStatusesReadOptions()
    {
        if($this->form && !$this->form->statuses->isEmpty()) {
            return $this->form->statuses->lists('name', 'id');
        }
        else {
            return [];
        }
    }

    public function getUserGroupsReadOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getUserGroupsEditOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function beforeSave()
    {
        if(!in_array($this->type, ['autocompleteForm', 'accordion', 'selectTable', 'select', 'radio', 'checkbox'])) {
            $this->options = null;
        }

        if(empty($this->is_parent)) {
            $this->parent_field_id = null;
            $this->parent_field_condition = null;
            $this->parent_field_value = null;
        }
    }

    public function afterSave() {
        $inputs = post();
        $groups = UserGroup::all();
        $field = Field::find(post('manage_id'));

        if(!$field) {
            return;
        }
        
        $statuses = $field->form->statuses;

        DB::table('perevorot_form_fields_to_form_statuses')->whereNull('permission')->delete();
        DB::table('perevorot_form_fields_to_form_statuses')->where('form_field_id', post('manage_id'))->delete();
        DB::table('perevorot_forms_field_to_user_group')->whereNull('permission')->delete();
        DB::table('perevorot_forms_field_to_user_group')->where('form_field_id', post('manage_id'))->delete();

        if(!empty($inputs['Field']['form_statuses_edit'])) {
            foreach($inputs['Field']['form_statuses_edit'] as $postItem) {
                $item = $statuses->where('id', $postItem)->first();
                $field->form_statuses_edit()->save($item, [
                    'permission' => 'edit',
                ]);
            }
        }

        if(!empty($inputs['Field']['form_statuses_read'])) {
            foreach($inputs['Field']['form_statuses_read'] as $postItem) {
                $item = $statuses->where('id', $postItem)->first();
                $field->form_statuses_read()->save($item, [
                    'permission' => 'read',
                ]);
            }
        }

        if(!empty($inputs['Field']['user_groups_edit'])) {
            foreach($inputs['Field']['user_groups_edit'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $field->user_groups_edit()->save($item, [
                    'permission' => 'edit',
                ]);
            }
        }

        if(!empty($inputs['Field']['user_groups_read'])) {
            foreach($inputs['Field']['user_groups_read'] as $postItem) {
                $item = $groups->where('id', $postItem)->first();
                $field->user_groups_read()->save($item, [
                    'permission' => 'read',
                ]);
            }
        }
    }

    public function getParentFieldIdOptions()
    {
        $tmp=explode('/', Route::current()->parameter('slug'));
        $formId=intval(end($tmp));
        
        $query=Field::query();

        $query->orderBy('sort_order', 'asc');

        if(!empty($formId)) {
            $query->where('form_id', '=', $formId);
        } else {
            $query->whereNull('form_id');
        }

        if($this->id) {
            $query->where('id', '<>', $this->id);
        }

        $fields=$query->get();
        
        if(!$fields->isEmpty()) {
            $options=[];

            foreach($fields as $item) {
                if(($this->id && $item->parent_field_id!==$this->id) || !$this->id) {
                    $options[$item->id]=$item->name;
                }
            }

            return $options;
        }

        return [];
    }

    public function getTypeOptions()
    {
        return [
            'text' => 'Текстовое',
            'textarea' => 'Многострочное текстовое',
            'select' => 'Выпадающий список',
            'radio' => 'Однозначный выбор',
            'checkbox' => 'Многозначный выбор',
            'map' => 'Карта',
            'phone' => 'Телефон',
            'number' => 'Целое число',
            //'decimal' => 'Дробное число',
            //'currency_uah' => 'Валюта, гривна',
            //'currency_usd' => 'Валюта, $',
            'switch' => 'Переключатель',
            'selectTable' => 'Выпадающий список по таблице',
            'accordion' => 'Аккордеон',
            'file' => 'Загрузка файла',
            'hidden' => 'Скрытое',
            'autocompleteForm' => 'Автокомплит по форме',
            'date' => 'Дата',
            'time' => 'Время',
        ];
    }

    public function filterFields($fields, $context = null)
    {
        if(in_array(@$fields->type->value, ['selectTable'])) {
            $options = SelectTable::all()->lists('name', 'name');

            if(is_array($this->options)) {
                $fields->options->value = current($this->options);
            }

            $fields->options->options = $options;
            $fields->options->type = 'dropdown';
        }
        elseif(in_array(@$fields->type->value, ['autocompleteForm'])) {
            //$options = Form::where('id', '<>', $this->form_id)->get()->lists('name', 'code');
            $options = Form::get()->lists('name', 'code');

            if(is_array($this->options)) {
                $fields->options->value = current($this->options);
            }

            $fields->options->options = $options;
            $fields->options->type = 'dropdown';
        }

        //print_r($this->parent);exit;

        if($this->form_id && $this->parent && in_array($this->parent->type, ['autocompleteForm','selectTable','select','radio','checkbox'])) {
            if(@$fields->parent_field_value) {
                $type = strtr($this->parent->type, ['checkbox' => 'checkboxlist', 'select' => 'dropdown', 'selectTable' => 'dropdown', 'autocompleteForm' => 'dropdown']);
                $post = post();

                if($fields->parent_field_value->type != $type && !empty($post['fields'])) {
                    $this->parent_field_value = '';
                    $this->save();
                    $fields->parent_field_value->value = '';
                }

                $fields->parent_field_value->hidden = false;
                $fields->parent_field_value->type = $type;

                $options = [];

                if(!empty($this->parent->options)) {
                    foreach($this->parent->options as $option) {
                        if($this->parent->type == 'selectTable') {
                            try {
                                if(Schema::hasTable($option)) {
                                    $tableOptions = DB::table($option)->get();

                                    if(!empty($tableOptions)) {
                                        foreach ($tableOptions as $tOption) {
                                            if(!isset($tOption->code) || !isset($tOption->name)) {
                                                throw new ApplicationException('Не найдены поля code и name в смежной таблице');
                                            }

                                            $options[$tOption->code] = $tOption->name;
                                        }
                                    }
                                }
                            } catch (\Exception $e) {
                                throw new ApplicationException('Ошибка: '.$e->getMessage());
                            }
                        } else {
                            $options[$option['code']] = $option['name'];
                        }
                    }
                }

                $fields->parent_field_value->options = $options;
            }
        } else {
            @$fields->parent_field_value->hidden = true;
        }
    }
}
