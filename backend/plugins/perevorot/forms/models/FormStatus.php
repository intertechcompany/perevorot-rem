<?php namespace Perevorot\Forms\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use Perevorot\Users\Models\UserGroup;

/**
 * Model
 */
class FormStatus extends Model
{
    use \October\Rain\Database\Traits\Validation, Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $hidden = ['id','form_id','sort_order'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'=>'required',
        'code'=>'required|statuscode',
    ];

    public $customMessages = [
        'code.statuscode' => 'Код статуса должен быть уникальным',
    ];

    protected $jsonable = ['dependents'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_forms_statuses';

    public $belongsTo = [
        'form' => ['Perevorot\Forms\Models\Form'],
    ];

    public $belongsToMany = [
        'user_groups_read' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_form_status_to_user_group_read',
            'key' => 'form_status_id',
            'otherKey' => 'user_group_id',
        ],
        'user_groups' => [
            'Perevorot\Users\Models\UserGroup',
            'table'    => 'perevorot_forms_form_status_to_user_group',
            'key' => 'form_status_id',
            'otherKey' => 'user_group_id',
        ],
    ];

    public function scopeIsEdit($q)
    {
        return $q->where('permission', 'edit');
    }

    public function scopeIsRead($q)
    {
        return $q->where('permission', 'read');
    }

    public function getUserGroupsOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getUserGroupsReadOptions()
    {
        return UserGroup::all()->lists('name', 'id');
    }

    public function getDependentsListAttribute()
    {
        if($this->dependents) {
            $statuses = FormStatus::whereIn('id', $this->dependents)->orderBy('sort_order')->get();

            if (!$statuses->isEmpty()) {
                return $statuses->lists('name');
            } else {
                return '';
            }
        }
    }

    public function getDependentsOptions()
    {
        return FormStatus::where('id', '<>', $this->id)->where('form_id', $this->form_id)->orderBy('sort_order')->get()->lists('name', 'id');
    }
}
