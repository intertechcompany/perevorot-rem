<?php namespace Perevorot\Settings;

use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{
    public function boot()
    {
        Event::listen('backend.menu.extendItems', function($manager) {
            if (env('PL_PROJECT')) {
                $manager->removeMainMenuItem('Perevorot.Forms', 'calendar');
                $manager->removeMainMenuItem('Perevorot.Forms', 'tables');
            }
        });
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
        return [
            'messagesexport' => [
                'label'       => 'Обмен сообщениями',
                'description' => 'Экспорт/импорт переводов на страницах',
                'category'    => 'rainlab.translate::lang.plugin.name',
                'icon'        => 'icon-list',
                'url'         => Backend::url('perevorot/settings/messagesexport'),
                'order'       => 1000,
                'keywords'    => '',
                'permissions' => ['settings.messagesexport_permission'],
            ],
            'common' => [
                'label'       => 'Настройки сайта',
                'description' => 'контактная информация, общие настройки сайта',
                'category'    => 'Общие настройки',
                'icon'        => 'icon-info',
                'class'       => 'Perevorot\Settings\Models\Common',
                'order'       => 0,
                'permissions' => ['common.settings']
            ],
            'domains' => [
                'label'       => 'Домены',
                'description' => 'домены и компании',
                'category'    => 'Общие настройки',
                'icon'        => 'icon-tasks',
                'url'         => Backend::url('perevorot/forms/domains'),
                'order'       => 20,
                'keywords'    => 'Domain',
                'permissions' => ['forms.domains'],
            ],
        ];
    }
}
