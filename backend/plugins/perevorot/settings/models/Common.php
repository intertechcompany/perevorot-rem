<?php

namespace Perevorot\Settings\Models;

use Model;
use Perevorot\Forms\Models\Form;

/**
 * Model
 */
class Common extends Model
{
    use \Perevorot\Page\Traits\CacheClear;

    public $implement = [
        'System.Behaviors.SettingsModel',
        'RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        'address',
        'working_hours',
    ];

    public $settingsCode = 'common';

    public $settingsFields = 'fields.yaml';

    public $attachOne = [
         'logo' => 'System\Models\File'
    ];

    public function getHomeViewModeOptions()
    {
        $forms = Form::enabled()->get();
        $items = ['mapMode' => 'Карта'];

        foreach($forms as $form) {
            $items[$form->code] = $form->name.(!empty($form->domain) ? " ({$form->domain->domain})" : '');
        }

        return $items;
    }
}
